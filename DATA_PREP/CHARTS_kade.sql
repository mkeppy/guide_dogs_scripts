

USE GDB_TC1_kade
go

		--CHART_DOG
		SELECT t.dog_Sex, t.dog_Status, COUNT(*) RecordCount 
		FROM dbo.tblDog AS t
 
		GROUP BY t.dog_Sex, t.dog_Status
		ORDER BY t.dog_Status, t.dog_Sex
		
	 
		--CHART_MedicalPreference1
		SELECT[psn_MediaPref1], COUNT(psn_PersonID)
		FROM dbo.tblPerson
		GROUP BY psn_MediaPref1

		--CHART_MedicalPreference2
		SELECT psn_MediaPref2, COUNT(psn_PersonID)
		FROM dbo.tblPerson
		GROUP BY  psn_MediaPref2

		---CHART_ADDRTYPE
		SELECT t.ad_AddressCode, t2.Description, COUNT(T.ad_AddressDetailID)
		FROM dbo.tblAddressDetail AS t INNER JOIN dbo.trefAddressCode AS T2 ON t.ad_AddressCode=t2.AddressCode
		GROUP BY T.ad_AddressCode, T2.Description
		ORDER BY T.ad_AddressCode

		--CHART_AgencyConRelRank
		SELECT DISTINCT cre_Rank
		FROM dbo.tblAgencyContactRel

		--CHART_PhoneType
		SELECT T2.PhoneCode, T2.[Description], COUNT(t.[pdl_PhoneDetailID])
		FROM dbo.tblPhoneDetail AS T INNER JOIN dbo.trefPhoneCode AS T2 ON t.pdl_PhoneCode=T2.PhoneCode
		GROUP BY T2.PhoneCode, T2.[Description]
		ORDER BY t2.[Description]

		--
		SELECT DISTINCT pd_CoRel, T2.Description
		FROM dbo.tblPersonDog AS T LEFT JOIN [trefRelCodeNew] AS T2 ON T.pd_CoRel=t2.RelationCode

		--CHART_PersonDog_RelCode
		SELECT DISTINCT T.pd_RelCode, T2.[Description], COUNT(t.[pd_RelationID])
		FROM dbo.tblPersonDog AS T LEFT JOIN [trefRelCodeNew] AS T2 ON T.pd_RelCode=t2.RelationCode
		GROUP BY T.pd_RelCode, T2.[Description]
		ORDER BY t2.[Description]

		--CHART_PersonDog_CoRel_RelCode
		SELECT DISTINCT T.pd_RelCode, T2.[Description], T2.DogDesc, t2.PersonDogDesc,COUNT(t.[pd_RelationID])
		FROM dbo.tblPersonDog AS T LEFT JOIN [trefRelCodeNew] AS T2 ON T.pd_RelCode=t2.RelationCode
		GROUP BY  T.pd_RelCode, T2.[Description], T2.DogDesc, t2.PersonDogDesc
		ORDER BY t2.[Description]

		--CHART_DogChangeStatus_Disposition
		SELECT T.dds_Disposition, T2.dsc_NewCS, T2.dsc_text, COUNT(t.dds_DogStatusID) FROM dbo.tblDogStatusChange AS T LEFT JOIN dbo.trefDogStatusChange AS T2 ON T.dds_Disposition=t2.dsc_ID
		GROUP BY T.dds_Disposition, T2.dsc_NewCS, T2.dsc_text
		ORDER BY T2.dsc_NewCS

		--CHART_PhoneNote_NatureOfCall
		SELECT t.not_NatureOfCall, t2.erc_ERCode, t2.erc_ERCodeText, COUNT(t.not_Key) 
		FROM dbo.tblPhoneNote AS T LEFT JOIN dbo.trefAGSNatureOfCallCodes AS T2 ON T.not_NatureOfCall=t2.erc_ID
		GROUP BY t.not_NatureOfCall, t2.erc_ERCode, t2.erc_ERCodeText
		ORDER BY t2.erc_ERCode

		--CHART_PersonNote_Category
		SELECT T.pnt_Category, COUNT(T.pnt_NotesID) FROM dbo.tblPersonNotes AS T 
		GROUP BY t.pnt_Category
		ORDER BY T.pnt_Category

		--CHART_Contacts_ContactType
		SELECT T.con_ContactType, t2.[Description], COUNT(T.con_ContactID) FROM dbo.tblContacts AS T INNER JOIN dbo.trefContactTypes AS T2 ON T.con_ContactType=T2.ContactType
		GROUP BY T.con_ContactType, T2.[Description]
		ORDER BY T.con_ContactType

		--CHART_DogRel_RelationsCode
		SELECT T.drl_Relationcode, T2.[Description], T2.DogDesc, t2.PersonDogDesc, COUNT(drl_relationid) FROM dbo.tblDogRel AS T LEFT JOIN dbo.trefRelCodeNew AS T2 ON T.drl_RelationCode=T2.RelationCode
		GROUP BY T.drl_Relationcode, T2.[Description], T2.DogDesc, t2.PersonDogDesc
		ORDER BY T.drl_RelationCode


		--CHART_PersonRel_RelationCode
		SELECT DISTINCT T.prl_RelationCode, T2.[Description], T2.DogDesc, t2.PersonDogDesc,COUNT(t.[prl_relationID])
		FROM dbo.tblPersonRel AS T LEFT JOIN [trefRelCodeNew] AS T2 ON T.prl_RelationCode=t2.RelationCode
		GROUP BY  T.prl_RelationCode, T2.[Description], T2.DogDesc, t2.PersonDogDesc
		ORDER BY t2.[Description]

		--CHART_tblDogComments_dc_Category
		SELECT T.dc_Category,COUNT(*)
		FROM tblDogcomments AS T
		GROUP BY dc_Category

		--CHART_tblDogFlags_FlagCode
		SELECT T.flgFlagCode, T2.Flag, COUNT(*)
		FROM dbo.tblDogFlags AS T LEFT JOIN dbo.trefDogFlags AS T2 ON T.flgFlagCode=t2.FlagCode
		GROUP BY T.flgFlagCode, T2.Flag
		ORDER BY T.flgFlagCode

		--CHART_tblKennelRegSchedule_krs_Reason
		SELECT t.krs_Reason, T2.KennelReasonText, COUNT(*)
		FROM dbo.tblKennelRegSchedule AS T LEFT JOIN dbo.trefKennelReasons AS T2 ON t.krs_Reason=T2.KennelReasonCode
		GROUP BY t.krs_Reason, T2.KennelReasonText
		ORDER BY T2.KennelReasonText


		--CHART_tblNursingMedNotesQDetail_mdqd_CategoryID
		SELECT T.mdqd_CategoryID, T2.Category, COUNT(*)
		FROM dbo.tblNursingMedNotesQDetail AS T LEFT JOIN dbo.trefNursingCategories AS T2 ON T.mdqd_CategoryID=T2.CategoryID
		GROUP BY T.mdqd_CategoryID, T2.Category
		ORDER BY T.mdqd_CategoryID

		--CHART_tblNursingMedNotesQDetail_mdqd_TreatmentID
		SELECT T.mnqd_TreatmentID, T2.Treatment,  COUNT(*)
		FROM dbo.tblNursingMedNotesQDetail AS T LEFT JOIN dbo.trefNursingCategories AS T2 ON T.mnqd_TreatmentID=T2.TreatmentID
		GROUP BY T.mnqd_TreatmentID, T2.Treatment
		ORDER BY T.mnqd_TreatmentID

		--Values [tblStaff].[Status]

		SELECT DISTINCT [Status]
		FROM dbo.tblStaff

		--Values [tblStaff].[DeptNum]/[DeptNumLong]

		SELECT [DeptNum], [DeptNumLong]
		FROM dbo.tblStaff
		GROUP BY  [DeptNum], [DeptNumLong]

		--CHART_tblREcall.TripCode
		SELECT T.rec_TripCode, T2.TripStateAbbrev, T2.TripState, T2.TripCity, T2.TripRegion, T2.TripDirection,  COUNT(*)
		FROM dbo.tblRecall AS T LEFT JOIN dbo.trefRecallTrips AS T2 ON T.rec_TripCode=T2.TripCode
		GROUP BY T.rec_TripCode, T2.TripStateAbbrev, T2.TripState, T2.TripCity, T2.TripRegion, T2.TripDirection 
		ORDER BY T.rec_TripCode

		SELECT * FROM dbo.trefRecallTrips