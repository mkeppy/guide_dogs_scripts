USE Kade
GO


--Begin***********************************************
--****Calculate Breed Percentage*and Filial Code******
--***********************7/10/2014 jf*****************
Declare @P_LABsir as float
Declare @P_GLDsir as float
Declare @P_LABdam as float
Declare @P_GLDdam as float
Declare @SireFCode as int
Declare @DamFCode as int

Set @SireID = (Select dog_SireID From inserted)
Set @DamID = (Select dog_DamID From inserted)

Set @P_LABsir = (Select dog_PercentLAB From tblDog Where dog_DogID = @SireID)
Set @P_GLDsir = (Select dog_PercentGLD From tblDog Where dog_DogID = @SireID)
Set @P_LABdam = (Select dog_PercentLAB From tblDog Where dog_DogID = @DamID)
Set @P_GLDdam = (Select dog_PercentGLD From tblDog Where dog_DogID = @DamID)
Set @SireFCode = (Select dog_FilialCode From tblDog Where dog_DogID = @SireID)
Set @DamFCode = (Select dog_filialCode From tblDog Where dog_DogID = @DamID)

IF (SELECT dog_Status FROM inserted) = 'OUTBRD'
	OR (SELECT dog_Status FROM inserted) = 'PED'
	BEGIN	
		IF (SELECT dog_Breed FROM inserted) = 'LAB'
			UPDATE tblDog
				SET	dog_PercentLAB = 100, 
					dog_PercentGLD = 0,
					dog_FilialCode = 0
				WHERE tblDog.dog_DogID = @DogID
		IF (SELECT dog_Breed FROM inserted) = 'GLD'
			UPDATE tblDog
				SET	dog_PercentLAB = 0, 
					dog_PercentGLD = 100,
					dog_FilialCode = 0
			WHERE tblDog.dog_DogID = @DogID
	END
ELSE			
	BEGIN
		UPDATE tblDog
			SET	dog_PercentLAB = (@P_LABsir/2) + (@P_LABdam/2),
				dog_PercentGLD = (@P_GLDsir/2) + (@P_GLDdam/2)
			WHERE tblDog.dog_DogID = @DogID
		UPDATE tblDog
			SET	dog_FilialCode = IIF(@DamFCode > 0 OR @SireFCode > 0,
						IIF(@DamFCode > @SireFCode,@DamFCode + 1,
						IIF(@SireFCode > @DamFCode,@SireFCode + 1,
						IIF(@SireFCode = @DamFCode,@DamFCode + 1,dog_FilialCode))),dog_FilialCode)
			WHERE tblDog.dog_DogID = @DogID
		UPDATE tblDog
			SET dog_FilialCode = IIF((@P_LABsir = 100 AND @P_GLDdam = 100) 
					OR (@P_GLDsir = 100 AND @P_LABdam = 100),@DamFCode + 1,dog_FilialCode)
			WHERE tblDog.dog_DogID = @DogID
		UPDATE tblDog
			SET dog_FilialCode = IIF((@P_LABsir = 100 AND @P_LABdam = 100) 
					OR (@P_GLDsir = 100 AND @P_GLDdam = 100),0,dog_FilialCode)
			WHERE tblDog.dog_DogID = @DogID
	END
--****************************************************
--****Calculate Breed Percentage*and Filial Code******
--End*************************************************
