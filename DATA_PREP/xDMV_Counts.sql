-- Shows all user tables and row counts for the current database 
-- Remove is_ms_shipped = 0 check to include system objects 
-- i.index_id < 2 indicates clustered index (1) or hash table (0) 
USE GDB_TC1_kade
GO 


DROP TABLE GDB_TC1_maps.dbo.__kade_counts

SELECT o.name, 
 ddps.row_count 
  INTO GDB_TC1_maps.dbo.__kade_counts
FROM sys.indexes AS i 
 INNER JOIN sys.objects AS o ON i.OBJECT_ID = o.OBJECT_ID 
 INNER JOIN sys.dm_db_partition_stats AS ddps ON i.OBJECT_ID = ddps.OBJECT_ID 
 AND i.index_id = ddps.index_id 

WHERE i.index_id < 2 
 AND o.is_ms_shipped = 0  and ddps.row_count !=0

-- ORDER BY ddps.row_count desc
ORDER BY o.name


USE GDB_TC1_maps
GO

 SELECT DISTINCT T.SF_Object , T.Source_Table, T2.row_count, T.SF_Object npspob
 FROM GDB_TC1_maps.dbo.__T1_ALL_FIELDS T
 LEFT JOIN dbo.__kade_counts T2 ON T.Source_Table=T2.[name]
 WHERE T.SF_Object IS NOT NULL 
 ORDER BY T.SF_Object, T.Source_Table

