USE GDB_TC1_migration
GO
 
BEGIN--Maps   
  			SELECT  [Source_Field],  [Convert], SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblGDBInvDispenseTrans
			WHERE   SF_Object LIKE '%ent%'
			
			SELECT  [Source_Field],  SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%xxxxxxxxxxxxxxxxxxxx%'
END 

BEGIN --drop IMP INVENTORY
	DROP TABLE  GDB_TC1_migration.dbo.IMP_INVENTORY
END 

BEGIN--INVENTORY

		--tblVolEquipment	
			SELECT DISTINCT  GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			 ,'vps-'+ vps_Item AS Legacy_ID__c 
			,vps_Item AS [Name] 
			,NULL AS [Type__c]
			,NULL  AS  Classification__c	 
			,NULL  AS  Category__c	
			,NULL  AS  Controlled_Item__c	
			,NULL  AS  Active__c	
	 		,NULL  AS  [Order_From__r:Legacy_Id__c]	
			,NULL  AS  Campus__c	 
		 	,NULL  AS  Department_Code__c	 
			,NULL  AS  Status__c	
			,NULL  AS  Buy_Ratio__c
		 	,NULL  AS  Buy_UOM__c	 
			,NULL  AS  Sell_Ratio__c	
		 	,NULL  AS  Sell_UOM__c	 
			,NULL  AS  ReOrder_Point__c	
			,NULL  AS  ReOrder_Qty__c	
			,NULL  AS  Overstock_Point__c	
			,NULL  AS  Required_Qty__c	
			,NULL  AS  Maintain_QOH__c
 			,NULL  AS  Ouantity_On_Hand__c
			,'tblVolEquipment' AS zrefSrc

		 	INTO GDB_TC1_migration.dbo.IMP_INVENTORY
			FROM GDB_TC1_kade.dbo.tblVolEquipment

		UNION 
		--stg_tblEquipFood
			SELECT DISTINCT  GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
			,'deq-'+T2.Item_Name AS Legacy_ID__c
			,T2.Item_Name AS [Name] 
			,T2.[Type] AS [Type__c]
			,NULL  AS  Classification__c	 
			,NULL  AS  Category__c	
			,NULL  AS  Controlled_Item__c	
			,NULL  AS  Active__c	
	 		,NULL  AS  [Order_From__r:Legacy_Id__c]	
			,NULL  AS  Campus__c	 
		 	,NULL  AS  Department_Code__c	 
			,NULL  AS  Status__c	
			,NULL  AS  Buy_Ratio__c
		 	,NULL  AS  Buy_UOM__c	 
			,NULL  AS  Sell_Ratio__c	
		 	,NULL  AS  Sell_UOM__c	 
			,NULL  AS  ReOrder_Point__c	
			,NULL  AS  ReOrder_Qty__c	
			,NULL  AS  Overstock_Point__c	
			,NULL  AS  Required_Qty__c	
			,NULL  AS  Maintain_QOH__c
 			,NULL  AS  Ouantity_On_Hand__c			
			,'stg_tblEquipFood' AS zrefSrc
			
			FROM GDB_TC1_migration.dbo.stg_tblEquipFood AS T1
			LEFT JOIN GDB_TC1_maps.dbo.CHART_EquipFood  AS T2 ON T1.deq_EquipFood =T2.deq_EquipFood 
																AND T1.deq_Type=T2.deq_Type 
																AND T1.deq_Size=T2.deq_Size
			WHERE  T2.Item_Name IS NOT NULL 
	 	UNION
			-- trefGDBInventroyItems 
			 SELECT DISTINCT GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 			,'Ii-'+CAST(T1.ii_ItemID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- Concatenate 'Ii-'+[ii_ItemId]
			,T1.ii_Description +' '+ CAST(YEAR(T1.ii_CreatedDate) AS NVARCHAR(4)) AS  Name__c	-- Concatenate [ii_Description]+' '+ ([ii_CreatedDate],YYYY)
			,'Medicine'  AS  Type__c	-- Medicine
			,T4.iicl_Classification  AS  Classification__c	-- migrate value from [iicl_Classification]
			,T5.iic_Category AS  Category__c	-- migrate value from [iic_Category]
			,T1.ii_ControlledItem  AS  Controlled_Item__c	
			,T1.ii_Active  AS  Active__c	
	 		,'iv-' +CAST(T2A.iv_VendorID AS NVARCHAR(20))   AS  [Order_From__r:Legacy_Id__c]	
			,T6.[Description]  AS  Campus__c	-- migrate value from [Description]
		 	,T7.iidc_Department  AS  Department_Code__c	-- migrate value from [iidc_Department]
			,T2.iid_Status  AS  Status__c	
			,T2.iid_BuyRatio  AS  Buy_Ratio__c
		 	,T8.iuom_Type AS  Buy_UOM__c	-- migrate value from [iuom_Type]
			,T2.iid_SellRatio  AS  Sell_Ratio__c	
		 	,T9.iuom_Type AS  Sell_UOM__c	-- migrate value from [iuom_Type]
			,T2.iid_ReOrderPt  AS  ReOrder_Point__c	
			,T2.iid_ReOrderQty  AS  ReOrder_Qty__c	
			,T2.iid_OverstockPt  AS  Overstock_Point__c	
			,T2.iid_RequiredQty  AS  Required_Qty__c	
			,T2.iid_MaintainQOH  AS  Maintain_QOH__c
 			,T3.inv_QtyOnHand  AS  Ouantity_On_Hand__c	
 
			,'trefGDBInventroyItems' AS zrefSrc
			FROM GDB_TC1_kade.dbo.trefGDBInventroyItems AS T1
			LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvItemsDetail AS T2 ON T2.iid_ItemID = T1.ii_ItemID
			LEFT JOIN GDB_TC1_kade.dbo.tblGDBVendor AS T2A ON T2A.iv_VendorID=T2.iid_VendorID
			LEFT JOIN GDB_TC1_kade.dbo.tblGDBInventroy AS T3 ON T3.inv_ItemDetailID=T2.iid_ItemDetailID
			LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvClassification AS T4 ON T1.ii_ClassificationID=T4.iicl_ClassificationID
			LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvCategories AS T5 ON T1.ii_ClassCatID=T5.iic_CategoryID
			LEFT JOIN GDB_TC1_kade.dbo.trefFacility AS T6 ON T6.FacilityID=T2.iid_Campus
		 	LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvDepartments AS T7 ON T7.iidc_DeptCode=T2.iid_DepartmentCode
		 	LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvUOM AS T8 ON T8.iuom_ID=T2.iid_BuyUOM
		 	LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvUOM AS T9 ON T9.iuom_ID=T2.iid_SellUOM
		
		 
	 
END --TC1 2438


BEGIN --audit

	SELECT * FROM GDB_TC1_migration.dbo.IMP_INVENTORY
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_INVENTORY GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 		sELECT zrefSrc, COUNT(*) C
			FROM GDB_TC1_migration.dbo.IMP_INVENTORY 
			GROUP BY zrefSrc
		 
END 