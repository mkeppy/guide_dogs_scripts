USE GDB_TC1_migration
GO

BEGIN--MAPS 
  			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API
					,','+SF_Field_API+' = T1.'+[Source_Field]     AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblAGSHomeVisit
			WHERE   SF_Object LIKE '%tas%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2
					,','+SF_Field_API_2+' = T1.'+[Source_Field]     AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblAGSHomeVisit 
			WHERE  SF_Object_2 LIKE '%ta%'
END 

BEGIN -- DROP IMP_TASK

	DROP TABLE GDB_TC1_migration.DBO.IMP_TASK

END 

BEGIN -- CREATE IMP_TASK 

					SELECT   DISTINCT
							 OwnerId= CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END 
							,Legacy_ID__c = 'APQ-'+CAST(T1.apq_AppInstanceID	 AS NVARCHAR(30)) -- Concatenate 'apq'+[apq_ApplInstanceID]	-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,WhoId = X2.ID		-- link to Contact through PersonRel.PersonID	-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,WhatId =X3.ID		-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,ActivityDate =  CAST(T1.apq_Date AS DATE)  		
							,[Subject] = 'AGS Applicant Phone Interview: ' + CAST(CAST(T1.apq_Date AS DATE) AS NVARCHAR(30))	-- concatenate'AGS Applicant Phone Interview' + [apq_Date]	
							,[Status] = CASE WHEN T1.apq_Date >GETDATE() THEN 'Open' ELSE 'Completed' END -- If >Today then 'Open' else 'Completed'	
							,[Description] = CAST( T1.apq_Notes AS NVARCHAR(MAX))		
							,[Recommendation__c]= NULL --filler
							,[Type] = NULL    --filler
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL  --filler
							,Time__c =   NULL--filler
							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

							,CreatedDate = CAST(T1.apq_CreatedDate AS DATE)
							,zrefSrc = 'tblAGSApplicantPhoneInt'
			 	 	INTO GDB_TC1_migration.DBO.IMP_TASK
					FROM GDB_TC1_kade.dbo.tblAGSApplicantPhoneInt AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.apq_AppInstanceID
					LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T3 ON T3.cli_ClientID = T2.prl_ClientInstanceID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.apq_Interviewer
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T2.prl_PersonID AS NVARCHAR(30))
					LEFT JOIN GDB_TC1_migration.dbo.XTR_APPLICATION AS X3 ON X3.Legacy_id__c = 'cli-'+CAST(T3.cli_ClientID AS NVARCHAR(30))
  			UNION 
					SELECT   DISTINCT
						     OwnerId= CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [TblStaff].[FIleNum]
							,Legacy_ID__c = 'HV-'+CAST(T1.hv_HomeVisitID	 AS NVARCHAR(30)) 	
							,WhoId = X2.ID		-- link to Contact through PersonRel.PersonID	-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,WhatId =X3.ID		-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,ActivityDate =CAST( T1.hv_Date AS DATE)
							,[Subject] ='AGS Home Visit: '+ CAST(CAST(T1.hv_Date AS DATE) AS NVARCHAR(30)) 	-- concatenate 'AGS Home Visit'+[hv_Date]	
							,[Status] =  CASE WHEN T1.hv_Date >GETDATE() THEN 'Open' ELSE 'Completed' END  	-- If >Today then 'Open' else 'Completed'	
							,[Description] = CAST(T1.hv_Narrative AS NVARCHAR(MAX))
							,[Recommendation__c]= NULL --filler
							,[Type] = NULL    --filler
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL  --filler
							,Time__c =   NULL--filler
							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

 							,CreatedDate = CAST(T1.hv_CreatedDate AS DATE)
							,zrefSrc = 'tblAGSHomeVisit'
					FROM GDB_TC1_kade.dbo.tblAGSHomeVisit AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.hv_AppInstanceID
					LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T3 ON T3.cli_ClientID = T2.prl_ClientInstanceID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.hv_Interviewer
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T2.prl_PersonID AS NVARCHAR(30))
					LEFT JOIN GDB_TC1_migration.dbo.XTR_APPLICATION AS X3 ON X3.Legacy_id__c = 'cli-'+CAST(T3.cli_ClientID AS NVARCHAR(30))
 			UNION 
					SELECT  DISTINCT 
							 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [TblStaff].[FIleNum]
							,Legacy_ID__c = 'APC-'+CAST(T1.apc_AppInstanceID AS NVARCHAR(30)) 	-- concatenate 'apc-'+[apc_AppinstanceID]	-- Link [tblPersonRel].[prl_ClientInstanceID]
							,WhoId = X2.ID		-- link to Contact through PersonRel.PersonID	-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,WhatId =X3.ID		-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,ActivityDate = CAST(T1.apc_Date AS DATE)
							,[Subject] = 'AGS Phone Consultation: '+ CAST(CAST(T1.apc_Date AS DATE) AS NVARCHAR(30))	 	-- 'AGS Phone Consultation' +[apc_Date]	
							,[Status] = CASE WHEN T1.apc_Date >GETDATE() THEN 'Open' ELSE 'Completed' END   	-- If >Today then 'Open' else 'Completed'	
 							,[Description] =CAST( T1.apc_Narrative	AS NVARCHAR(MAX))					
							,[Recommendation__c]= NULL --filler
							,[Type] = NULL    --filler
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
 							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL  --filler
							,Time__c =   NULL--filler
 							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

 							,CreatedDate = CAST(T1.apc_CreatedDate AS NVARCHAR(30))
							,zrefSrc = 'tblAGSPhoneConsultation'
					FROM GDB_TC1_kade.dbo.tblAGSPhoneConsultation AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.apc_AppInstanceID
					LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T3 ON T3.cli_ClientID = T2.prl_ClientInstanceID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.apc_Interviewer
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T2.prl_PersonID AS NVARCHAR(30))
					LEFT JOIN GDB_TC1_migration.dbo.XTR_APPLICATION AS X3 ON X3.Legacy_id__c = 'cli-'+CAST(T3.cli_ClientID AS NVARCHAR(30))
 			UNION 
					SELECT  DISTINCT 
						     OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [TblStaff].[FIleNum]
							,Legacy_ID__c = 'APF-'+CAST(T1.apf_ID	 AS NVARCHAR(30)) -- concatenate 'apf-'+[afp_id]	
							,WhoId = X2.ID		-- link to Contact through PersonRel.PersonID	-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,WhatId =X3.ID		-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,ActivityDate = CAST(T1.apf_Date AS DATE)
							,[Subject] = 'AGS Application Request Follow Up: '+ CAST(CAST(T1.apf_Date AS DATE) AS NVARCHAR(30))  	-- concatenate 'AGS Application Request Follow Up "+[apf_Date]	
							,[Status] = CASE WHEN T1.apf_Date >GETDATE() THEN 'Open' ELSE 'Completed' END 	-- If >Today then 'Open' else 'Completed'	
							,[Description] = CAST(T1.apf_FUNotes AS NVARCHAR(MAX))		
  							,[Recommendation__c]= NULL --filler
							,[Type] = T1.apf_FUType		
							,Qualified__c = T1.apf_Qualified		
							,Additional_Follow_Up__c = T1.apf_AdditionalFU		
							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL  --filler
							,Time__c =   NULL--filler
 							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

							,CreatedDate = CAST(T1.apf_FUDateDue AS DATE)
							,zrefSrc = 'tblAGSAppRequestFU'
					FROM GDB_TC1_kade.dbo.tblAGSAppRequestFU AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.apf_AppInstanceID
					LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T3 ON T3.cli_ClientID = T2.prl_ClientInstanceID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.apf_Interviewer
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T2.prl_PersonID AS NVARCHAR(30))
					LEFT JOIN GDB_TC1_migration.dbo.XTR_APPLICATION AS X3 ON X3.Legacy_id__c = 'cli-'+CAST(T3.cli_ClientID AS NVARCHAR(30))
					WHERE T1.apf_AppInstanceID!='0'
			UNION 
					SELECT   DISTINCT
							 OwnerId = GDB_TC1_migration.[dbo].[fnc_OwnerId]()
 							,Legacy_ID__c = 'PSL-'+CAST(T1.psl_ClientListID	 AS NVARCHAR(30)) -- Concatenate 'psl-'+[psl_ClientListID]	
							,WhoId = X2.ID -- Link to Contact through PersonRel.PersonID	-- Link [tblPersonRel].[prl_ClientInstanceID]
							,WhatId =X3.ID	-- Link to Campaign	-- Link [tblAGSPrescreenDetail].[psd_DetailID]
						 	,ActivityDate =CAST( T1.psl_CreatedDate	AS DATE)
							,[Subject] ='AGS Prescreen Client List: ' + CAST(CAST(T1.psl_CreatedDate AS DATE) AS NVARCHAR(30))   -- concatenate 'AGS Prescreen Client List ' +[psl_CreatedDate]	
							,[Status] = CASE WHEN T1.psl_CreatedDate >GETDATE() THEN 'Open' ELSE 'Completed' END   	-- If > Today then 'Open' else 'Completed'	
							,[Description] = T1.psl_FSMComments
							,[Recommendation__c]= NULL --filler
							,[Type] = T1.psl_FUType
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
 							,Task_Subtype__c = T1.psl_FUStatus		
							,Attempt_1__c = CASE T1.psl_Attempt1 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
							,Attempt_2__c = CASE T1.psl_Attempt2	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
							,Attempt_3__c = CASE T1.psl_Attempt3	 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END 	
							,SC_Comments__c = T1.psl_SCComments		
							,Visit_Date__c = CAST(T1.psl_VistiDate	AS DATE)
							,Visit_Time__c = CONVERT(NVARCHAR(15),CAST(T1.psl_VisitTime  AS TIME), 100)
							,Visit_Location__C = T1.psl_VistLocation		
							,Time__c =   NULL--filler		
 							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

							,CreatedDate = CAST(T1.psl_CreatedDate AS DATE)
							,zrefSrc = 'tblAGSPrescreenClientList'
					FROM GDB_TC1_kade.dbo.tblAGSPrescreenClientList AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.psl_ClientID
 					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T2.prl_PersonID AS NVARCHAR(30))
					LEFT JOIN GDB_TC1_kade.dbo.tblAGSPrescreenDetail AS T3 ON T3.psd_DetailID = T1.psl_DetailID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CAMPAIGN AS X3 ON X3.LEGACY_ID__C = 'psd-'+CAST(T3.psd_DetailID  AS NVARCHAR(30))
					 
			UNION 
					SELECT  DISTINCT
							 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [TblStaff].[FIleNum]
							,Legacy_ID__c = 'SPC-'+CAST( T1.spc_AppInstanceID  AS NVARCHAR(30)) 
							,WhoId = X2.ID		-- link to Contact through PersonRel.PersonID	-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
							,WhatId =X3.ID		-- Link [tbPersonRel].[prl_ClientInstanceID] And [tblClient].[cli_ClientID]
						 	,ActivityDate = CAST(T1.spc_Date AS DATE)
							,[Subject] = 'AGS Second Phone consultation: '+ CAST(CAST(T1.spc_Date AS DATE) AS NVARCHAR(30))  	-- concatenate 'AGS Second Phone consultation "+[spc_Date]	
							,[Status] = CASE WHEN T1.spc_Date >GETDATE() THEN 'Open' ELSE 'Completed' END  	-- If >Today then 'Open' else 'Completed'	
							,[Description] = CAST(T1.spc_narrative	AS NVARCHAR(MAX))
							,[Recommendation__c] = T1.spc_recommendation		
						 	,[Type] = NULL --filler
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
  							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL--filler
							,Time__c =   NULL--filler
							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

							,CreatedDate =CAST( T1.spc_CreadtedDate	AS DATE)			
							,zrefSrc = 'tblAGSSecondPhoneConsultation'
					FROM GDB_TC1_kade.dbo.tblAGSSecondPhoneConsultation AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.spc_AppInstanceID
					LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T3 ON T3.cli_ClientID = T2.prl_ClientInstanceID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.spc_Interviewer
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T2.prl_PersonID AS NVARCHAR(30))
					LEFT JOIN GDB_TC1_migration.dbo.XTR_APPLICATION AS X3 ON X3.Legacy_id__c = 'cli-'+CAST(T3.cli_ClientID AS NVARCHAR(30))

			UNION 
					SELECT   DISTINCT
							 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [TblStaff].[FIleNum]
							,Legacy_ID__c = 'ACL-'+CAST(T1.acl_LogID  AS NVARCHAR(30)) 		
							,WhoId = X2.ID	-- Link to contact	-- Link [tblPersonRel].[prl_ClientInstanceID]
							,WhatId = X3.ID	-- link to Contact by tblPersonRel.prl_PersonId	-- Link [tblPersonRel].[prl_ClientInstanceID]
							,ActivityDate = CAST(T1.acl_Date AS DATE)		
							,[Subject] = 'Application contact Log: '+ CAST(CAST(T1.acl_Date AS DATE) AS NVARCHAR(30))  -- concatenate 'Application contact Log' +[acl_Date]	
							,[Status] = CASE WHEN T1.acl_Date >GETDATE() THEN 'Open' ELSE 'Completed' END   	-- If >Today then 'Open' else 'Completed'	
							,[Description] = T1.acl_ContactNotes
							,[Recommendation__c] = NULL --filler		
						 	,[Type] = NULL --filler
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
  							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL--filler
							,Time__c =  CONVERT(NVARCHAR(15),CAST(T1.acl_Time  AS TIME), 100) 	-- only migration TIME portion of field value.	
 							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler
									
							,CreatedDate = CAST(T1.acl_CreatedDate AS date)
							,zrefSrc = 'tblAppContactLog'
					FROM GDB_TC1_kade.dbo.tblAppContactLog AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.acl_ClientID
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T2.cli_ClientID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T3.prl_PersonID AS NVARCHAR(30))
  		 			LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.acl_Contactor
	 				LEFT JOIN GDB_TC1_migration.dbo.XTR_APPLICATION AS X3 ON X3.Legacy_id__c = 'cli-'+CAST(T2.cli_ClientID AS NVARCHAR(30))
	  
			
			UNION 
					SELECT   DISTINCT
							 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [TblStaff].[FIleNum]
							,Legacy_ID__c = 'GL-'+CAST(T1.gl_Key	 AS NVARCHAR(30)) 	
							,WhoId = X2.ID	-- link to Contact by tblPersonRel.prl_PersonId	-- Link [tblPersonRel].[prl_ClientInstanceID]
							,WhatId =  X3.ID	-- link to Application	-- Link [tblPersonRel].[prl_ClientInstanceID]
							,ActivityDate = CAST(T1.gl_Date	AS DATE)
							,[Subject] = 'Generic Client Letter: '+ CAST(CAST(T1.gl_Date AS DATE) AS NVARCHAR(30)) -- concatenate 'Generic Client Letter '+[gl_Date]	
							,[Status] = CASE WHEN T1.gl_Date >GETDATE() THEN 'Open' ELSE 'Completed' END  	-- If >Today then 'Open' else 'Completed'	
 							,[Description] = CAST(T1.gl_Body AS NVARCHAR(MAX))
							,[Recommendation__c] = NULL --filler		
						 	,[Type] = NULL --filler
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
  							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL--filler
							,Time__c =   NULL--filler
							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

							,CreatedDate = CAST(T1.gl_CreatedDate AS date)
							,zrefSrc = 'tblGenericClientLetter'
					FROM GDB_TC1_kade.dbo.tblGenericClientLetter AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.gl_ClientID
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T2.cli_ClientID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T3.prl_PersonID AS NVARCHAR(30))
  		 			LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.gl_ByWho
	 				LEFT JOIN GDB_TC1_migration.dbo.XTR_APPLICATION AS X3 ON X3.Legacy_id__c = 'cli-'+CAST(T2.cli_ClientID AS NVARCHAR(30))
			
			UNION 
					SELECT   DISTINCT
							 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [TblStaff].[FIleNum]
							,Legacy_ID__c = 'APR-'+CAST(T1.apr_ID	 AS NVARCHAR(30)) 	
 							,WhoId = X2.ID		-- Link [tblStaff].[FileNum]
							,WhatId = NULL 
							,ActivityDate = CAST(T1.apr_TrainingTripDate AS DATE)	
							,[Subject] = T1.apr_TrainingTrip +': '+CAST(CAST(T1.apr_TrainingTripDate AS DATE) AS NVARCHAR(30))	-- concatenate [apr_TrainingTrip]+' '+[apr_TrainingTripdate]	
							,[Status] = CASE WHEN T1.apr_TrainingTripDate >GETDATE() THEN 'Open' ELSE 'Completed' END  	-- If >today then 'Open', else 'Completed' 					
 							,[Description]= NULL --filler	
							,[Recommendation__c] = NULL --filler		
						 	,[Type] = NULL --filler
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
  							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL--filler
							,Time__c =   NULL--filler
							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

							,CreatedDate= CAST(T1.apr_CreatedDate AS DATE) 
							,zrefSrc = 'tblApprentice'
					FROM GDB_TC1_kade.dbo.tblApprentice AS T1
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.apr_EmpFileNum
					LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T2 ON T2.FileNum=T1.apr_EmpFileNum
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = 'Staff-'+ CAST(T2.FileNum  AS NVARCHAR(30)) 
		
			UNION 
					SELECT   DISTINCT
					         OwnerId = GDB_TC1_migration.[dbo].[fnc_OwnerId]()
							,Legacy_ID__c = 'DBS-'+CAST(T1.dbs_ID	 AS NVARCHAR(30)) -- concatenate 'Dbs-'+[dbs_ID]	
							,WhoId = NULL 
							,WhatId = X2.ID		-- Link [tblDogBreeding].[dbg_BreedingID]
							,ActivityDate = CAST(T1.dbs_Date AS DATE) 
							,[Subject] = T3.CheckListEntryText	-- migrate value from [CheckListEntryText]	-- Yes/Link [trefdogbreedingentryType]
							,[Status] = CASE WHEN T1.dbs_Date >GETDATE() THEN 'Open' ELSE 'Completed' END  	-- If >today then 'Open', else 'Completed' 
							,[Description] = T1.dbs_Comment							
							,[Recommendation__c] = NULL --filler		
						 	,[Type] = NULL --filler
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
  							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL--filler
							,Time__c =   NULL--filler
							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

							,CreatedDate = CAST(T1.dbs_CreatedDate AS DATE)
							,zrefSrc = 'tblDogBreedingSchedule'
					FROM GDB_TC1_kade.dbo.tblDogBreedingSchedule AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblDogBreeding AS T2 ON T2.dbg_BreedingID=T1.dbs_BreedingID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_BREEDING AS X2 ON X2.Legacy_Id__c='DBG-'+CAST(T2.dbg_BreedingID AS NVARCHAR(30)) 
					LEFT JOIN GDB_TC1_kade.dbo.trefDogBreedingEntryType AS T3 ON T3.CheckListEntryCode=T1.dbs_Type
			UNION 
					SELECT   DISTINCT  
							 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [tblStaff].[FileNum]
							,Legacy_ID__c = 'K9P-'+CAST(T1.k9p_k9PhoneID AS NVARCHAR(30)) 	-- concatenate 'k9p - '+[k9p_k9PhoneID]	
							,WhoID= X2.ID-- link to Contact	-- Link [tblPerson].[PersonID]
							,WhatId = NULL 
							,ActivityDate = CAST(T1.k9p_Date AS DATE)
							,[Subject] = T1.K9p_Subject		
							,[Status] = CASE WHEN T1.k9p_Date >GETDATE() THEN 'Open' ELSE 'Completed' END	-- If >Today then 'Open' else 'Completed'	
 							,[Description] = CAST(T1.k9p_Memo AS NVARCHAR(MAX))
							,[Recommendation__c] = NULL --filler
							,[Type] = T3.erc_ERCode	-- migrate value from [erc_ERCode]	-- Ref [trefAGSNatureOfCallCodes].[erc_ID]
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
  							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL--filler
							,Time__c =   NULL--filler
							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler
							
							,CreatedDate= CAST(T1.k9p_CreatedDate AS DATE)
							,zrefSrc = 'tblK9BuddyPhoneNote'
					FROM GDB_TC1_kade.dbo.tblK9BuddyPhoneNote AS T1
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.k9p_ByWhom
					LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T2 ON T2.psn_PersonID=T1.k9p_PersonID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T2.psn_PersonID  AS NVARCHAR(30)) 
					LEFT JOIN GDB_TC1_kade.dbo.trefAGSNatureOfCallCodes AS T3 ON T3.erc_ID=T1.k9p_NatureOfCall
		
			UNION 
					SELECT   DISTINCT  
							 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [tblStaff].[FileNum]
							,Legacy_ID__c = 'PNT-'+CAST(T1.pnt_NotesID	 AS NVARCHAR(30)) -- concatenate 'pnt-'+[pnt_NotesID]	
							,WhoID= X2.ID-- link to Contact	-- Link [tblPerson].[PersonID]
							,WhatId = NULL 
							,ActivityDate = CAST(T1.pnt_Date AS DATE)
							,[Subject] = 'Person Notes: '+ CAST(CAST(T1.pnt_Date AS DATE) AS NVARCHAR(30))	-- concatenate 'Person Notes ' +[pnt_Date]	
							,[Status] =CASE WHEN T1.pnt_Date >GETDATE() THEN 'Open' ELSE 'Completed' END	 	-- If >Today then 'Open' else 'Completed'	
							,[Description] = T1.pnt_Notes
							,[Recommendation__c] = NULL --filler
							,[Type] = T2.[New_Value]	
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
  							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL--filler
							,Time__c =   NULL--filler
							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler

							,CreatedDate = CAST(T1.pnt_CreatedDate AS DATE)
							,zrefSrc = 'tblPersonNotes'
					FROM GDB_TC1_kade.dbo.tblPersonNotes AS T1
					LEFT JOIN GDB_TC1_maps.DBO.CHART_PersonNote_Category AS T2 ON T2.pnt_Category=T1.pnt_Category
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.pnt_WhoBy
					LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON T3.psn_PersonID=T1.pnt_PersonID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T3.psn_PersonID  AS NVARCHAR(30)) 

			UNION 
					SELECT   DISTINCT  
							 OwnerId = CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]()  END -- Link [tblStaff].[FileNum]
							,Legacy = 'SPB-'+CAST(T1.spb_StaffID AS NVARCHAR(30))
							,WhoId= NULL
							,WhatId= NULL 
				  			,ActivityDate = CAST(T1.spb_Date AS DATE)		
							,[Subject] = 'Staff Dog Poop Bucket'	-- Staff Dog Poop Bucket	
							,[Status] = CASE T1.spb_Completed WHEN 'Yes' THEN 'Completed' ELSE 'Open' END 	-- If 'Yes' then 'Completed' else 'Open'	
							,[Description] = NULL 
							,[Recommendation__c] = NULL --filler
							,[Type] = NULL 
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
  							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL--filler
							,Time__c =   NULL--filler
							,Amount_1__c = NULL--filler	
							,Amount_2__c = NULL--filler		
							,GDB_Employee__C = NULL--filler	
							,Department__c = NULL--filler	
							,Denial__c = NULL--filler		
 							,Print_Date__c = NULL--filler  
 							,Dog__c = NULL--filler
							
							,CreatedDate= CAST(T1.spb_Date AS DATE)
							,zrefSrc = 'tblStaffDogPoopBucket'
					FROM GDB_TC1_kade.dbo.tblStaffDogPoopBucket AS T1
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C = T1.spb_StaffID
			
			UNION 
					SELECT   DISTINCT
							 OwnerId = GDB_TC1_migration.[dbo].[fnc_OwnerId]()
							,Legacy_ID__c ='VCP-'+CAST( T1.vcp_VRID	 AS NVARCHAR(30))-- concatenate 'Vcp-'+[vcp_VRID]	
							,WhoId = X2.ID		-- Link [tblPerson].[psn_PersonID]
							,WhatId = X3.ID		-- Ref/Link [trefOutsideVet].[OutsideVetCode]
						 	,ActivityDate = CAST(T1.vcp_LetterDate AS DATE) 
							,[Subject] = T1.vcp_LetterType		
							,[Status] =CASE WHEN T1.vcp_LetterDate >GETDATE() THEN 'Open' ELSE 'Completed' END	-- if > Today then 'Open' else 'Completed'	
							,[Description] =CAST( T1.vcp_Lettertxt	AS NVARCHAR(MAX))
 							,[Recommendation__c] = NULL --filler
							,[Type] = NULL 
							,[Qualified__c] = NULL --filler
							,[Additional_Follow_Up__c] = NULL--filler
  							,Task_Subtype__c = NULL--filler
							,Attempt_1__c = NULL--filler
							,Attempt_2__c = NULL--filler
							,Attempt_3__c = NULL--filler
							,SC_Comments__c = NULL--filler
							,Visit_Date__c = NULL--filler
							,Visit_Time__c = NULL--filler
							,Visit_Location__C = NULL--filler
							,Time__c =   NULL--filler
							,Amount_1__c = T1.vcp_Amount1		
							,Amount_2__c = T1.vcp_Amount2		
							,GDB_Employee__C = T1.vcp_GDBEmp		
							,Department__c = T1.vcp_Dept		
							,Denial__c = T1.vcp_Denial		
 							,Print_Date__c = CAST(T1.vcp_PrintDate	AS DATE) 
 							,Dog__c = X4.ID		-- Link [tblDog].[dog_DogID]

							,CreatedDate = CAST(T1.vcp_CreatedDate AS DATE) 
							,zrefSrc = 'tblVCPLetterInfo'
					FROM GDB_TC1_kade.dbo.tblVCPLetterInfo AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON T3.psn_PersonID=T1.vcp_PersonID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X2 ON X2.LEGACY_ID__C = CAST(T3.psn_PersonID  AS NVARCHAR(30)) 
					LEFT JOIN GDB_TC1_kade.dbo.trefOutsideVet AS T2 ON CAST(T2.OutsideVetCode AS NVARCHAR(30))= T1.vcp_OutsideVetCode
					LEFT JOIN GDB_TC1_migration.dbo.XTR_ACCOUNT AS X3 ON X3.LEGACY_ID__C='ov-' +CAST(T2.OutsideVetCode AS NVARCHAR(20)) 
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T4 ON T4.dog_DogID=T1.vcp_DogID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_DOG AS X4 ON X4.LEGACY_ID__C=CAST(T4.dog_DogID AS NVARCHAR(30))


END --tc1: 131,744

BEGIN-- AUDIT IMP_TASK
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_TASK 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_TASK GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_TC1_migration.dbo.IMP_TASK 
	GROUP BY zrefSrc

	SELECT Type  
	FROM GDB_TC1_migration.dbo.IMP_TASK
	GROUP BY Type

	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_TASK
	 

END 