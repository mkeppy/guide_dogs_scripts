USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblPuppyRaisingApplicationDetai
			WHERE   SF_Object LIKE '%RE%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_RELATED_APPLICANT

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,'PAD-'+CAST(T1.pad_ApplicationDetailID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Pad-'+[pad_ApplicaitonDetailID]	
						,'PRA-'+CAST(T2.pra_ApplicationID AS NVARCHAR(30))  AS  [Volunteer_Application__r:Legacy_Id__c]	-- Link [tblPuppyRaisingApplication].[pra_ApplicationID]
						,T3.psn_PersonID  AS  [Contact__r:Legacy_Id__c]		-- Link [tblPerson].[PersonID]
						,T5.[Description]  AS  Assigned_Dog_Role__c	-- Link thru [tblPersonDog] to [trefRelCodeNew].[RelationCode]  	-- Link [tblPersonDog].[pd_RelationID]
						,T7.[Description]  AS  Placed_Dog_Role__c	-- Link thru [tblPersonDog] to [trefRelCodeNew].[RelationCode]  	-- Link [tblPersonDog].[pd_RelationID]
					 	,CAST(T1.pad_CreatedDate AS DATE)  AS CreatedDate

					INTO GDB_TC1_migration.DBO.IMP_RELATED_APPLICANT
					FROM GDB_TC1_kade.dbo.tblPuppyRaisingApplicationDetail AS T1
					LEFT JOIN GDB_TC1_kade.DBO.tblPuppyRaisingApplication AS T2 ON T2.pra_ApplicationID=T1.pad_ApplicationID
					LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON T3.psn_PersonID=T1.pad_PersonID
					LEFT JOIN GDB_TC1_kade.DBO.tblPersonDog AS T4 ON T4.pd_RelationID=T1.pad_AssignedPDID
						LEFT JOIN GDB_TC1_kade.DBO.trefRelCodeNew AS T5 ON T5.RelationCode = T4.pd_RelCode
					LEFT JOIN GDB_TC1_kade.DBO.tblPersonDog AS T6 ON T6.pd_RelationID=T1.pad_PlacedPDID
						LEFT JOIN GDB_TC1_kade.DBO.trefRelCodeNew AS T7 ON T7.RelationCode = T6.pd_RelCode
					
			 


END --TC1: 48525

 

BEGIN-- AUDIT

	SELECT * FROM GDB_TC1_migration.dbo.IMP_RELATED_APPLICANT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_RELATED_APPLICANT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	
	SELECT * FROM GDB_TC1_migration.DBO.IMP_RELATED_APPLICANT
 
	SELECT COUNT(*) FROM  GDB_TC1_migration.DBO.IMP_RELATED_APPLICANT
 
 END 