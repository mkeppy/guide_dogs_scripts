USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblNursingMedNotesQDetail
			WHERE   SF_Object LIKE '%tre%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_TREATMENT

END 

BEGIN -- CREATE IMP 

					SELECT   DISTINCT
						GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId

						,T1.mnqd_ID  AS  Legacy_ID__c		
						,'MN-'+CAST(T2.mn_MedicalNoteID AS NVARCHAR(30)) AS [Medical_Chart__r:Legacy_ID__c]		-- Link [tblNursingMedicalNotes].[mn_MedicalNoteID]
						,T3.Category  AS  Category__c		-- Ref [trefNursingCategories].[CategoryID]
						,T4.Treatment AS  Treatment__c		-- Ref [trefNursingCategories].[TreatmentID]
						
					 INTO GDB_TC1_migration.DBO.IMP_TREATMENT
					FROM GDB_TC1_kade.dbo.tblNursingMedNotesQDetail AS T1
					LEFT JOIN GDB_TC1_kade.DBO.tblNursingMedicalNotes AS T2 ON T2.mn_MedicalNoteID=T1.mnqd_MedicalNoteID
					LEFT JOIN GDB_TC1_kade.DBO.trefNursingCategories AS T3 ON T3.CategoryID=T1.mdqd_CategoryID
					LEFT JOIN GDB_TC1_kade.DBO.trefNursingCategories AS T4 ON T4.TreatmentID=T1.mnqd_TreatmentID


					                                                                                                                                                                                                                                                                                                                                                                                     N                                                                                                                                                                                                                                                                    
END --tc1: 12224
BEGIN-- AUDIT

SELECT * FROM GDB_TC1_migration.DBO.IMP_TREATMENT

 
END 