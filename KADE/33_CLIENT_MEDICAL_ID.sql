USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblNursingMedicalNotes
			WHERE   SF_Object LIKE '%med%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 
 
 SELECT * FROM GDB_TC1_migration.DBO.XTR_RECORD_TYPE ORDER BY SOBJECTTYPE

BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_CLIENT_MEDICAL_ID
	 

END 


BEGIN -- CREATE IMP_CLIENT_MEDICAL_ID 
					 SELECT DISTINCT
					  GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					 ,Client_Medical_Id__c AS [Name]
					 ,psn_PersonId AS zrefPersonId
				 	 INTO GDB_TC1_migration.DBO.IMP_CLIENT_MEDICAL_ID
					 FROM GDB_TC1_migration.dbo.stg_tblMedicalChartIds
					 ORDER BY Client_Medical_Id__c

END  --TC1 1005

		--check dupes
					SELECT * FROM GDB_TC1_migration.dbo.IMP_CLIENT_MEDICAL_ID
					WHERE [Name] IN (SELECT [Name] FROM GDB_TC1_migration.dbo.IMP_CLIENT_MEDICAL_ID GROUP BY [Name] HAVING COUNT(*)>1)
					ORDER BY [Name]

  








