USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblPersonRel
			WHERE   SF_Object LIKE '%gdb%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 


BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_GDB_ROLE

END 


BEGIN -- CREATE IMP 

					SELECT   
					'prl-'+CAST(T1.prl_RelationID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'prl'+[prl_RelationID]	
					,T3.psn_PersonID  AS  [Person__r:Legacy_Id__c]			-- Link [tblPerson].[psn_PersonID]
			 		,T1.prl_RelationCode  AS  Relation_Code__c		
					,T1.prl_RelationCode  AS  Dog_Relation__c		
					,CAST(T1.prl_StartDate AS DATE) AS  Start_Date__c		
					,CAST(T1.prl_EndDate  AS DATE) AS  End_Date__c		
					,T4.erc_ERCodeText  AS  End_Reason__c		-- Yes/Link [dbo_trefEndReasonCodes]
					,T5.rct_ERCodeCatText AS  End_Reason_Cateogry__C
			 
					,T1.prl_EndReason AS zrefEndReason
				--	 INTO GDB_TC1_migration.dbo.IMP_GDB_ROLE
					FROM GDB_TC1_kade.dbo.tblPersonRel AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID = T1.prl_RelationID
				 	LEFT JOIN GDB_TC1_kade.DBO.tblPerson AS T3 ON T3.psn_PersonID=T1.prl_PersonID
					LEFT JOIN GDB_TC1_kade.dbo.trefEndReasonCodes AS T4 ON T4.erc_ID=T1.prl_EndReason
					LEFT JOIN GDB_TC1_kade.dbo.trefEndReasonCodesCat AS T5 ON T5.rct_ID=T4.erc_RECodeCat
					WHERE T1.prl_ClientInstanceID='0'

END -- tc1: 90,096
 
BEGIN --AUDIT 

			SELECT * FROM GDB_TC1_migration.dbo.IMP_GDB_ROLE 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_GDB_ROLE GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			
		 --0
		 

END 





























