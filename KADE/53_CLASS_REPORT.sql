USE GDB_TC1_migration
GO


BEGIN--maps 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblFinalStudentDogSummary
			WHERE   SF_Object LIKE '%class%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*
SELECT * FROM GDB_TC1_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%class%'
0123D0000004ax0QAA	Continued Assessment	Class_Report__c
0123D0000004ax1QAA	Dog Final				Class_Report__c
0123D0000004ax2QAA	Student Final			Class_Report__c
0123D0000004ax3QAA	Student and Dog Final	Class_Report__c
*/

/*****************************************************************************************************************************/

BEGIN -- DROP IMP_CLASS_REPORT_cfd
	DROP TABLE GDB_TC1_migration.DBO.IMP_CLASS_REPORT_cfd
END 

BEGIN -- CREATE IMP_CLASS_REPORT_cfd 

					SELECT	DISTINCT
									GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
									,'0123D0000004ax1QAA' AS RecordTypeId   --Dog Final
									,'CFD-'+CAST(T1.cfd_ID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "CFD-" with cfd_ID	
									,T4.psn_PersonID AS [Contact__r:Legacy_Id__c]-- Link [tblPersonRel].[prl_ClientInstanceID]
 								 	,T2I.Legacy_Id__c AS  [Class__r:Legacy_Id__c]	-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]
									,T6.drl_DogID  AS  [Dog__r:Legacy_Id__c]	-- link to [tblPersonRel] to link to [tblPersonDog] to link to [tblDogRel] to link to [tblDog]	-- Link [tblPersonRel].[prl_ClientInstanceID]		
								 	,CAST(T1.cfd_Health  AS NVARCHAR(MAX)) AS  Health__c		
									,T1.cfd_FinalClassPace  AS  Final_Class_Pace__c		
									,T1.cfd_PaceDescription  AS  Pace_Description__c		
									,T1.cfd_Sensitivity  AS  Sensitivity__c		
									,T1.cfd_Energy  AS  Energy__c		
									,T1.cfd_Confidence  AS  Confidence__c		
									,T1.cfd_Willingness  AS  Willingness__c		
									,T1.cfd_Focus  AS  Focus__c		
									,T1.cfd_Dogs  AS  Dogs__c		
									,T1.cfd_Cats  AS  Cats__c		
									,T1.cfd_Birds  AS  Birds__c		
									,T1.cfd_Food  AS  Food__c		
									,T1.cfd_People  AS  People__c		
									,T1.cfd_Scents  AS  Scents__c		
									,CAST(T1.cfd_OtherDistractions AS NVARCHAR(MAX)) AS  Other_Distractions__c		
									,CAST(T1.cfd_TOH_HandlingTechnique  AS NVARCHAR(MAX)) AS  TOH_Handling_Technique__c		
									,CAST(T1.cfd_TOH_RewardStrategy  AS NVARCHAR(MAX)) AS  TOH_Reward_Strategy__c		
									,CAST(T1.cfd_TOH_Pace AS NVARCHAR(MAX))  AS  TOH_Pace__c		
									,CAST(T1.cfd_TOH_OtherDistractions  AS NVARCHAR(MAX)) AS  TOH_Other_Distractions__c		
									,T1.cfd_RelievingNormalLimits  AS  Relieving_Normal_Limits__c		
									,T1.cfd_Inconsistent  AS  Inconsistent__c		
									,T1.cfd_Distracted  AS  Distracted__c		
									,T1.cfd_ReluctanttoMove  AS  Reluctant_to_Move__c		
									,T1.cfd_#Urinations  AS  Number_of_Urinations__c		
									,CAST(T1.cfd_Challenges  AS NVARCHAR(MAX)) AS  Challenges__c		
									,T1.cfd_CreatedBy  AS  KD_Created_By__c		
									,CAST(T1.cfd_CreatedDate AS DATE) AS  CreatedDate		
									,T1.cfd_ModifiedBy  AS  KD_Modified_By__c		
									,CAST(T1.cfd_ModifiedDate AS DATE) AS  KD_Modified_Date__c		
									 	
					-- INTO GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfd
					FROM GDB_TC1_kade.dbo.tblClassFinalDog AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.cfd_ClientID
							LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
							LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.cfd_ClientID
							LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
				 	LEFT JOIN GDB_TC1_kade.dbo.tblPersonDog AS T5 ON T5.pd_PersonRelID=T3.prl_RelationID
							INNER JOIN GDB_TC1_kade.dbo.tblDogRel AS T6 ON T6.drl_RelationID=T5.pd_DogRelID
							INNER JOIN GDB_TC1_kade.dbo.tblDog AS T7 ON T7.dog_DogID=T6.drl_DogID

 
END-- end IMP_CLASS_REPORT_cfd: tc1:  3667

BEGIN -- AUDIT IMP_CLASS_REPORT_cfd
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfd 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfd GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	 
		--update legacy id to make unique. 
	UPDATE GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfd 
	SET Legacy_ID__c = Legacy_ID__c + '-'+ CAST([Dog__r:Legacy_Id__c] AS NVARCHAR(30))
		WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfd GROUP BY Legacy_ID__c HAVING COUNT(*)>1)


	SELECT * FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfd ORDER BY Legacy_Id__c
	
	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfd
	 
END 

/*****************************************************************************************************************************/

BEGIN -- DROP IMP_CLASS_REPORT_cfs
	DROP TABLE GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfs 
END 

BEGIN -- CREATE IMP_CLASS_REPORT_cfs
 
							SELECT DISTINCT
									GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
									,'0123D0000004ax2QAA' AS RecordTypeId   -- Student Final
									,'CFS-'+CAST(T1.cfs_ID AS NVARCHAR(30))  AS  Legacy_Id__c	-- Concatenate "CFS-" with cfs_ID	
									,T4.psn_PersonID AS [Contact__r:Legacy_Id__c]-- Link [tblPersonRel].[prl_ClientInstanceID]
 								 	,T2I.Legacy_Id__c AS  [Class__r:Legacy_Id__c]	-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]

									,T1.cfs_VisionLoss  AS  Vision_Loss__c		
									,T1.cfs_Retrain  AS  Retrain__c		
									,T1.cfs_DegreeVisionLoss  AS  Degree_Vision_Loss__c		
									,T1.cfs_ClassType  AS  Class_Type__c		
									,T1.cfs_LearningStyle  AS  Learning_Style__c		
									,CAST(T1.cfs_Health  AS NVARCHAR(MAX)) AS  Health__c		
									,CAST(T1.cfs_ClassExperience  AS NVARCHAR(MAX)) AS  Class_Experience__c		
									,CAST(T1.cfs_OM   AS NVARCHAR(MAX))AS  OM__c		
									,T1.cfs_DogChange  AS  Dog_Change__c		
									,CAST(T1.cfs_Challenges  AS NVARCHAR(MAX)) AS  Challenges__c		
									,CAST(T1.cfs_DHS_FoodReward  AS NVARCHAR(MAX)) AS  DHS_Food_Reward__c		
									,CAST(T1.cfs_DHS_PassDirections  AS NVARCHAR(MAX)) AS  DHS_Pass_Directions__c		
									,CAST(T1.cfs_DHS_SecReinforcement AS NVARCHAR(MAX))  AS  DHS_Sec_Reinforcement__c		
									,CAST(T1.cfs_DHS_HandlingStyle  AS NVARCHAR(MAX)) AS  DHS_Handling_Style__c		
									,CAST(T1.cfs_DHS_HandlingTech  AS NVARCHAR(MAX)) AS  DHS_Handling_Tech__c		
									,CAST(T1.cfs_DHS_DogAsGuide  AS NVARCHAR(MAX)) AS  DHS_Dog_As_Guide__c		
									,CAST(T1.cfs_DHS_PacePull  AS NVARCHAR(MAX)) AS  DHS_Pace_Pull__c		
									,CAST(T1.cfs_DHS_Escalator  AS NVARCHAR(MAX)) AS  DHS_Escalator__c		
									,CAST(T1.cfs_Goals  AS NVARCHAR(MAX)) AS  Goals__c		
									,T1.cfs_Graduated  AS  Graduated__c		
									,CAST(T1.cfs_CreatedDate AS DATE)  AS  CreatedDate		

						 		INTO GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfs 
								FROM GDB_TC1_kade.dbo.tblClassFinalStudent AS T1
								LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.cfs_ClientID
									LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
									LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
								LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.cfs_ClientID
									LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
				 	            
								--3507

END -- end IMP_CLASS_REPORT_cfs: tc1:  3507

BEGIN -- AUDIT IMP_CLASS_REPORT_cfs
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfs 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfs GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	 
	SELECT * FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfs ORDER BY Legacy_Id__c
	
	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_cfs
	 
END 

/*****************************************************************************************************************************/

BEGIN -- DROP IMP_CLASS_REPORT_ca
	DROP TABLE GDB_TC1_migration.dbo.IMP_CLASS_REPORT_ca
END 

BEGIN -- CREATE IMP_CLASS_REPORT_ca

							SELECT  DISTINCT
									GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
									,'0123D0000004ax0QAA' AS RecordTypeId   -- Continued Assessment
 									,'CA-'+CAST(T1.ca_ContAssessID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "CA-" with ca_ConstAssessID	
									,T4.psn_PersonID AS [Contact__r:Legacy_Id__c]-- Link [tblPersonRel].[prl_ClientInstanceID]
									,'cli-'+CAST(T2.cli_ClientID AS NVARCHAR(30))  AS  [Application__r:Legacy_Id__c]		 
 									,T1.ca_AGSNumber  AS  AGS_Number__c		
									,T1.ca_Campus  AS  Campus__c		
									,CAST(T1.ca_StartDate  AS DATE) AS  Start_Date__c		
									,CAST(T1.ca_EndDate  AS DATE)  AS   End_Date__c				
									,T1.ca_OMTravelInd  AS  OM_Travel_Ind__c		
									,T1.ca_OMStraightLine  AS  OM_Straight_Line__c		
									,T1.ca_OMManeuverOb  AS  OM_Maneuver_Ob__c		
									,T1.ca_OMCues  AS  OM_Cues__c		
									,T1.ca_OMSolveProb  AS  OM_Solve_Prob__c		
									,T1.ca_OMTrafficControl  AS  OM_Traffic_Control__c		
									,T1.ca_OMTrafficFlow  AS  OM_Traffic_Flow__c		
									,T1.ca_OMStreetCross  AS  OM_Street_Cross__c		
									,T1.ca_OMAlignCurb  AS  OM_Align_Curb__c		
									,T1.ca_OMCorrectVeer  AS  OM_Correct_Veer__c		
									,T1.ca_OMComments  AS  OM_Comments__c		
									,T1.ca_JunoUseHarness  AS  Juno_Use_Harness__c		
									,T1.ca_JunoFootwork  AS  Juno_Foot_work__c		
									,T1.ca_JunoSequence  AS  Juno_Sequence__c		
									,T1.ca_JunoControl  AS  Juno_Control__c		
									,T1.ca_JunoPraise  AS  Juno_Praise__c		
									,T1.ca_JunoReactions  AS  Juno_Reactions__c		
									,T1.ca_JunoRelate  AS  Juno_Relate__c		
									,T1.ca_JunoIntegrate  AS  Juno_Integrate__c		
									,T1.ca_JunoComments  AS  Juno_Comments__c		
									,CASE T1.ca_PMSMedStable WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  PMS_Med_Stable__c		
									,T1.ca_PMSMedStableComments  AS  PMS_Med_Stable_Comments__c		
									,CASE T1.ca_PMSPhysStable  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  PMS_Phys_Stable__c		
									,T1.ca_PMSPhysStableComments  AS  PMS_Phys_Stable_Comments__c		
									,CASE T1.ca_STNeuro  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  ST_Neuro__c		
									,T1.ca_STNeuroComments  AS  ST_Neuro_Comments__c		
									,CASE T1.ca_STProp  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  ST_Prop__c		
									,T1.ca_STPropComments  AS  ST_Prop_Comments__c		
									,CASE T1.ca_STBalance  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  ST_Balance__c		
									,T1.ca_STBalanceComments  AS  ST_Balance_Comments__c		
									,CASE T1.ca_StSupport  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  ST_Support__c		
									,T1.ca_STSupportComments  AS  ST_Support_Comments__c		
									,CASE T1.ca_SHNormLimits  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  SH_Norm_Limits__c		
									,T1.ca_SHNormLimitsComments  AS  SH_Norm_Limits_Comments__c		
									,CASE T1.ca_SHLossRightEar WHEN '1' THEN 'TRUE' ELSE 'FALSE' END   AS  SH_Loss_Right_Ear__c		
									,CASE T1.ca_SHLossLeftEar  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  SH_Loss_Left_Ear__c		
									,CASE T1.ca_SHAidRightEar  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  SH_Aid_Right_Ear__c		
									,CASE T1.ca_SHAidLeftEar   WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  SH_Aid_Left_Ear__c		
									,CASE T1.ca_SHLocal  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  SH_Local__c		
									,CASE T1.ca_SHSpeech WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END   AS  SH_Speech__c		
									,CASE T1.ca_SHFM  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  SH_FM__c		
									,T1.ca_CSApplyInstr  AS  CS_Apply_Instr__c		
									,T1.ca_CSRememberInstr  AS  CS_Remember_Instr__c		
									,T1.ca_CSSpatial  AS  CS_Spatial__c		
									,T1.ca_CSExtInt  AS  CS_Ext_Int__c		
									,T1.ca_CSConfidence  AS  CS_Confidence__c		
									,T1.ca_CSMotivation  AS  CS_Motivation__c		
									,CASE T1.ca_CSConsistent  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  CS_Consistent__c		
									,T1.ca_CSConsistentComments  AS  CS_Consistent_Comments__c		
									,T1.ca_ESInstructionComments  AS  ES_Instruction_Comments__c		
									,CASE T1.ca_ESConsistent  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  ES_Consistent__c		
									,T1.ca_ESConsistentComments  AS  ES_Consistent_Comments__c		
									,T1.ca_ContAssessSummary  AS  Cont_Assess_Summary__c		
									,CASE T1.ca_DeterAccept  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Deter_Accept__c		
									,T1.ca_DeterAcceptClass  AS  Deter_Accept_Class__c		
								 	,X1.ID AS Cont_Assess_Emp_1__c				-- Link [tblStaff].[FileNum]
							 		,X2.ID AS  Cont_Assess_Emp_2__c				-- Link [tblStaff].[FileNum]
							 		,X3.ID AS  Cont_Assess_Emp_3__c				-- Link [tblStaff].[FileNum]
							 		,X4.ID AS  Con_Assess_Reporter_Emp__c		-- Link [tblStaff].[FileNum]
									,CAST(T1.ca_CreatedDate  AS DATE) AS  CreatedDate		

							--	INTO GDB_TC1_migration.dbo.IMP_CLASS_REPORT_ca 
								FROM GDB_TC1_kade.dbo.tblClientContAssess AS T1
							 	LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.ca_ClientID
								LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T2.cli_ClientID
									LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID				 			
								LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.ca_ContAssessEmp1
								LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X2 ON X2.ADP__C=T1.ca_ContAssessEmp2
								LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X3 ON X3.ADP__C=T1.ca_ContAssessEmp3
								LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X4 ON X4.ADP__C=T1.ca_ContAssessReporterEmp 


END -- end IMP_CLASS_REPORT_ca: tc1: 138   

BEGIN -- AUDIT IMP_CLASS_REPORT_ca
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_ca 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_ca GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	 
	SELECT * FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_ca ORDER BY Legacy_Id__c
	
	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_ca
	 
END 

/*****************************************************************************************************************************/


BEGIN -- DROP IMP_CLASS_REPORT_fsd
	DROP TABLE GDB_TC1_migration.dbo.IMP_CLASS_REPORT_fsd
END 

BEGIN -- CREATE IMP_CLASS_REPORT_fsd

							SELECT  DISTINCT
									GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
									,'0123D0000004ax3QAA' AS RecordTypeId   -- Student and Dog Final
 									,'FSD-'+CAST(T1.fsd_ClientID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "FSD-" with fsd_ClientID	-- Link [tblPersonRel].[prl_ClientInstanceID]
									,T4.psn_PersonID AS [Contact__r:Legacy_Id__c]-- Link [tblPersonRel].[prl_ClientInstanceID]
 								 	,T2I.Legacy_Id__c AS  [Class__r:Legacy_Id__c]	-- link to [tblClient] to link to [TblClass] through tblClient.cli_ClassCode	-- Link [tblClient].[cli_ClientID]
									,T6.drl_DogID  AS  [Dog__r:Legacy_Id__c]	-- link to [tblPersonRel] to link to [tblPersonDog] to link to [tblDogRel] to link to [tblDog]	-- Link [tblPersonRel].[prl_ClientInstanceID]		
									,T1.fsd_AltEquip  AS  Alt_Equip__c		
									,T1.fsd_ApplicationGuideWork  AS  Application_Guide_Work__c		
									,T1.fsd_ApplicationObedience  AS  Application_Obedience__c		
									,T1.fsd_AttitudeClassmates  AS  Attitude_Classmates__c		
									,T1.fsd_AttitudeCriticism  AS  Attitude_Criticism__c		
									,T1.fsd_AttitudeDog  AS  Attitude_Dog__c		
									,T1.fsd_AttitudeInstruction  AS  Attitude_Instruction__c		
									,T1.fsd_AttitudeSchool  AS  Attitude_School__c		
									,T1.fsd_Balance  AS  Balance__c		
									,T1.fsd_CareOfDog  AS  Care_Of_Dog__c		
									,T1.fsd_Coordination  AS  Coordination__c		
									,T1.fsd_Distractions  AS  Distractions__c		
									,T1.fsd_DogCF  AS  Dog_CF__c		
									,T1.fsd_DogChange  AS  Dog_Change__c		
									,T1.fsd_DogDormBehavior  AS  Dog_Dorm_Behavior__c		
									,T1.fsd_DogGuideWork  AS  Dog_Guide_Work__c		
									,T1.fsd_DogLeashRelieving  AS  Dog_Leash_Relieving__c		
									,T1.fsd_DogObedience  AS  Dog_Obedience__c		
									,T1.fsd_DogWorkCF  AS  Dog_Work_CF__c		
									,T1.fsd_Grooming  AS  Grooming__c		
									,T1.fsd_HealthBegin  AS  Health_Begin__c		
									,T1.fsd_HealthEnd  AS  Health_End__c		
									,T1.fsd_Hearing  AS  Hearing__c		
									,CAST(T1.fsd_Narrative AS NVARCHAR(30)) AS  Narrative__c		
									,T1.fsd_OMDorm  AS  OM_Dorm__c		
									,T1.fsd_OMGuideWork  AS  OM_Guide_Work__c		
									,T1.fsd_OMRouteLearning  AS  OM_Route_Learning__c		
									,T1.fsd_OMTrafficReading  AS  OM_Traffic_Reading__c		
									,T1.fsd_PreferredPace  AS  Preferred_Pace__c		
									,T1.fsd_Retrain  AS  Retrain__c		
									,CAST(T1.fsd_StudentInfoComments AS NVARCHAR(30)) AS  Student_Info_Comments__c		
									,CASE T1.fsd_TotalVisionLoss WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Total_Vision_Loss__c		
									,T1.fsd_Understanding  AS  Understanding__c		
									,T1.fsd_Willingness  AS  Willingness__c		
									,CASE T1.fsd_WorkCityBus  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_City_Bus__c		
									,CASE T1.fsd_WorkCountryRoad WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Work_Country_Road__c		
									,T1.fsd_WorkCustomRoutes  AS  Work_Custom_Routes__c		
									,CASE T1.fsd_WorkEscalators  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Escalators__c		
									,CASE T1.fsd_WorkPlatforms  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Platforms__c		
									,CASE T1.fsd_WorkRevolvingDoor  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Revolving_Door__c		
									,CASE T1.fsd_WorkRoundCorners  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Round_Corners__c		
									,CASE T1.fsd_WorkSidewalkless  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Sidewalkless__c		
									,CASE T1.fsd_WorkTraffic  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Work_Traffic__c		
									,CAST(T1.fsd_CreatedDate  AS DATE) AS  CreatedDate		
 	
							-- 	INTO GDB_TC1_migration.dbo.IMP_CLASS_REPORT_fsd 
								FROM GDB_TC1_kade.dbo.tblFinalStudentDogSummary AS T1
							 	LEFT JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID=T1.fsd_ClientID
										LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T2C ON T2C.cls_ClassID=T2.cli_ClassCode
										LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T2I ON T2I.Legacy_Id__c=('cls-'+T2C.cls_ClassID)
								LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.fsd_ClientID
										LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
				 				LEFT JOIN GDB_TC1_kade.dbo.tblPersonDog AS T5 ON T5.pd_PersonRelID=T3.prl_RelationID
										INNER JOIN GDB_TC1_kade.dbo.tblDogRel AS T6 ON T6.drl_RelationID=T5.pd_DogRelID
										INNER JOIN GDB_TC1_kade.dbo.tblDog AS T7 ON T7.dog_DogID=T6.drl_DogID
								ORDER BY Legacy_Id__c


END -- end IMP_CLASS_REPORT_ca: tc1: 138   

BEGIN -- AUDIT IMP_CLASS_REPORT_fsd
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_fsd 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_fsd GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 --update legacy id to make unique. 
	UPDATE GDB_TC1_migration.dbo.IMP_CLASS_REPORT_fsd 
	SET Legacy_ID__c = Legacy_ID__c + '-'+ CAST([Dog__r:Legacy_Id__c] AS NVARCHAR(30))
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_fsd GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	 
	SELECT * FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_fsd ORDER BY Legacy_Id__c
	
	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_CLASS_REPORT_fsd
	 
END 

/*****************************************************************************************************************************/































