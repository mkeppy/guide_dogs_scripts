USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblBCSGoals
			WHERE   SF_Object LIKE '%bree%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_BREEDING_GOAL

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,YEAR(T1.gl_FiscalYearStart) AS [Name]
							,'GL-'+CAST(T1.gl_GoalID AS NVARCHAR(30))  AS  Legacy_ID__c		
							,CAST(T1.gl_FiscalYearStart AS DATE)  AS  Fiscal_Year_Start__c		
							,CAST(T1.gl_FiscalYearEnd  AS DATE) AS  Fiscal_Year_End__c		
							,T1.gl_BroodBitches  AS  Brood_Bitches__c		
							,T1.gl_ResultPreg  AS  Result_Pregnancies__c		
							,T1.gl_ConceptionRate  AS  Conception_Rate__c		
							,T1.gl_AverageLitterSize  AS  Average_Litter_Size__c		
							,T1.gl_MortalityRate  AS  Mortality_Rate__c		
							,T1.gl_LittersWhelped  AS  Litters_Whelped__c		
							,T1.gl_LabWhelpedPuppies  AS  Lab_Whelped_Puppies__c		
							,T1.gl_GoldenWhelpedPuppies  AS  Golden_Retriever_Whelped_Puppies__c		
							,T1.gl_GermanWhelpedPuppies  AS  German_Shepherd_Whelped_Puppies__c		
							,T1.gl_LabGldXWhelpedPuppies  AS  Lab_Gold_Whelped_Puppies__c		
							,T1.gl_SSCWhelpedPuppies  AS  SSC_Whelped_Puppies__c		
							,T1.gl_GDBWhelpedPuppies  AS  GDB_Whelped_Puppies__c		
							,T1.gl_NonGDBWhelpedPuppies  AS  NonGDB_Whelped_Puppies__c		
							,T1.gl_BreedingStockAddFemale  AS  Breeding_Stock_Add_Female__c		
							,T1.gl_BreedingStockAddMale  AS  Breeding_Stock_Add_Male__c		
							,T1.gl_BreedingEvalDogs  AS  Breeding_Eval_Dogs__c		
						
						INTO GDB_TC1_migration.DBO.IMP_BREEDING_GOAL
						FROM GDB_TC1_kade.dbo.tblBCSGoals AS T1





END --tc1 21 

BEGIN-- AUDIT

	SELECT * FROM  GDB_TC1_migration.DBO.IMP_BREEDING_GOAL

END 