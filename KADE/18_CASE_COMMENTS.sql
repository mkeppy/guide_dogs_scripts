USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX
			WHERE   SF_Object LIKE '%XXXXXXXXXXXX%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_CASE_COMMENTS

END 

BEGIN -- CREATE IMP 

			SELECT DISTINCT
				 ParentId =  X1.ID  
				,'FALSE' IsPublished
 				,CommentBody =  'Pratt Fund Case Discussion: '+  LEFT(CAST(CAST(T1.pfcd_Time AS  DATETIME2) AS NVARCHAR(30)),20) +CHAR(10)+ T1.pfcd_CaseNotes   	-- concatenate FirstName + " "+ LastName form [tblStaff] +LineBreak+[pfcd_CaseNotes]
		 		,CreatedByID = X2.id
				,CreatedDate = CAST(T1.pfcd_CreatedDate AS date)
				,T1.pfcd_EntryID AS zrefLegacyId
				,'tblPrattFundCaseDiscussion' zrefSrc
			INTO GDB_TC1_migration.dbo.IMP_CASE_COMMENTS
			FROM GDB_TC1_kade.dbo.tblPrattFundCaseDiscussion AS T1
			INNER JOIN GDB_TC1_kade.dbo.tblPrattFundAuthorization AS T2 ON T2.pfa_AuthID = T1.pfcd_AuthID 	
			LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T3 ON T3.FileNum=T2.pfa_WhoBy		
			INNER JOIN GDB_TC1_migration.dbo.XTR_CASE AS X1 ON 'Pfa-'+CAST(T2.pfa_AuthID AS NVARCHAR(30)) = X1.Legacy_Id__c
 			LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X2 ON X2.ADP__C=T1.pfcd_ByWho
 
   
END --tc1: 197

BEGIN-- AUDIT


	SELECT * FROM GDB_TC1_migration.dbo.IMP_CASE_COMMENTS


END 