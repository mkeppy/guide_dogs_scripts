
USE GDB_TC1_migration
GO

	--ACCOUNT: UPDATE PRIMARY CONTACT

		SELECT  T1.zrefAccountId AS [Legacy_Id__c] 
			   ,T1.psn_PersonID  AS [npe01__One2OneContact__r:Legacy_Id__c]
			 
	 
			   ,T1.psn_First 
			   ,T1.psn_Last 
		INTO GDB_TC1_migration.dbo.IMP_ACCOUNT_UPDATE
		FROM GDB_TC1_kade.DBO.tblPerson T1
	 	WHERE T1.zrefRecordType ='HHD'   AND (T1.zrefAccountID=T1.psn_PersonID) 
		ORDER BY T1.zrefAccountId, T1.psn_PersonID

 
		SELECT * FROM GDB_TC1_migration.dbo.IMP_ACCOUNT_UPDATE