USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblVBPayments
			WHERE   SF_Object LIKE '%rec%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*
	SELECT * FROM GDB_TC1_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%vet%'
	0123D0000004dsKQAQ	Dog Concern			Vet_Record__c
	0123D0000004dsPQAQ	Physical Exam		Vet_Record__c
*/

BEGIN -- DROP IMP
	DROP TABLE GDB_TC1_migration.DBO.IMP_VET_RECORD
	DROP TABLE GDB_TC1_migration.DBO.IMP_VET_RECORD_srt
END 

BEGIN -- CREATE IMP 
					SELECT   DISTINCT 
								GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							--tblVetEntry
							,CASE WHEN T10.zrefSeq>1 THEN ('VDL-'+CAST(T1.vdl_VetEntryID AS NVARCHAR(30)) +'-'+CAST(T10.zrefSeq AS NVARCHAR(1)))
								ELSE 'VDL-'+CAST(T1.vdl_VetEntryID AS NVARCHAR(30)) END AS  Legacy_Id__c	-- Concatenate "VDL-" with vdl_VetEntryID	
							,T4.dog_DogID AS  [Dog__r:Legacy_Id__c]		-- Link [tbDog].[dog_DogID]
							,CAST(T1.vdl_Date  AS DATE) AS  Date__c		
							,CAST(T1.vdl_TreatmentDate AS DATE) AS  Treatment_Date__c		
							,T1.vdl_Complaint  AS  [Name]	
							,T5.VetSourceDesc AS  Source__c	-- migrate value from [VetSourceDesc]	-- Link/trefVetSource.VetSourceID
							,T6.[Description]  AS  Facility__c	-- migrate value from [Description]	-- Yes/Link [trefVetFacility].[VetFacilityID]
 							,LTRIM((CASE WHEN T7.VetFirstName IS NOT NULL THEN COALESCE(T7.VetFirstName, '') ELSE '' END + ' ' +
							+ CASE WHEN T7.VetLastName IS NOT NULL THEN COALESCE(T7.VetLastName, '') ELSE '' END)) AS VetID__c	  	-- migrate concatenation of [VetFirstName] +" "+[VetLastName]	-- Yes/Link trefVet.VetID
 							,CAST(T1.vdl_Notes AS NVARCHAR(MAX)) AS  Notes__c		
							,T1.vdl_DonatedBy  AS  Donated_By__c		
							,T1.vdl_PaidTo  AS  Paid_To__c		
							,CAST(T1.vdl_CreatedDate  AS DATE) AS  CreatedDate		
							,CAST(T1.vdl_DatePaid  AS DATE) AS  Date_Paid__c		
							,CASE WHEN T3.vpe_VetEntryID IS NOT NULL THEN '0123D0000004dsPQAQ'  ELSE '0123D0000004dsKQAQ' END  AS  RecordTypeId	-- If linked to [tblVetPE] then 'Physical Exam' else 'Dog Concern'	
							
							--tblVetPE
							,T3.vpe_TC  AS  TC__c
							,T3.vpe_TCR  AS  TCR__c
							,T3.vpe_GA  AS  GA__c
							,T3.vpe_GAAbFind  AS  GAA_Find__c
							,T3.vpe_Eyes  AS  Eyes__c
							,T3.vpe_EyesAbFind  AS  Eyes_Ab_Find__c
							,T3.vpe_Oral  AS  Oral__c
							,T3.vpe_OralAbFind  AS  Oral_Ab_Find__c
							,T3.vpe_MucousMemb  AS  Mucous_Member__c
							,T3.vpe_MucousMembAbFind  AS  Mucous_Member_Ab_Find__c
							,T3.vpe_Ears  AS  Ears__c
							,T3.vpe_EarsAbFind  AS  Ears_Ab_Find__c
							,T3.vpe_Integ  AS  Integ__c
							,T3.vpe_IntegAbFind  AS  Integ_Ab_Find__c
							,T3.vpe_Abdomen  AS  Abdomen__c
							,T3.vpe_AbdomenAbFind  AS  Abdomen_Ab_Find__c
							,T3.vpe_UrogenitAL  AS  Urogenit_AL__c
							,T3.vpe_UrogenitALAbFind  AS  Urogenit_AL_Ab_Find__c
							,T3.vpe_HeartLungs  AS  Heart_Lungs__c
							,T3.vpe_HeartLungsAbFind  AS  Heart_Lungs_Ab_Find__c
							,T3.vpe_LymphNodes  AS  Lymph_Nodes__c
							,T3.vpe_LymphNodesAbFind  AS  Lymph_Nodes_Ab_Find__c
							,T3.vpe_MusSkel  AS  Mus_Skel__c
							,T3.vpe_MusSkelAbFind  AS  Mus_Skel_Ab_Find__c
							,T3.vpe_NeuSys  AS  Neu_Sys__c
							,T3.vpe_NeuSysAbFind  AS  Neu_Sys_Ab_Find__c
							,T3.vpe_Pulse  AS  Pulse__c
							,T3.vpe_Respiration  AS  Respiration__c
							,T3.vpe_Temp  AS  Temp__c
							,T3.vpe_Weight  AS  Weight__c
							,T3.vpe_Height  AS  Height__c
							,T3.vpe_BodyConditionScore  AS  Body_Condition_Score__c
							
							--tblVetLabs
							,CAST(T8.vlb_Labs AS NVARCHAR(MAX)) AS Labs__c

							--tblVetSOAP
							,CAST(T9.vsp_Subject  AS NVARCHAR(MAX))  AS  Subject__c
							,CAST(T9.vsp_Objective  AS NVARCHAR(MAX))  AS  Objective__c
							,CAST(T9.vsp_Assessment  AS NVARCHAR(MAX))  AS  Assessment__c
							,CAST(T9.vsp_Plan  AS NVARCHAR(MAX))  AS  Plan__c

							--tblVBPayments
		 					,T10.Reference_No__c		
							,T10.[Vendor__r:Legacy_Id__c]	-- concatenate any records that are one to many	-- Yes/Link [TblVendor]
						 	,T10.Batch_ID__c		
							,T10.Transaction_Number__c		
							
							--tblOutsideVet
							,'ov-' +CAST(T12.OutsideVetCode AS NVARCHAR(20)) AS [Outside_Veterinary__r:Legacy_Id__c]   --link [trefOutsidevet].[OutsideVetcode]

 							--tblSupportCtrSurvey
							,CAST(CAST(T2.scs_CreatedDate AS DATE) AS NVARCHAR(10)) +': '+T2.scs_SurveyOffered AS Survey_Summary__c
										
							
							--ZREF
							,T1.vdl_VetEntryID AS zrefVetEntryId
							,T10.zrefSeq
							INTO GDB_TC1_migration.DBO.IMP_VET_RECORD
							FROM GDB_TC1_kade.dbo.tblVetEntry AS T1
							LEFT JOIN (SELECT * FROM GDB_TC1_kade.DBO.tblSupportCtrSurvey WHERE scs_FormID=3) AS T2 ON T2.scs_FormRecID=T1.vdl_VetEntryID
							LEFT JOIN GDB_TC1_kade.dbo.tblVetPE AS T3 ON T3.vpe_VetEntryID=T1.vdl_VetEntryID
							LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T4 ON T4.dog_DogID=T1.vdl_DogID
							LEFT JOIN GDB_TC1_kade.dbo.trefVetSource AS T5 ON T5.VetSourceID=T1.vdl_Source
							LEFT JOIN GDB_TC1_kade.dbo.trefVetFacility AS T6 ON T6.VetFacilityID=T1.vdl_Facility
							LEFT JOIN GDB_TC1_kade.dbo.trefVet AS T7 ON T7.VetID = T1.vdl_VetID
							LEFT JOIN GDB_TC1_kade.dbo.tblVetLabs AS T8 ON T8.vlb_VetEntryID=T1.vdl_VetEntryID
							LEFT JOIN GDB_TC1_kade.dbo.tblVetSOAP AS T9 ON T9.vsp_VetEntryID=T1.vdl_VetEntryID
							LEFT JOIN GDB_TC1_migration.dbo.stg_tblVBPayments_1 AS T10 ON T10.vbi_VetEntryID=T1.vdl_VetEntryID
							LEFT JOIN GDB_TC1_kade.dbo.tblOutsideVet AS T11 ON T11.ov_VetEntryID=T1.vdl_VetEntryID
							LEFT JOIN GDB_TC1_kade.dbo.trefOutsidevet AS T12 ON T12.OutsideVetCode=T11.ov_OutsideVetCode
							 
							--TEST WHERE T1.vdl_VetEntryID='934141' OR T1.vdl_VetEntryID='729587'

							 
END --tc1: 1200136
 
  
BEGIN-- AUDIT

			SELECT ROW_NUMBER() OVER(ORDER BY [Dog__r:Legacy_Id__c], Legacy_Id__c) AS zrefSrtId
				   ,*
			INTO GDB_TC1_migration.dbo.IMP_VET_RECORD_srt    
 			FROM GDB_TC1_migration.dbo.IMP_VET_RECORD
	
			SELECT * FROM GDB_TC1_migration.dbo.IMP_VET_RECORD_srt 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_VET_RECORD_srt GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			
  		    --0
		 
		 --create imps.--this is to be able to export direclty from SQL results to preserve double quotes. 
		 SELECT * FROM GDB_TC1_migration.dbo.IMP_VET_RECORD_srt 
		 WHERE zrefSrtId <600001
		 ORDER BY zrefSrtId

		 SELECT * FROM GDB_TC1_migration.dbo.IMP_VET_RECORD_srt 
		 WHERE zrefSrtId >600000
		 ORDER BY zrefSrtId

END 






























