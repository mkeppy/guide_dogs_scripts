USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblGDBInvOrderTrans
			WHERE   SF_Object LIKE '%vend%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_VENDOR_ENTRY

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId

							,'IOT-'+CAST(T1.iot_TransID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Iot-'+[iot_TransID]	
							,'Io-'+ CAST(T2.io_OrderID  AS NVARCHAR(30)) AS  [Vendor_Order__r:Legacy_Id__c]		-- Link [tblGDBInvOrders].[io_OrderID]
							,T1.iot_TransType  AS  Transaction_Type__c		
							,T1.iot_OpenTrans  AS  Open_Transaction__C		
							,'Ii-'+CAST(T3.iid_ItemID AS NVARCHAR(30)) AS  [Inventory__r:Legacy_ID__c] -- Yes/Link trefGDBInvItemsDetail.iid_ItemDetailID
 							,T1.iot_Quantity  AS  Quantity__c		
							,T1.iot_UnitCost  AS  Unit_Cost__C		
							,T4.iuom_Type  AS  UOM__c	-- migrate value from [iuom_type]	-- Ref [trefGDBInvUOM].[iuom_ID]
							,T1.iot_UOMType  AS  UOM_Type__c		
							,CAST(T1.iot_DateProcessed AS DATE) AS  Date_Processed__c		
							,CAST(T1.iot_ExpirationDate AS DATE) AS  Expiration_Date__c		
							,T1.iot_UnitsReceived  AS  Units_Received__c		
							,T1.iot_BackOrderAmt  AS  Back_Order_Amount__c		
							,T1.iot_Cancelled  AS  Cancelled__c		
							,T5.ia_AdjustmentReason  AS  Adjustment_Reason__c		-- migrate value from [ia_AdjustmentReason]	-- Yes/ Link [trefGDBInvAdjustment].[ia_AdjustmentID]
							,T5.ia_AdjustmentType  AS  Adjustment_Type__c		-- migrate value from [ia_AdjustmentType]	-- Yes/ Link [trefGDBInvAdjustment].[ia_AdjustmentID]
							,T5.ia_BuySell  AS  Adjustment_Buy_Sell__c	-- migrate value form [ia_BuySell]			-- Yes/ Link [trefGDBInvAdjustment].[ia_AdjustmentID]
						 	,T6.iidc_Department  AS  Internal_Customer__c		-- migrate value from [iidc_Department]		-- Yes/ Link [trefGDBInvDepartments].[iidc_DeptCode]
					
							,CAST(T1.iot_CreatedDate AS DATE) AS CreatedDate
						 
					--INTO GDB_TC1_migration.DBO.IMP_VENDOR_ENTRY
					FROM GDB_TC1_kade.dbo.tblGDBInvOrderTrans AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblGDBInvOrders AS T2 ON T2.io_OrderID=T1.iot_OrderID
					LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvItemsDetail  AS T3 ON T3.iid_ItemDetailID=T1.iot_ItemDetailID
					LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvUOM AS T4 ON T4.iuom_ID=T1.iot_UOM
					LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvAdjustment AS T5 ON T5.ia_AdjustmentID=T1.iot_AdjustmentReason
					LEFT JOIN GDB_TC1_kade.DBO.trefGDBInvDepartments AS T6 ON T6.iidc_DeptCode=T1.iot_InternalCustomer
					ORDER BY [Inventory__r:Legacy_ID__c]
END 

BEGIN-- AUDIT

		SELECT * FROM GDB_TC1_migration.DBO.IMP_VENDOR_ENTRY

		 
END 