USE GDB_TC1_migration
GO
 
  
BEGIN--Map: ACCOUNT  
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblVolEquipment 
			WHERE   SF_Object LIKE '%account%'
 END 

--RECORDTYPE
/*
	 SELECT * FROM GDB_TC1_migration.dbo.xtr_record_type ORDER BY sobjecttype
			0123D0000004awVQAQ	Kennel	Account
			0123D0000004awWQAQ	Puppy Club	Account
			01241000000cXzXAAU	Household Account	Account
			01241000000cXzYAAU	Organization	Account
*/

BEGIN --DROP IMP_ACCOUNT
	DROP TABLE GDB_TC1_migration.dbo.IMP_ACCOUNT
END 

BEGIN --ACCOUNT 
					

				SELECT	GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,CAST(T1.psn_PersonID  AS NVARCHAR(30)) AS  Legacy_ID__c			-- Reference table [tblPersonGroup] as some Household Accounts have multiple Contacts. Only migrate for Organization and Household Account created from Primary Contact.
	 					,'TRUE' AS Active__c						--include as a filler b/c of 'agency' tbl
						,T1.psn_Prefix  AS  Legacy_Prefix__c	-- mig	arate as is
						,CASE WHEN T1.zrefRecordType ='HHD' THEN CASE WHEN T1.psn_first IS NULL THEN T1.psn_Last  WHEN T1.psn_First  IS NOT NULL THEN T1.psn_First +' '+ T1.psn_Last END 
							  WHEN T1.zrefRecordType ='ORG' THEN T1.psn_Last END AS  [Name]	-- Account Name format [Primary Contact FirstName] + [Secondary Contact First Name] + [Last Name]
						,CASE WHEN T1.zrefRecordType ='HHD' THEN '01241000000cXzXAAU' WHEN T1.zrefRecordType ='ORG' THEN '01241000000cXzYAAU' END AS  RecordTypeID	-- If [psn_First] is null and [psn_Last] not like '%Family%' then Organization else Household.
						,CAST(T1.psn_CreatedDate  AS DATE) AS  CreatedDate	
						,'tblPerson'  AS  Source_Data_Table__c	-- tblPerson
 						,T3.Phone
						,T3.Fax
						,NULL AS Website		--filler from other tble
 			 			,T4.AddressLines AS BillingStreet
						,T4.add_City AS BillingCity
						,T4.add_State AS BillingState
						,T4.add_ZipCode AS BillingPostalCode
						,T4.add_Country AS BillingCountry
							----fillers for other tbl.
							,NULL AS [Type], NULL AS  Grade__c, NULL AS Campus__c, NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
							,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description]	
			 				,NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
			  				,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
							,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c
 				
				INTO GDB_TC1_migration.dbo.IMP_ACCOUNT
		 		
				FROM GDB_TC1_kade.DBO.tblPerson AS T1
				INNER JOIN GDB_TC1_migration.dbo.stg_tblAccount  AS T2 ON T1.psn_PersonID=T2.zrefAccountId
	 			LEFT JOIN [GDB_TC1_migration].dbo.stg_Phone_acct_final AS T3 ON T1.psn_PersonID=T3.pdl_PersonID
				LEFT JOIN (SELECT * FROM GDB_TC1_migration.dbo.stg_Address WHERE npsp__Default_Address__c='TRUE')   AS T4
							ON T1.psn_PersonID = T4.ad_PersonID
				--WHERE t1.psn_PersonID='58'
			 	--72513
			 UNION ALL 
				--tblAgency
				SELECT	GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerID
				  		,'acy-' +CAST(T1.acy_AgencyID AS NVARCHAR(20)) AS  Legacy_ID__c	
	 					,CASE T1.acy_Active WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END AS  Active__c	
						,NULL AS Legacy_Prefix__c   --filler for other tbl.
						,T1.acy_AgencyName  AS  [Name]	
						,'01241000000cXzYAAU' AS RecordTypeId   --org 
						,CAST(T1.acy_CreatedDate  AS DATE) AS  CreatedDate	
						,'tblAgency'  AS  Source_Data_Table__c	-- tblPerson
						,NULL AS Phone			--filler from other tbl.
						,NULL AS Fax			--filler from other tbl.
						,NULL AS Website		--filler from other tble
						,CASE WHEN T1.acy_Address2 IS NOT NULL THEN T1.acy_Address1 + CHAR(10) + T1.acy_Address2  WHEN T1.acy_Address2 IS NULL THEN T1.acy_Address1 END AS BillingStreet
				 		,T1.acy_City  AS  BillingCity	
						,CASE WHEN T1.acy_State IS NULL THEN T1.acy_CountryRegion ELSE T1.acy_State END AS  BillingState	-- If [acy_State] is null migrate [acy_CountryRegion] to State/Province
						,CASE WHEN T1.acy_ZipCode IS NULL THEN T1.acy_CountryCode ELSE T1.acy_ZipCode END  AS  BillingPostalCode	-- If [acy_ZipCode] is null migrate [acy_CountryCode] to Zip/Postal Code
			 			,T1.acy_Country  AS  BillingCountry	
				 
 						,T2.agt_AgencyType AS  [Type]	-- Migrate value in  [trefAgencyType].[agt_AgencyType]
						,NULL AS  Grade__c
						,NULL AS Campus__c   --filler from other table> 
						,T1.acy_ServicesProvided  AS  Services_Provided__c	
						,T1.acy_Funding  AS  Funding__c	
						,T1.acy_AgesServed  AS  Ages_Served__c	
						,T1.acy_ClientsServedAnnually  AS  Clients_Served_Annually__c	
						,CASE T1.acy_GDBSupport WHEN  '5' THEN 'Excellent' WHEN '4' THEN 'Very Good' WHEN '3' THEN 'Good' WHEN '2' THEN 'Fair' WHEN '1' THEN 'Poor'	 END AS  GDB_Support__c	--  5 - Excellent; 4 - Very Good; 3 - Good; 2 - Fair; 1 - Poor
						,T3.agm_Description  AS  Mail_Category__c	-- Migrate values in [trefAgencyMailCodes].[agm_Description]
					 	,CAST(T1.acy_Comments as varchar(4000)) AS  [Description]	
			 			----filler for other table
							,NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c
							,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
							,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c
 						FROM GDB_TC1_kade.dbo.tblAgency AS T1
						LEFT JOIN GDB_TC1_kade.dbo.[trefAgencyType] AS T2 ON T1.acy_AgencyType=T2.agt_ID
						LEFT JOIN GDB_TC1_kade.dbo.trefAgencyMailCodes AS T3 ON T1.acy_MailCategory=T3.agm_Code
						
				UNION
				--trefPuppyClub/ tblPuppyClubDetail
				SELECT  
						GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerID
						,'pcd-'+CAST(T1.pcd_ClubID  AS NVARCHAR(30)) AS  Legacy_ID__C	-- concatenate 'Pcd-'+[pcd_id]
 						,'TRUE' AS Active__c		--filler for other table
						,NULL AS Legacy_Prefix__c	--filler for other table
						,T2.clb_ClubName  AS  [Name]	-- Migrate value from [trefPuppyClub].[Description]
						,'0123D0000004awWQAQ' AS  RecordTypeId	-- Puppy Club
						,MIN(CAST(T1.pcd_CreatedDate AS DATE))  AS  CreatedDate	
						,'tblPuppyClubDetail'  AS  Source_Data_Table__c	-- Puppy Club Detail
						--filler from other tble
							,NULL AS Phone, NULL AS Fax, NULL AS Website		  
							,NULL AS BillingStreet, NULL AS BillingCity	,NULL AS BillingState, NULL AS BillingPostalCode, NULL AS BillingCountry	
				  			,NULL AS [Type],  NULL AS  Grade__c, NULL AS Campus__c, NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
							,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description]	
			 				,NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
					 		,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
							,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c
 			 		
						FROM GDB_TC1_kade.dbo.trefPuppyClub AS T2
						INNER JOIN GDB_TC1_kade.dbo.tblPuppyClubDetail AS T1 ON T1.pcd_ClubID=T2.clb_ClubID
						GROUP BY T1.pcd_ClubID, T2.clb_ClubName 
						--258
				UNION 
				--trefPuppyClub/ tblPuppyClubDetail
					SELECT  
						GDB_TC1_migration.[dbo].[fnc_OwnerId]()  AS OwnerID
						,'ken-'+CAST(T1.KennelID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'ken-'+[KennelId]
	 					,CASE T1.Active WHEN 'Yes' then 'TRUE' WHEN 'No' THEN 'FALSE' ELSE 'TRUE' END AS Active__c		 
						,NULL AS Legacy_Prefix__c	--filler for other table
						,T1.KennelName  AS  [Name]	
						,'0123D0000004awVQAQ' AS RecordTypeID
						,NULL AS CreatedDate
						,'trefKennels' AS  Source_Data_Table__c	
						--filler from other tble
							,NULL AS Phone, NULL AS Fax, NULL AS Website		
							,NULL AS BillingStreet, NULL AS BillingCity	,NULL AS BillingState, NULL AS BillingPostalCode, NULL AS BillingCountry	
				  			,NULL AS [Type], NULL AS  Grade__c, NULL AS Campus__c, NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
							,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description]	
						,T1.LeftRuns  AS  Left_Runs__c	
						,T1.RightRuns  AS  Right_Runs__c	
						,T1.[Location]  AS  Location__c	
						--fillers
						,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
						,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c
 			 		
					FROM GDB_TC1_kade.dbo.trefKennels AS T1
				
				UNION 
			--trefOutsideVet
				SELECT
						 GDB_TC1_migration.[dbo].[fnc_OwnerId]()  AS OwnerID
						,'ov-' +CAST(T1.OutsideVetCode AS NVARCHAR(20)) AS  Legacy_Id__c
			 			,CASE T1.OutsideVetActive WHEN '1' THEN 'TRUE' WHEN '0' THEN 'FALSE' ELSE 'TRUE' END AS  Active__c
						,NULL AS Legacy_Prefix__c	--filler for other table
						,T1.OutsideVetClinic AS [Name]
						,'01241000000cXzYAAU'	AS RecordTypeID  --Organization
						,CAST(T1.OutsideVetCreatedDate AS DATE) AS CreatedDate
						,'trefOutsideVet' AS  Source_Data_Table__c	
				 		,T1.OutsideVetPhone AS Phone 
						,NULL AS Fax	  	--filler from other tble
						,NULL AS Website		--filler from other tble
 						,T1.OutsideVetStreet  AS BillingStreet   
 						,T1.OutsideVetCity AS BillingCity
     					,T1.OutsideVetState AS BillingState    
 						,CAST(T1.OutsideVetZipCode AS NVARCHAR(30)) AS BillingPostalCode   
						,T1.OutsideVetCountry AS BillingCountry  
						--fillers 
						,NULL AS [Type], NULL AS  Grade__c, NULL AS Campus__c, NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
						,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description], NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
			
						,T1.OutsideVetEmail  AS  Email__c
						,CAST(T1.OutsideVetDateSolicited AS DATE) AS  Vet_Date_Solicited__c
						,CAST(T1.OutsideVetDateJoined  AS DATE) AS  Vet_Date_Joined__c
						,CAST(T1.OutsideVetDateDeclined  AS DATE) AS  Vet_Date_Declined__c
						,T1.OutsideVetPercentCommitment  AS  Vet_Percent_Commitment__c
						,T1.OutsideVetFreeServices  AS  Vet_Free_Services__c
						,T1.OutsideVetDonation  AS  Vet_Donation__c
						,T1.OutsideVetSolicited  AS  Vet_Solicited__c
						,T1.OutsideVetJoined  AS  Vet_Joined__c
						,T2.ovt_Text  AS  Type_of_Vet__c
						,T1.OutsideVetRefer  AS  Vet_Refer__c
			 
					FROM GDB_TC1_kade.dbo.trefOutsideVet AS T1
					LEFT JOIN GDB_TC1_kade.dbo.[trefOutsideVetType] AS T2 ON T1.OutsideVetTypeofVet=T2.ovt_TypeID
			UNION
			
			--tblGDBVendor
				SELECT
						 GDB_TC1_migration.[dbo].[fnc_OwnerId]()  AS OwnerID
						,'iv-' +CAST(T1.iv_VendorID AS NVARCHAR(20)) AS   Legacy_ID__c
				 		,CASE T1.iv_Active WHEN 'Yes' THEN 'TRUE' WHEN 'No' THEN 'FALSE' ELSE 'TRUE' END AS  Active__c
						,NULL AS Legacy_Prefix__c	--filler for other table
						,T1.iv_VendorName  AS  [Name]
						,'01241000000cXzYAAU'  AS  RecordTypeId	-- Organization	
						,CAST(T1.iv_CreatedDate  AS DATE) AS  CreatedDate	
						,'tblGDBVendor' AS  Source_Data_Table__c	-- tblGDBVendor
						,NULL AS Phone		--filler from other tble
						,NULL AS Fax	  	--filler from other tble
						,T1.iv_WebSite  AS  Website	
						,T1.iv_Address  AS  BillingStreet	
						,T1.iv_City  AS  BillingCity	
						,T1.iv_State  AS  BillingState	
						,CAST(T1.iv_ZipCode AS varchar(30))  AS  BillingPostalCode	
						,T1.iv_Country  AS  BillingCountry	
						,'Vendor'  AS  [Type]	-- Vendor
						,NULL AS  Grade__c
						,T2.[Description] AS  Campus__c	-- migrate value from [Description]
						--fillers 
						,NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
						,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description], NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
						,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
						,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c

  					FROM GDB_TC1_kade.dbo.tblGDBVendor AS T1
					LEFT JOIN GDB_TC1_kade.DBO.trefFacility AS T2 ON T1.iv_Campus = T2.FacilityID
			UNION
			
			--tblContacts
					SELECT GDB_TC1_migration.[dbo].[fnc_OwnerId]()  AS OwnerID
						,'Con-'+CAST(T1.con_ContactID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Con-'+[con_ContactID]
						,'TRUE'   AS  Active__c
						,T1.con_NamePrefix  AS  Legacy_Prefix__c	-- migrate as is
						,T1.zrefAccountName AS [Name]
						,CASE WHEN T1.zrefRecordType ='HHD' THEN '01241000000cXzXAAU' WHEN T1.zrefRecordType ='ORG' THEN '01241000000cXzYAAU' END AS  RecordTypeID	-- If [con_OrgName] is not null then RecordType=Organization, else Household
						,CAST(T1.con_CreatedDate  AS DATE) AS  CreatedDate	
						,'tblContacts'  AS  Source_Data_Table__c	-- tblContacts	
						,[dbo].[fnc_phone_fmt](T1.con_PhoneDaytime) AS  Phone	-- Migrate to Account for Organization records.
						,NULL AS Fax
						,NULL AS Website

						,CASE WHEN T1.con_Address2 IS NOT NULL THEN T1.con_Address1+CHAR(10)+T1.con_Address2 ELSE T1.con_Address1 END AS BillingStreet 	-- Concatenate [con_Address1] and [con_Address2] into Account.BillingStreet with a line break in between
				 		,T1.con_City  AS  BillingCity	
						,CASE WHEN T1.con_State IS NULL THEN T1.con_CountryRegion ELSE T1.con_State END  AS  BillingState	-- If [con_State] is null migrate [con_CountryRegion] to State/Province
						,CASE WHEN T1.con_ZipCode IS NULL THEN T1.con_CountryCode ELSE T1.con_ZipCode END AS  BillingPostalCode	-- If [con_Zipcode] is null migrate [con_CountryCode] to Zip/Postal Code
						,T1.con_Country  AS  BillingCountry	
		 				,CASE WHEN T1.zrefRecordType='ORG' THEN T3.[Description] ELSE NULL END  AS  [Type]	-- Migrate values from [Description] if [con_OrgName] is not blank migrate to Account 
						
						--fillers 
						,NULL AS  Grade__c, NULL AS Campus__c ,NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
						,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description], NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
						,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
						,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c	
						 
					FROM GDB_TC1_kade.dbo.tblContacts AS T1
					INNER JOIN GDB_TC1_migration.dbo.stg_tblContact_Account  AS T2 ON T1.con_ContactID=T2.zrefAccountId
					LEFT JOIN GDB_TC1_kade.dbo.trefContactTypes AS T3 ON T1.con_ContactType=T3.ContactType
			  
			UNION 
			--tblVolCalOrganization
						SELECT GDB_TC1_migration.[dbo].[fnc_OwnerId]()  AS OwnerID
						,'Org-'+CAST(T1.org_OrganizationID AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'Org-'+[org_OrganizationID]
						,'TRUE' AS Active__c
						,NULL AS Legacy_Prefix__c
						,T1.org_Organization  AS  [Name]
						,'01241000000cXzYAAU' AS  RecordTypeID -- Organization
						,CAST(T1.org_Date AS DATE) AS  CreatedDate	
						,'tblVolCalOrganization' AS  Source_Data_Table__c	-- tblVolCalOrganization	
						,NULL AS Phone	 
						,NULL AS Fax
						,NULL AS Website
						,T1.org_Address  AS  BillingStreet	
						,T1.org_City  AS  BillingCity	
						,T1.org_State  AS  BillingState	
						,CAST(T1.org_Zip  AS NVARCHAR(30)) AS  BillingPostalCode
						,NULL AS BillingCountry
						,NULL AS [Type]
						,T1.org_Grade  AS  Grade__c	
							
					 	--fillers 
						,NULL AS Campus__c ,NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
						,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description], NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
						,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
						,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c	
 					FROM GDB_TC1_kade.dbo.tblVolCalOrganization AS T1
			UNION 
					--tblStaff
						SELECT GDB_TC1_migration.[dbo].[fnc_OwnerId]()  AS OwnerID
						,'GDB'  AS  Legacy_ID__c	 
						,'TRUE' AS Active__c
						,NULL AS Legacy_Prefix__c
						,'Guide Dogs for the Blind'  AS  [Name]
						,'01241000000cXzYAAU' AS  RecordTypeID -- Organization
						,NULL AS  CreatedDate	
						,'tblStaff' AS  Source_Data_Table__c	-- tblStaff	
						,'(415) 499-4000' AS Phone	 
						,'(415) 499-4035' AS Fax
						,'www.guidedogs.com' AS Website
						,'350 Los Ranchitos Road' AS  BillingStreet	
						,'San Rafael'  AS  BillingCity	
						,'CA'  AS  BillingState	
						,'94903'  AS  BillingPostalCode
						,'USA' AS BillingCountry
							
					 	--fillers 
						,NULL AS [Type],NULL  AS  Grade__c, NULL AS Campus__c ,NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
						,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description], NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
						,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
						,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c	
  			UNION 
					--tblVolCalDocents
						SELECT GDB_TC1_migration.[dbo].[fnc_OwnerId]()  AS OwnerID
						,'RE-'+CAST(T1.ConID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'RE-' [conid]
						,'TRUE' AS Active__c
						,NULL AS Legacy_Prefix__c
						,T1.FullName  AS  [Name]	
						,'01241000000cXzXAAU'  AS  RecordTypeID	-- Household
						,NULL  AS  CreatedDate	
						,'tblVolCalDocents' AS  Source_Data_Table__c	-- tblStaff	
						,NULL AS Phone	 
						,NULL AS Fax
						,NULL AS Website
						,NULL AS  BillingStreet	
						,NULL AS  BillingCity	
						,NULL AS  BillingState	
						,NULL AS  BillingPostalCode
						,NULL AS BillingCountry
						--fillers 
						,NULL AS [Type],NULL  AS  Grade__c, NULL AS Campus__c ,NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
						,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description], NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
						,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
						,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c
						FROM GDB_TC1_kade.dbo.tblVolCalDocents AS T1

			UNION 
					--tblVolEquipment

					SELECT  GDB_TC1_migration.[dbo].[fnc_OwnerId]()  AS OwnerID
 						,'vps-'+CAST(T1.vps_PathScarfID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Vps-'+[Vps_PathScarfID]
						,'TRUE' AS Active__c
						,NULL AS Legacy_Prefix__c
						,T1.vps_OrgName  AS  [Name]	-- Create unique Account record per OrgName and OrgAddress.
						,'01241000000cXzYAAU' AS  RecordTypeID -- Organization
						,NULL  AS  CreatedDate	
						,'tblVolEquipment'  AS  Source_Data_Table__c	-- tblVolEquipment
						,T1.vps_OrgPhone  AS  Phone	
						,NULL AS Fax
						,NULL AS Website
						,T1.vps_OrgAddress  AS  BillingStreet	
						,T1.vps_OrgCity  AS  BillingCity	
						,T1.vps_OrgState  AS  BillingState	
						,CAST(T1.vps_OrgZip AS NVARCHAR(30)) AS  BillingPostalCode	
						,NULL AS BillingCountry
						--fillers 
						,NULL AS [Type],NULL  AS  Grade__c, NULL AS Campus__c ,NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
						,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description], NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
						,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
						,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c
		 			FROM GDB_TC1_migration.DBO.stg_tblVolEquipment_1 AS T1
					 
			UNION 
					--trefDogSourceNew

					SELECT  GDB_TC1_migration.[dbo].[fnc_OwnerId]()  AS OwnerID
 						,'BRS-'+CAST(T1.brs_SourceID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Vps-'+[Vps_PathScarfID]
						,'TRUE' AS Active__c
						,NULL AS Legacy_Prefix__c
						,T1.brs_Kennel  AS  [Name]	-- Create unique Account record per OrgName and OrgAddress.
						,'01241000000cXzYAAU' AS  RecordTypeID -- Organization
						,NULL  AS  CreatedDate	
						,'trefDogSourceNew'  AS  Source_Data_Table__c	-- tblVolEquipment
						,T1.brs_Phone  AS  Phone	
						,T1.brs_Fax AS Fax
						,T1.brs_Web AS Website
						,T1.brs_Address  AS  BillingStreet	
						,T1.brs_City AS  BillingCity	
						,T1.brs_State AS  BillingState	
						,T1.brs_Zip AS  BillingPostalCode	
						,T1.brs_Country AS BillingCountry
						--fillers 
						,NULL AS [Type],NULL  AS  Grade__c, NULL AS Campus__c ,NULL AS Services_Provided__c, NULL AS Funding__c, NULL AS Ages_Served__c, NULL AS Clients_Served_Annually__c	
						,NULL AS GDB_Support__c	, NULL AS Mail_Category__c,NULL AS [Description], NULL AS Left_Runs__c, NULL AS Right_Runs__c, NULL AS  Location__c	
						,NULL AS Email__c ,NULL AS Vet_Date_Solicited__c ,NULL AS Vet_Date_Joined__c ,NULL AS Vet_Date_Declined__c ,NULL AS Vet_Percent_Commitment__c
						,NULL AS Vet_Free_Services__c, NULL AS Vet_Donation__c,NULL AS Vet_Solicited__c ,NULL AS Vet_Joined__c ,NULL AS Type_of_Vet__c, NULL AS Vet_Refer__c
		 			FROM GDB_TC1_kade.DBO.trefDogSourceNew AS T1
					 
END--E.O.ACCOUNT TC1: 112,825
	
BEGIN-- audits/updates

		--UPDATE '&' to 'and' 
 			EXEC GDB_TC1_migration.dbo.sp_FindStringInTable '% & %', 'DBO', 'IMP_ACCOUNT'

			UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET [Name]=REPLACE([Name],'&','and') where [Name] like '% & %'
 
				 SELECT * FROM GDB_TC1_migration.dbo.IMP_ACCOUNT  WHERE Legacy_ID__c='18391'
					
	 
		--PHONE CLEAN UP

				SELECT DISTINCT Phone, LEN(Phone) AS l 
				FROM GDB_TC1_migration.dbo.IMP_ACCOUNT
				ORDER BY l desc  

				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Phone=REPLACE(Phone,'/','') where Phone IS NOT null
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Phone=REPLACE(Phone,'\','') where Phone IS NOT null
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Phone=REPLACE(Phone,'(','') where Phone IS NOT null
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Phone=REPLACE(Phone,')','') where Phone IS NOT NULL
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Phone=REPLACE(Phone,'-','') where Phone IS NOT null
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Phone=REPLACE(Phone,' ','') where Phone IS NOT NULL
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Phone=REPLACE(Phone,'.','') where Phone IS NOT null

				SELECT DISTINCT Phone, LEN(Phone) AS l 
				FROM GDB_TC1_migration.dbo.IMP_ACCOUNT
				WHERE Phone LIKE '%[0-9]%'   --'%[^a-zA-Z0-9]%'
				ORDER BY l
		
				--clean up only valid 10-digit numbers. 
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Phone=GDB_TC1_migration.dbo.fnc_phone_fmt(Phone)
				WHERE Phone LIKE '%[0-9]%' AND LEN(Phone) =10

			--FAX CLEANUP
				SELECT DISTINCT Fax, LEN(Fax) AS l, Source_Data_Table__c
				FROM GDB_TC1_migration.dbo.IMP_ACCOUNT
				ORDER BY l desc 
				
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Fax=REPLACE(Fax,'/','') where Fax IS NOT null
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Fax=REPLACE(Fax,'\','') where Fax IS NOT null
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Fax=REPLACE(Fax,'(','') where Fax IS NOT null
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Fax=REPLACE(Fax,')','') where Fax IS NOT NULL
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Fax=REPLACE(Fax,'-','') where Fax IS NOT null
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Fax=REPLACE(Fax,' ','') where Fax IS NOT NULL
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Fax=REPLACE(Fax,'.','') where Fax IS NOT null

				SELECT DISTINCT Fax, LEN(Fax) AS l 
				FROM GDB_TC1_migration.dbo.IMP_ACCOUNT
				WHERE Fax LIKE '%[0-9]%'   --'%[^a-zA-Z0-9]%'
				ORDER BY l
		
				--clean up only valid 10-digit numbers. 
				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT SET Fax=GDB_TC1_migration.dbo.fnc_phone_fmt(Fax)
				WHERE Fax LIKE '%[0-9]%' AND LEN(Fax) =10

		
		--COUNTRY CLEANUP
				SELECT BillingCountry, COUNT(*) C
				FROM GDB_TC1_migration.dbo.IMP_ACCOUNT
				GROUP BY BillingCountry
				ORDER BY c DESC

				UPDATE GDB_TC1_migration.dbo.IMP_ACCOUNT
				SET BillingCountry='United States' 
				WHERE BillingCountry='USA'

				SELECT BillingState, COUNT(*) C
				FROM GDB_TC1_migration.dbo.IMP_ACCOUNT
				WHERE BillingCountry IS null
				GROUP BY BillingState
				ORDER BY c DESC
                
				 UPDATE  GDB_TC1_migration.dbo.IMP_ACCOUNT 
				 SET BillingCountry = 'United States'  
				 WHERE BillingCountry IS NULL 
				 AND ([BillingState]='AL' OR [BillingState]='AK' OR [BillingState]='AZ' OR [BillingState]='AR' OR [BillingState]='CA' OR [BillingState]='CO' OR [BillingState]='CT' OR [BillingState]='DE' OR [BillingState]='DC' OR [BillingState]='FL'
				   OR [BillingState]='GA' OR [BillingState]='HI' OR [BillingState]='ID' OR [BillingState]='IL' OR [BillingState]='IN' OR [BillingState]='IA' OR [BillingState]='KS' OR [BillingState]='KY' OR [BillingState]='LA' OR [BillingState]='ME' 
				   OR [BillingState]='MD' OR [BillingState]='MA' OR [BillingState]='MI' OR [BillingState]='MN' OR [BillingState]='MS' OR [BillingState]='MO' OR [BillingState]='MT' OR [BillingState]='NE' OR [BillingState]='NV' OR [BillingState]='NH' 
				   OR [BillingState]='NJ' OR [BillingState]='NM' OR [BillingState]='NY' OR [BillingState]='NC' OR [BillingState]='ND' OR [BillingState]='OH' OR [BillingState]='OK' OR [BillingState]='OR' OR [BillingState]='PA' OR [BillingState]='RI' 
				   OR [BillingState]='SC' OR [BillingState]='SD' OR [BillingState]='TN' OR [BillingState]='TX' OR [BillingState]='UT' OR [BillingState]='VT' OR [BillingState]='VA' OR [BillingState]='WA' OR [BillingState]='WV' OR [BillingState]='WI' OR [BillingState]='WY')

 
		--DUPLICATE
			SELECT * FROM GDB_TC1_migration.dbo.IMP_ACCOUNT 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_ACCOUNT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c
 
		--CHECK
			SELECT * FROM GDB_TC1_migration.dbo.IMP_ACCOUNT ORDER BY Source_Data_Table__c

			SELECT Source_Data_Table__c, COUNT(*) C 
			FROM GDB_TC1_migration.dbo.IMP_ACCOUNT 
			GROUP BY Source_Data_Table__c
			ORDER BY Source_Data_Table__c

			SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_ACCOUNT  --112825

END


			 