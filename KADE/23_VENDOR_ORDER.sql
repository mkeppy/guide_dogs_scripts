USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblGDBInvOrders
			WHERE   SF_Object LIKE '%ORDER%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 



BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_VENDOR_ORDER

END 

BEGIN -- CREATE IMP 

					SELECT   
						CASE WHEN X1.ID IS NULL THEN GDB_TC1_migration.[dbo].[fnc_OwnerId]()  ELSE X1.ID END  AS  OwnerID		-- Link [tblStaff].[FileNum]
 						,'Io-'+ CAST(T1.io_OrderID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- concatenate 'Io-'+[io_OrderID]	
						,CAST(T1.io_OrderDate AS DATE) AS  Order_Date_c		
						,'iv-' +CAST(T2.iv_VendorID AS NVARCHAR(20))   AS  [Vendor__r:Legacy_Id__c]		-- Ref [tblGDBVendor].[iv_VendorID]
						,T3.[Description] AS  Campus__c	-- migrate value from [Description]	-- Ref [trefFacility].[FacilityID]
						,T4.iidc_Department  AS  Department__c	-- migrate value from [iidc_Department]	-- Ref [trefGDBInvDepartments].[iidc_DeptCode]
						,T1.io_InvoiceNumber  AS  Invoice_Number__c		
						,T1.io_OrderProcessed  AS  Order_Processed__c		
						,T1.io_OrderClosed  AS  Order_Closed__c		
						,T5.is_ShippingMethod  AS  Shipping_Method__c	-- migrate value from [is_ShippingMethod]	-- Ref [trefGDBInvShipping].[is_ShippingID]
						,T1.io_RoutingNumber  AS  Routing_Number__c		
						,T1.io_Notes  AS  Notes__c		
						,T1.io_OrderType  AS  Order_Type__c	
				  
		 		    INTO GDB_TC1_migration.dbo.IMP_VENDOR_ORDER
 					FROM GDB_TC1_kade.dbo.tblGDBInvOrders AS T1
					LEFT JOIN GDB_TC1_migration.DBO.XTR_USERS AS X1 ON X1.ADP__C=T1.io_EmployeeID 
					LEFT JOIN GDB_TC1_kade.DBO.tblGDBVendor AS T2 ON T2.iv_VendorID=T1.io_VendorID
					LEFT JOIN GDB_TC1_kade.DBO.trefFacility AS T3 ON T3.FacilityID=T1.io_Campus
					LEFT JOIN GDB_TC1_kade.DBO.trefGDBInvDepartments AS T4 ON T4.iidc_DeptCode=T1.io_Department
					LEFT JOIN GDB_TC1_kade.DBO.trefGDBInvShipping AS T5 ON T5.is_ShippingID=T1.io_ShippingMethod



END  --TC1: 4532

BEGIN-- AUDIT
		
			SELECT * FROM GDB_TC1_migration.dbo.IMP_VENDOR_ORDER 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_VENDOR_ORDER GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			


			SELECT COUNT(*) FROM GDB_TC1_kade.DBO.tblGDBInvOrders
			 



END 






























