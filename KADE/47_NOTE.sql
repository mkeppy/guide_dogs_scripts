USE GDB_TC1_migration
GO

BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblDogComments 
			WHERE   SF_Object LIKE '%note%'
			
				SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%contact%'
END

BEGIN-- drop notes

	DROP TABLE GDB_TC1_migration.dbo.IMP_NOTES

END

BEGIN --CREATE NOTE
	
			 SELECT  
				ParentId =  X1.ID  --T1.an_DogID  AS  ParentId	 --Link [tblDog].[dog_DogID]
				,Title = 'Dog Acc Notes: '+  CAST(CAST(T1.an_Date AS  DATE) AS NVARCHAR(30))  + '.  Entered by: '+ T1.an_EnterBy
				,Body = CAST(T1.an_Note	 AS NVARCHAR(MAX))

				,T1.an_AcctNoteID AS zrefLegacyId
				,'tblDogAcctNotes' zrefSrc
			INTO GDB_TC1_migration.dbo.TBL_NOTES
			FROM GDB_TC1_kade.dbo.tblDogAcctNotes AS T1
			INNER JOIN GDB_TC1_migration.dbo.XTR_DOG AS X1 ON X1.Legacy_Id__c=T1.an_DogID

		UNION 
		   SELECT  
				ParentId =  X1.ID  --T1.an_DogID  AS  ParentId	 --Link [tblDog].[dog_DogID]
				,Title = 'Kennel Notes: '+  CAST(CAST(T1.dkn_CreatedDate AS  DATE) AS NVARCHAR(30))   
				,Body = CAST(T1.dkn_Notes	 AS NVARCHAR(MAX))

				,T1.dkn_DogKennelNotesID AS zrefLegacyId
				,'tblDogKennelNotes' zrefSrc
				 
			FROM GDB_TC1_kade.dbo.tblDogKennelNotes AS T1
			INNER JOIN GDB_TC1_migration.dbo.XTR_DOG AS X1 ON X1.Legacy_Id__c=T1.dkn_DogID
			WHERE LEN(CAST(T1.dkn_Notes	 AS NVARCHAR(MAX))) >0
	
		UNION 
			 SELECT  
				ParentId =  X1.ID  --T1.an_DogID  AS  ParentId	 --Link [tblDog].[dog_DogID]
				,Title= LEFT(('Breeding Notes :' + CAST(CAST(T1.dbn_Date  AS DATE) AS NVARCHAR(30)) +'. '+T1.dbn_Notes),80)	-- Concatenate LEFT([dbn_Notes],55) +[dbn_Date]	
				,Body = 'Notes by: ' + T1.dbn_ByWho  +CHAR(10) + 'Notes: ' +T1.dbn_Notes  -- Concatenate [dbn_byWho] +line break+[dbn_Notes]	
				 
				,t1.dbn_NotesID AS zrefLegacyId
				,'tblDogBreedingNotes' zrefSrc
			FROM GDB_TC1_kade.dbo.tblDogBreedingNotes AS T1
			INNER JOIN GDB_TC1_migration.dbo.XTR_DOG AS X1 ON X1.Legacy_Id__c=T1.dbn_DogID
		
 
		UNION 
			SELECT  
				ParentId =  X1.ID  --T1.an_DogID  AS  ParentId	 --Link [tblDog].[dog_DogID]
				,Title = T2.New_Value  +': '+CAST(CAST(T1.dc_Date AS DATE) AS NVARCHAR(30))  -- concatenate [chart_DogComments].[New Value] +" "+[dc_Date]	
				,Body = CASE WHEN T1.dc_By IS NULL AND t1.dc_Comments   IS NOT NULL THEN 'Comments by: n/a'  +CHAR(10)+ t1.dc_Comments  
						   WHEN T1.dc_By IS NOT NULL  AND t1.dc_Comments   IS NOT NULL THEN 'Comments by:'  +T1.dc_By + CHAR(10)+ t1.dc_Comments  -- concatenate [dc_By] +line break+[dc_Comments]	
						END 
			
				,t1.dc_CommentID AS zrefLegacyId
			 	,'tblDogComments' zrefSrc
			 	FROM GDB_TC1_kade.dbo.tblDogComments AS T1 
				INNER JOIN GDB_TC1_migration.dbo.XTR_DOG AS X1 ON X1.Legacy_Id__c=T1.dc_DogID
				LEFT JOIN GDB_TC1_maps.dbo.CHART_DogComments_Category  AS T2 ON T2.dc_Category =T1.dc_Category   
				WHERE  T2.New_Value  IS NOT NULL 
	  
END 



BEGIN---create file for NOTES in LIGHTING EXPERIENCE.
--HELP: https://help.salesforce.com/articleView?id=000230867&language=en_US&type=1 
		
--1. create the XLSX file to create the single TXT files.
		--ID: unique ID for the Note content record file that will be created with the excel VB code. (e.g. tblDogAcctNotes_6_a183D0000004YItQAM)
			--The ID has to be on the first column of the XLSX file to be used to create the .TXT single files. 
		--BODY: content to be added in the new txt file.
		    --The Body has to be the 2nd column in the XLSX file. 
		--PARENTID: Id of the record that the Conten Document will be linked on the SECOND upload.
		--Title: field to be used on the first upload
		--CONTENT: field t obe used in the first upload. This is the path and name of the txt file. (eg. D:\DATA_MIGRATIONS\GUIDE_DOGS\Import Files\Notes\tblDogAcctNotes_6_a183D0000004YItQAM.txt)
		--SHARETYPE= field to be used on the second upload. required with "V" value.
		--VISIBILITY=field to be used on the second upload. required but left null to assign default value. 

	--Replace special characters to comply with Salesforce notes upload criteria
	/*	Find Special Character	Replace with...
		&						&amp;
		<						&lt;
		>						&gt;
		"						&quot;
		'						&#39;
	*/

			SELECT * FROM GDB_TC1_migration.dbo.TBL_NOTES WHERE Body LIKE '%&%'
			SELECT * FROM GDB_TC1_migration.dbo.TBL_NOTES WHERE Body LIKE '%<%'
			SELECT * FROM GDB_TC1_migration.dbo.TBL_NOTES WHERE Body LIKE '%>%'
			SELECT * FROM GDB_TC1_migration.dbo.TBL_NOTES WHERE Body LIKE '%"%'
			SELECT * FROM GDB_TC1_migration.dbo.TBL_NOTES WHERE Body LIKE '%''%'

			UPDATE GDB_TC1_migration.dbo.TBL_NOTES SET Body=REPLACE(Body,'&','&amp;') where Body LIKE '%&%'
			UPDATE GDB_TC1_migration.dbo.TBL_NOTES SET Body=REPLACE(Body,'<','&lt;') where Body LIKE '%<%'
			UPDATE GDB_TC1_migration.dbo.TBL_NOTES SET Body=REPLACE(Body,'>','&gt;') where Body LIKE '%>%'
			UPDATE GDB_TC1_migration.dbo.TBL_NOTES SET Body=REPLACE(Body,'"','&quot;') where Body LIKE '%"%'
			UPDATE GDB_TC1_migration.dbo.TBL_NOTES SET Body=REPLACE(Body,'''','&#39;') where Body LIKE '%''%'

			SELECT * FROM GDB_TC1_migration.dbo.TBL_NOTES WHERE zrefLegacyId='34694' OR zrefLegacyId='3827'


	--Run the select statement and do "Open in Excel" and save as "STG_NOTES_ContentNote.xlsx"
			DROP TABLE GDB_TC1_migration.dbo.IMP_NOTES

			SELECT	ID = zrefSrc+'_'+CAST(zrefLegacyId AS VARCHAR(20)), 
					Body, 
					ParentId, 
					Title, 
				    CONTENT = 'D:\DATA_MIGRATIONS\GUIDE_DOGS\Import_Files\Notes\'+zrefSrc+'_'+CAST(zrefLegacyId AS VARCHAR(20))+'.txt', 
				    SHARETYPE = 'V', 
 					VISIBILITY = NULL ,
					zrefLegacyId, 
					zrefSrc,
					ROW_NUMBER() OVER(ORDER BY ParentId, zrefSrc, zrefLegacyId) AS zrefSeq
	 		INTO GDB_TC1_migration.dbo.STG_NOTES_ContentNote   --STG STaGe
			FROM GDB_TC1_migration.dbo.TBL_NOTES 
		 
		 --check dupes
			SELECT * FROM GDB_TC1_migration.dbo.STG_NOTES_ContentNote
 			WHERE ID IN (SELECT ID FROM GDB_TC1_migration.dbo.STG_NOTES_ContentNote GROUP BY ID HAVING COUNT(*)>1)
			ORDER BY ID

		--RUN SELECT, select the results and "Open in Excel".  Save Excell as STG_NOTES_ContentNote.xlsx"
			SELECT * FROM GDB_TC1_migration.dbo.STG_NOTES_ContentNote
 			ORDER BY zrefSeq 



--2: In the Excel file STG_NOTES_ContentNote.xlsx.
			/*
			Create Text Files with Code
			Right click on worksheet tab and click on �View  Code�.  Paste the code below:
			
			Sub Export_Files()
				Dim sExportFolder, sFN
				Dim rArticleName As Range
				Dim rDisclaimer As Range
				Dim oSh As Worksheet
				Dim oFS As Object
				Dim oTxt As Object

				'sExportFolder = path to the folder you want to export to
				'oSh = The sheet where your data is stored
				sExportFolder = "D:\DATA_MIGRATIONS\GUIDE_DOGS\Import_Files\Notes"    '--UPDATE THE PATH TO THE FOLDER WHERE FILES ARE TO BE SAVED
				Set oSh = Sheet1

				Set oFS = CreateObject("Scripting.Filesystemobject")

				For Each rArticleName In oSh.UsedRange.Columns("A").Cells
					Set rDisclaimer = rArticleName.Offset(, 1)

					'Add .txt to the article name as a file name
					sFN = rArticleName.Value & ".txt"
					Set oTxt = oFS.OpenTextFile(sExportFolder & "\" & sFN, 2, True)
					oTxt.Write rDisclaimer.Value
					oTxt.Close
				Next
			End Sub    'Source: https://stackoverflow.com/questions/7149539/outputting-excel-rows-to-a-series-of-text-files 

			Save the file, note you may need to Enable Macros.
			After you save the file and have the notes folder and first two Columns (ID and Body), then go back to view Code and push play.

--3. Inserting the Note with the Apex Data Loader 
	Create the 1st CSV for the update. Only CONTENT and TITLE are mapped fields.
	a. Open the file: STG_NOTES_ContentNote.xlsx
	b. Save as IMP_NOTES_ContentNote.CSV. Break file in as many pieces needed and run 1 per day. see note below. 
		--NOTE: SF has an insert limit of "Content Version" of 2,500 records / 24-hr. 
			    Need to create a SF case with the "Feature Activation" team. SF can only increase to 36,000 records/24-hr in a SANDOBOX. PRODUCTION has a limit of 200,000, so no need to request increase. 
				
				1. Open Data Loader
				2. Click "Insert".
				4. Click "Next".
				5. Check the box next to "Show all Salesforce objects"
				6. Select "Note (ContentNote)" from the list of available objects.  (If you don't see ContentNote listed, you may need to enable the new Notes feature for your org.)
				7. Click "Browse..." and select the CSV file you created at step 1.
				8. Click "Next"
				9. When the Data Selection initialization dialog appears, confirm the number of records is correct and click "OK".
				10. Click "Create or Edit a Map" on the next screen.
				11. Click "Auto-Match Fields to Columns" or manually match the TITLE and CONTENT fields to the corresponding columns of the CSV file.
				12. Click "OK" to proceed.
				13. Confirm the mapping is correct and click "Next"
				14. Click "Browse..." and specify the location for the success and error files.
				15. Click "Finish".
				16. Review the Warning and if you're ready to proceed, click "Yes".

-4.  Relating the inserted Note to a Parent Record with the Apex Data Loader.
				Once the Note is inserted we need to relate it to the parent record. Since the note is stored in Content, we need to insert a record to the Content Document Link table in order to do this.
 
				1. Create a new csv file with the following columns: ContentDocumentId, LinkedEntityId, ShareType, and Visibility

					a. in the success CSV file: replace "ID" column heading with "ContentDocumentId"
					b. in the success CSV file: replace "PARENTID" column heading with "LinkedEntityId"
					c. "Visibility" is already in the success CSV file.
					d. "ShareType" is already in the success CSV file.

				2. Locate the success log from the Note import (Step 3 above) and populate the ContentDocumentId column in the new file with Id value from the success files for the Notes that you want to relate to records.
				3. Populate the LinkedEntityId column with the id of the record that you want to relate the Note to.
				4. Set the ShareType and Visibility fields as desired. Documentation here describes the valid values for these fields and their function, there is also an example insert file attached to this article.  Repeat as needed for further records then save your file.(Please note that you can only specify the values 'I' or 'V' for the standard objects since type 'C' is not supported)
				5. Launch the Apex Data Loader.
				6. Click "Insert".
				7. Enter your login credentials.
				8. Click "Next".
				9. Check the box next to "Show all Salesforce objects"
				10. Select "Content Document Link(ContentDocumentLink)" from the list of available objects.
				11. Click "Browse..." and select the CSV file you created in the earlier steps.
				12. Click "Next"
				13. When the Data Selection initialization dialog appears, confirm the number of records is correct and click "OK".
				14. Click "Create or Edit a Map" on the next screen.
				15. Click "Auto-Match Fields to Columns" or manually match the fields to the corresponding columns of the CSV file.
				16. Click "OK" to proceed.
				17. Confirm the mapping is correct and click "Next"
				18. Click "Browse..." and specify the location for the success and error files.
				19. Click "Finish".
				20. Review the Warning and if you're ready to proceed, click "Yes". At this point if you view the Notes related list under the Parent record the Note should now be visible.

Tips for trouble-shooting ContentNote insert errors.
ContentNote currently provides a generic error like "Note could not be saved." whenever an issue is encountered while parsing the Note file itself. This is often an indication of an issue with the contents of the text body (Content) file, such as special character(s) that have not been properly escaped.
To resolve the error, you'll need to pinpoint the affected file or files and replace the special characters with their HTML character code equivalent as described in step 2. 
If Data Loader reports errors on all the files in your insert, but you're able to confirm based on inspection that at least some of the files have no special characters, then follow these instructions to pinpoint the affected files:
1. Set your data loader batch size to 1.
2. Reattempt the import by following Step 3 above. You may see some files successfully imported.
3. If you receive errors this time, review the error file from the import to see which rows in the CSV file produced an error.
4. If the error file gives the "Note could not be saved" error for any rows, review the .txt or .html file associated with that row and replace the special characters in that file with their HTML character code equivalent as described in step 2.
5. If there are any other kinds of error messages, review and resolve those messages as well.
   If you require assistance with determining how to resolve any other errors that can't be answered by searching the Salesforce Help documents, please log a case with Salesforce Support.
6. Reattempt the import, using the Error file for the previous import as your CSV import file, again by following Step 3 above.
See ContentNote insert error: Note Can't Be Saved Because It Contains HTML Tags Or Unescaped Characters That Are Not Allowed In A Note for additional details

See Also:
New Note button on the legacy Notes & Attachments related list
Considerations for enabling the enhanced note-taking tool (Notes) and using Files

--SOURCE: https://help.salesforce.com/articleView?id=000230867&language=en_US&type=1

END 