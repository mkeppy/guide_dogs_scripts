USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblAGSStudentApplication
			WHERE   SF_Object LIKE '%appl%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

BEGIN --DROP APPLICATION

	DROP TABLE GDB_TC1_migration.dbo.IMP_APPLICATION

END

BEGIN --CREATE IMP APPLICATION
 							SELECT   DISTINCT
								GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
								,'cli-'+CAST(T1.cli_ClientID AS NVARCHAR(30)) AS  Legacy_ID__c	-- cli-'+[cli_clientID]	-- Yes/Link  tblPersonRel.prl_ClientInstanceID
								,T3.psn_PersonID AS [Contact__r:Legacy_Id__c]
								,T1.cli_OldFileNum  AS  Old_File_Number__c		
								,t4.ClientStatus AS  Contact_Status__c		-- Yes/Reference trefClientStatus
								,T1.cli_Priority  AS  Priority__c		
								,CAST(T1.cli_DateStatChg AS DATE) AS  Date_Status_Change__c		
								,CASE T1.cli_IsReturning WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Is_Returning__c		
								,CASE T1.cli_WaitList WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Wait_List__c		
								,CASE T1.cli_ReqMaterialsSent WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Required_Materials_Sent__c	-- Convert nulls as 'No'	
								,CAST(T1.cli_AppDateSent  AS DATE) AS  Application_Date_Sent__c		
								,CAST(T1.cli_AppReceived  AS DATE) AS  Application_Received__c		
								,CAST(T1.cli_AppDateAckSent  AS DATE) AS  Application_Date_Acknowledgement_Sent__c		
								,CAST(T1.cli_MedDateSent  AS DATE) AS  Med_Date_Sent__c		
								,CAST(T1.cli_MedDateReceived  AS DATE) AS  Med_Date_Received__c		
								,CAST(T1.cli_OMDateSent  AS DATE) AS  OM_Date_Sent__c		
								,CAST(T1.cli_OMDateReceived  AS DATE) AS  OM_Date_Received__c		
								,CAST(T1.cli_RefsDateSent  AS DATE) AS  References_Date_Sent__c		
								,CAST(T1.cli_OphDateSent  AS DATE) AS  Oph_Date_Sent__c		
								,CAST(T1.cli_OphDateReceived  AS DATE) AS  Oph_Date_Received__c		
								,CAST(T1.cli_MHProDateSent AS DATE)  AS  MH_Pro_Date_Sent__c		
								,CAST(T1.cli_MHProDateReceived  AS DATE) AS  MH_Pro_Date_Received__c		
								,CAST(T1.cli_SchoolDateSent  AS DATE) AS  School_Date_Sent__c		
								,CAST(T1.cli_SchoolDateReceived  AS DATE) AS  School_Date_Received__c		
								,CAST(T1.cli_PreScreenDate AS DATE)  AS  PreScreen_Date__c		
								,CAST(T1.cli_HomeIntDateSched  AS DATE) AS  Home_Interview_Date_Scheduled__c		
								,CASE T1.cli_HomeIntOK WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END  AS  Home_Interview_OK__c		
								,CAST(T1.cli_DateAllComplete AS DATE)  AS  Date_All_Complete__c		
								,CAST(T1.cli_ARCDate  AS DATE) AS  ARC_Date__c		
								,T5.[Text Value for ARC Action]  AS  ARC_Action__c		-- Yes/Link trefClientARCAction
								,T1.cli_ClassLength  AS  Class_Length__c		
								,T1.cli_ClassTentative  AS  Class_Tentative__c		
								,'cls-'+T6.cls_ClassID  AS  [Class__r:Legacy_Id__c]		-- Yes/Link (tblClass)
								,T1.cli_RetrainLength  AS  Retrain_Length__c		
								,X1.ID  AS  Instructor__c		-- Yes/Link [tblStaff].[FileNum]
								,X2.ID  AS  Supervisor__c		-- Yes/Link [tblStaff].[FileNum]
								,T1.cli_RoomAssignment  AS  Room_Assignment__c		
								,CAST(T1.cli_DateGraduated  AS DATE) AS  Date_Graduated__c		
								,CAST(T1.cli_NotesAdmission AS NVARCHAR(MAX))  AS  Notes_Admission__c		
								,T7.reg_DogReqText AS  Dog_Requirement__c		-- Yes/Link trefClientDogRequirement
								,CAST(T1.cli_DogRequirements  AS NVARCHAR(MAX))  AS  Dog_Requirements__c		
								,CAST(T1.cli_TBDateReceived  AS DATE) AS  TB_Date_Received__c		
								,T1.cli_MedSentAppDr  AS  Med_Sent_App_Dr__c		
								,T1.cli_Sponsor  AS  Sponsor__c		
								,CASE T1.cli_MedComplete  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END   AS  Medical_Complete__c		
								,T8.ChklistStatus AS  Applcation_Status__c		-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,T8A.ChklistStatus  AS  Med_Status__c			-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,T8B.ChklistStatus AS  OM_Status__c				-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,T8C.ChklistStatus   AS  Refs_Status__c			-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,T8D.ChklistStatus  AS  Rehab_Status__c			-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,T8E.ChklistStatus  AS  Oph_Status__c			-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,T8F.ChklistStatus   AS  MH_Pro_Status__c		-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,T8G.ChklistStatus  AS  School_Status__c		-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,T8H.ChklistStatus AS  PreScreen_Status__c	-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,T8I.ChklistStatus  AS  HI_Status__c				-- Migrate value form [trefCheckListStatus].[ChklistStatus]	-- Yes/Link [trefCheckListStatus].[ChklistStatusCode]
								,CAST(T1.cli_School1DateReceived  AS DATE) AS  School_1_Date_Received__c		
								,CAST(T1.cli_School2DateReceived  AS DATE) AS  School_2_Date_Received__c		
								,CAST(T1.cli_School3DateReceived  AS DATE) AS  School_3_Date_Received__c		
								,CAST(T1.cli_OwnReviewDate  AS DATE) AS  Own_Review_Date__c		
								,CAST(T1.cli_OwnPendingDate  AS DATE) AS  Own_Pending_Date__c		
								,T1.cli_OwnDisposition  AS  Own_Disposition__c		
								,CAST(T1.cli_OwnDispositionDate  AS DATE) AS  Own_Disposition_Date__c		
								,T9.hrs_HighRiskReason  AS  High_Risk_Student__c		-- Yes/Link [trefhighriskstudents].[hrs_ID]
								,T9A.hrs_HighRiskReason  AS  Permission_1__c				-- Yes/Link [trefhighriskstudents].[hrs_ID]
								,T9B.hrs_HighRiskReason  AS  Permission_2__c				-- Yes/Link [trefhighriskstudents].[hrs_ID]
								,T10.erc_ERCode  AS  Status_End_Reason__c			-- Migrate value form [trefAGSNatureOfCallCodes].[erc_ERCode]	-- Yes/Link [trefAGSNatureOfCallCodes].[erc_ID]

								--tblAGSStudentApplication
								,CAST(T11.asa_Date  AS DATE) AS  Date__c		
								,CASE T11.asa_AppliedBefore  WHEN 'Yes' THEN 'Yes' ELSE 'No' END    AS  Applied_Before__c	-- Migrate blanks as No	
								,CASE T11.asa_PreviousDog  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Previous_Dog__c		-- Migrate blanks as No	
								,CASE T11.asa_GDB  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  GDB__c						-- Migrate blanks as No	
								,CASE T11.asa_SeeingEye  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Seeing_Eye__c	-- Migrate blanks as No	
								,CASE T11.asa_LeaderDogs  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Leader_Dogs__c	-- Migrate blanks as No	
								,CASE T11.asa_GDF WHEN 'Yes' THEN 'Yes' ELSE 'No' END    AS  GDF__C	-- Migrate blanks as No	
								,CASE T11.asa_GuidingEyes  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Guiding_Eyes__c	-- Migrate blanks as No	
								,CASE T11.asa_PilotDogs  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Pilot_Dogs__c	-- Migrate blanks as No	
								,CASE T11.asa_GDA  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  GDA__c	-- Migrate blanks as No	
								,CASE T11.asa_Other  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Other__c	-- Migrate blanks as No	
								, T11.asa_ListDates  AS  List_Dates__c		
								,CASE T11.asa_Veteran  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Veteran__c	-- Migrate blanks as No	
								,CASE T11.asa_DrugAbuse  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Drug_Abuse__c	-- Migrate blanks as No	
								,CASE T11.asa_Felony  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Felony__c	-- Migrate blanks as No	
								,T11.asa_YearBlinded  AS  Year_Blinded__c		
								,T12.cob_Description  AS  Cause_Blindness__c			-- Migrate value from [cob_Description]	-- Yes/Link [trefCauseOfBlindness].[cob_id]
								,T11.asa_CauseBlindnessDetail  AS  Cause_Blindness_Detail__c		
								,T11.asa_OtherDisabilities  AS  Other_Disabilities__c		
								,T11.asa_Wheelchair  AS  Wheelchair__c		
								,T11.asa_OMTraining  AS  OM_Training__c		
								,T11.asa_OMCompleted  AS  OM_Completed__c		
								,T11.asa_MobilityAid  AS  Mobilit_Aid__c		
								,T11.asa_CrossStreets  AS  Cross_Streets__c		
								,T11.asa_BlocksWalked  AS  Blocks_Walked__c		
								,T11.asa_Dest1  AS  Dest_1__c		
								,T11.asa_Dest2  AS  Dest_2__c		
								,T11.asa_Dest3  AS  Dest_3__c		
								,T11.asa_Enthnicity  AS  Ethnicity__c		
								,T11.asa_Media  AS  Media__c		
								,T11.asa_ConsultationTime  AS  Consultation_Time__c		
								,CASE T11.asa_Signed WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS  Signed__c		
								,'Con-'+CAST(T13.con_ContactID  AS NVARCHAR(30))  AS  [Previous_Scheduled_1__r:Legacy_Id__c]							-- Yes/Link tblContacts.con_ContactID
								,'Con-'+CAST(T13A.con_ContactID  AS NVARCHAR(30)) AS  [Previous_Scheduled_2__r:Legacy_Id__c]							-- Yes/Link tblContacts.con_ContactID
								,'Con-'+CAST(T13B.con_ContactID  AS NVARCHAR(30)) AS  [Previous_Scheduled_3__r:Legacy_Id__c]							-- Yes/Link tblContacts.con_ContactID
								,'Con-'+CAST(T13CA.zrefAccountID AS NVARCHAR(30)) AS  [OM_Agency__r:Legacy_Id__c]								--lookupAccount -- Yes/Link tblContacts.con_ContactID
								,'Con-'+CAST(T13D.con_ContactID  AS NVARCHAR(30)) AS  [Phys__r:Legacy_Id__c]										-- Yes/Link tblContacts.con_ContactID
								,'Con-'+CAST(T13EA.zrefAccountID  AS NVARCHAR(30)) AS  [Agency_Rehab__r:Legacy_Id__c]								--lookupAccount  -- Yes/Link tblContacts.con_ContactID
								,'Con-'+CAST(T13F.con_ContactID  AS NVARCHAR(30)) AS  [Opth__r:Legacy_Id__c]										-- Yes/Link tblContacts.con_ContactID
								,'Con-'+CAST(T13G.con_ContactID  AS NVARCHAR(30)) AS  [Mental_Health_Professional__r:Legacy_Id__c]					-- Yes/Link tblContacts.con_ContactID
								,CASE T11.asa_WebApp WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Web_Application__c						
				
								--tblStudentPrefs
								,T14.sda_Table AS Table__c
 								,T14.sda_Preference AS Preference__c

								INTO GDB_TC1_migration.DBO.IMP_APPLICATION
								FROM GDB_TC1_kade.dbo.tblClient AS T1
								LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.cli_ClientID
								LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
								LEFT JOIN GDB_TC1_kade.dbo.tblAGSStudentApplication AS T11 ON T11.asa_AppInstanceID=T2.prl_ClientInstanceID
								LEFT JOIN GDB_TC1_kade.DBO.tblStudentPrefs AS T14 ON T14.sda_ClientID=T2.prl_ClientInstanceID
								LEFT JOIN GDB_TC1_kade.dbo.trefClientStatus AS T4 ON T4.StatusCode=T1.cli_Status
								LEFT JOIN GDB_TC1_kade.dbo.trefClientARCAction AS T5 ON T5.[Value for ARC Action]=T1.cli_ARCAction
								LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T6 ON T6.cls_ClassID=T1.cli_ClassCode
								LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.cli_Instructor
								LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X2 ON X2.ADP__C=T1.cli_Supervisor
								LEFT JOIN GDB_TC1_kade.dbo.trefClientDogRequirement AS T7 ON T7.req_ReqID=T1.cli_DogRequirement
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8 ON T8.ChklistStatusCode=T1.cli_AppStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8A ON T8A.ChklistStatusCode=T1.cli_MedStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8B ON T8B.ChklistStatusCode=T1.cli_OMStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8C ON T8C.ChklistStatusCode=T1.cli_RefsStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8D ON T8D.ChklistStatusCode=T1.cli_RehabStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8E ON T8E.ChklistStatusCode=T1.cli_OphStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8F ON T8F.ChklistStatusCode=T1.cli_MHProStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8G ON T8G.ChklistStatusCode=T1.cli_SchoolStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8H ON T8H.ChklistStatusCode=T1.cli_PreScreenStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefCheckListStatus AS T8I ON T8I.ChklistStatusCode=T1.cli_HIStatus
								LEFT JOIN GDB_TC1_kade.dbo.trefhighriskstudents AS T9 ON T9.hrs_ID=T1.cli_HighRiskStudent
								LEFT JOIN GDB_TC1_kade.dbo.trefhighriskstudents AS T9A ON T9A.hrs_ID=T1.cli_Permission1
				 				LEFT JOIN GDB_TC1_kade.dbo.trefhighriskstudents AS T9B ON T9B.hrs_ID=T1.cli_Permission2
								LEFT JOIN GDB_TC1_kade.dbo.trefAGSNatureOfCallCodes AS T10 ON T10.erc_ID=T1.cli_StatusEndReason
								LEFT JOIN GDB_TC1_kade.dbo.trefCauseOfBlindness AS T12 ON T12.cob_ID=T11.asa_CauseBlindness
								LEFT JOIN (SELECT * FROM GDB_TC1_kade.dbo.tblContacts WHERE con_NameLast IS NOT NULL) AS T13 ON T13.con_ContactID=T11.asa_PrevSchID1
								LEFT JOIN (SELECT * FROM GDB_TC1_kade.dbo.tblContacts WHERE con_NameLast IS NOT NULL) AS T13A ON T13A.con_ContactID=T11.asa_PrevSchID2
								LEFT JOIN (SELECT * FROM GDB_TC1_kade.dbo.tblContacts WHERE con_NameLast IS NOT NULL) AS T13B ON T13B.con_ContactID=T11.asa_PrevSchID3
								LEFT JOIN GDB_TC1_kade.dbo.tblContacts AS T13C ON T13C.con_ContactID=T11.asa_OMAgencyId
								LEFT JOIN GDB_TC1_migration.dbo.stg_tblContact_Account AS T13CA ON T13CA.zrefAccountName= T13C.zrefAccountName
								LEFT JOIN (SELECT * FROM GDB_TC1_kade.dbo.tblContacts WHERE con_NameLast IS NOT NULL) AS T13D ON T13D.con_ContactID=T11.asa_PhysID
								LEFT JOIN GDB_TC1_kade.dbo.tblContacts AS T13E ON T13E.con_ContactID=T11.asa_AgencyRehabId
								LEFT JOIN GDB_TC1_migration.dbo.stg_tblContact_Account AS T13EA ON T13EA.zrefAccountName= T13E.zrefAccountName
								LEFT JOIN (SELECT * FROM GDB_TC1_kade.dbo.tblContacts WHERE con_NameLast IS NOT NULL) AS T13F ON T13F.con_ContactID=T11.asa_OpthId
								LEFT JOIN (SELECT * FROM GDB_TC1_kade.dbo.tblContacts WHERE con_NameLast IS NOT NULL) AS T13G ON T13G.con_ContactID=T11.asa_MentalHealthProID

END --TC1: 

BEGIN --AUDIT 

			SELECT * FROM GDB_TC1_migration.dbo.IMP_APPLICATION 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_APPLICATION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			
			--2
END 


 









