USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblGDBInvDispenseTrans
			WHERE   SF_Object LIKE '%en%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_ENTRY

END 

BEGIN -- CREATE IMP 

					
			SELECT DISTINCT GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'Idt-'+ CAST(T1.idt_DispTransID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- concatenate 'Idt-'+[idt_DispTransID]	
				 	,'id-'+  CAST(T2.id_DispenseID AS NVARCHAR(30))  AS  [Order__r:Legacy_Id__c]			 -- Link [tblGDBInvDispense].[id_DispenseID]
					,'Ii-'+  CAST(T4.ii_ItemID AS NVARCHAR(30))  AS  [Inventory__r:Legacy_Id__c]		-- Ref [trefGDBInvItemsDetail].[iid_ItemDetailID]
			 	 	 ,T1.idt_OpenTrans  AS  Open__c		
					,T1.idt_Quantity  AS  Quantity__c		
					,T5.iuom_Type  AS  Sell_UOM__c	-- migrate value in [iuom_Type]	-- Ref [trefGDBInvUOM].[iuom_ID]
					,T1.idt_SellUnitCost  AS  Sell_Unit_Cost__c		
					,CAST(T1.idt_DateProcessed AS DATE) AS  Date_Processed__c		
 					,CAST(T1.idt_CreatedDate AS DATE) AS CreatedDate

			INTO  GDB_TC1_migration.DBO.IMP_ENTRY
			FROM GDB_TC1_kade.dbo.tblGDBInvDispenseTrans AS T1
			LEFT JOIN GDB_TC1_kade.dbo.tblGDBInvDispese AS T2 ON T2.id_DispenseID=T1.idt_DispenseID
			LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvItemsDetail AS T3 ON T3.iid_ItemDetailID=T1.idt_ItemDetailID
			LEFT JOIN GDB_TC1_kade.dbo.trefGDBInventroyItems AS T4 ON T4.ii_ItemID=T3.iid_ItemID
			LEFT JOIN GDB_TC1_kade.dbo.trefGDBInvUOM AS T5 ON T5.iuom_ID=T1.idt_SellUOM





END --tc1: 4623
 
BEGIN --audit

	SELECT * FROM GDB_TC1_migration.dbo.IMP_ENTRY
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_ENTRY GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 		SELECT zrefSrc, COUNT(*) C
			FROM GDB_TC1_migration.dbo.IMP_ENTRY 
			GROUP BY zrefSrc
		 
END 
