USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblDogSemenInventory
			WHERE   SF_Object LIKE '%SE%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_SEMEN_INVENTORY

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId

						,'DSB-' + CAST(T1.dsb_CollectionID AS NVARCHAR(30)) AS  Legacy_ID__c		
						,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]		
						,CAST(T1.dsb_DateCollected AS DATE) AS  Date_Collected__c		
						,T1.dsb_CollectionIDLetter  AS  Collection_Letter__c		
						,T1.dsb_CaneLabel  AS  Cane_Label__c		
						,T1.dsb_StrawsCollected  AS  Straws_Collected__c		
						,T1.dsb_StrawsDiscarded  AS  Straws_Discarded__c		
						,T1.dsb_CountPerStraw  AS  Count_Per_Straw__c		
						,T1.dsb_Location  AS  Location__c		
						,T1.dsb_Sublocation  AS  Sublocation__c		
						,T1.dsb_SpermMotility  AS  Sperm_Motility__c		
						,T1.dsb_SpermMotilitySpeed  AS  Sperm_Motility_Speed__c	-- Fast, Moderate, Slow	
						,T1.dsb_SpermQuality  AS  Sperm_Quality__c				-- Average, Excellent, Fair, Good	
 						,'BRS-'+ CAST(T3.brs_SourceID AS NVARCHAR(30)) AS  [Straws_Donated_To__r:Legacy_Id__c]		-- Yes/Link trefDogSourceNew.brs_SourceID
						,T1.dsb_StrawsPerBreed  AS  Straws_Per_Breed__c		
						,T1.dsb_Collector  AS  Collector__c		
						,T1.dsb_Notes  AS  Notes__c		
					
				 	INTO GDB_TC1_migration.DBO.IMP_SEMEN_INVENTORY
					FROM GDB_TC1_kade.dbo.tblDogSemenInventory AS T1
					LEFT JOIN GDB_TC1_kade.DBO.tblDog AS T2 ON T2.dog_DogID=T1.dsb_DogID
					LEFT JOIN GDB_TC1_kade.DBO.trefDogSourceNew AS T3 ON T3.brs_SourceID=T1.dsb_StrawsDonatedTo
					
 
END 


BEGIN --audit

	SELECT * FROM GDB_TC1_migration.dbo.IMP_SEMEN_INVENTORY
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_SEMEN_INVENTORY GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 		SELECT zrefSrc, COUNT(*) C
			FROM GDB_TC1_migration.dbo.IMP_SEMEN_INVENTORY 
			GROUP BY zrefSrc
		 
END 