USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblVetDiag
			WHERE   SF_Object LIKE '%VET%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_VET_DIAGNOSIS

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
								,'VDD-'+CAST(T1.vdd_DiagID AS NVARCHAR(30)) AS  Legacy_ID__C	-- concatenate 'Vdd-'+[vdd_DiagID]	
								,'VDL-'+CAST(T.vdl_VetEntryID AS NVARCHAR(30)) AS  [Vet_Record__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
								,T1.vdd_DiagCode  AS  Diagnosis_Code__c	-- migrate as is	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.DiagnosisText  AS  Diagnosis_Text__c				-- migrate value from [DiagnosisText]	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T5.CategoryDesc AS  Diagnosis_Category__c			-- Links to [trefVetDiagCategory].[CategoryCode] migrate value from [CategoryDesc] migrate value from [CategoryDesc]	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.Location AS  Location_Code__c			-- migrate value from [Location]			-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.Value1Text  AS  Value_1_Text__c			-- migrate value from [ValueText1]			-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.Value2Text    AS  Value_2_Text__c			-- migrate value from [ValueText2]			-- Link  [trefVetPetChamp].[DiagnosisCode]
								,CASE T2.Hide  WHEN 1 THEN 'Yes' ELSE 'No' END   AS  Hide_1__c					-- migrate value from [Hide]				-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.DogReleaseCategory AS  Dog_Release_Category__c	-- migrate value from [DogReleaseCategory]	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.Main AS  Main__c					-- migrate value from [Main]			-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.MainText AS  Main_Text__c				-- migrate value from [MainText]			-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.Detail1  AS  Detail_1__c				-- migrate value from [Detail1]	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.Detail1Text  AS  Detail_1_Text__c	-- migrat detail from [Detail1Text]	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.Detail2  AS  Detail_2__c	-- migrate value from [Detail2]	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.Detail2Text  AS  Detail_2_Text__c	-- migrat detail from [Detail2Text]	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.[Description]  AS  Description__c	-- migrate value from [Description]	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T2.RelCodeStatus AS  Rel_Code_Status__c	-- migrate value from [RelCodeStatus]	-- Link  [trefVetPetChamp].[DiagnosisCode]
								,T1.vdd_Value1  AS  Value_1__c	-- migrate as is	-- Link [trefVetDiagValues.vdv_Value]
								,T1.vdd_Value2  AS  Value_2__c	-- migrate as is	-- Link [trefVetDiagValues].[vdv_Value]
								,T3.[Location]  AS  Location__c	-- migrate value from [trefVetPetChampLoc].[Locaction]	-- Link  [trefVetPetChampLoc].[LocCode]
								,T4.DiagSeverityDesc  AS  Severity__c	-- migrate value from [DiagSeverityDesc]	-- Link [trefVetDiagSeverity].[DiagSeverityCode]
								,CASE T1.vdd_Hide WHEN 1 THEN 'Yes' ELSE 'No' END  AS  Hide_2__c	-- migrate value from [Hide]	
					INTO GDB_TC1_migration.DBO.IMP_VET_DIAGNOSIS
					FROM GDB_TC1_kade.dbo.tblVetDiag AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblVetEntry AS T ON T.vdl_VetEntryID=T1.vdd_VetEntryID
					LEFT JOIN GDB_TC1_kade.DBO.trefVetPetChamp AS T2 ON T2.DiagnosisCode =T1.vdd_DiagCode
					LEFT JOIN GDB_TC1_kade.dbo.trefVetPetChampLoc AS T3 ON T3.LocCode=T1.vdd_Location
					LEFT JOIN GDB_TC1_kade.dbo.trefVetDiagSeverity AS T4 ON T4.DiagSeverityCode=T1.vdd_Severity
					LEFT JOIN GDB_TC1_kade.dbo.trefVetDiagCategory AS T5 ON T5.CategoryCode=T2.DiagnosisCategory 
				
 		  

END --tc1: 594998

BEGIN-- AUDIT
	SELECT * FROM GDB_TC1_migration.dbo.IMP_VET_DIAGNOSIS
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_VET_DIAGNOSIS GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_TC1_migration.dbo.IMP_VET_DIAGNOSIS 
	GROUP BY zrefSrc
	
	SELECT * FROM GDB_TC1_kade.DBO.trefVetPetChamp
	SELECT * FROM GDB_TC1_kade.dbo.trefDogReleaseCategories
	
	SELECT * FROM GDB_TC1_migration.DBO.IMP_VET_DIAGNOSIS




END 