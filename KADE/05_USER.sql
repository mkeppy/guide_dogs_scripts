USE GDB_TC1_migration
GO

BEGIN--Map: USER  
			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblStaff
			WHERE  SF_Object LIKE '%use%'
END 


BEGIN--DROP USERS
	
	DROP TABLE GDB_TC1_migration.dbo.IMP_USERS

END 

BEGIN--USERS
			SELECT 
				 T1.FileNum  AS  ADP__c	
				,T1.LastName  AS  LastName	
				,T1.FirstName  AS  FirstName	
				,X.ID AS [CONTACTID]
	 			,T1.Position  AS  Title
				,CASE WHEN T1.[Status] ='Active' THEN 'TRUE' ELSE 'FALSE' END AS IsActive	
				,T1.DeptNum AS  Department_Number__c	
				,T1.DeptNumLong  AS  Department_Number_Long__c	
				,T1.LoginID +'@guidedogs.com.uat' AS  Username	-- concatenate [loginID] + '@guidedogs.com'
				,T1.LoginID +'@guidedogs.com.uat' AS  Email	-- concatenate [loginID] + '@guidedogs.com.uat'
				,LEFT(T1.LoginID,8) AS Alias
				,CAST(T1.HireDate  AS DATE) AS  Hire_Date__c	
				,CAST(T1.TermDate  AS DATE) AS  Termination_Date__c	
				,CAST(T1.RehireDate  AS DATE) AS  Rehire_Date__c	
			 	,T1.[Location]  AS  Location	
				
				,T1.DogStatusChangeBy  AS  Dog_Status_Changed_By__c	
				,T1.DogNameChange  AS  Dog_Name_Change__c	
				,T1.PuppyConcerns  AS  Puppy_Concerns__c	
				,T1.PrattCommittee  AS  Pratt_Committee__c	
				,'ISO-8859-1'  AS EMAILENCODINGKEY	-- ISO-8859-1
				,'en_US' AS LANGUAGELOCALEKEY	-- en_US
				,'en_US' AS LOCALESIDKEY	-- en_US
				,'America/Los_Angeles'  AS TIMEZONESIDKEY	-- America/Los_Angeles 
				,'00e410000010ia4AAA'  AS ProfileId	-- "Read Only"
				, 'Staff-'+ CAST(T1.FileNum  AS NVARCHAR(30)) zrefFineNum
				, CASE WHEN T1.[Status] ='Active' THEN 'TRUE' ELSE 'FALSE' END AS zrefStatus
				,T1.LoginID zrefLoginID

			INTO GDB_TC1_migration.dbo.IMP_USER
			FROM GDB_TC1_kade.dbo.tblStaff AS T1
			LEFT JOIN GDB_TC1_migration.dbo.XTR_CONTACT AS X ON 'Staff-'+ CAST(T1.FileNum  AS NVARCHAR(30))=X.Legacy_Id__c

			 
END
 
BEGIN -- audit

		--checkc dupe Alias
			SELECT * 
			FROM GDB_TC1_migration.dbo.IMP_USERS 
			WHERE Alias IN (SELECT Alias FROM GDB_TC1_migration.dbo.IMP_USERS  GROUP BY Alias HAVING COUNT(*)>1)
			ORDER BY LastName, FirstName

END

BEGIN--USERS ---update managerID 
			
			SELECT 
			T1.FileNum  AS [ADP__c]
			,T1.Manager  AS  [ManagerId:ADP__c]
			INTO GDB_TC1_migration.dbo.IMP_USERS_MANAGER_UPDATE
			FROM GDB_TC1_kade.dbo.tblStaff AS T1
			WHERE T1.Manager IS NOT NULL 

END 