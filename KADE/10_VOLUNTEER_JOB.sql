USE GDB_TC1_migration
GO
BEGIN-- 
	 		SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblVolCalEntry
			WHERE   SF_Object LIKE '%vol%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%xxxxxxxxxxxxxxxxxxxx%'
END 

BEGIN --drop volunteer

	DROP TABLE GDB_TC1_migration.dbo.IMP_VOLUNTEER_JOB

END 

BEGIN-- create VOLUNTEER

					SELECT DISTINCT
							  GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerID 
							,T1.ce_EntryID  AS  Legacy_ID__c	-- concatenate 'Ce-'+[ce_EntryId]
							,T1.ce_WebUploadID  AS  Web_Upload_ID__c	
							,CAST(T1.ce_EntryDate  AS DATE) AS  Entry_Date__c	
							,CAST(T1.ce_Time  AS TIME) AS  Time__c	
							,'Vc-'+CAST(T1.ce_ContactID  AS NVARCHAR(30))  AS  [Contact__r:Legacy_Id__c]	  --Yes/Link tblVolCalOrgContact.vc_ContactID
							,T1.ce_Campus  AS  Campus__c	
							,T2.EntryTypeText  AS  Entry_Type__c	-- migrate value from [EntryTypeText] -- Ref/Yes [trefVolCalTourType].[EntryTypeID]
							,T2.EntryCatText  AS  Entry_Category__c	-- migrate value from [entryCatText] -- Ref/Yes [trefVolCalTourType].[EntryTypeID]
							,T1.ce_SchoolGrade  AS  School_Grade__c	
							,T1.ce_NumAttendees  AS  Number_Attendees__c	
							,T1.ce_NumAdults  AS  Number_Adults__c	
							,T1.ce_Confirmation  AS  Responded__c
							,T1.ce_HowFound  AS  How_Found__c	
							,'RE-'+ CAST(T1.ce_Docent1 AS NVARCHAR(30))  AS  [Docent_1__r:Legacy_ID__c]
							,'RE-'+ CAST(T1.ce_Docent2   AS NVARCHAR(30))  AS  [Docent_2__r:Legacy_ID__c]
							,T1.ce_Notes  AS  GW_Volunteers__Description__c	
							,'LVCE'  AS  [GW_Volunteers__Campaign__r:Legacy_Id__c]	-- migrate above created Campaign 'Legacy Volunteer Calendar Entry' as parent
							INTO GDB_TC1_migration.dbo.IMP_VOLUNTEER_JOB
						 FROM GDB_TC1_kade.dbo.tblVolCalEntry AS T1 
						 LEFT JOIN GDB_TC1_kade.dbo.trefVolCalTourType AS T2 ON T2.EntryTypeID=T1.ce_EntryType
					 
END -- tc1: 					 


SELECT * FROM GDB_TC1_migration.dbo.IMP_VOLUNTEER_JOB 
