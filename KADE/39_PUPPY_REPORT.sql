USE GDB_TC1_migration
GO


BEGIN--mapping 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblDogPuppyFunThings
			WHERE   SF_Object LIKE '%puppy%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 


/*
SELECT * FROM GDB_TC1_migration.dbo.XTR_RECORD_TYPE  WHERE SOBJECTTYPE LIKE '%pupp%'
0123D0000004axMQAQ	Eval	Puppy_Report__c
0123D0000004axNQAQ	Final	Puppy_Report__c
0123D0000004axOQAQ	Monthly	Puppy_Report__c
*/

BEGIN --DROP IMPs

	DROP TABLE GDB_TC1_migration.DBO.IMP_PUPPY_REPORT_pfr
	DROP TABLE GDB_TC1_migration.DBO.IMP_PUPPY_REPORT_mpr
	DROP TABLE GDB_TC1_migration.DBO.IMP_PUPPY_REPORT_pre
	DROP TABLE GDB_TC1_migration.DBO.IMP_PUPPY_REPORT_pft
END 

BEGIN-- CREATE IMP_PUPPY_REPORT_pfr 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId

							,'PRF-'+CAST(T1.pfr_ID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "PFR-" with pfr_ID	
							,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]  			-- Link  [tblDog].[dog_DogID]
							,T3.psn_PersonID AS [Contact__r:Legacy_Id__c]		-- Link  [tblPerson].[psn_PersonID]
							,'0123D0000004axNQAQ'  AS  RecordTypeID	-- Final	
							,CAST(T1.pfr_AlteredAtHomeDate AS DATE)  AS  Altered_At_Home_Date__c		
							,T1.pfr_AdultOrYouth  AS  Adult_Or_Youth__c		
							,T1.pfr_Leader  AS  Leader__c		
							,T1.pfr_CFR  AS  CFR__c		
							,T1.pfr_CFRComments  AS  CFR_Comments__c		
							,T1.pfr_FoodBrand  AS  Food_Brand__c		
							,T1.pfr_FoodBrandOther  AS  Food_Brand_Other__c		
							,T1.pfr_AmountFoodAM  AS  Amount_Food_AM__c		
							,T1.pfr_AmountFoodPM  AS  Amount_Food_PM__c		
							,T1.pfr_BodyScore  AS  Body_Score__c		
							,CAST(T1.pfr_DateSeasonFirstNoticed1  AS DATE)  AS  Date_Season_First_Noticed_1__c		
							,CAST(T1.pfr_DateSeasonEnded1  AS DATE)  AS  Date_Season_Ended_1__c		
							,CAST(T1.pfr_DateSeasonFirstNoticed2   AS DATE) AS  Date_Season_First_Noticed_2__c		
							,CAST(T1.pfr_DateSeasonEnded2   AS DATE) AS  Date_Season_Ended_2__c		
							,CAST(T1.pfr_DateSeasonFirstNoticed3  AS DATE)  AS  Date_Season_First_Noticed_3__c		
							,CAST(T1.pfr_DateSeasonEnded3  AS DATE)  AS  Date_Season_Ended_3__c		
							,T1.pfr_SeasonOtherInformation  AS  Season_Other_Information__c		
							,CASE T1.pfr_EqFlatCollar WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Eq_Flat_Collar__c		
							,CASE T1.pfr_EqHalti WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Eq_Halti__c		
							,CASE T1.pfr_EqGentleLeader WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Eq_Gentle_Leader__c		
							,CASE T1.pfr_EqTrainingCollar WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Eq_Training_Collar__c		
							,CASE T1.pfr_CanBeUnsupervisedAtHome WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Can_Be_Unsupervised_At_Home__c		
							,T1.pfr_UnsupervisedHowLong  AS  Unsupervised_How_Long__c		
							,T1.pfr_NotCratedIn  AS  Not_Crated_In__c		
							,CASE T1.pfr_NoisyAlone WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Noisy_Alone__c		
							,CASE T1.pfr_LickHimself WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Lick_Himself__c		
							,CASE T1.pfr_ComfortableOnTieDown WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Comfortable_On_Tie_Down__c		
							,CASE T1.pfr_ComfortableOnCrate WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Comfortable_On_Crate__c		
							,CASE T1.pfr_RemoveItemsFromCounters WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Remove_Items_From_Counters__c		
							,CASE T1.pfr_RemoveItemsFromTrash WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Remove_Items_From_Trash__c		
							,CASE T1.pfr_ConsistentlyRelieveLeash WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Consistently_Relieve_Leash__c		
							,CASE T1.pfr_InappropriatelyRelieveOnOutings WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Inappropriately_Relieve_On_Outings__c		
							,CASE T1.pfr_InappropriatelyRelieveOnWalks WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Inappropriately_Relieve_On_Walks__c		
							,T1.pfr_ReactionsInHomeToMen  AS  Reactions_In_Home_To_Men__c		
							,T1.pfr_ReactionsInHomeToWomen  AS  Reactions_In_Home_To_Women__c		
							,T1.pfr_ReactionsInHomeToChildren  AS  Reactions_In_Home_To_Children__c		
							,T1.pfr_AgeofChilderInHome  AS  Age_of_Children_In_Home__c		
							,T1.pfr_ReactionToDogs  AS  Reaction_To_Dogs__c		
							,T1.pfr_ReactionToCats  AS  Reaction_To_Cats__c		
							,T1.pfr_GrowlOverFood  AS  Growl_Over_Food__c		
							,T1.pfr_ReactionsOutsideHomeToMen  AS  Reactions_Outside_Home_To_Men__c		
							,T1.pfr_ReactionsOutsideHomeToWomen  AS  Reactions_Outside_Home_To_Women__c		
							,T1.pfr_ReactionsOutsideHomeToChildren  AS  Reactions_Outside_Home_To_Children__c		
							,T1.pfr_AgeOfChilderOutsideHome  AS  Age_Of_Children_Outside_Home__c		
							,T1.pfr_GrowlOverToys  AS  Growl_Over_Toys__c		
							,CASE T1.pfr_AttemptToChase  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Attempt_To_Chase__c		
							,T1.pfr_ChaseEplanation  AS  Chase_Explanation__c		
							,T1.pfr_TimidOrAgressivePupsAge  AS  Timid_Or_Aggressive_Pups_Age__c		
							,T1.pfr_ReactionBall  AS  Reaction_Ball__c		
							,T1.pfr_ReactionFood  AS  Reaction_Food__c		
							,T1.pfr_ReactionDogs  AS  Reaction_Dogs__c		
							,T1.pfr_ReactionToys  AS  Reaction_Toys__c		
							,T1.pfr_ReactionTrucks  AS  Reaction_Trucks__c		
							,T1.pfr_ReactionSkateBoards  AS  Reaction_Skate_Boards__c		
							,T1.pfr_ReactionWheelChairs  AS  Reaction_Wheel_Chairs__c		
							,T1.pfr_ReactionPeopleCrutches  AS  Reaction_People_Crutches__c		
							,T1.pfr_ReactionPeopleParcels  AS  Reaction_People_Parcels__c		
							,T1.pfr_ReactionSurfaces  AS  Reaction_Surfaces__c		
							,T1.pfr_ReactionHeavyTraffic  AS  Reaction_Heavy_Traffic__c		
							,T1.pfr_ReactionMachinery  AS  Reaction_Machinery__c		
							,T1.pfr_ReactionBikes  AS  Reaction_Bikes__c		
							,T1.pfr_ReactionElevators  AS  Reaction_Elevators__c		
							,T1.pfr_ReactionShoppingCarts  AS  Reaction_Shopping_Carts__c		
							,T1.pfr_ReactionCrowds  AS  Reaction_Crowds__c		
							,T1.pfr_ReactionLoudNoises  AS  Reaction_Loud_Noises__c		
							,T1.pfr_ReactionStairs  AS  Reaction_Stairs__c		
							,T1.pfr_ReactionOdors  AS  Reaction_Odors__c		
							,T1.pfr_ReactionOther  AS  Reaction_Other__c		
							,T1.pfr_ReactioOtherDescription  AS  Reaction_Other_Description__c		
							,T1.pfr_ReactionUFDescription  AS  Reaction_UF_Description__c		
							,CASE T1.pfr_SickInCar  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Sick_In_Car__c		
							,T1.pfr_SickInCarExplanation  AS  Sick_In_Car_Explanation__c		
							,T1.pfr_SickInCarVehicleType  AS  Sick_In_Car_Vehicle_Type__c		
							,T1.pfr_SickInCarFrecuency  AS  Sick_In_Car_Frequency__c		
							,T1.pfr_SickInCarFrecuencyOther  AS  Sick_In_Car_Frequency_Other__c		
							,CASE T1.pfr_DidPuppyOvercome  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Did_Puppy_Overcome__c		
							,CASE T1.pfr_PuppySalivate  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Puppy_Salivate__c		
							,T1.pfr_SalivationCause  AS  Salivation_Cause__c		
							,T1.pfr_SalivationFrecuency  AS  Salivation_Frequency__c		
							,T1.pfr_ThreeBehaviorsBest  AS  Three_Behaviors_Best__c		
							,T1.pfr_ThreeBehaviorsLeastBest  AS  Three_Behaviors_Least_Best__c		
							,T1.pfr_SpecialProtocols  AS  Special_Protocols__c		
							,T1.pfr_AdditionalInfo  AS  Additional_Info__c		
							,CAST(T1.pfr_CreatedDate AS DATE) AS   CreatedDate		
							
					INTO GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_pfr
					FROM GDB_TC1_kade.dbo.tblDogPuppyFinalReport AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.pfr_DogID
					LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON T3.psn_PersonID=T1.pfr_PersonID

 
END --tc1 1545

BEGIN-- AUDIT  IMP_PUPPY_REPORT_pfr

	SELECT * FROM GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_pfr

END 

BEGIN-- CREATE IMP_PUPPY_REPORT_mpr 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
									,'MPR-'+CAST(T1.mpr_id  AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "MPR-" with mpr_id	
									,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]  			-- Link  [tblDog].[dog_DogID]
									,T3.psn_PersonID AS [Contact__r:Legacy_Id__c]		-- Link  [tblPerson].[psn_PersonID]
									,'0123D0000004axOQAQ'  AS  RecordTypeID	-- Monthly	
									,CAST(T1.mpr_ReportDate AS DATE) AS  Report_Date__c		
									,T1.mpr_DogFood  AS  Dog_Food__c		
									,T1.mpr_DogFoodOther  AS  Dog_Food_Other__c		
									,T1.mpr_CupsPerMeal  AS  Cups_Per_Meal__c		
									,T1.mpr_MealsPerDay  AS  Meals_Per_Day__c		
									,T1.mpr_BodyScore  AS  Body_Score__c		
									,CAST(T1.mpr_FirstSeason  AS DATE) AS  First_Season__c		
									,CASE T1.mpr_SpayNeuter WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Spay_Neuter__c		
									,CAST(T1.mpr_SpayNeuterDate AS DATE) AS  Spay_Neuter_Date__c		
									,CASE T1.mpr_PuppyHealth WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Puppy_Health__c		
									,CASE T1.mpr_MedicalProb WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Medical_Problem__c		
									,T1.mpr_ConcernsTreatments  AS  Concerns_Treatments__c		
									,CASE T1.mpr_FoodProtocol WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Food_Protocol__c		
									,CAST(T1.mpr_FoodProtocolDate AS DATE) AS  Food_Protocol_Date__c		
									,T1.mpr_ReasonFoodProtocol  AS  Food_Protocol_Reason__c		
									,T1.mpr_CommandsOverall  AS  Commands_Overall__c		
									,T1.mpr_FirOnLeash  AS  Fir_On_Leash__c		
									,T1.mpr_FirOffLeash  AS  Fir_Off_Leash__c		
									,T1.mpr_GotoBed  AS  Go_to_Bed__c		
									,T1.mpr_LooseInHome  AS  Loose_In_Home__c		
									,T1.mpr_AmountTime  AS  Amount_Time__c		
									,T1.mpr_LooseinHomeAbsent  AS  Loose_In_Home_Absent__c		
									,T1.mpr_AmountTimeAbsent  AS  Amount_Time_Absent__c		
									,CASE T1.mpr_RelieveCommand WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Relieve_Command__c		
									,CASE T1.mpr_RelieveAnySurface WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Relieve_Any_Surface__c		
									,CASE T1.mpr_RelieveIndoors WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Relieve_Indoors__c		
									,T1.mpr_RelieveWalks  AS  Relieve_Walks__c		
									,T1.mpr_PrimaryCollar  AS  Primary_Collar__c		
									,T1.mpr_CollarOther  AS  Collar_Other__c		
									,CASE T1.mpr_RelaxedComfortable WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Relaxed_Comfortable__c		
									,CASE T1.mpr_RelaxedComfortableOther WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Relaxed_Comfortable_Other__c		
									,T1.mpr_socialw1_1  AS  Social_work_1_1__c		
									,T1.mpr_socialw1_2  AS  Social_work_1_2__c		
									,T1.mpr_socialw1_3  AS  Social_work_1_3__c		
									,T1.mpr_socialw1_4  AS  Social_work_1_4__c		
									,T1.mpr_socialw1_5  AS  Social_work_1_5__c		
									,T1.mpr_socialw2_1  AS  Social_work_2_1__c		
									,T1.mpr_socialw2_2  AS  Social_work_2_2__c		
									,T1.mpr_socialw2_3  AS  Social_work_2_3__c		
									,T1.mpr_socialw2_4  AS  Social_work_2_4__c		
									,T1.mpr_socialw2_5  AS  Social_work_2_5__c		
									,T1.mpr_socialw3_1  AS  Social_work_3_1__c		
									,T1.mpr_socialw3_2  AS  Social_work_3_2__c		
									,T1.mpr_socialw3_3  AS  Social_work_3_3__c		
									,T1.mpr_socialw3_4  AS  Social_work_3_4__c		
									,T1.mpr_socialw3_5  AS  Social_work_3_5__c		
									,T1.mpr_socialw4_1  AS  Social_work_4_1__c		
									,T1.mpr_socialw4_2  AS  Social_work_4_2__c		
									,T1.mpr_socialw4_3  AS  Social_work_4_3__c		
									,T1.mpr_socialw4_4  AS  Social_work_4_4__c		
									,T1.mpr_socialw4_5  AS  Social_work_4_5__c		
									,T1.mpr_SocializePerWeek  AS  Socialize_Per_Week__c		
									,T1.mpr_SitorTrade  AS  Sit_or_Trade__c		
									,T1.mpr_Headcollar  AS  Head_collar__c		
									,T1.mpr_PuppyJaket  AS  Puppy_Jacket__c		
									,T1.mpr_PeopleHackling  AS  People_Hackling__c		
									,T1.mpr_PeoplePosturing  AS  People_Posturing__c		
									,T1.mpr_PeopleGrowlingBarking  AS  People_Growling_Barking__c		
									,T1.mpr_PeopleMouthing  AS  People_Mouthing__c		
									,T1.mpr_PeopleNippingBiting  AS  People_Nipping_Biting__c		
									,T1.mpr_PeopleFearful  AS  People_Fearful__c		
									,T1.mpr_PeopleSubmissive  AS  People_Submissive__c		
									,T1.mpr_PeopleKeying  AS  People_Keying__c		
									,T1.mpr_PeopleSolicitous  AS  People_Solicitous__c		
									,T1.mpr_StationaryObjects  AS  Stationary_Objects__c		
									,T1.mpr_ObjectsInMotion  AS  Objects_In_Motion__c		
									,T1.mpr_StrangeUnusualObjects  AS  Strange_Unusual_Objects__c		
									,T1.mpr_Thunderstorms  AS  Thunderstorms__c		
									,T1.mpr_Fireworks  AS  Fireworks__c		
									,T1.mpr_LoudNoises  AS  Loud_Noises__c		
									,T1.mpr_Hackling  AS  Hackling__c		
									,T1.mpr_Posturing  AS  Posturing__c		
									,T1.mpr_GrowlingBarking  AS  Growling_Barking__c		
									,T1.mpr_Fearful  AS  Fearful__c		
									,T1.mpr_Submissive  AS  Submissive__c		
									,T1.mpr_DogDistracted  AS  Dog_Distracted__c		
									,T1.mpr_FenceFighting  AS  Fence_Fighting__c		
									,T1.mpr_RoughPlay  AS  Rough_Play__c		
									,T1.mpr_Grates  AS  Grates__c		
									,T1.mpr_SlickFloors  AS  Slick_Floors__c		
									,T1.mpr_Stairs  AS  Stairs__c		
									,T1.mpr_RelievesIndoors  AS  Relieves_Indoors__c		
									,T1.mpr_RelievesOnOutings  AS  Relieves_On_Outings__c		
									,T1.mpr_LickingChewing  AS  Licking_Chewing__c		
									,T1.mpr_CarSickness  AS  Car_Sickness__c		
									,T1.mpr_BarkingWhining  AS  Barking_Whining__c		
									,T1.mpr_DestructiveChewing  AS  Destructive_Chewing__c		
									,T1.mpr_Digging  AS  Digging__c		
									,T1.mpr_ToyBallObsession  AS  Toy_Ball_Obsession__c		
									,T1.mpr_FoodOrotective  AS  Food_Protective__c		
									,T1.mpr_FoodObsessionForaging  AS  Food_Obsession_Foraging__c		
									,T1.mpr_GarbageMouth  AS  Garbage_Mouth__c		
									,T1.mpr_FilthEater  AS  Filth_Eater__c		
									,T1.mpr_PoorKennelBehavior  AS  Poor_Kennel_Behavior__c		
									,T1.mpr_PoorCrateBehavior  AS  Poor_Crate_Behavior__c		
									,T1.mpr_PoorTieDownBehavior  AS  Poor_Tie_Down_Behavior__c		
									,T1.mpr_ExcessiveSniffing  AS  Excessive_Sniffing__c		
									,T1.mpr_MouthingGrabbing  AS  Mouthing_Grabbing__c		
									,T1.mpr_JumpingOnPeople  AS  Jumping_On_People__c		
									,T1.mpr_JumpingOnFurniture  AS  Jumping_On_Furniture__c		
									,T1.mpr_MountingPeople  AS  Mounting_People__c		
									,T1.mpr_MountingObjects  AS  Mounting_Objects__c		
									,T1.mpr_Drooling  AS  Drooling__c		
									,T1.mpr_ChargingOutDoors  AS  Charging_Outdoors__c		
									,T1.mpr_PullingLungingOnLeash  AS  Pulling_Lunging_On_Leash__c		
									,T1.mpr_Stealing  AS  Stealing__c		
									,T1.mpr_SeparationAnxiety  AS  Separation_Anxiety__c		
									,T1.mpr_Balking  AS  Balking__c		
									,T1.mpr_KeepAway  AS  Keep_Away__c		
									,T1.mpr_TakesFoodRewardRoughly  AS  Takes_Food_Reward_Roughly__c		
							    	,CASE T1.mpr_TrafficReactions WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Traffic_Reactions__c		
									,CASE T1.mpr_OdorReactions WHEN 'Yes'  THEN 'TRUE' ELSE 'FALSE' END  AS  Odor_Reactions__c		
									,CASE T1.mpr_SmallAnimal WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Small_Animal__c		
									,T1.mpr_concern_behavior_other  AS  Concern_behavior_other__c		
									,T1.mpr_concern_noise_other  AS  Concern_noise_other__c		
									,T1.mpr_concern_animal  AS  Concern_animal__c		
								 	,T1.mpr_concern_traffic  AS  Concern_traffic__c		
									,T1.mpr_concern_surface_other  AS  Concern_surface_other__c		
									,CASE T1.mpr_concern_odor WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Concern_odor__c		
									,T1.mpr_concern_desc  AS  Concern_description__c		
									,T1.mpr_accomplishment  AS  Accomplishment__c		
									,T1.mpr_raiser_comments  AS  Raiser_comments__c		
									,T1.mpr_leader_comments  AS  Leader_comments__c		
									,T1.mpr_LeaderName  AS  Leader_Name__c		
									,T1.mpr_cfr_comments  AS  CFR_comments__c		
									,T1.mpr_FormGUID  AS  Form_GUID__c		
					 				,CAST(T1.mpr_CreatedDate AS DATE) AS  CreatedDate
									
					 INTO GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_mpr
					FROM GDB_TC1_kade.dbo.tblDogPuppyMonthlyProgressRpt AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.mpr_DogID
					LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON T3.psn_PersonID=T1.mpr_PersonID

 
END --tc1 : 32348

BEGIN-- AUDIT  IMP_PUPPY_REPORT_mpr

	SELECT * FROM GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_mpr

END 

BEGIN-- CREATE IMP_PUPPY_REPORT_pre 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
									,'PRE-'+CAST(T1.pre_EvalID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "PRE-" with pre_EvalID
									,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]  			-- Link  [tblDog].[dog_DogID]
									,T3.psn_PersonID AS [Contact__r:Legacy_Id__c]		-- Link  [tblPerson].[psn_PersonID]
									,'0123D0000004axMQAQ'  AS  RecordTypeID	-- Eval	
									,CAST(T1.pre_EvalDate AS DATE)  AS  Evaluation_Date__c		
									,T1.pre_TypeEval  AS  Type_of_Evaluation__c		
									,T1.pre_Request  AS  Request__c		
									,T1.pre_AgeinMonths  AS  Age_in_Months__c		
									,T4.AlterRequestType  AS  Alter_Decision__c	-- migrate value from [AlterRequestType]	-- Yes/Link [trefAlterDecision]
									,CAST(T1.pre_AlterDecisonDate AS DATE)  AS  Alter_Decision_Date__c		
									,CAST(T1.pre_DateLastSeen AS DATE)  AS  Date_Last_Seen__c		
									,T1.pre_HandlingHist  AS  Handling_History__c		
									,CASE T1.pre_Keepaway WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' end AS  Keep_away__c		
									,T1.pre_FoodRewardInterest  AS  Food_Reward_Interest__c		
									,T1.pre_Collar  AS  Collar__c		
									,T1.pre_K9Buddy  AS  K9_Buddy__c		
									,T1.pre_OverallScore  AS  Overall_Score__c		
									,T1.pre_Confidence  AS  Confidence__c		
									,T1.pre_DogReaction  AS  Dog_Reaction__c		
									,T1.pre_DistractionLevel  AS  Distraction_Level__c		
									,T1.pre_EaseHandling  AS  Ease_Handling__c		
									,T1.pre_TakingFood  AS  Taking_Food__c		
									,T1.pre_EditedMemoGeneral  AS  Edited_Memo_General__c		
									,T1.pre_EditedMemoSocial  AS  Edited_Memo_Social__c		
									,CAST(T1.pre_CreatedDate AS DATE) AS  CreatedDate		
									,T1.pre_FoodRewardInterestComments  AS  Food_Reward_Interest_Comments__c		
									,T1.pre_DietaryComments  AS  Dietary_Comments__c		
									,T1.pre_Socialization  AS  Socialization__c		
									,T1.pre_Comments  AS  Comments__c		
									,T1.pre_ConfidenceTxt  AS  Confidence_text__c		
									,T1.pre_Sensitivity  AS  Sensitivity__c		
									,T1.pre_Manageability  AS  Manageability__c		
									,T1.pre_Behavior  AS  Behavior__c		
									,T1.pre_Relieving  AS  Relieving__c		
									,T1.pre_Conclusion  AS  Conclusion__c		
						 
									 INTO GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_pre
									FROM GDB_TC1_kade.dbo.tblPRADogEval AS T1
									LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.pre_DogID
									LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON T3.psn_PersonID=T1.pre_PRA
									LEFT JOIN GDB_TC1_kade.dbo.trefAlterDecision AS T4 ON T4.AlterRequestID=T1.pre_AlterDecision
 
END --tc1 : 12565

BEGIN-- AUDIT  IMP_PUPPY_REPORT_pre

	SELECT * FROM GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_pre ORDER BY Legacy_Id__c

END 

BEGIN-- CREATE IMP_PUPPY_REPORT_pft 

									SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
										,'PFT-'+CAST(T1.pft_ID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "PRE-" with pre_EvalID
										,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]  			-- Link  [tblDog].[dog_DogID]
										,T3.psn_PersonID AS [Contact__r:Legacy_Id__c]		-- Link  [tblPerson].[psn_PersonID]
										,'0123D0000004eIDQAY'  AS  RecordTypeID	-- Fun Things	
										,CASE T1.pft_ProvideContactToCaretaker WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Provide_Contact_To_Caretaker__c
 										,T1.pft_Date AS Report_Date__c
										,T1.pft_GuardianName AS Guardian_Name__c
 										,T1.pft_NumberOfPuppysRaised  AS  Number_of_Puppies_Raised__c
										,T1.pft_PlayHabits  AS  Play_Habits__c
										,T1.pft_Environment  AS  Environment__c
										,T1.pft_AnimalsAndChildrenAtHome  AS  Animals_and_Children_At_Home__c
										,T1.pft_Behavior  AS  Behavior__c
										,T1.pft_BestAndToImproveBehaviors  AS  Three_behaviors_least_best__c
										,T1.pft_BestAndToImproveBehaviors  AS  Three_behaviors_best__c
										,T1.pft_AnythingElse  AS  Comments__c

  										,CAST(T1.pft_CreatedDate AS DATE) AS  CreatedDate		
								 
									 INTO GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_pft
									FROM GDB_TC1_kade.dbo.tblDogPuppyFunThings AS T1
									LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.pft_DogID
									LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON T3.psn_PersonID=T1.pft_PersonID
				
								 
END --tc1 : 12565

BEGIN-- AUDIT  IMP_PUPPY_REPORT_pft

	SELECT * FROM GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_pft

END 

SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_pfr
SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_mpr
SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_pre
SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_PUPPY_REPORT_pft


