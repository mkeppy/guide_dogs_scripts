USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblGuideStatus
			WHERE   SF_Object LIKE '%fi%' OR   [Translation_Rules] IS NOT NULL 
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'  OR  [Translation_Rules] IS NOT NULL 
END 

/*
SELECT * FROM GDB_TC1_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%fie%'
0123D0000004axBQAQ	Grad Report			Field_Report__c
0123D0000004axCQAQ	Guide Evaluation	Field_Report__c
*/


BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_FIELD_REPORT

END 

BEGIN -- CREATE IMP 

					SELECT   CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]() END AS OwnerId  -- Link [tblInstr].[db_InstrID]
 							,[Name] = 'Foster Care'
							,'GDS-'+CAST(T1.gds_GuideStatusID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'GDS-'+[gds_GuideStatusID]	
							,'0123D0000004axCQAQ' AS   RecordTypeID	-- Guide Evaluation
						  	,CAST(T1.gds_CreatedDate AS DATE) AS CreatedDate
					 
							,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
 							,NULL AS [Contact__r:Legacy_Id__c]
							,CAST(T1.gds_Date AS DATE) AS  Date__c	
						 	,CAST(T1.gds_TrainNotes AS NVARCHAR(MAX)) AS  Training_Notes__c	
							,NULL AS  Issue_Date__c		
							,NULL AS  Float__c		
							,NULL AS  Reason__c		
							,NULL AS  Graduation_Notes__c		
							,NULL AS  PRP_Name__c		
							,NULL AS  Name__c		
							,NULL AS  PRP_Date__c		
							,NULL AS  PRP_Notes__c		
							,NULL AS  Workout__c		
							,NULL AS  [Instructor__r:Legacy_Id__c]		 		 
							,NULL AS  Supervisor_Summary__c		
							,NULL AS  Disposition__c		
							,NULL AS  Director_of_training__c					
							,NULL AS Int_Position__c
							,NULL AS Visit_Type__c
							,'Foster Care' AS Visit_Category__c
							,NULL AS Appr_Training__c
							,NULL AS Comments__c
							,NULL AS Dog_Recommend__c
							,NULL AS Update_Information__c
							,NULL AS CS_Dog__c
							,NULL AS CS_Mechanics__c
							,NULL AS CS_Behavior__c
							,NULL AS CS_Home_Behavior__c
							,NULL AS CS_Relieving__c
							,NULL AS IG_Food_Type__c
							,NULL AS IG_Food_Ration__c
							,NULL AS IG_Dog_Weight__c
							,NULL AS IG_Health_Concerns__c
							,NULL AS IG_Ingested_Items__c
							,NULL AS IG_Food_Reward_Use__c
							,NULL AS IG_Techniques_Used__c
							,NULL AS IG_Home_Challenges__c
							,NULL AS IG_Environmental_Issues__c
							,NULL AS OC_Vison_Status__c
							,NULL AS OC_VWD_Line_Neg__c
							,NULL AS OC_VWD_Clearance_Neg__c
							,NULL AS OC_VWD_Decreasing_Effect__c
							,NULL AS OC_VWD_Positive_Influence__c
							,NULL AS OC_Overall_Health__c
							,NULL AS OC_Hearing__c
							,NULL AS OC_Emotional__c
							,NULL AS OC_Learning_Skills__c
							,NULL AS OC_Financial_Stability__c
							,NULL AS OC_Employment__c
							,NULL AS OC_Home_Condition__c
							,NULL AS OC_Mobility_Skills__c
							,NULL AS OC_Rapport_with_Dog__c
							,NULL AS OC_Manage_Dog__c
							,NULL AS OC_Reinforcement__c
							,NULL AS OC_Observed_Food_Reward__c
							,NULL AS OD_House_Behavior__c
							,NULL AS OD_Condition__c
							,NULL AS OD_BCS__c
							,NULL AS OD_Relieving_Issues__c
							,NULL AS OD_Perform_Guide_Work__c
							,NULL AS OD_UMT_RPrim__c
							,NULL AS OD_UMT_R2nd__c
							,NULL AS OD_UMT_PAverage__c
							,NULL AS OD_UMT_PTimeout__c
							,NULL AS OD_Distraction_Focus__c
							,NULL AS OD_Guide_Mechanics__c
							,NULL AS OD_Observed_Pace__c
							,NULL AS OD_Observed_Lead__c
							,NULL AS IP_Behavior_Overall__c
							,NULL AS IP_Pace_For_Client__c
							,NULL AS IP_Lead_For_Client__c
							,NULL AS IP_Stress_Relief__c
							,NULL AS IP_Team_Compatible__c
							,NULL AS IP_Home_Interview__c
							,NULL AS IP_Comply_WRecom__c
							,NULL AS IP_Time_With_Client__c
							,NULL AS Conclusion_Recommendation__c
 							,NULL AS Conclusion_Narrative__c
							,NULL AS Conclusion_Narrative_2__c
							,NULL AS Survey_Emailed__c		
							,T1.gds_GuideStatusID AS zrefId
							,'tblGuideStatus' AS zrefSrc
				 --	INTO GDB_TC1_migration.DBO.IMP_FIELD_REPORT
					FROM GDB_TC1_kade.dbo.tblGuideStatus AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.gds_DogID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.gds_InstrID
					WHERE T1.gds_Type='FC'
				UNION 

					SELECT  CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]() END AS OwnerId   
 							,[Name] = CASE WHEN T1.ev_EvalType IS NOT NULL THEN 'Evaluation ' + T1.ev_EvalType WHEN t1.ev_EvalType IS NULL THEN 'Evaluation' END 
							,'EV-'+CAST(T1.ev_Count AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "EV-" with ev_Count	
							,'0123D0000004axCQAQ' AS   RecordTypeID	-- Guide Evaluation	
							,CAST(T1.ev_CreatedDate AS DATE) AS CreatedDate
 							,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,NULL AS [Contact__r:Legacy_Id__c]
							,CAST(T1.ev_Date  AS DATE) AS  Date__c
							,NULL AS Training_Notes__c
 							,CAST(T1.ev_IssueDate AS DATE) AS  Issue_Date__c		
							,T1.ev_Float  AS  Float__c		
	 						,CAST(T1.ev_Reason   AS NVARCHAR(MAX))   AS  Reason__c		
							,CAST(T1.ev_Gradnotes  AS NVARCHAR(MAX)) AS  Graduation_Notes__c		
							,T1.ev_PRPName  AS  PRP_Name__c		
							,T1.ev_Name  AS  Name__c		
							,CAST(T1.ev_PRPDate AS DATE)  AS  PRP_Date__c		
							,CAST(T1.ev_PRPnotes  AS NVARCHAR(MAX))   AS  PRP_Notes__c		
							,CAST(T1.ev_WorkOut  AS NVARCHAR(MAX))  AS  Workout__c		
							,'Staff-'+ CAST(T3.FileNum  AS NVARCHAR(30))AS  [Instructor__r:Legacy_Id__c]		 		-- Link [tblStaff].[FileNum]
								
							,CAST(T1.ev_SupervisorSummary AS NVARCHAR(MAX)) AS  Supervisor_Summary__c		
							,T1.ev_Disp  AS  Disposition__c		
							,CAST(T1.ev_DOT AS DATE) AS  Director_of_training__c		
							
							,NULL AS Int_Position__c
							,T1.ev_EvalType   AS Visit_Type__c
							,'Evaluation' AS Visit_Category__c
							,NULL AS Appr_Training__c
							,NULL AS Comments__c
							,NULL AS Dog_Recommend__c
							,NULL AS Update_Information__c
							,NULL AS CS_Dog__c
							,NULL AS CS_Mechanics__c
							,NULL AS CS_Behavior__c
							,NULL AS CS_Home_Behavior__c
							,NULL AS CS_Relieving__c
							,NULL AS IG_Food_Type__c
							,NULL AS IG_Food_Ration__c
							,NULL AS IG_Dog_Weight__c
							,NULL AS IG_Health_Concerns__c
							,NULL AS IG_Ingested_Items__c
							,NULL AS IG_Food_Reward_Use__c
							,NULL AS IG_Techniques_Used__c
							,NULL AS IG_Home_Challenges__c
							,NULL AS IG_Environmental_Issues__c
							,NULL AS OC_Vison_Status__c
							,NULL AS OC_VWD_Line_Neg__c
							,NULL AS OC_VWD_Clearance_Neg__c
							,NULL AS OC_VWD_Decreasing_Effect__c
							,NULL AS OC_VWD_Positive_Influence__c
							,NULL AS OC_Overall_Health__c
							,NULL AS OC_Hearing__c
							,NULL AS OC_Emotional__c
							,NULL AS OC_Learning_Skills__c
							,NULL AS OC_Financial_Stability__c
							,NULL AS OC_Employment__c
							,NULL AS OC_Home_Condition__c
							,NULL AS OC_Mobility_Skills__c
							,NULL AS OC_Rapport_with_Dog__c
							,NULL AS OC_Manage_Dog__c
							,NULL AS OC_Reinforcement__c
							,NULL AS OC_Observed_Food_Reward__c
							,NULL AS OD_House_Behavior__c
							,NULL AS OD_Condition__c
							,NULL AS OD_BCS__c
							,NULL AS OD_Relieving_Issues__c
							,NULL AS OD_Perform_Guide_Work__c
							,NULL AS OD_UMT_RPrim__c
							,NULL AS OD_UMT_R2nd__c
							,NULL AS OD_UMT_PAverage__c
							,NULL AS OD_UMT_PTimeout__c
							,NULL AS OD_Distraction_Focus__c
							,NULL AS OD_Guide_Mechanics__c
							,NULL AS OD_Observed_Pace__c
							,NULL AS OD_Observed_Lead__c
							,NULL AS IP_Behavior_Overall__c
							,NULL AS IP_Pace_For_Client__c
							,NULL AS IP_Lead_For_Client__c
							,NULL AS IP_Stress_Relief__c
							,NULL AS IP_Team_Compatible__c
							,NULL AS IP_Home_Interview__c
							,NULL AS IP_Comply_WRecom__c
							,NULL AS IP_Time_With_Client__c
							,NULL AS Conclusion_Recommendation__c
 							,NULL AS Conclusion_Narrative__c
							,NULL AS Conclusion_Narrative_2__c
							,NULL AS Survey_Emailed__c	
							 ,T1.ev_Count AS zrefId
							,'tbl_GuideEval' AS zrefSrc

					FROM GDB_TC1_kade.dbo.tbl_GuideEval AS T1
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.ev_FileNum
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.ev_DogId
					LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T3 ON T3.FileNum=T1.ev_Instructor

				UNION 

					SELECT   CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]() END AS OwnerId-- Link [tblStaff].[FileNum]
 							,[Name] = T5.Visit_Category + ' ' + T5.VisitType
							,'GFU-'+CAST(T1.gfu_ID  AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'Gfu-'+[gfu_ID]	
							,'0123D0000004axBQAQ' AS RecordTypeID-- Graduate Field Report
							,CAST(T1.gfu_CreatedDate AS DATE) AS CreatedDate
							,NULL AS  [Dog__r:Legacy_Id__c]
  							,T4.psn_PersonID AS [Contact__r:Legacy_Id__c]	-- Link to Contact	-- Link [tblPersonRel].[prl_ClientInstanceID]
							,CAST(T1.gfu_Date AS DATE) AS  Date__c		
								,NULL AS  Training_Notes__c	
								,NULL AS  Issue_Date__c		
								,NULL AS  Float__c		
								,NULL AS  Evaluation_Type__c		
								,NULL AS  Reason__c		
								,NULL AS  Graduation_Notes__c		
								,NULL AS  PRP_Name__c		
								,NULL AS  Name__c		
								,NULL AS  PRP_Date__c		
								,NULL AS  PRP_Notes__c		
								,NULL AS  Workout__c		
								,NULL AS  [Instructor__r:Legacy_Id__c]		 		 
								,NULL AS  Supervisor_Summary__c		
								,NULL AS  Disposition__c		
								,NULL AS  Director_of_training__c	
							,T1.gfu_IntPosition  AS  Int_Position__c		
							,T5.VisitType  AS  Visit_Type__c			-- migrate value from [VisitType]	-- Ref [trefAGSVisitTypes].[VisitTypeID]
							,T5.Visit_Category AS  Visit_Category__c	-- migrate value from [Visit_Category]	-- Ref [trefAGSVisitTypes].[VisitTypeID]
							,T1.gfu_ApprTraining  AS  Appr_Training__c		
							,CAST(T1.gfu_Comments  AS NVARCHAR(MAX)) AS  Comments__c		
							,T1.gfu_DogRecommend  AS  Dog_Recommend__c		
							,T1.gfu_UpdateInfo  AS  Update_Information__c		
							,T1.gfu_CS_Dog  AS  CS_Dog__c		
							,T1.gfu_CS_Mechanics  AS  CS_Mechanics__c		
							,T1.gfu_CS_Behavior  AS  CS_Behavior__c		
							,T1.gfu_CS_HomeBehavior  AS  CS_Home_Behavior__c		
							,T1.gfu_CS_Relieving  AS  CS_Relieving__c		
							,T1.gfu_IG_FoodType  AS  IG_Food_Type__c		
							,T1.gfu_IG_FoodRation  AS  IG_Food_Ration__c		
							,T1.gfu_IG_DogWeight  AS  IG_Dog_Weight__c		
							,T1.gfu_IG_HealthConcerns  AS  IG_Health_Concerns__c		
							,T1.gfu_IG_IngestedItems  AS  IG_Ingested_Items__c		
							,T1.gfu_IG_FoodRewardUse  AS  IG_Food_Reward_Use__c		
							,T1.gfu_IG_TechniquesUsed  AS  IG_Techniques_Used__c		
							,T1.gfu_IG_HomeChallenges  AS  IG_Home_Challenges__c		
							,T1.gfu_IG_EnvironmentalIssues  AS  IG_Environmental_Issues__c		
							,T1.gfu_OC_VisionStatus  AS  OC_Vison_Status__c		
							,T1.gfu_OC_VWD_LineNeg  AS  OC_VWD_Line_Neg__c		
							,T1.gfu_OC_VWD_ClearanceNeg  AS  OC_VWD_Clearance_Neg__c		
							,T1.gfu_OC_VWD_DecreasingEffect  AS  OC_VWD_Decreasing_Effect__c		
							,T1.gfu_OC_VWD_PositiveInfluence  AS  OC_VWD_Positive_Influence__c		
							,T1.gfu_OC_OverallHealth  AS  OC_Overall_Health__c		
							,T1.gfu_OC_Hearing  AS  OC_Hearing__c		
							,T1.gfu_OC_Emotional  AS  OC_Emotional__c		
							,T1.gfu_OC_LearningSkills  AS  OC_Learning_Skills__c		
							,T1.gfu_OC_FinancialStubility  AS  OC_Financial_Stability__c		
							,T1.gfu_OC_Employment  AS  OC_Employment__c		
							,T1.gfu_OC_HomeCondition  AS  OC_Home_Condition__c		
							,T1.gfu_OC_MobilitySkills  AS  OC_Mobility_Skills__c		
							,T1.gfu_OC_RapportWDog  AS  OC_Rapport_with_Dog__c		
							,T1.gfu_OC_ManageDog  AS  OC_Manage_Dog__c		
							,T1.gfu_OC_Reinforcement  AS  OC_Reinforcement__c		
							,T1.gfu_OC_ObservedFoodReward  AS  OC_Observed_Food_Reward__c		
							,T1.gfu_OD_HouseBehavior  AS  OD_House_Behavior__c		
							,T1.gfu_OD_Condition  AS  OD_Condition__c		
							,T1.gfu_OD_BCS  AS  OD_BCS__c		
							,T1.gfu_OD_RelievingIssues  AS  OD_Relieving_Issues__c		
							,T1.gfu_OD_PerformGuideWork  AS  OD_Perform_Guide_Work__c		
							,T1.gfu_OD_UMT_RPrim  AS  OD_UMT_RPrim__c		
							,T1.gfu_OD_UMT_R2nd  AS  OD_UMT_R2nd__c		
							,T1.gfu_OD_UMT_PAverage  AS  OD_UMT_PAverage__c		
							,T1.gfu_OD_UMT_PTimeout  AS  OD_UMT_PTimeout__c		
							,T1.gfu_OD_DistractionFocus  AS  OD_Distraction_Focus__c		
							,T1.gfu_OD_GuideMachanics  AS  OD_Guide_Mechanics__c		
							,T1.gfu_OD_ObservedPace  AS  OD_Observed_Pace__c		
							,T1.gfu_OD_ObservedLead  AS  OD_Observed_Lead__c		
							,T1.gfu_IP_BehaviorOverall  AS  IP_Behavior_Overall__c		
							,T1.gfu_IP_PaceForClient  AS  IP_Pace_For_Client__c		
							,T1.gfu_IP_LeadForClient  AS  IP_Lead_For_Client__c		
							,T1.gfu_IP_StressRelief  AS  IP_Stress_Relief__c		
							,T1.gfu_IP_TeamCompatible  AS  IP_Team_Compatible__c		
							,T1.gfu_IP_HomeInterview  AS  IP_Home_Interview__c		
							,T1.gfu_IP_ComplyWRecom  AS  IP_Comply_WRecom__c		
							,T1.gfu_IP_TimeWithClient  AS  IP_Time_With_Client__c		
							,T1.gfu_Conclusions_Recomm  AS  Conclusion_Recommendation__c		
							,CAST(T1.gfu_Conclusions_Narrative AS NVARCHAR(MAX)) AS  Conclusion_Narrative__c		
							,CAST(T1.gfu_Conclusions_Narrative2 AS NVARCHAR(MAX))   AS  Conclusion_Narrative_2__c		
							,T2.hve_Email AS Survey_Emailed__c	

							,T1.gfu_ID AS zrefId
							,'tblGradFieldReport' AS zrefSrc
					FROM GDB_TC1_kade.dbo.tblGradFieldReport AS T1 
					LEFT JOIN GDB_TC1_kade.dbo.tblGFRSurveyEmail AS T2 ON T2.hve_GFRID=T1.gfu_ID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.gfu_ByWhom
				    LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.gfu_ClientID
							LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_ClientInstanceID
				 	LEFT JOIN GDB_TC1_kade.dbo.trefAGSVisitTypes AS T5 ON T5.VisitTypeID=T1.gfu_VisitType


				 
END ---tc1: 58439

BEGIN-- AUDIT
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_FIELD_REPORT 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_FIELD_REPORT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_TC1_migration.dbo.IMP_FIELD_REPORT 
	GROUP BY zrefSrc

	SELECT * FROM GDB_TC1_migration.dbo.IMP_FIELD_REPORT ORDER BY [Dog__r:Legacy_Id__c]
	
	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_FIELD_REPORT
	 

END 