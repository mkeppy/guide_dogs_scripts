USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblVolCalWebTourRequest
			WHERE   SF_Object LIKE '%lead%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%xxxxxxxxxxxxxxxxxxxx%'
END 

BEGIN 

	DROP TABLE GDB_TC1_migration.DBO.IMP_LEAD

END 

BEGIN --CREATE LEAD
				SELECT DISTINCT
				  GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerID 
				,T1.Uploaded  AS  Uploaded__c		
				,T1.ID  AS  Legacy_ID__c		
				,T1.First  AS  FirstName		
				,T1.Last  AS  LastName		
				,'Individual' AS [Company]   -- add 'Individual' as default for required field [company]
				,T1.Email  AS  Email		
				,T1.Address  AS  Street		
				,T1.City  AS  City		
				,T1.State  AS  State		
				,T1.Zip  AS  PostalCode		
				,T1.Phone  AS  Phone		
				,T1.Campus  AS  Campus__C		
				,T1.TourType  AS  Tour_Type__c		
				,T1.SchoolName  AS  School_Name__c		
				,T1.TeacherName  AS  Teacher_Name__c		
				,T1.Grade  AS  Grade__c		
				,T1.PreferredDates  AS  Preferred_Dates__c		
				,T1.NumOfKids  AS  Number_of_Kids__c		
				,T1.NumOfAdults  AS  Number_of_Adultas__c		
				,T1.Requirements  AS  Requirements__c		
				,T1.HowFound  AS  How_Found__c		
				,CAST(T1.CreateDate AS DATE) AS  CreatedDate		
				INTO GDB_TC1_migration.dbo.IMP_LEAD
				FROM GDB_TC1_kade.DBO.tblVolCalWebTourRequest AS T1
 
END --tc1: 720


SELECT * FROM  GDB_TC1_migration.dbo.IMP_LEAD



