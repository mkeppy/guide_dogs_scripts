USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblVetCommPostOp
			WHERE   SF_Object LIKE '%post%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

SELECT * FROM GDB_TC1_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%post%'


BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_VET_POST_CARE


END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 						,'VPO-'+CAST(T1.vpo_PostOpID  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'vpo'+[vpo_PostOpId]	
						,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Entry__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
						,CASE T1.vpo_DischargeType WHEN 1 THEN 'Surgical' WHEN 2 THEN 'Non-surgical' WHEN 3 THEN 'Dental' END  AS  Discharge_Type__c	-- 1 = Surigcal 2 = Non-Surgical 3 = Dental	
						,CAST(T1.vpo_ProcDate AS DATE) AS  Procedure_Date__c		
						,T3.dog_DogID  AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
						,T1.vpo_Procedures  AS  Procedures__c		
						,T1.vpo_TestPending  AS  Test_Pending__c		
						,T1.vpo_PostOpInstr  AS  PostOp_Instructions__c		
						,T1.vpo_ToothNum1  AS  Tooth_Number_1__c		
						,T1.vpo_ToothNum2  AS  Tooth_Number_2__c		
						,T1.vpo_ToothNum3  AS  Tooth_Number_3__c		
						,T1.vpo_ToothReason1  AS  Tooth_Reason_1__c		
						,T1.vpo_ToothReason2  AS  Tooth_Reason_2__c		
						,T1.vpo_ToothReason3  AS  Tooth_Reason_3__c		
						,T1.vpo_OralCare  AS  Oral_Care__c		
						,T1.vpo_Exercise  AS  Exercise__c		
						,T1.vpo_Diet  AS  Diet__c		
						,T1.vpo_Medications  AS  Medications__c		
						,CAST(T1.vpo_Recheck AS DATE) AS  Recheck_Date__c		
						,CAST(T1.vpo_ReminderDate AS DATE) AS  Reminder_Date__c		   
						,NULL AS Memo_Recipient__c  --filler
						,NULL AS Memo_Date_Sent__c  --filler
						,NULL AS Memo__c --filler
						,NULL AS Description__c--filler
						,NULL AS Amount__c--filler
						,CAST(T1.vpo_CreatedDate AS DATE) AS CreatedDate
						,'0123D0000004f8zQAA' AS RecordTypeId --Post Operative
 
						,t1.vpo_PostOpID AS zrefId
						,'tblVetCommPostOp' AS zrefSrc
					
				 	INTO GDB_TC1_migration.dbo.IMP_VET_POST_CARE
					FROM GDB_TC1_kade.dbo.tblVetCommPostOp AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vpo_VetEntryID
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T3 ON T3.dog_DogID=T1.vpo_DogID
					 

				UNION 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,'VDL-'+CAST(T1.vdl_VetEntryMemoID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'vdl'+[vdl_VetEntryMemoID]	
						,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Entry__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
						
						--filler
						,NULL AS Discharge_Type__c	 
						,NULL AS Procedure_Date__c		
						,NULL AS [Dog__r:Legacy_Id__c]		 
						,NULL AS Procedures__c		
						,NULL AS Test_Pending__c		
						,NULL AS PostOp_Instructions__c		
						,NULL AS Tooth_Number_1__c		
						,NULL AS Tooth_Number_2__c		
						,NULL AS Tooth_Number_3__c		
						,NULL AS Tooth_Reason_1__c		
						,NULL AS Tooth_Reason_2__c		
						,NULL AS Tooth_Reason_3__c		
						,NULL AS Oral_Care__c		
						,NULL AS Exercise__c		
						,NULL AS Diet__c		
						,NULL AS Medications__c		
						,NULL AS Recheck_Date__c		
						,NULL AS Reminder_Date__c		   
 						,T1.vdl_MemoRecip  AS  Memo_Recipient__c		
						,CAST(T1.vdl_DateMemoSent AS DATE) AS  Memo_Date_Sent__c		
						,CAST(T1.vdl_VetMemo AS NVARCHAR(MAX)) AS  Memo__c		
 						,NULL AS Description__c
						,NULL AS Amount__c
 						,CAST(T1.vdl_CreatedDate AS DATE) AS CreatedDate
						,'0123D0000004f94QAA'  AS  RecordTypeID	-- Memo	


						,t1.vdl_VetEntryMemoID AS zrefId
						,'tblVetEntryMemo' AS zrefSrc
					FROM GDB_TC1_kade.dbo.tblVetEntryMemo AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vdl_VetEntryID

				UNION
					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,'VDM-'+CAST(T1.vdm_MedID AS NVARCHAR(30)) AS  Legacy_ID__c		
						,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Entry__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
						
						--filler
						,NULL AS Discharge_Type__c	 
						,NULL AS Procedure_Date__c		
						,NULL AS [Dog__r:Legacy_Id__c]		 
						,NULL AS Procedures__c		
						,NULL AS Test_Pending__c		
						,NULL AS PostOp_Instructions__c		
						,NULL AS Tooth_Number_1__c		
						,NULL AS Tooth_Number_2__c		
						,NULL AS Tooth_Number_3__c		
						,NULL AS Tooth_Reason_1__c		
						,NULL AS Tooth_Reason_2__c		
						,NULL AS Tooth_Reason_3__c		
						,NULL AS Oral_Care__c		
						,NULL AS Exercise__c		
						,NULL AS Diet__c		
						,NULL AS Medications__c		
						,NULL AS Recheck_Date__c		
						,NULL AS Reminder_Date__c		   
 						,NULL AS Memo_Recipient__c		
						,NULL AS Memo_Date_Sent__c		
						,NULL AS Memo__c
 						,T1.vii_Description  AS  Description__c		
						,T1.Amount  AS  Amount__c		
						,NULL AS CreatedDate
						,'0123D0000004f8zQAA'  AS  RecordTypeID	 -- Post Operative	

						,t1.vdm_MedID AS zrefId
						,'tblVetMed_Local' AS zrefSrc
					FROM GDB_TC1_kade.dbo.tblVetMed_Local AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vdl_VetEntryID

END   ---tc1: 198488

BEGIN-- AUDIT
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_VET_POST_CARE 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_VET_POST_CARE GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_TC1_migration.dbo.IMP_VET_POST_CARE 
	GROUP BY zrefSrc

	SELECT * FROM GDB_TC1_migration.dbo.IMP_VET_POST_CARE ORDER BY [Vet_Entry__r:Legacy_Id__c]
	
	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_VET_POST_CARE
	 

END 