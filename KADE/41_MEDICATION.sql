USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblVetMedication
			WHERE   SF_Object LIKE '%med%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 




BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_MEDICATION

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,[Name] = T3.ii_Description
							,'VDM-'+CAST(T1.vdm_MedID  AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'vdm'+[vdm_MedID]	
							,'VDL-'+CAST(T2.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Record__r:Legacy_Id__c]		-- Link [tblVetEntry].[VetEntryID]
							,'Ii-'+CAST(T3.ii_ItemID  AS NVARCHAR(30)) AS  [Medicine__r:Legacy_Id__c]		-- trefGDBInventroyItems.ii_ItemID
							,T1.vdm_Quantity  AS  Quantity__c		
							,T1.vdm_Directions  AS  Direction__c		
							,CAST(T1.vdm_CreatedDate AS DATE) AS CreatedDate
				 	INTO GDB_TC1_migration.dbo.IMP_MEDICATION
					FROM GDB_TC1_kade.dbo.tblVetMedication AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblVetEntry AS T2 ON T2.vdl_VetEntryID=T1.vdm_VetEntryID
					LEFT JOIN GDB_TC1_kade.dbo.trefGDBInventroyItems AS T3 ON T3.ii_ItemID=T1.vdm_MedCode
		 
 

END 

BEGIN-- AUDIT


	SELECT * FROM   GDB_TC1_migration.dbo.IMP_MEDICATION


END 