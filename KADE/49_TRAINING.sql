USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblTRNBackUpTest
			WHERE   SF_Object LIKE '%trai%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*
SELECT * FROM GDB_TC1_migration.dbo.XTR_RECORD_TYPE  WHERE SOBJECTTYPE LIKE '%train%'
0123D0000004axPQAQ	Passback	Training__c
0123D0000004axQQAQ	Testing	Training__c
0123D0000004axRQAQ	Traffic	Training__c
0123D0000004axSQAQ	Weekly	Training__c
*/

BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_TRAINING

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
						,'TRT-'+CAST(T1.trt_TrafficID AS NVARCHAR(30)) AS  Legacy_ID__c	-- migrate 'Trt-'+[trt_TrafficID]	
						,NULL AS [Contact__r:Legacy_Id__c] --filler
						,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
						,'Staff-'+ CAST(T3.FileNum  AS NVARCHAR(30))   AS  [Instructor_Trainer__r:Legacy_Id__c]		-- Link [tblStaff].[FileNum]
						,NULL AS  [String__r:Legacy_Id__c] 
							,NULL AS  [Parent_Training__r:Legacy_Id__c]
							,NULL AS  Backup_Test__c

 								,NULL AS Pass_Back_Date__c		
								,NULL AS Pass_Back_Confirmed__c		
 								,NULL AS [New_Instructor_Trainer__r:Legacy_Id__c]	 
								,NULL AS Pass_To_Be_Determined__c		
								,NULL AS Doubling_Issues__c		
								,NULL AS Filth_Eater__c		
								,NULL AS Community_Issues__c		
								,NULL AS Eating_Issues__c		
								,NULL AS Counter_Conditioning__c		
								,NULL AS Relieving_Issues__c		
								,NULL AS Harness_Sensitivity__c		
								,NULL AS Soundness_Issues__c		
 								,NULL AS Health_Issues__c		
								,NULL AS Trainer_Restrictions__c		
								,NULL AS Other_Kennel_Issues__c		
								,NULL AS Reason_for_Pass_Back__c		
 					 			,NULL AS [New_String__r:Legacy_Id__c] 

							,NULL AS  Test_Date__c		--filler
							,NULL AS  Tester__c		 --filler
							,NULL AS  Test_Type__c		--filler 
							,NULL AS  Grade__c		 --filler
							,NULL AS  Comments__c	--filler	
							,NULL AS  Preliminary_GW_Pace__c	--filler	
							,NULL AS  Final_GW_Pace__c	--filler	
							,NULL AS  Route__c	--filler

						,T1.trt_TrafficTest  AS  Traffic_Test__c		
						,CAST(T1.trt_Date  AS DATE) AS  Date__c		
						,T1.trt_WorkoutNumber  AS  Workout_Number__c		
						,X1.ID  AS  Driver__c					-- Link [tblStaff].[FileNum]
						,T1.trt_AcceptableResponse  AS  Acceptable_Response__c		
						,T1.trt_BackupTRNRequired  AS  Backup_Training_Required__c		
							--filler
							,NULL AS Train_Week__c		
							,NULL AS Float__c		
 							,NULL AS PB__c		
							,NULL AS Current_Instructor__c		
							,NULL AS Current_Weekly_Report__c		
							,NULL AS Hold__c		
							,NULL AS Phase__c		
							,NULL AS Total_Workouts__c		
							,NULL AS Total_Count__c		
							,NULL AS First_Date__c		
							,NULL AS Counter_Con__c		
							,NULL AS Previous_Experience__c		
							,NULL AS CLR__c		
							,NULL AS RG__c		
							,NULL AS Relieving_Program__c		
							,NULL AS Health__c		
							,NULL AS Breeding__c		
							,NULL AS Control__c		
							,NULL AS Temperament__c		
							,NULL AS Body_Sensitivity__c		
							,NULL AS Rough_Food__c		
							,NULL AS Sec_R_Concern__c		
							,NULL AS High_Value__c		
							,NULL AS Train_Notes__c		
							,NULL AS Archive__c		
							,NULL AS Non_Escalator__c		
							,NULL AS Normal_Progress__c	
								,NULL  AS  Traffic__c		
							,NULL  AS  City__c		
							,NULL  AS  Revolving_Door__c		
							,NULL  AS  Escalators__c		
							,NULL  AS  Public_Transit__c		
							,NULL  AS  Night_Route__c		
							,NULL  AS  Sidewalkless__c		
							,NULL  AS  Country_Road__c		
							,NULL  AS  Rounded_Corners__c		
							,NULL  AS  Freelance__c		
							,NULL  AS  Bus_Lounge__c		
							,NULL  AS  College_Campus__c		
							,NULL  AS  Underground_Tran__c		
							,NULL  AS  Nursing_Home__c		
							,NULL  AS  Self_Orientation__c		
							,NULL  AS  Wheelchair_Exposure__c		
							,NULL   AS  Kid_Exposure__c		
							,NULL  AS  Additional_SWL__c		
							,NULL  AS  Offset_Crossings__c		
							,NULL  AS  Wide_Crossing__c		
							,NULL  AS  Island_Crossing__c		
							,NULL  AS  Custom_Route__c	
										
						,CAST(T1.trt_CreatedDate AS DATE) AS CreatedDate
						,'0123D0000004axRQAQ'  AS  RecordTypeId	-- Traffic	
 						,T1.trt_TrafficID AS zrefId
						,'1-tblTRNTrafficTest' AS zrefSrc

					 INTO GDB_TC1_migration.DBO.IMP_TRAINING
					FROM GDB_TC1_kade.dbo.tblTRNTrafficTest AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.trt_DogID
					LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T3 ON T3.FileNum=T1.trt_Trainer
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.trt_Driver

				UNION 

 					SELECT   DISTINCT
							GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'WK-'+CAST(T1.wk_WklyTrainID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "WK-" with wk_WklyTrainID	
							,NULL AS [Contact__r:Legacy_Id__c]  --filler
							,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]			-- Link [tblDog].[dog_DogID]
							,'Staff-'+ CAST(T3.FileNum  AS NVARCHAR(30))AS  [Instructor_Trainer__r:Legacy_Id__c]		-- Link [tblStaff].[FileNum]
							,T5.Legacy_Id__c   AS  [String__r:Legacy_Id__c]   	-- Link [tblClass].[cls_FirstString] 
							,NULL AS  [Parent_Training__r:Legacy_Id__c]
							,NULL AS  Backup_Test__c

 								,NULL AS Pass_Back_Date__c		
								,NULL AS Pass_Back_Confirmed__c		
 								,NULL AS [New_Instructor_Trainer__r:Legacy_Id__c]	 
								,NULL AS Pass_To_Be_Determined__c		
								,NULL AS Doubling_Issues__c		
								,NULL AS Filth_Eater__c		
								,NULL AS Community_Issues__c		
								,NULL AS Eating_Issues__c		
								,NULL AS Counter_Conditioning__c		
								,NULL AS Relieving_Issues__c		
								,NULL AS Harness_Sensitivity__c		
								,NULL AS Soundness_Issues__c		
 								,NULL AS Health_Issues__c		
								,NULL AS Trainer_Restrictions__c		
								,NULL AS Other_Kennel_Issues__c		
								,NULL AS Reason_for_Pass_Back__c		
 					 			,NULL AS [New_String__r:Legacy_Id__c] 

							,NULL AS  Test_Date__c		--filler
							,NULL AS  Tester__c		 --filler
							,NULL AS  Test_Type__c		--filler 
							,NULL AS  Grade__c		 --filler
							,NULL AS  Comments__c	--filler	
							,NULL AS  Preliminary_GW_Pace__c	--filler	
							,NULL AS  Final_GW_Pace__c	--filler	
							,NULL AS  Route__c	--filler

							,NULL AS Traffic_Test__c --filler
							,CAST(T1.wk_Date AS DATE)  AS  Date__c
							,NULL AS Workout_Number__c   --filler
							,NULL AS Driver__c	 --filler
							,NULL AS Acceptable_Response__c --filler
							,NULL AS Backup_Training_Required__c --filler
							
							,T1.wk_TrainWeek  AS  Train_Week__c		
							,CASE T1.wk_Float WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Float__c		
 							,CASE T1.wk_PB WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  PB__c		
							,CASE T1.wk_CurrInstr WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Current_Instructor__c		
							,CASE T1.wk_CurWklyRpt WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Current_Weekly_Report__c		
							,CASE T1.wk_Hold WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Hold__c		
							,T1.wk_Phase  AS  Phase__c		
							,T1.wk_TotWkouts  AS  Total_Workouts__c		
							,T1.wk_TotCnt  AS  Total_Count__c		
							,CAST(T1.wk_FirstDate  AS DATE) AS  First_Date__c		
							,CASE T1.wk_CounterCon  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Counter_Con__c		
							,CASE T1.wk_PreExper  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Previous_Experience__c		
							,CASE T1.wk_CLR  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  CLR__c		
							,CASE T1.wk_RG   WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS  RG__c		
							,CASE T1.wk_RelievingProgram  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Relieving_Program__c		
							,CASE T1.wk_Health  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Health__c		
							,CASE T1.wk_Breeding  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Breeding__c		
							,CASE T1.wk_Control  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Control__c		
							,CASE T1.wk_Temperament  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Temperament__c		
							,CASE T1.wk_BodySensitivity  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Body_Sensitivity__c		
							,CASE T1.wk_RoughFood  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Rough_Food__c		
							,CASE T1.wk_SecRConcern  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Sec_R_Concern__c		
							,CASE T1.wk_HighValue  WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  High_Value__c		
							,CAST(T1.wk_TrainNotes AS NVARCHAR(MAX)) AS  Train_Notes__c		
							,CASE T1.wk_Archive  WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Archive__c		
							,CASE T1.wk_NonEscalator   WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Non_Escalator__c		
							,CASE T1.wk_NormalProgress   WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  Normal_Progress__c		
							,NULL  AS  Traffic__c		
							,NULL  AS  City__c		
							,NULL  AS  Revolving_Door__c		
							,NULL  AS  Escalators__c		
							,NULL  AS  Public_Transit__c		
							,NULL  AS  Night_Route__c		
							,NULL  AS  Sidewalkless__c		
							,NULL  AS  Country_Road__c		
							,NULL  AS  Rounded_Corners__c		
							,NULL  AS  Freelance__c		
							,NULL  AS  Bus_Lounge__c		
							,NULL  AS  College_Campus__c		
							,NULL  AS  Underground_Tran__c		
							,NULL  AS  Nursing_Home__c		
							,NULL  AS  Self_Orientation__c		
							,NULL  AS  Wheelchair_Exposure__c		
							,NULL  AS  Kid_Exposure__c		
							,NULL  AS  Additional_SWL__c		
							,NULL  AS  Offset_Crossings__c		
							,NULL  AS  Wide_Crossing__c		
							,NULL  AS  Island_Crossing__c	
							,NULL  AS  Custom_Route__c	


 							,CAST(T1.wk_CreatedDate AS DATE) AS  CreatedDate		
							,'0123D0000004axSQAQ'  AS  RecordTypeID	-- Weekly
 							,T1.wk_WklyTrainID AS zrefId
							,'2-tbl_WklyTrain' AS zrefSrc	

						FROM GDB_TC1_kade.dbo.tbl_WklyTrain AS T1
						LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.wk_DogId
						LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T3 ON T3.FileNum=T1.wk_InstrId
						--camp  
						LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T4 ON  T4.cls_FirstString =T1.wk_StringId
						LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T5 ON T5.Legacy_Id__c=('cls-'+T4.cls_FirstString )
			 
				UNION 

 					SELECT  DISTINCT   
							GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'CAW-'+CAST(T1.caw_ID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "CAW-" with caw_ID	
							,T4.psn_PersonID AS [Contact__r:Legacy_Id__c]-- Link [tblPersonRel].[prl_ClientInstanceID]
						 	,T6.drl_DogID  AS  [Dog__r:Legacy_Id__c]	-- link to [tblPersonRel] to link to [tblPersonDog] to link to [tblDogRel] to link to [tblDog]	-- Link [tblPersonRel].[prl_ClientInstanceID]		
							,NULL AS [Instructor_Trainer__r:Legacy_Id__c]--filler
							,NULL AS [String__r:Legacy_Id__c] --filler
							,NULL AS  [Parent_Training__r:Legacy_Id__c]
							,NULL AS  Backup_Test__c

 								,NULL AS Pass_Back_Date__c		
								,NULL AS Pass_Back_Confirmed__c		
 								,NULL AS [New_Instructor_Trainer__r:Legacy_Id__c]	 
								,NULL AS Pass_To_Be_Determined__c		
								,NULL AS Doubling_Issues__c		
								,NULL AS Filth_Eater__c		
								,NULL AS Community_Issues__c		
								,NULL AS Eating_Issues__c		
								,NULL AS Counter_Conditioning__c		
								,NULL AS Relieving_Issues__c		
								,NULL AS Harness_Sensitivity__c		
								,NULL AS Soundness_Issues__c		
 								,NULL AS Health_Issues__c		
								,NULL AS Trainer_Restrictions__c		
								,NULL AS Other_Kennel_Issues__c		
								,NULL AS Reason_for_Pass_Back__c		
 					 			,NULL AS [New_String__r:Legacy_Id__c] 

							,NULL AS  Test_Date__c		--filler
							,NULL AS  Tester__c		 --filler
							,NULL AS  Test_Type__c		--filler 
							,NULL AS  Grade__c		 --filler
							,NULL AS  Comments__c	--filler	
							,NULL AS  Preliminary_GW_Pace__c	--filler	
							,NULL AS  Final_GW_Pace__c	--filler	
							,NULL AS  Route__c	--filler
							
							,NULL AS Traffic_Test__c --filler
							,NULL AS Date__c --filler
							,NULL AS Workout_Number__c   --filler
							,NULL AS Driver__c	 --filler
							,NULL AS Acceptable_Response__c --filler
							,NULL AS Backup_Training_Required__c --filler
							--filler
							,NULL AS Train_Week__c		
							,NULL AS Float__c		
 							,NULL AS PB__c		
							,NULL AS Current_Instructor__c		
							,NULL AS Current_Weekly_Report__c		
							,NULL AS Hold__c		
							,NULL AS Phase__c		
							,NULL AS Total_Workouts__c		
							,NULL AS Total_Count__c		
							,NULL AS First_Date__c		
							,NULL AS Counter_Con__c		
							,NULL AS Previous_Experience__c		
							,NULL AS CLR__c		
							,NULL AS RG__c		
							,NULL AS Relieving_Program__c		
							,NULL AS Health__c		
							,NULL AS Breeding__c		
							,NULL AS Control__c		
							,NULL AS Temperament__c		
							,NULL AS Body_Sensitivity__c		
							,NULL AS Rough_Food__c		
							,NULL AS Sec_R_Concern__c		
							,NULL AS High_Value__c		
							,NULL AS Train_Notes__c		
							,NULL AS Archive__c		
							,NULL AS Non_Escalator__c		
							,NULL AS Normal_Progress__c

							,T1.caw_Traffic  AS  Traffic__c		
							,T1.caw_City  AS  City__c		
							,T1.caw_RevolvingDoor  AS  Revolving_Door__c		
							,T1.caw_Escalators  AS  Escalators__c		
							,T1.caw_PublicTransit  AS  Public_Transit__c		
							,T1.caw_NightRoute  AS  Night_Route__c		
							,T1.caw_Sidewalkless  AS  Sidewalkless__c		
							,T1.caw_CountryRoad  AS  Country_Road__c		
							,T1.caw_RoundedCorners  AS  Rounded_Corners__c		
							,T1.caw_Freelance  AS  Freelance__c		
							,T1.caw_BusLounge  AS  Bus_Lounge__c		
							,T1.caw_CollegeCampus  AS  College_Campus__c		
							,T1.caw_UndergroundTran  AS  Underground_Tran__c		
							,T1.caw_NursingHome  AS  Nursing_Home__c		
							,T1.caw_SelfOrientation  AS  Self_Orientation__c		
							,T1.caw_WheelchairExposure  AS  Wheelchair_Exposure__c		
							,T1.caw_KidExposure  AS  Kid_Exposure__c		
							,T1.caw_AdditionalSWL  AS  Additional_SWL__c		
							,T1.caw_OffsetCrossings  AS  Offset_Crossings__c		
							,T1.caw_WideCrossing  AS  Wide_Crossing__c		
							,T1.caw_IslandCrossing  AS  Island_Crossing__c		
 							,CAST(T1.caw_CustomRoute AS NVARCHAR(MAX)) AS  Custom_Route__c		
				 
							,CAST(T1.caw_CreatedDate AS DATE) AS  CreatedDate		
							,'0123D0000004axQQAQ'  AS  RecordTypeID	-- Testing
 							,T1.caw_ID AS zrefId
							,'3-tblClassAreasWorked' AS zrefSrc	
						FROM GDB_TC1_kade.dbo.tblClassAreasWorked AS T1
					 	LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.caw_ClientID
							LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_ClientInstanceID
				 		LEFT JOIN GDB_TC1_kade.dbo.tblPersonDog AS T5 ON T5.pd_PersonRelID=T3.prl_RelationID
						INNER JOIN GDB_TC1_kade.dbo.tblDogRel AS T6 ON T6.drl_RelationID=T5.pd_DogRelID
							LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T7 ON T7.dog_DogID=T6.drl_DogID
						 
					UNION 		
						
						SELECT  DISTINCT   
							GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId 
 							,'TDT-'+CAST(T1.tdt_TestingID AS NVARCHAR(30)) AS  Legacy_ID	-- concatenate 'tdt-'+[tdt_TestingId]	
							,NULL AS [Contact__r:Legacy_Id__c] 
							,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,'Staff-'+ CAST(T3.FileNum  AS NVARCHAR(30))   AS  [Instructor_Trainer__r:Legacy_Id__c]		-- Link [tblStaff].[FileNum]
							,T5.Legacy_Id__c   AS  [String__r:Legacy_Id__c]   	-- Link [tblClass].[cls_FirstString] 
 							,NULL AS  [Parent_Training__r:Legacy_Id__c]
							,NULL AS  Backup_Test__c

 								,NULL AS Pass_Back_Date__c		
								,NULL AS Pass_Back_Confirmed__c		
 								,NULL AS [New_Instructor_Trainer__r:Legacy_Id__c]	 
								,NULL AS Pass_To_Be_Determined__c		
								,NULL AS Doubling_Issues__c		
								,NULL AS Filth_Eater__c		
								,NULL AS Community_Issues__c		
								,NULL AS Eating_Issues__c		
								,NULL AS Counter_Conditioning__c		
								,NULL AS Relieving_Issues__c		
								,NULL AS Harness_Sensitivity__c		
								,NULL AS Soundness_Issues__c		
 								,NULL AS Health_Issues__c		
								,NULL AS Trainer_Restrictions__c		
								,NULL AS Other_Kennel_Issues__c		
								,NULL AS Reason_for_Pass_Back__c		
 					 			,NULL AS [New_String__r:Legacy_Id__c] 


 							,CAST(T1.tdt_TestDate AS DATE) AS  Test_Date__c		
							,X1.ID  AS  Tester__c		-- Link [tblStaff].[FileNum]
							,T1.tdt_TestType  AS  Test_Type__c		-- Ref [trefTRNDogTestType].[dt_DogTestID]
							,T1.tdt_Grade  AS  Grade__c		-- Yes/Link?
							,CAST(T1.tdt_Comments  AS NVARCHAR(MAX)) AS  Comments__c		
							,T1.tdt_PrelimGWPace  AS  Preliminary_GW_Pace__c		
							,T1.tdt_FinalGWPace  AS  Final_GW_Pace__c		
							,CAST(T1.tdt_Route AS NVARCHAR(MAX))   AS  Route__c							
							
							,NULL AS Traffic_Test__c --filler
							,NULL AS Date__c --filler
							,T1.tdt_WorkoutNumber  AS  Workout_Number__c
							,NULL AS Driver__c	 --filler
							,NULL AS Acceptable_Response__c --filler
							,NULL AS Backup_Training_Required__c --filler
			  				,NULL AS Train_Week__c		
							,NULL AS Float__c		
 							,NULL AS PB__c		
							,NULL AS Current_Instructor__c		
							,NULL AS Current_Weekly_Report__c		
							,NULL AS Hold__c		
							,NULL AS Phase__c		
							,NULL AS Total_Workouts__c		
							,NULL AS Total_Count__c		
							,NULL AS First_Date__c		
							,NULL AS Counter_Con__c		
							,NULL AS Previous_Experience__c		
							,NULL AS CLR__c		
							,NULL AS RG__c		
							,NULL AS Relieving_Program__c		
							,NULL AS Health__c		
							,NULL AS Breeding__c		
							,NULL AS Control__c		
							,NULL AS Temperament__c		
							,NULL AS Body_Sensitivity__c		
							,NULL AS Rough_Food__c		
							,NULL AS Sec_R_Concern__c		
							,NULL AS High_Value__c		
							,NULL AS Train_Notes__c		
							,NULL AS Archive__c		
							,NULL AS Non_Escalator__c		
							,NULL AS Normal_Progress__c
							,NULL  AS  Traffic__c		
							,NULL  AS  City__c		
							,NULL  AS  Revolving_Door__c		
							,NULL  AS  Escalators__c		
							,NULL  AS  Public_Transit__c		
							,NULL  AS  Night_Route__c		
							,NULL  AS  Sidewalkless__c		
							,NULL  AS  Country_Road__c		
							,NULL  AS  Rounded_Corners__c		
							,NULL  AS  Freelance__c		
							,NULL  AS  Bus_Lounge__c		
							,NULL  AS  College_Campus__c		
							,NULL  AS  Underground_Tran__c		
							,NULL  AS  Nursing_Home__c		
							,NULL  AS  Self_Orientation__c		
							,NULL  AS  Wheelchair_Exposure__c		
							,NULL  AS  Kid_Exposure__c		
							,NULL  AS  Additional_SWL__c		
							,NULL  AS  Offset_Crossings__c		
							,NULL  AS  Wide_Crossing__c		
							,NULL  AS  Island_Crossing__c	
							,NULL  AS  Custom_Route__c	

						 	,CAST(T1.tdt_CreatedDate AS DATE) AS  CreatedDate		
							,'0123D0000004axQQAQ'  AS  RecordTypeID	-- Testing
 							,T1.tdt_TestingID AS zrefId
							,'4-tblTRNTesting' AS zrefSrc	
						
						FROM GDB_TC1_kade.dbo.tblTRNTesting AS T1 
						LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.tdt_DogID
						LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T3 ON T3.FileNum=T1.tdt_Instr
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.tdt_Tester
						--camp  
						LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T4 ON  T4.cls_FirstString =T1.tdt_TestingString
						LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T5 ON T5.Legacy_Id__c=('cls-'+T4.cls_FirstString )
						
					UNION
					
						SELECT  DISTINCT   
							GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId 
 							,'PBI-'+CAST(T1.pbi_PassBackID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'Pbi-'+[pbi_PassBackID]	
							,NULL AS [Contact__r:Legacy_Id__c]
							,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,'Staff-'+ CAST(T3.FileNum  AS NVARCHAR(30))   AS  [Instructor_Trainer__r:Legacy_Id__c]		-- Link [tblStaff].[FileNum]
							,T5.Legacy_Id__c   AS [String__r:Legacy_Id__c]
							,NULL AS  [Parent_Training__r:Legacy_Id__c]
							,NULL AS  Backup_Test__c

							,CAST(T1.pbi_PBDate  AS DATE) AS  Pass_Back_Date__c		
							,T1.pbi_PBConfirmed  AS  Pass_Back_Confirmed__c		
 							,'Staff-'+ CAST(T3N.FileNum  AS NVARCHAR(30))   AS  [New_Instructor_Trainer__r:Legacy_Id__c]		-- Yes/Link tblStaff.FileNum
							,T1.pbi_PassToBeDetermined  AS  Pass_To_Be_Determined__c		
							,T1.pbi_DoublingIssues  AS  Doubling_Issues__c		
							,T1.pbi_FilthEater  AS  Filth_Eater__c		
							,T1.pbi_CommunityIssues  AS  Community_Issues__c		
							,T1.pbi_EatingIssues  AS  Eating_Issues__c		
							,T1.pbi_CounterConditioning  AS  Counter_Conditioning__c		
							,T1.pbi_RelievingIssues  AS  Relieving_Issues__c		
							,T1.pbi_HarnessSensitivity  AS  Harness_Sensitivity__c		
							,T1.pbi_SoundnessIssues  AS  Soundness_Issues__c		
 							,T1.pbi_HealthIssues  AS  Health_Issues__c		
							,T1.pbi_TrainerRestrictions  AS  Trainer_Restrictions__c		
							,T1.pbi_OtherKennelIssues  AS  Other_Kennel_Issues__c		
							,T1.pbi_ReasonforPassBack  AS  Reason_for_Pass_Back__c		
 					 	    ,T7.Legacy_Id__c   AS [New_String__r:Legacy_Id__c]-- Yes/ Link tblClass.cls_FirstString
						
								,NULL AS  Test_Date__c		--filler
								,NULL AS  Tester__c		 --filler
								,NULL AS  Test_Type__c		--filler 
								,NULL AS  Grade__c		 --filler
								,NULL AS  Comments__c	--filler	
								,NULL AS  Preliminary_GW_Pace__c	--filler	
								,NULL AS  Final_GW_Pace__c	--filler	
								,NULL AS  Route__c	--filler
 								,NULL AS  Traffic_Test__c		
								,NULL AS  Date__c		
								,NULL AS  Workout_Number__c		
								,NULL AS  Driver__c					 
								,NULL AS  Acceptable_Response__c		
								,NULL AS  Backup_Training_Required__c		
								--filler
								,NULL AS Train_Week__c		
								,NULL AS Float__c		
 								,NULL AS PB__c		
								,NULL AS Current_Instructor__c		
								,NULL AS Current_Weekly_Report__c		
								,NULL AS Hold__c		
								,NULL AS Phase__c		
								,NULL AS Total_Workouts__c		
								,NULL AS Total_Count__c		
								,NULL AS First_Date__c		
								,NULL AS Counter_Con__c		
								,NULL AS Previous_Experience__c		
								,NULL AS CLR__c		
								,NULL AS RG__c		
								,NULL AS Relieving_Program__c		
								,NULL AS Health__c		
								,NULL AS Breeding__c		
								,NULL AS Control__c		
								,NULL AS Temperament__c		
								,NULL AS Body_Sensitivity__c		
								,NULL AS Rough_Food__c		
								,NULL AS Sec_R_Concern__c		
								,NULL AS High_Value__c		
								,NULL AS Train_Notes__c		
								,NULL AS Archive__c		
								,NULL AS Non_Escalator__c		
								,NULL AS Normal_Progress__c	
							,T1.pbi_Traffic  AS  Traffic__c			
								,NULL  AS  City__c		
								,NULL  AS  Revolving_Door__c		
								,NULL  AS  Escalators__c		
								,NULL  AS  Public_Transit__c		
								,NULL  AS  Night_Route__c		
								,NULL  AS  Sidewalkless__c		
								,NULL  AS  Country_Road__c		
								,NULL  AS  Rounded_Corners__c		
								,NULL  AS  Freelance__c		
								,NULL  AS  Bus_Lounge__c		
								,NULL  AS  College_Campus__c		
								,NULL  AS  Underground_Tran__c		
								,NULL  AS  Nursing_Home__c		
								,NULL  AS  Self_Orientation__c		
								,NULL  AS  Wheelchair_Exposure__c		
								,NULL   AS  Kid_Exposure__c		
								,NULL  AS  Additional_SWL__c		
								,NULL  AS  Offset_Crossings__c		
								,NULL  AS  Wide_Crossing__c		
								,NULL  AS  Island_Crossing__c		
								,NULL  AS  Custom_Route__c	
	

						 	,CAST(T1.pbi_CreatedDate AS DATE) AS  CreatedDate		
							,'0123D0000004axPQAQ'  AS  RecordTypeID	--- Passback	
 							,T1.pbi_PassBackID AS zrefId
							,'5-tblTRNPassbackInfo' AS zrefSrc	
						
						FROM GDB_TC1_kade.dbo.tblTRNPassbackInfo AS T1 
						INNER JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.pbi_DogID
						LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T3 ON T3.FileNum=T1.pbi_PassedFrom
						LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T3N ON T3N.FileNum=T1.pbi_PassedTo
						--string
						LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T4 ON  T4.cls_FirstString =T1.pbi_CurrentString
						LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T5 ON T5.Legacy_Id__c=('cls-'+T4.cls_FirstString)
						--new string
						LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T6 ON  T6.cls_FirstString =T1.pbi_NewString
						LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T7 ON T7.Legacy_Id__c=('cls-'+T6.cls_FirstString )
				UNION
                

					SELECT  DISTINCT   
							GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId 
  							,'BUT-'+CAST(T1.but_BackupID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'But-'+[but_BackupId]	
							,NULL AS [Contact__r:Legacy_Id__c]
							,T2.dog_DogID  AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,'Staff-'+ CAST(T3.FileNum  AS NVARCHAR(30))   AS  [Instructor_Trainer__r:Legacy_Id__c]		-- Link [tblStaff].[FileNum]
							,NULL AS [String__r:Legacy_Id__c] 
							
							,'TRT-'+CAST(T4.trt_TrafficID AS NVARCHAR(30)) AS  [Parent_Training__r:Legacy_Id__c]
							,T1.but_BackupTest  AS  Backup_Test__c
 							
								,NULL AS Pass_Back_Date__c		
								,NULL AS Pass_Back_Confirmed__c		
 								,NULL AS [New_Instructor_Trainer__r:Legacy_Id__c]	 
								,NULL AS Pass_To_Be_Determined__c		
								,NULL AS Doubling_Issues__c		
								,NULL AS Filth_Eater__c		
								,NULL AS Community_Issues__c		
								,NULL AS Eating_Issues__c		
								,NULL AS Counter_Conditioning__c		
								,NULL AS Relieving_Issues__c		
								,NULL AS Harness_Sensitivity__c		
								,NULL AS Soundness_Issues__c		
 								,NULL AS Health_Issues__c		
								,NULL AS Trainer_Restrictions__c		
								,NULL AS Other_Kennel_Issues__c		
								,NULL AS Reason_for_Pass_Back__c		
 					 			,NULL AS [New_String__r:Legacy_Id__c] 
 								,NULL AS  Test_Date__c		--filler
								,NULL AS  Tester__c		 --filler
								,NULL AS  Test_Type__c		--filler 
								,NULL AS  Grade__c		 --filler
								,NULL AS  Comments__c	--filler	
								,NULL AS  Preliminary_GW_Pace__c	--filler	
								,NULL AS  Final_GW_Pace__c	--filler	
								,NULL AS  Route__c	--filler
 								,NULL AS  Traffic_Test__c		
								,CAST(T1.but_Date AS DATE) AS  Date__c		
								,T1.but_WorkoutNumber AS  Workout_Number__c		
								,X1.ID   AS  Driver__c					 
								,T1.but_AcceptableResponse AS  Acceptable_Response__c		
								,NULL AS  Backup_Training_Required__c		
								--filler
								,NULL AS Train_Week__c		
								,NULL AS Float__c		
 								,NULL AS PB__c		
								,NULL AS Current_Instructor__c		
								,NULL AS Current_Weekly_Report__c		
								,NULL AS Hold__c		
								,NULL AS Phase__c		
								,NULL AS Total_Workouts__c		
								,NULL AS Total_Count__c		
								,NULL AS First_Date__c		
								,NULL AS Counter_Con__c		
								,NULL AS Previous_Experience__c		
								,NULL AS CLR__c		
								,NULL AS RG__c		
								,NULL AS Relieving_Program__c		
								,NULL AS Health__c		
								,NULL AS Breeding__c		
								,NULL AS Control__c		
								,NULL AS Temperament__c		
								,NULL AS Body_Sensitivity__c		
								,NULL AS Rough_Food__c		
								,NULL AS Sec_R_Concern__c		
								,NULL AS High_Value__c		
								,NULL AS Train_Notes__c		
								,NULL AS Archive__c		
								,NULL AS Non_Escalator__c		
								,NULL AS Normal_Progress__c	
								,NULL AS Traffic__c			
								,NULL  AS  City__c		
								,NULL  AS  Revolving_Door__c		
								,NULL  AS  Escalators__c		
								,NULL  AS  Public_Transit__c		
								,NULL  AS  Night_Route__c		
								,NULL  AS  Sidewalkless__c		
								,NULL  AS  Country_Road__c		
								,NULL  AS  Rounded_Corners__c		
								,NULL  AS  Freelance__c		
								,NULL  AS  Bus_Lounge__c		
								,NULL  AS  College_Campus__c		
								,NULL  AS  Underground_Tran__c		
								,NULL  AS  Nursing_Home__c		
								,NULL  AS  Self_Orientation__c		
								,NULL  AS  Wheelchair_Exposure__c		
								,NULL  AS  Kid_Exposure__c		
								,NULL  AS  Additional_SWL__c		
								,NULL  AS  Offset_Crossings__c		
								,NULL  AS  Wide_Crossing__c		
								,NULL  AS  Island_Crossing__c		
								,NULL  AS  Custom_Route__c	
 
							,CAST(T1.but_CreatedDate AS DATE) AS  CreatedDate		
							,'0123D0000004axRQAQ'  AS  RecordTypeID	--- Traffic	
 							,T1.but_BackupID AS zrefId
							,'6-tblTRNBackUpTest' AS zrefSrc	
						
						FROM GDB_TC1_kade.dbo.tblTRNBackUpTest AS T1 
						LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.but_DogID
						LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T3 ON T3.FileNum=T1.but_Trainer
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.but_Driver
						LEFT JOIN GDB_TC1_kade.dbo.tblTRNTrafficTest AS T4 ON T4.trt_TrafficID=T1.but_TrafficID
 






END --tc1: 278344  

BEGIN-- AUDIT
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_TRAINING 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_TRAINING GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

	--update legacy id to make unique. 
	UPDATE GDB_TC1_migration.dbo.IMP_TRAINING 
	SET Legacy_ID__c = Legacy_ID__c + '-'+ CAST([Dog__r:Legacy_Id__c] AS NVARCHAR(30))
		WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_TRAINING GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	--dog is required. check for nulls. 

	SELECT * FROM GDB_TC1_migration.dbo.IMP_TRAINING 
	WHERE [Dog__r:Legacy_Id__c] IS null


	SELECT zrefSrc, COUNT(*) C
	FROM GDB_TC1_migration.dbo.IMP_TRAINING 
	GROUP BY zrefSrc
	ORDER BY zrefSrc

	SELECT * FROM GDB_TC1_migration.dbo.IMP_TRAINING ORDER BY  [Dog__r:Legacy_Id__c], [Contact__r:Legacy_Id__c], [Instructor_Trainer__r:Legacy_Id__c]
	
	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_TRAINING
	 

END 