USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblCCHDogInfo
			WHERE   SF_Object LIKE '%loca%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 


/*
SELECT * FROM GDB_TC1_migration.DBO.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%LOC%'
0123D0000004axGQAQ	Off Campus	Location__c
0123D0000004axHQAQ	On Campus	Location__c

*/

BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_LOCATION

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'DS-'+CAST(T1.ds_DogSitID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "DS-" with ds_DogSitID	
							,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]						-- Link [tblDog].[dog_DogID]
							,T3.psn_PersonID  AS  [Contact__r:Legacy_Id__c]						-- Link [tblPerson].[psn_PersonID]
							,NULL AS Month__c
							,CAST(T1.ds_DateInToDS AS DATE) AS  Start_Date__c		
							,CAST(T1.ds_DateOutOfDS AS DATE)  AS  End_Date__c		
							,NULL AS  Date_File_Received__c	 --filler	
							,NULL AS  Location__c	  --filler	
							,NULL AS  Local__c			--filler
							,NULL AS  Time__c			--filler
							,NULL AS  Department_Requesting__c --filler
							,CAST(T1.ds_Reason  AS NVARCHAR(max)) AS  Reason_Detail__c	
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Kennel__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
 							,NULL AS  Walking_Notes__c		--filler
							,NULL AS  Notes__c		--filler
							,NULL AS  Date_Season_Begin__c		--filler
							,NULL AS  Date_Season_End__c		--filler
 							,CAST(T1.ds_CreatedDate AS DATE) AS  CreatedDate	
							,'0123D0000004axGQAQ'  AS  RecordTypeId	-- Off Campus	
							,'Dog Sitting' AS  Reason__c	-- Dog Sitting	
							,T1.ds_DogSitID AS zrefID
							,'tblDogSitting' AS zrefSrc

				  	INTO GDB_TC1_migration.DBO.IMP_LOCATION
					FROM GDB_TC1_kade.dbo.tblDogSitting AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.ds_DogID
					LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON CAST(T3.psn_PersonID AS NVARCHAR(30))=T1.ds_PersonID  
			UNION 
					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'REC-'+CAST(T1.rec_RecallID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "REC-" with rec_RecallID	
							,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]			-- Link [tblDog].[dog_DogID]
							,NULL AS  [Contact__r:Legacy_Id__c]   --filler
							,T3.[Month]  AS  Month__c		
							,CAST(T1.rec_FromDate  AS DATE) AS  Start_Date__c		
							,CAST(T1.rec_ToDate  AS DATE) AS  End_Date__c		
							,NULL AS  Date_File_Received__c	 --filler	
							,T1.rec_Location  AS  Location__c		
							,T1.rec_LocalYN  AS  Local__c		
							,LEFT(CAST(CAST(T1.rec_FromDate AS DATE )AS NVARCHAR(20)) +'T'+  CAST(CAST(T1.rec_Time AS TIME) AS NVARCHAR(20)),23) +'Z' AS  Time__c
							,NULL  AS  Department_Requesting__c	 --filler
							,T1.rec_Comment  AS  Reason_Detail__c		
							,T4.TripState  AS  TripState__c
							,T4.TripCity  AS  TripCity__c
							,T4.TripRegion  AS  TripRegion__c
							,T4.TripDirection  AS  Directions__c
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Kennel__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
 							,NULL AS  Walking_Notes__c		--filler
							,NULL AS  Notes__c		--filler
							,NULL AS  Date_Season_Begin__c		--filler
							,NULL AS  Date_Season_End__c		--filler
 							,CAST(T1.rec_CreatedDate AS DATE)  AS  CreatedDate		
							,'0123D0000004axGQAQ'  AS  RecordTypeId	-- Off Campus	
							,'Recall'  AS  Reason__c	-- Hard code value = "Recall"	
 							,T1.rec_RecallID AS zrefID
							,'tblRecall' AS zrefSrc
					FROM GDB_TC1_kade.dbo.tblRecall AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.rec_DogID
					LEFT JOIN GDB_TC1_kade.dbo.trefMonth AS T3 ON T3.ID=T1.rec_MonthCode
					LEFT JOIN GDB_TC1_kade.dbo.trefRecallTrips AS T4 ON T4.TripCode=T1.rec_TripCode
				 
			UNION
		        SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'FC-'+CAST(T1.fc_ID AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "FC-" with fc_ID	
							,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]						-- Link [tblDog].[dog_DogID]
							,T3.psn_PersonID  AS  [Contact__r:Legacy_Id__c]						-- Link [tblPerson].[psn_PersonID]
							,NULL  AS  Month__c --filler
							,CAST(T1.fc_DateInToFC  AS DATE) AS  Start_Date__c		
							,CAST(T1.fc_DateOutOfFC  AS DATE) AS  End_Date__c		
							,NULL AS  Date_File_Received__c	 --filler	
							,NULL  AS  Location__c	--filler	
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,T4.DeptLongID +' ' + T4.DeptName AS  Department_Requesting__c	-- Concatenate [DeptLongID] + ' '+ [DeptName]	-- Link [TblDepartment].[DeptNum]
							,CAST(T1.fc_Reason  AS NVARCHAR(MAX)) AS  Reason_Detail__c		
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Kennel__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
 							,NULL AS  Walking_Notes__c		--filler
							,NULL AS  Notes__c		--filler
							,NULL AS  Date_Season_Begin__c		--filler
							,NULL AS  Date_Season_End__c		--filler
 							,CAST(T1.fc_CreatedDate AS DATE) AS  CreatedDate		
							,'0123D0000004axGQAQ'  AS  RecordTypeId	-- Off Campus	
							,'Foster Care'  AS  Reason__c	-- Foster Care	
							,T1.fc_ID AS zrefID
							,'tblFosterCare' AS zrefSrc

							FROM GDB_TC1_kade.dbo.tblFosterCare AS T1
							LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.fc_DogID
							LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON CAST(T3.psn_PersonID AS NVARCHAR(30))=T1.fc_PersonID  
							LEFT JOIN GDB_TC1_kade.dbo.tblDepartment AS T4 ON T4.DeptNum = T1.fc_DeptRequesting

			UNION
					 SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'KR-'+CAST(T1.kr_RegistryID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'kr-'+[kr_RegistryID]	
							,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,NULL AS  [Contact__r:Legacy_Id__c]		--filler						 
							,NULL  AS  Month__c --filler
							,CAST(T1.kr_DateIn  AS DATE) AS  Start_Date__c		
							,CAST(T1.kr_DateLeft  AS DATE) AS  End_Date__c
							,NULL AS  Date_File_Received__c	 --filler	
							,T1.kr_Where  AS  Location__c		
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,NULL AS Department_Requesting__c --filler
							,NULL AS Reason_Detail__c --filler
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
 							
							,T3.KennelReasonText  AS  Reason_In_2__c							-- Migrate value from [trefKennelReasons].[KennelReasonText]	-- Link [trefKennelReasons].[KennelReasonCode]
							,'ken-'+CAST(T4.KennelID AS NVARCHAR(30)) AS  [Kennel__r:Legacy_Id__c]		-- Ref [trefKennels].[KennelID]
							,CAST(T1.kr_DateDueOut AS DATE) AS  Expected_End_Date__c		
 							,CAST(T1.kr_WalkingNotes AS NVARCHAR(MAX)) AS  Walking_Notes__c		
							,CAST(T1.kr_Notes AS NVARCHAR(MAX)) AS  Notes__c		
							,CAST(T1.kr_DateSeasonBegin  AS DATE) AS  Date_Season_Begin__c		
							,CAST(T1.kr_DateSeasonEnd  AS DATE) AS  Date_Season_End__c		
								
							,CAST(T1.kr_CreatedDate AS DATE) AS  CreatedDate		
							,'0123D0000004axHQAQ'  AS  RecordTypeId	--ON Campus	
							,T5.KennelReasonText  AS  Reason__c	-- Migrate value from [trefKennelReasons].[KennelReasonText]	-- Link [trefKennelReasons].[KennelReasonCode]
							,T1.kr_RegistryID AS zrefID
							,'tblKennelRegistry' AS zrefSrc
							
							FROM GDB_TC1_kade.dbo.tblKennelRegistry AS T1
							LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.kr_DogID
							LEFT JOIN GDB_TC1_kade.DBO.trefKennelReasons AS T3 ON T3.KennelReasonCode=T1.kr_ReasonIn_2
							LEFT JOIN GDB_TC1_kade.DBO.trefKennels AS T4 ON T4.KennelID=T1.kr_Kennel
							LEFT JOIN GDB_TC1_kade.DBO.trefKennelReasons AS T5 ON T5.KennelReasonCode=T1.kr_ReasonIn

			UNION
					 SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'KRS'+CAST(T1.krs_ScheduleID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'krs-'+[krs_ScheduleID]	
							,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,NULL AS  [Contact__r:Legacy_Id__c]		--filler						 
							,NULL  AS  Month__c --filler
							,CAST(T1.krs_Date AS DATE) AS  Start_Date__c		
 							,CAST(T1.krs_DateOut AS DATE) AS  End_Date__c		
							,NULL AS  Date_File_Received__c	 --filler	
							,T1.krs_Where  AS  Location__c		
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,NULL AS Department_Requesting__c --filler
							,NULL AS Reason_Detail__c --filler
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Kennel__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
 							,NULL AS  Walking_Notes__c		--filler
							,CAST(T1.krs_Notes AS NVARCHAR(MAX)) AS  Notes__c
							,NULL AS Date_Season_Begin__c
							,NULL AS Date_Season_End__c
							,CAST(T1.krs_CreatedDate AS DATE) AS  CreatedDate		
							,'0123D0000004axHQAQ'  AS  RecordTypeID	-- Campus
							,T3.KennelReasonText  AS  Reason__c	-- migrate value from [KennelReasonText]	-- Link [trefKennelReasons].[KennelReasonCode]
							,T1.krs_ScheduleID AS zrefID
							,'tblKennelRegSchedule' AS zrefSrc	
							
							FROM GDB_TC1_kade.DBO.tblKennelRegSchedule AS T1
							LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.krs_DogID
							LEFT JOIN GDB_TC1_kade.DBO.trefKennelReasons AS T3 ON T3.KennelReasonCode=T1.krs_Reason

			UNION
					 SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 							,'BS-'+CAST(T1.bs_ID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'bs-'+[bs_id]	
							,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]						-- Link [tblDog].[dog_DogID]
							,T3.psn_PersonID  AS  [Contact__r:Legacy_Id__c]					-- Link [tblPerson].[psn_PersonID]
							,NULL  AS  Month__c --filler
							,CAST(T1.bs_DateIn AS DATE) AS  Start_Date__c		
							,CAST(T1.bs_DateOut AS DATE)  AS  End_Date__c	
							,NULL AS  Date_File_Received__c	 --filler	
							,NULL AS  Location__c	--filler	
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,NULL AS  Department_Requesting__c --filler
							,NULL AS  Reason_Detail__c --filler
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Kennel__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
 							,NULL AS  Walking_Notes__c		--filler
							,T1.bs_Notes  AS  Notes__c		
							,NULL AS Date_Season_Begin__c  --filler	
							,NULL AS Date_Season_End__c    --filler	
							,CAST(T1.bs_CreatedDate AS DATE) AS  CreatedDate
							,'0123D0000004axGQAQ'  AS  RecordTypeID	-- Off Campus	
							,'Breeder Sitter'  AS  Reason__c	-- Breeder Sitter	
							,T1.bs_ID AS zrefID
							,'tblDogBreederSitter' AS zrefSrc	
						
							FROM GDB_TC1_kade.dbo.tblDogBreederSitter AS T1
							LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.bs_DogID
							LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T3 ON CAST(T3.psn_PersonID AS NVARCHAR(30))=T1.bs_PersonID  

			UNION
					 SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
							,'CCH-'+CAST(T1.cch_DogID AS NVARCHAR(30)) +'-'+CAST(ROW_NUMBER() OVER(PARTITION BY T1.cch_DogID ORDER BY t1.cch_DogID) AS NVARCHAR(30)) AS Legacy_ID__c
							,T2.dog_DogID AS  [Dog__r:Legacy_Id__c]			-- Link [tblDog].[dog_DogID]
							,NULL AS  [Contact__r:Legacy_Id__c]
							,NULL  AS  Month__c --filler
							,CAST(T1.cch_DatePlaced AS DATE) AS  Start_Date__c		
							,CAST(T1.cch_DatePlaceEnded AS DATE) AS  End_Date__c		
							,CAST(T1.cch_DateFileReceived AS DATE) AS  Date_File_Received__c		
							,NULL AS  Location__c	--filler	
							,NULL AS  Local__c		--filler
							,NULL AS  Time__c		--filler
							,NULL AS  Department_Requesting__c --filler
							,NULL AS  Reason_Detail__c --filler
							,NULL AS  TripState__c--filler
							,NULL AS  TripCity__c--filler 
							,NULL AS  TripRegion__c--filler
							,NULL AS  Directions__c	--filler
							,NULL AS  Reason_In_2__c					--filler
							,NULL AS  [Kennel__r:Legacy_Id__c]		--filler
							,NULL AS  Expected_End_Date__c		--filler
 							,NULL AS  Walking_Notes__c		--filler
							,CAST(T1.cch_Notes AS NVARCHAR(MAX)) AS  Notes__c		
							,NULL AS Date_Season_Begin__c  --filler	
							,NULL AS Date_Season_End__c    --filler
							,CAST(T1.cch_CreatedDate AS DATE) AS  CreatedDate
							,'0123D0000004axGQAQ'  AS  RecordTypeId	-- Off Campus	
							,'CCH Dog'  AS  Reason__c	-- CCH Dog	
 							,T1.cch_DogID AS zrefID
							,'tblCCHDogInfo' AS zrefSrc

							FROM GDB_TC1_kade.dbo.tblCCHDogInfo AS T1
							LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.cch_DogID

END --tc1:100826  


BEGIN-- AUDIT
	
			SELECT * FROM GDB_TC1_migration.dbo.IMP_LOCATION 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_LOCATION GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			
  		--0
			SELECT zrefSrc, COUNT(*) C
			FROM GDB_TC1_migration.dbo.IMP_LOCATION 
			GROUP BY zrefSrc
			SELECT time__c, COUNT(*) C
			FROM GDB_TC1_migration.dbo.IMP_LOCATION 
			GROUP BY time__c
			 
			  
END 


--salesforce datetime format '2017-07-05T16:31:00.000Z'
			--	             '2002-02-25T12:30:00.000Z'
	 SELECT DISTINCT
	 T1.rec_FromDate, t1.rec_Time,  FORMAT(T1.rec_Time,'yyyy/mm/dd hh:mm tt') datetimetest,
 	 LEFT(CAST(CAST(T1.rec_FromDate AS DATE )AS NVARCHAR(20)) +'T'+  CAST(CAST(T1.rec_Time AS TIME) AS NVARCHAR(20)),23) +'Z' AS  Time__c	
 	 FROM GDB_TC1_kade.dbo.tblRecall AS T1
	 WHERE rec_Time IS NOT NULL 


 





 