 USE GDB_TC1_migration
GO


BEGIN-- 
  			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API
					,','+SF_Field_API+' = T1.'+[Source_Field]     AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblTRNEvalDogTrainingSills
			WHERE   SF_Object LIKE '%ins%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2
					,','+SF_Field_API_2+' = T1.'+[Source_Field]     AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*
SELECT * FROM GDB_TC1_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%ins%'
0123D0000004axDQAQ	Apprentice	Instructor_Evaluation__c
0123D0000004axEQAQ	Instructor	Instructor_Evaluation__c
0123D0000004axFQAQ	String		Instructor_Evaluation__c
0123D0000004dolQAA	Meeting		Instructor_Evaluation__c	
*/



BEGIN -- DROP   IMP_INSTRUCTOR_EVALUATION_lie_sie

	DROP TABLE GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_lie_sie

END 

BEGIN -- CREATE IMP_INSTRUCTOR_EVALUATION_lie_sie 

					SELECT  DISTINCT 
							 OwnerId =  GDB_TC1_migration.[dbo].[fnc_OwnerId]() 
							,RecordTypeID = '0123D0000004axEQAQ'	-- Instructor	
							,Legacy_ID__c ='LIE-'+CAST(T1.lie_LicInstrId	AS NVARCHAR(30)) -- Concatenate 'Lie-'+[lie_LicInstrID]	
							,[Instructor__r:Legacy_Id__c] = 'Staff-'+ CAST(T2.FileNum  AS NVARCHAR(30)) 	-- Link [tblStaff].[FileNum]
							
							,[Class__r:Legacy_Id__c] = T4.Legacy_Id__c		-- tblClass.cls_ClassID
							,[String__r:Legacy_Id__c] = T6.Legacy_Id__c		-- tblClass.cls_FirstString
							,Appraiser__c = x1.id		-- Link [tblStaff].[FileNum]
							,Graduation_Date__c = CAST(T1.lie_GradDate	AS DATE)	
							,Implements_Techniques__c = T1.lie_ImplementsTechniques		
							,Guided_Apprentices__c = T1.lie_GuidedApprentices		
							,Accurate_Written__c = T1.lie_AccurateWritten		
							,Sufficient_String__c = T1.lie_SufficientString		
							,Sufficient_Class_Ready__c = T1.lie_SufficientClassReady		
							,Implemented_Program__c = T1.lie_ImplementedProgram		
							,Team_Leader_status__c = T1.lie_TeamLeader		
							,Class_Work__c = T1.lie_ClassWork		
							,Lecture__c = T1.lie_Lecture		
							,Comments__c = T1.lie_Comments		
							,Goals_Next_String__c = T1.lie_GoalsNextString		
							,Assignments__c = T1.lie_Assignments		
							,Class_Schedule__c = T1.lie_ClassSchedule		
							,Team_Coordinator__c = T1.lie_TeamCoordinator						
							,String_Eval_Appraiser__c = NULL
							,String_Eval_Primary__c = NULL
							,String_Eval_Date__c = NULL
							,Dependability__c = NULL
							,Adaptability__c = NULL
							,Assimilate__c = NULL
							,Attendance__c = NULL
							,Cooperation__c = NULL
							,Quantity__c = NULL
							,Quality__c = NULL
							,Job_Knowledge__c = NULL
							,Interpersonal__c = NULL
							,Safety__c = NULL
							,Overall_Rating__c = NULL
							,Dependability_Comments__c = NULL
							,Adaptability_Comments__c = NULL
							,Cooperation_Comments__c = NULL
							,Quantity_Comments__c = NULL
							,Quality_Comments__c = NULL
							,Job_Knowledge_Comments__c = NULL
							,Safety_Comments__c = NULL
							,Team_Leader_Comments__c = NULL
							,Evaluation_Summary__c = NULL
 							,Employee_Comments__c = NULL						
							,CreatedDate = CAST(T1.lie_CreatedDate AS DATE)
							,zrefId = T1.lie_LicInstrId 
							,zrefSrc = 'tblTRNEvalLicInstr'
					INTO GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_lie_sie
					FROM GDB_TC1_kade.dbo.tblTRNEvalLicInstr AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T2 ON T2.FileNum=T1.lie_InstrID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.lie_AppraiserID
					LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T3 ON T3.cls_ClassID=T1.lie_Class 
							LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T4 ON T4.Legacy_Id__c=('cls-'+T3.cls_ClassID)
					LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T5 ON T5.cls_ClassID=T1.lie_String
							LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T6 ON T6.Legacy_Id__c=('cls-'+T5.cls_ClassID)
 
				UNION 

					SELECT   OwnerId =  GDB_TC1_migration.[dbo].[fnc_OwnerId]() 
							,RecordTypeID = '0123D0000004axFQAQ'	-- -- Record type name = "String"		
							,Legacy_ID__c = 'SIE-'+CAST(T1.sie_EvalStringID	AS NVARCHAR(30))  -- concatenate 'Sie-'+[sie_EvalStringID]	
							,[Instructor__r:Legacy_Id__c] = 'Staff-'+ CAST(T2.FileNum  AS NVARCHAR(30)) 	-- Link [tblStaff].[FileNum]
							,[Class__r:Legacy_Id__c] = T4.Legacy_Id__c		-- tblClass.cls_ClassID
							,[String__r:Legacy_Id__c] = T6.Legacy_Id__c		-- tblClass.cls_FirstString
							,Appraiser__c = NULL 
							,Graduation_Date__c = NULL 
							,Implements_Techniques__c =NULL 
							,Guided_Apprentices__c = NULL 
							,Accurate_Written__c = NULL 
							,Sufficient_String__c = NULL 
							,Sufficient_Class_Ready__c = NULL 
							,Implemented_Program__c = NULL 
							,Team_Leader_status__c = NULL 
							,Class_Work__c = NULL 
							,Lecture__c = NULL 
							,Comments__c = NULL 
							,Goals_Next_String__c =CAST( T1.sie_GoalsNextString		AS NVARCHAR(MAX))	
							,Assignments__c = NULL 
							,Class_Schedule__c = NULL 
							,Team_Coordinator__c = NULL 					
 							 
							,String_Eval_Appraiser__c = X1.ID		-- Link [tblStaff].[FileNum]
							,String_Eval_Primary__c = T1.sie_StringEvalPrimary		
							,String_Eval_Date__c = CAST(T1.sie_StringEvalDate AS DATE)
							,Dependability__c = T1.sie_Dependability		
							,Adaptability__c = T1.sie_Adaptability		
							,Assimilate__c = T1.sie_Assimilate		
							,Attendance__c = T1.sie_Attendance		
							,Cooperation__c = T1.sie_Cooperation		
							,Quantity__c = T1.sie_Quantity		
							,Quality__c = T1.sie_Quality		
							,Job_Knowledge__c = T1.sie_JobKnowledge		
							,Interpersonal__c = T1.sie_Interpersonal		
							,Safety__c = T1.sie_Safety		
							,Overall_Rating__c = T1.sie_OverallRating		
							,Dependability_Comments__c =CAST( T1.sie_DependabilityComments	AS NVARCHAR(MAX))	
							,Adaptability_Comments__c =CAST( T1.sie_AdaptabilityComments AS NVARCHAR(MAX))	
							,Cooperation_Comments__c = CAST(T1.sie_CooperationComments	AS NVARCHAR(MAX))		
							,Quantity_Comments__c =CAST( T1.sie_QuantityComments	AS NVARCHAR(MAX))		
							,Quality_Comments__c =CAST( T1.sie_QualityComments		AS NVARCHAR(MAX))	
							,Job_Knowledge_Comments__c = CAST(T1.sie_JobKnowledgeComments	AS NVARCHAR(MAX))		
							,Safety_Comments__c =CAST( T1.sie_SafetyComments		AS NVARCHAR(MAX))	
							,Team_Leader_Comments__c =CAST( T1.sie_TeamLeaderComments	AS NVARCHAR(MAX))		
							,Evaluation_Summary__c = CAST(T1.sie_EvaluationSummary	AS NVARCHAR(MAX))		
							
							,Employee_Comments__c =CAST( T1.sie_EmployeeComments	AS NVARCHAR(MAX))		
 						
							,CreatedDate = CAST(T1.sie_CreatedDate AS DATE) 
							,zrefId = T1.sie_EvalStringID 
							,zrefSrc = 'tblTRNEvalAppString'
				 
					FROM GDB_TC1_kade.dbo.tblTRNEvalAppString AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T2 ON T2.FileNum=T1.sie_InstrID
					LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.sie_StringEvalAppraiser
					LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T3 ON T3.cls_ClassID=T1.sie_Class 
							LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T4 ON T4.Legacy_Id__c=('cls-'+T3.cls_ClassID)
					LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T5 ON T5.cls_ClassID=T1.sie_String
							LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T6 ON T6.Legacy_Id__c=('cls-'+T5.cls_ClassID)
			
 
END --GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_lie_sie -- tc1: 

BEGIN -- AUDIT  IMP_INSTRUCTOR_EVALUATION_lie_sie
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_lie_sie 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_lie_sie GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_lie_sie 
	GROUP BY zrefSrc

	SELECT * FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_lie_sie ORDER BY zrefSrc, zrefID
	
	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_lie_sie
	
	
	SELECT Team_Leader__c, COUNT(*) C
	FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_lie_sie 
	GROUP BY Team_Leader__c
	SELECT Team_Leader__c, COUNT(*) C
	FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_aie  
	GROUP BY Team_Leader__c


END 

BEGIN -- DROP IMP_INSTRUCTOR_EVALUATION_aie

	DROP TABLE GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_aie

END 

BEGIN -- CREATE IMP_INSTRUCTOR_EVALUATION_aie

					SELECT  OwnerId =  GDB_TC1_migration.[dbo].[fnc_OwnerId]() 
							,RecordTypeID = '0123D0000004axDQAQ'	-- Record type name = "Apprentice"	
 							,Legacy_ID__c = 'AIE-'+CAST(T1.aie_AIEval AS NVARCHAR(30)) -- concatenate 'Aie-'+[aie_AIEval]	
							,[Instructor__r:Legacy_Id__c] = 'Staff-'+ CAST(T2.FileNum  AS NVARCHAR(30)) 		-- Link [tblStaff].[FileNum]
							,[Class__r:Legacy_Id__c] = T4.Legacy_Id__c		-- tblClass.cls_ClassID
						    ,[String__r:Legacy_Id__c] = T6.Legacy_Id__c		-- tblClass.cls_FirstString
							
							,Primary_Instructor__c = X1.ID		-- Link [tblStaff].[FileNum]
						
							,Grad_Date__c = CAST(T1.aie_GradDate AS DATE)
							,Preliminary_OB_Appraiser__c = X2.ID	-- Yes/Link [tblStaff].[FileNum]
							,Prelim_Heel__c = T1.aie_PrelimHeel		
							,Preliminary_Formal_Recall_Instructor__c = T1.aie_PrelimFormalRecall		
							,Preliminary_Informal_Recall__c = T1.aie_PrelimInformalRecall		
							,Preliminary_Down__c = T1.aie_PrelimDown		
							,Preliminary_Sit__c = T1.aie_PrelimSit		
							,Preliminary_Stay__c = T1.aie_PrelimStay		
							,Preliminary_Body_Handling__c = T1.aie_PrelimBodyHandling		
							,Preliminary_Overall_Positioning__c = T1.aie_PrelimOverallPositioning		
							,Preliminary_Food_Refusal__c = T1.aie_PrelimFoodRefusal		
							,Preliminary_Off_Leash_Responses__c = T1.aie_PrelimOffLeashResponses		
							,Final_OB_Appraiser__c = X3.ID		-- Yes/Link [tblStaff].[FileNum]
							,Final_Heel__c = T1.aie_FinalHeel		
							,Final_Formal_Recall__c = T1.aie_FinalFormalRecall		
							,Final_Informal_Recall_Instructor__c = T1.aie_FinalInformalRecall		
							,Final_Down__c = T1.aie_FinalDown		
							,Final_Sit__c = T1.aie_FinalSit		
							,Final_Stay__c = T1.aie_FinalStay		
							,Final_Body_Handling__c = T1.aie_FinalBodyHandling		
							,Final_Overall_Positioning__c = T1.aie_FinalOverallPositioning		
							,Final_Food_Refusal__c = T1.aie_FinalFoodRefusal		
							,Final_Leash_Responses__c = T1.aie_FinalOffLeashResponses		
							,OB_Preliminary_Instructor__c = T1.aie_OBPrelimComments		
							,OB_Final_Instructor__c = T1.aie_OBFinalComments		
							,OB_Final_Plan__c = T1.aie_OBFinalPlan		
							,Preliminary_GW_Appraiser__c = X4.ID	-- Yes/Link [tblStaff].[FileNum]
							,Preliminary_Straight_Line__c = T1.aie_PrelimStraightLine		
							,Preliminary_Lead__c = T1.aie_PrelimLead		
							,Preliminary_Curb_Checks__c = T1.aie_PrelimCurbChecks		
							,Preliminary_Command_Resp_OBGW__c = T1.aie_PrelimCommandRespOBGW		
							,Preliminary_Attention_Work__c = T1.aie_PrelimAttentionWork		
							,Preliminary_Clearances__c = T1.aie_PrelimClearances		
							,Preliminary_Surface_Confidence__c = T1.aie_PrelimSurfaceConfidence		
							,Preliminary_Building_Instructor__c = T1.aie_PrelimBuildingWork		
							,Preliminary_Attitude_Instructor__c = T1.aie_PrelimAttitudeInstructor		
							,Preliminary_Instructor__c = T1.aie_PrelimSidewalkless		
							,Preliminary_Street_Crossings__c = T1.aie_PrelimStreetCrossings		
							,Preliminary_Turns__c = T1.aie_PrelimTurns		
							,Preliminary_OB_on_Route__c = T1.aie_PrelimObonRoute		
							,GW_Preliminary_Instructor__c = T1.aie_GWPrelimComments		
							,GW_Final_Plan__c = T1.aie_GWFinalPlan		
							,Final_GW_Appraiser__c = X5.ID		-- Yes/Link [tblStaff].[FileNum]
							,Final_Straight_Line__c = T1.aie_FinalStraightLine		
							,Final_Lead__c = T1.aie_FinalLead		
							,Final_Curb_Checks__c = T1.aie_FinalCurbChecks		
							,Final_Command_Resp_OBGW__c = T1.aie_FinalCommandRespOBGW		
							,Final_Attention_Work__c = T1.aie_FinalAttentionWork		
							,Final_Clearances__c = T1.aie_FinalClearances		
							,Final_Surface_Confidence__c = T1.aie_FinalSurfaceConfidence		
							,Final_Building_Work__c = T1.aie_FinalBuildingWork		
							,Final_Attitude_Instructor__c = T1.aie_FinalAttitudeInstructor		
							,Final_Sidewalkless__c = T1.aie_FinalSidewalkless		
							,Final_Street_Crossings__c = T1.aie_FinalStreetCrossings		
							,Final_Turns__c = T1.aie_FinalTurns		
							,Final_OB_on_Route__c = T1.aie_FinalObonRoute		
							,GW_Final_comments__c = T1.aie_GWFinalComments		
							,Schedule__c = T1.aie_Schedule		
							,Team_Leader__c = X6.ID		-- Yes/Link [tblStaff].[FileNum]
							,Supervisor__c = X7.ID		-- Yes/Link [tblStaff].[FileNum]
							,Prep_for_Class__c = T1.aie_PrepforClass		
							,Class_Lectures__c = T1.aie_ClassLectures		
							,Student_Instr__c = T1.aie_StudentInstr		
							,Report_Writing__c = T1.aie_ReportWriting		
							,Speaking_Skills__c = T1.aie_SpeakingSkills		
							,Initiative__c = T1.aie_Initiative		
							,Interpersonal_Relations__c = T1.aie_InterpersonalRelations		
							,Academic_Progress__c = T1.aie_AcademicProgress		
							,CW_General_Comments__c = T1.aie_CWGeneralComments		
							,String_Eval_Appraiser__c = X8.ID		-- Yes/Link [tblStaff].[FileNum]
							,Team_Leader_Comments__c = T1.aie_TeamLeaderComments		
						
						   --tblTRNEvalClassClientSkills
							,[Apprentice__r:Legacy_Id__c]   = CASE WHEN T8.FileNum IS NULL THEN 'Staff-'+ CAST(T10.FileNum  AS NVARCHAR(30)) ELSE 'Staff-'+ CAST(T8.FileNum  AS NVARCHAR(30))	END -- migrate one Apprentice from [tblTRNEvalClassClientSkills] or [tblTRNEvalDogTrainingSills]	-- Link [tblStaff].[FileNum]
							,Class_Date__c = CAST(T7.ecc_ClassDate AS DATE)
							,Number_Clients__c = T7.ecc_NumClients		
							,Client_Names__c = T7.ecc_ClientNames		
							,Mentor_Supervisor__c = X9.ID		-- Yes/TblStaff.FileNum
							,Skill__c = T7.ecc_Skill		
							,Skill_1__c = T7.ecc_Skill1		
							,Skill_2__c = T7.ecc_Skill2		
							,Skill_3__c = T7.ecc_Skill3		
							,Skill_4__c = T7.ecc_Skill4		
							,Skill_5__c = T7.ecc_Skill5		
							,Skill_6__c = T7.ecc_Skill6		
							,Skill_7__c = T7.ecc_Skill7		
							,Skill_8__c = T7.ecc_Skill8		
							,Skill_9__c = T7.ecc_Skill9		
							,Skill_10__c = T7.ecc_Skill10		
							,Skill_11__c = T7.ecc_Skill11		
							,Skill_12__c = T7.ecc_Skill12		
							,Skill_13__c = T7.ecc_Skill13		
							,Skill_14__c = T7.ecc_Skill14		
							,Skill_15__c = T7.ecc_Skill15		
							,Skill_16__c = T7.ecc_Skill16		
							,Skill_17__c = T7.ecc_Skill17		
							,Mentor_Supervisor_Comments__c = CASE WHEN T7.ecc_MentorSuperComm IS NOT NULL THEN COALESCE(' ' + LTRIM(RTRIM(T7.ecc_MentorSuperComm)), '') ELSE ' ' END + CHAR(10) +
					 									     CASE WHEN T9.edt_MentorSuperComm IS NOT NULL THEN COALESCE(' ' + LTRIM(RTRIM(T9.edt_MentorSuperComm)), '') ELSE ' ' END 
							,Apprentice_Comments__c =		 CASE WHEN T7.ecc_ApprenticeComm IS NOT NULL THEN COALESCE(' ' + LTRIM(RTRIM(T7.ecc_ApprenticeComm)), '') ELSE ' ' END + CHAR(10) +
					 									     CASE WHEN T9.edt_ApprenticeComm IS NOT NULL THEN COALESCE(' ' + LTRIM(RTRIM(T9.edt_ApprenticeComm)), '') ELSE ' ' END 
						 		
						  --tblTRNEvalDogTrainingSills
							 
						 
							,Date__c = CAST(T9.edt_Date AS DATE)
							,Number_Dogs__c = T9.edt_NumDogs		
							,Dog_Names__c = T9.edt_DogNames		
							,Dog_Mentor_Supervisor__c = X10.ID	-- Link [tblStaff].[FileNum]
							,Dog_Skill__c = T9.edt_Skill		
							,Dog_Skill_1__c = T9.edt_Skill1		
							,Dog_Skill_2__c = T9.edt_Skill2		
							,Dog_Skill_3__c = T9.edt_Skill3		
							,Dog_Skill_4__c = T9.edt_Skill4		
							,Dog_Skill_5__c = T9.edt_Skill5		
							,Dog_Skill_6__c = T9.edt_Skill6		
							,Dog_Skill_7__c = T9.edt_Skill7		
							,Dog_Skill_8__c = T9.edt_Skill8		
							,Dog_Skill_9__c = T9.edt_Skill9		
							,Dog_Skill_10__c = T9.edt_Skill10		
							,Dog_Skill_11__c = T9.edt_Skill11		
							,Dog_Skill_12__c = T9.edt_Skill12		
							,Dog_Skill_13__c = T9.edt_Skill13		
							,Dog_Skill_14__c = T9.edt_Skill14		
							,Performance__c = T9.edt_Performance		
							,Performance_1__c = T9.edt_Performance1		
							,Performance_2__c = T9.edt_Performance2		
							,Performance_3__c = T9.edt_Performance3		
						 
								
 						
			 				,CreatedDate = CAST(T1.aie_CreatedDate AS DATE)
							,zrefId = T1.aie_InstrID 
							,zrefSrc = 'tblTRNEvalApprInstr'
				 		INTO GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_aie
						FROM GDB_TC1_kade.dbo.tblTRNEvalApprInstr AS T1
						LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T2 ON T2.FileNum=T1.aie_InstrID
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.aie_PrimaryInstrID
						LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T3 ON T3.cls_ClassID=T1.aie_Class 
								LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T4 ON T4.Legacy_Id__c=('cls-'+T3.cls_ClassID)
						LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T5 ON T5.cls_ClassID=T1.aie_String
								LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_2 AS T6 ON T6.Legacy_Id__c=('cls-'+T5.cls_ClassID)
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X2 ON X2.ADP__C=T1.aie_PrelimOBAppraiser
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X3 ON X3.ADP__C=T1.aie_FinalOBAppraiser
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X4 ON X4.ADP__C=T1.aie_PrelimGWAppraiser
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X5 ON X5.ADP__C=T1.aie_FinalGWAppraiser
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X6 ON X6.ADP__C=T1.aie_TeamLeader
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X7 ON X7.ADP__C=T1.aie_Supervisor
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X8 ON X8.ADP__C=T1.aie_StringEvalAppraiser
						--tblTRNEvalClassClientSkills
							LEFT JOIN GDB_TC1_kade.dbo.tblTRNEvalClassClientSkills AS T7 ON T7.ecc_AIEval=T1.aie_AIEval
							LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T8 ON T8.FileNum=T7.ecc_ApprenticeID
							LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X9 ON X9.ADP__C=T7.ecc_MentorSuper
						--tblTRNEvalDogTrainingSills
							LEFT JOIN GDB_TC1_kade.dbo.tblTRNEvalDogTrainingSills AS T9 ON T9.edt_AIEval=T1.aie_AIEval
								LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T10 ON T10.FileNum=T9.edt_ApprenticeID
								LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X10 ON X10.ADP__C=T9.edt_MentorSuper


END 

BEGIN -- AUDIT  IMP_INSTRUCTOR_EVALUATION_aie
	
	SELECT * FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_aie 
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_aie GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	SELECT zrefSrc, COUNT(*) C
	FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_aie 
	GROUP BY zrefSrc

	SELECT * FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_aie
	
	SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_INSTRUCTOR_EVALUATION_aie
	 

END 