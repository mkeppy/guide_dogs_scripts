USE GDB_TC1_migration
GO


BEGIN-- 
 	 		SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblAGSTravelInfo
			WHERE   SF_Object LIKE '%memb%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblVolCalWebTourRequest 
			WHERE  SF_Object_2 LIKE '%cam%'
END 

BEGIN --DROP CAMP MEMBER

	DROP TABLE GDB_TC1_migration.dbo.IMP_CAMPAIGN_MEMBER

END


BEGIN ---create	IMP CAMPAIGN MEMBER

	
			SELECT DISTINCT
					T3.cls_FirstString  +' '+ T3.cls_Campus +'-'+CAST(T5.psn_PersonID AS NVARCHAR(30)) AS Legacy_Id__c
					,T3.cls_FirstString  +' '+ T3.cls_Campus  AS  [Campaign:Legacy_id__c]	-- link through [tblClient].[cli_ClassCode] to Campaign	-- Link [tblClient].[cli_ClientID]
					,NULL AS [Lead:Legacy_Id__c]
					,T5.psn_PersonID  AS  [Contact:Legacy_id__c]	-- link through [tblPersonRel].[prl_PersonID] to Contact	-- Link [tblPersonRel].[prl_ClientInstanceID]
					,CASE T1.ByCar WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  By_Car__c		
					,CASE T1.ByBus WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  By_Bus__c	
					,CASE T1.ByTrain WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  By_Train__c		
					,CAST(T1.ArrivalDAte AS DATE) AS  Arrival_Date__c		
					,T1.ArrivalFlight1  AS  Arrival_Flight_1__c		
					,T1.LVPlace1  AS  LV_Place_1__c		
					,T1.LVTime1  AS  LV_Time_1__c		
					,T1.ARPlace1  AS  AR_Place_1__c		
					,T1.ARTime1  AS  AR_Time_1__c		
					,T1.ArrivalFlight2  AS  Arrival_Flight_2__c		
					,T1.LVPlace2  AS  LV_Place_2__c		
					,T1.LVTime2  AS  LV_Time_2__c		
					,T1.ARPlace2  AS  AR_Place_2__c		
					,T1.ARTime2  AS  AR_Time_2__c		
					,T1.ArrivalFlight3  AS  Arrival_Flight_3__c		
					,T1.LVPlace3  AS  LV_Place_3__c		
					,T1.LVTime3  AS  LV_Time_3__c		
					,T1.ARPlace3  AS  AR_Place_3__c		
					,T1.ARTime3  AS  AR_Time_3__c		
					,CAST(T1.DepartDate AS DATE) AS  Departing_Date__c		
					,T1.DepartFlight1  AS  Departing_Flight_1__c		
					,T1.DLVPlace1  AS  DLV_Place_1__c		
					,T1.DLVTime1  AS  DLV_Time_1__c		
					,T1.DARPlace1  AS  DAR_Place_1__c		
					,T1.DARTime1  AS  DAR_Time_1__c		
					,T1.DepartFlight2  AS  Departing_Flight_2__c		
					,T1.DLVPlace2  AS  DLV_Place_2__c		
					,T1.DLVTime2  AS  DLV_Time_2__c		
					,T1.DARPlace2  AS  DAR_Place_2__c		
					,T1.DARTime2  AS  DAR_Time_2__c		
					,T1.DepartFlight3  AS  Departing_Flight_3__c		
					,T1.DLVPlace3  AS  DLV_Place_3__c		
					,T1.DLVTime3  AS  DLV_Time_3__c		
					,T1.DARPlace3  AS  DAR_Place_3__c		
					,T1.DARTime3  AS  DAR_Time_3__c	

					,'tblAGSTravelInfo' AS zrefsrc
		    INTO GDB_TC1_migration.dbo.IMP_CAMPAIGN_MEMBER			
			FROM GDB_TC1_kade.dbo.tblAGSTravelInfo AS T1 
			INNER JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON CAST(T2.cli_ClientID AS NVARCHAR(30)) =CAST(T1.ClientID AS NVARCHAR(30)) 
			INNER JOIN GDB_TC1_kade.dbo.tblClass AS T3 ON CAST(T3.cls_ClassID AS NVARCHAR(30)) =CAST(T2.cli_ClassCode AS NVARCHAR(30)) 
			INNER JOIN GDB_TC1_kade.dbo.tblPersonRel AS T4 ON T4.prl_ClientInstanceID = T1.ClientID
			INNER JOIN GDB_TC1_kade.dbo.tblPerson AS T5 ON T5.psn_PersonID=T4.prl_PersonID
			--715
			UNION 
		
		    SELECT DISTINCT
 					'LWTR-'+CAST(T1.ID AS NVARCHAR(30)) AS Legacy_id__c
					,'LWTR'  AS  [Campaign:Legacy_id__c]   ---Campaign Name: "Legacy Web Tour Request"
					,T1.ID AS   [Lead:Legacy_Id__c]
					,NULL AS  [Contact:Legacy_id__c]
					,NULL AS  By_Car__c		
					,NULL AS  By_Bus__c	
					,NULL AS  By_Train__c		
					,NULL AS  Arrival_Date__c		
					,NULL AS  Arrival_Flight_1__c		
					,NULL AS  LV_Place_1__c		
					,NULL AS  LV_Time_1__c		
					,NULL AS  AR_Place_1__c		
					,NULL AS  AR_Time_1__c		
					,NULL AS  Arrival_Flight_2__c		
					,NULL AS  LV_Place_2__c		
					,NULL AS  LV_Time_2__c		
					,NULL AS  AR_Place_2__c		
					,NULL AS  AR_Time_2__c		
					,NULL AS  Arrival_Flight_3__c		
					,NULL AS  LV_Place_3__c		
					,NULL AS  LV_Time_3__c		
					,NULL AS  AR_Place_3__c		
					,NULL AS  AR_Time_3__c		
					,NULL AS  Departing_Date__c		
					,NULL AS  Departing_Flight_1__c		
					,NULL AS  DLV_Place_1__c		
					,NULL AS  DLV_Time_1__c		
					,NULL AS  DAR_Place_1__c		
					,NULL AS  DAR_Time_1__c		
					,NULL AS  Departing_Flight_2__c		
					,NULL AS  DLV_Place_2__c		
					,NULL AS  DLV_Time_2__c		
					,NULL AS  DAR_Place_2__c		
					,NULL AS  DAR_Time_2__c		
					,NULL AS  Departing_Flight_3__c		
					,NULL AS  DLV_Place_3__c		
					,NULL AS  DLV_Time_3__c		
					,NULL AS  DAR_Place_3__c		
					,NULL AS  DAR_Time_3__c	
					,'tblVolCalWebTourRequest' AS zrefsrc
 		 	FROM GDB_TC1_kade.dbo.tblVolCalWebTourRequest AS T1

			
END 

SELECT * FROM GDB_TC1_migration.dbo.IMP_CAMPAIGN_MEMBER	
 