USE GDB_TC1_migration
GO


BEGIN--scripts  
 			SELECT  [Source_Field],  [Convert], SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblDogStatusChange
			WHERE   SF_Object LIKE '%DOG%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblDogReleaseReasons 
			WHERE  SF_Object_2 LIKE '%dog%'
END 
 
BEGIN -- drop DOG STATUS

	DROP TABLE GDB_TC1_migration.DBO.IMP_DOG_STATUS

END 

BEGIN -- CREATE DOG STATUS

			SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'DDR-'+CAST(T17.drr_ID AS NVARCHAR(30)) AS  Legacy_Id__c
					,T17.drr_DogID			AS  [Dog__r:Legacy_Id__c]    -- Link [tblDog].[dog_DogID]
 					,NULL AS  Date__c		 --filler
					,NULL AS  Placement__c --filler
					,T20.[Location]         AS  Location__c
					,CAST(T17.drr_CreatedDate  AS DATE)   AS  CreatedDate
					,T17.drr_Importance     AS  Importance__c		
					,T17.drr_DiagnosisCode  AS  Diagnosis_Code__c		-- Ref trefVetPetChamp
					,T18.DiagnosisText      AS  Diagnosis	-- migrate [DiagnosisText]	-- Ref trefVetPetChamp
					,T19.CategoryDesc		AS  Diagnosis_Category__c	-- link through [trefVetPetChamp].[DiagnosisCategory]=[trefVetDiagCategory], migrate value from [CategoryDesc]	-- Ref trefVetPetChamp
					,T18.DogReleaseCategory AS  Diagnosis_Release_Category__c	-- migrate [DogReleaseCategory]	-- Ref trefVetPetChamp
					,T18.RelCodeStatus      AS  Release_Code_Status__c	-- migrate [RelCodeStatus]	-- Ref trefVetPetChamp
					,T17.drr_Notes          AS  Notes__c		
	 				,NULL AS  Disposition__c			--filler
					,NULL AS  Status_Change_By__c		--filler
					,NULL AS  Into_String__c		--filler
					,NULL AS  Status_Change_Notes__c		--filler
					,NULL AS  Manage_Companion__c		--filler
					,NULL AS  Priority_Placement__c	--filler
					,NULL AS  Summary__c--filler

					,T17.drr_ID AS zrefID
					,'tblDogReleaseReasons' AS zrefSrc
			
			INTO GDB_TC1_migration.DBO.IMP_DOG_STATUS

			FROM GDB_TC1_kade.dbo.tblDog AS T1 
 			INNER JOIN GDB_TC1_kade.dbo.tblDogReleaseReasons AS T17 ON T17.drr_DogID=T1.dog_DogID
			LEFT JOIN GDB_TC1_kade.dbo.trefVetPetChamp AS T18 ON T18.DiagnosisCode=T17.drr_DiagnosisCode
			LEFT JOIN GDB_TC1_kade.DBO.trefVetDiagCategory T19 ON T19.CategoryCode=T18.DiagnosisCategory
			LEFT JOIN GDB_TC1_kade.DBO.trefVetPetChampLoc AS T20 ON T20.LocCode=T17.drr_LocationCode
			UNION 
			SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'DDS-'+CAST(T1.dds_DogStatusID AS NVARCHAR(30))   AS  Legacy_Id__c	-- Concatenate "DDS-" with dds_DogStatusID	
					,T1.dds_DogID  AS  [Dog:Legacy_Id__c] 		-- Link [tblDog].[dog_DogID]
					,CAST(T1.dds_Date  AS DATE) AS  Date__c		
					,T1.dds_RelPlacement  AS  Placement__c		
					,T1.dds_Location  AS  Location__c
					,CAST(T1.dds_CreatedDate AS DATE) AS  CreatedDate		
					,NULL AS  Importance__c	--filler	
					,NULL AS  Diagnosis_Code__c		-- filler
					,NULL AS  Diagnosis	-- filler
					,NULL AS  Diagnosis_Category__c	-- filler
					,NULL AS  Diagnosis_Release_Category__c	-- filler
					,NULL AS  Release_Code_Status__c	-- filler
					,NULL AS  Notes__c  --filler

					,T2.dsc_text  AS  Disposition__c					-- migrate value from [dsc_text]	-- Ref trefDogStatusChange.dsc_ID
					,X1.ID  AS  Status_Change_By__c		-- Link [tblStaff].[FileNum]
					,T1.dds_IntoString  AS  Into_String__c		
					,T1.dds_StatusChangeNotes  AS  Status_Change_Notes__c		
					,T1.dds_ManageCompanion  AS  Manage_Companion__c		
					,T1.dds_PriorityPlacement  AS  Priority_Placement__c		
 					,CAST(T1.dds_RelSummary AS NVARCHAR(MAX)) AS  Summary__c	

					,T1.dds_DogStatusID AS zrefID
					,'tblDogStatusChange' zrefSrc
			FROM GDB_TC1_kade.dbo.tblDog AS T
			INNER JOIN GDB_TC1_kade.dbo.tblDogStatusChange AS T1 ON T1.dds_DogID=T.dog_DogID
			LEFT JOIN GDB_TC1_kade.dbo.trefDogStatusChange AS T2 ON T2.dsc_ID=T1.dds_Disposition 
			LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.dds_StatusChangeBy

END -- TC1:	62991

	
BEGIN --AUDIT

		SELECT Release_Code_Status__c, COUNT(*) c 
		FROM GDB_TC1_migration.DBO.IMP_DOG_STATUS
		GROUP BY Release_Code_Status__c
		ORDER BY Release_Code_Status__c


		SELECT * FROM GDB_TC1_migration.DBO.IMP_DOG_STATUS
		WHERE Legacy_Id__c IN (SELECT Legacy_Id__c FROM GDB_TC1_migration.DBO.IMP_DOG_STATUS GROUP BY Legacy_Id__c HAVING COUNT(*)>1)
		ORDER BY Legacy_Id__c

END 
