
 

--TBLPERSON and related tbl. 
	--ACCOUNTS/CONTACTS
	--UPDATE: add zrefRecordType field
	
	BEGIN 
		ALTER TABLE GDB_TC1_kade.dbo.tblPerson
		ADD zrefRecordType NVARCHAR(3)

 		--check trans rule
		SELECT psn_PersonID, psn_First, psn_Last, zrefRecordType
		,CASE WHEN psn_First IS NULL AND psn_Last NOT LIKE '%family%' THEN 'ORG' ELSE 'HHD' END AS test
		FROM GDB_TC1_kade.dbo.tblPerson
		ORDER BY zrefRecordType 

			--update
			UPDATE GDB_TC1_kade.dbo.tblPerson
			SET zrefRecordType ='ORG'
			WHERE psn_First IS NULL AND psn_Last NOT LIKE '%family%' 

			UPDATE GDB_TC1_kade.dbo.tblPerson
			SET zrefRecordType ='HHD'
			WHERE zrefRecordType IS NULL 
	END 

	BEGIN -- ACCOUNTS. UPDATE add zrefFirstName and zrefSecFirstName  fields for prim and sec contacts when firstname like ' & ' or ' and '

		ALTER TABLE GDB_TC1_kade.dbo.tblPerson
		ADD zrefFirstName NVARCHAR(50)

		ALTER TABLE GDB_TC1_kade.dbo.tblPerson
		ADD zrefSecFirstName NVARCHAR(50)

			--UPDATE GDB_TC1_kade.dbo.tblPerson 
			--SET zrefFirstName =NULL, zrefSecFirstName=NULL 

			--&..  first and sec.
			UPDATE GDB_TC1_kade.dbo.tblPerson 
			SET zrefFirstName = SUBSTRING(psn_First, 1, CHARINDEX('&', psn_First)-2 ) 
			WHERE psn_First LIKE  '% & %' 
		
			UPDATE GDB_TC1_kade.dbo.tblPerson 
			SET zrefSecFirstName = SUBSTRING(psn_First, CHARINDEX('&', psn_First)+2, 255)
			WHERE psn_First LIKE  '% & %' 
		
			--and... first and sec.
			UPDATE GDB_TC1_kade.dbo.tblPerson 
			SET zrefFirstName = SUBSTRING(psn_First, 1, CHARINDEX(' and ', psn_First)-1)
			WHERE psn_First LIKE  '% and %' 
		
			UPDATE GDB_TC1_kade.dbo.tblPerson 
			SET zrefSecFirstName = SUBSTRING(psn_First, CHARINDEX(' and ', psn_First)+5, 255) 
			WHERE psn_First LIKE  '% and %' 

				SELECT psn_First, psn_Last, zreffirstname, zrefsecfirstname 
				FROM GDB_TC1_kade.dbo.tblPerson
				WHERE psn_First LIKE  '% & %' OR psn_First LIKE '% and %'
	END 
		  
	BEGIN--ACCOUNTS. UPDATE: add zrefSalutation
		ALTER TABLE GDB_TC1_kade.DBO.tblPerson
		ADD zrefSalutation NVARCHAR(20)

			SELECT T1.psn_Prefix, T2.[Convert], T2.[New Value], T1.zrefSalutation
			FROM GDB_TC1_kade.DBO.tblPerson AS T1
			INNER JOIN GDB_TC1_maps.DBO.CHART_Person_Prefix AS T2 ON T1.psn_Prefix=T2.psn_Prefix
			WHERE T2.[New Value] IS NOT NULL 
			ORDER BY T1.psn_Prefix

			UPDATE GDB_TC1_kade.DBO.tblPerson
			SET zrefSalutation = T2.[New Value]
			FROM GDB_TC1_kade.DBO.tblPerson AS T1
			INNER JOIN GDB_TC1_maps.DBO.CHART_Person_Prefix AS T2 ON T1.psn_Prefix=T2.psn_Prefix
			WHERE T2.[New Value] IS NOT NULL 
	END 
	
	BEGIN-- STG (STAGING TABLE): stg_tblPersonGroup: table that has all GroupId that exist in the tblPerson. 
			SELECT  T1.* 
			INTO GDB_TC1_migration.dbo.stg_tblPersonGroup
			FROM GDB_TC1_kade.dbo.tblPersonGroup AS T1
			LEFT JOIN GDB_TC1_kade.DBO.tblPerson AS T2 ON T1.pg_GroupID=T2.psn_PersonID
			WHERE t2.psn_PersonID IS NOT NULL  
			ORDER BY t1.pg_GroupID, t1.pg_PersonID
	END --10,823
		
	BEGIN--ACCOUNTS. UPDATE: add zrefAccountID -- this is to indicate the legacy ID of the household account or organization account.

		ALTER TABLE GDB_TC1_kade.dbo.tblPerson
		ADD zrefAccountId INT
      
		UPDATE GDB_TC1_kade.DBO.tblPerson
		SET zrefAccountId = CASE WHEN T2.pg_GroupID IS NULL THEN psn_PersonID ELSE t2.pg_GroupID END
		FROM GDB_TC1_kade.dbo.tblPerson AS T1
		LEFT JOIN GDB_TC1_migration.DBO.stg_tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID  
		--79074

			--these are the issue record where there is an "&" or "and" on the first name AND also a groupId. 
			--for the migration we used the groupId and ignored the parsing translation rule. 
			--GDB needs to corret for the TC2. 
				UPDATE GDB_TC1_kade.dbo.tblPerson
				SET zrefFirstName = NULL,   zrefSecFirstName =NULL 
	  			FROM GDB_TC1_kade.dbo.tblPerson AS T1
				LEFT JOIN GDB_TC1_kade.DBO.tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID 
		 			 WHERE  ( T2.pg_GroupID='5677'
						OR T2.pg_GroupID='14760'
						OR T2.pg_GroupID='15256'
						OR T2.pg_GroupID='35035'
						OR T2.pg_GroupID='67878'
						OR T2.pg_GroupID='79709') AND t1.zrefFirstName IS NOT NULL 
			--check1
				SELECT T1.psn_PersonID, T1.psn_Prefix, T1.psn_First, T1.psn_Last, t1.zrefRecordType, t1.zrefAccountId, 
				CASE WHEN T2.pg_GroupID IS NULL THEN  t1.psn_PersonID ELSE t2.pg_GroupID END AS test_zrefAccountId
				,T2.* 
				FROM GDB_TC1_kade.dbo.tblPerson AS T1
				LEFT JOIN GDB_TC1_kade.DBO.tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID 
				ORDER BY T2.pg_GroupID, T1.psn_PersonID
			--check2 
				SELECT T1.psn_PersonID, T1.psn_Prefix, T1.psn_First, T1.psn_Last, t1.zrefRecordType, t1.zrefAccountId, 
				CASE WHEN T2.pg_GroupID IS NULL THEN  t1.psn_PersonID ELSE t2.pg_GroupID END AS test_zrefAccountId
				,T2.* 
				FROM GDB_TC1_kade.dbo.tblPerson AS T1
				LEFT JOIN GDB_TC1_kade.DBO.tblPersonGroup AS T2 ON T1.psn_PersonID=T2.pg_PersonID 
				WHERE T2.pg_GroupID='22463' OR T2.pg_GroupID='58' OR T2.pg_GroupID='62520'	  
				OR T2.pg_GroupID='14760' OR T2.pg_GroupID='15256'
	END 			

	BEGIN-- ACCOUNTS.  STG (STAGING TABLE): stg_tblPersonGroup_dnc: table that has all GroupId (PersonID) that DO NOT exist in the tblPerson. 
			SELECT  DISTINCT T1.pg_GroupID 
	 		INTO GDB_TC1_migration.dbo.stg_tblPersonGroup_dnc
			FROM GDB_TC1_kade.dbo.tblPersonGroup AS T1
			LEFT JOIN GDB_TC1_kade.DBO.tblPerson AS T2 ON T1.pg_GroupID=T2.psn_PersonID
			WHERE t2.psn_PersonID IS NULL  
			ORDER BY t1.pg_GroupID 
	END  --68 GroupId
 

	BEGIN--ACCOUNTS.  STG (STAGING TABLE): stg_tblAccounts: unique account records to create household and org accts. 
 			SELECT DISTINCT  zrefAccountId  , zrefRecordType
		 	INTO GDB_TC1_migration.dbo.stg_tblAccount
			FROM GDB_TC1_kade.dbo.tblPerson T1
  			ORDER BY  zrefRecordType, zrefAccountId
	END   -- 72513


	BEGIN--PHONES/EMAILS
			--delet stg tbls
			DROP TABLE [GDB_TC1_migration].dbo.stg_Phone
			DROP TABLE [GDB_TC1_migration].dbo.stg_Phone_seq2
			DROP TABLE [GDB_TC1_migration].dbo.stg_Phone_acct
			DROP TABLE [GDB_TC1_migration].dbo.stg_Phone_cont
 			DROP TABLE [GDB_TC1_migration].dbo.stg_Phone_acct_final
			DROP TABLE [GDB_TC1_migration].dbo.stg_Phone_cont_final

	 
			--create master stg tbl
			SELECT DISTINCT 
			T1.pdl_PersonID, T1.pdl_PrimaryContact, T1.pdl_PhoneID, T2.phn_PhoneID
			,T2.phn_Number, T2.phn_FormatedNum
			,CASE WHEN T3.SF_Field_API LIKE '%email%' THEN T2.phn_Number WHEN T3.SF_Field_API NOT LIKE '%email%' THEN t2.phn_FormatedNum END AS SF_Field_Value
			,T3.SF_Object, T3.SF_Field_API, T3.Custom, T3.Translation_Rules
			,T.zrefRecordType, T.zrefAccountID 
			,ROW_NUMBER() OVER(PARTITION BY t1.pdl_PersonID, t3.SF_Field_API ORDER BY t1.pdl_PersonID, t3.SF_Field_API) AS seq
	 		INTO GDB_TC1_migration.dbo.stg_Phone
			FROM GDB_TC1_kade.dbo.tblPhoneDetail AS T1
			INNER JOIN GDB_TC1_kade.DBO.tblPhone AS T2 ON T2.phn_PhoneID = T1.pdl_PhoneID
			INNER JOIN GDB_TC1_kade.dbo.tblPerson AS T ON T1.pdl_PersonID = T.psn_PersonID	
			INNER  JOIN GDB_TC1_maps.DBO.CHART_PhoneType AS T3 ON T1.pdl_PhoneCode=T3.PhoneCode
			WHERE T3.[Convert]='Yes'  
			 --208535

			--check nulls
				SELECT * FROM GDB_TC1_migration.dbo.stg_Phone 
				WHERE SF_Field_Value IS NULL ORDER BY SF_Object, SF_Field_API

				UPDATE GDB_TC1_migration.dbo.stg_Phone 
				SET SF_Field_Value=phn_Number 
				WHERE SF_Field_Value IS NULL
				--2472 

			--create list of record to DNC.
				SELECT * 
				INTO GDB_TC1_migration.dbo.stg_Phone_seq2
				FROM GDB_TC1_migration.dbo.stg_Phone
				WHERE seq>1
					--107
	  
			--delete record to DNC from master stg tbl. 
				DELETE GDB_TC1_migration.dbo.stg_Phone WHERE seq>1
				--107

			--check duplicates
			SELECT * FROM GDB_TC1_migration.dbo.stg_Phone
			WHERE CAST(pdl_PersonID AS NVARCHAR(30))+SF_Field_API IN 
			(SELECT CAST(pdl_PersonID AS NVARCHAR(30))+SF_Field_API FROM GDB_TC1_migration.dbo.stg_Phone
			GROUP BY CAST(pdl_PersonID AS NVARCHAR(30))+SF_Field_API HAVING count(*) >1)
			ORDER BY pdl_PersonID 
				--0
		
			--create stg phone acct
			SELECT * 
	 		INTO GDB_TC1_migration.dbo.stg_Phone_acct
			FROM GDB_TC1_migration.dbo.stg_Phone
			WHERE SF_Object='ACCOUNT' 
				AND phn_PhoneID NOT IN (SELECT phn_PhoneID FROM GDB_TC1_migration.dbo.stg_Phone WHERE Translation_Rules='Migrate when Organization Account.' AND zrefRecordType='HHD')
			ORDER BY Translation_Rules, zrefRecordType
			--675
				--create final table
		 		EXEC HC_PivotWizard_p		'pdl_PersonID',	--fields to include as unique identifier/normally it is just a unique ID field.
											'SF_Field_API',											--column that stores all new phone types
											'SF_Field_Value',					--phone numbers
											'[GDB_TC1_migration].dbo.stg_Phone_acct_final',			--INTO..     
											'[GDB_TC1_migration].dbo.stg_Phone_acct',				--FROM..
											'SF_Field_Value is not null'							--WHERE..
				--672
				--check dupes
				SELECT * FROM [GDB_TC1_migration].dbo.stg_Phone_acct_final
				WHERE pdl_PersonID IN (SELECT pdl_PersonID FROM  [GDB_TC1_migration].dbo.stg_Phone_acct_final GROUP BY pdl_PersonID HAVING COUNT(*)>1)
				ORDER BY pdl_PersonId					

			--create stg phone cont
			SELECT * 
	 		INTO GDB_TC1_migration.dbo.stg_Phone_cont
			FROM GDB_TC1_migration.dbo.stg_Phone
			WHERE SF_Object='CONTACT'
		 		AND phn_PhoneID NOT IN (SELECT phn_PhoneID FROM GDB_TC1_migration.dbo.stg_Phone WHERE Translation_Rules='Migrate when Household Account.' AND zrefRecordType='ORG')
			ORDER BY Translation_Rules, zrefRecordType
			--176369

				--create final table
		 		EXEC HC_PivotWizard_p		'pdl_PersonID',	--fields to include as unique identifier/normally it is just a unique ID field.
											'SF_Field_API',											--column that stores all new phone types
											'SF_Field_Value',										--phone numbers
											'[GDB_TC1_migration].dbo.stg_Phone_cont_final',			--INTO..     
											'[GDB_TC1_migration].dbo.stg_Phone_cont',				--FROM..
											'SF_Field_Value is not null'							--WHERE..
					--76871 
				--check dupes
				SELECT * FROM [GDB_TC1_migration].dbo.stg_Phone_cont_final
				WHERE pdl_PersonID IN (SELECT pdl_PersonID FROM  [GDB_TC1_migration].dbo.stg_Phone_cont_final GROUP BY pdl_PersonID HAVING COUNT(*)>1)
				ORDER BY pdl_PersonId					

				--add Preferred Phone
				ALTER TABLE [GDB_TC1_migration].dbo.stg_Phone_cont_final
				ADD npe01__PreferredPhone__c NVARCHAR(20)

				--update preferred phone
 		 		 UPDATE [GDB_TC1_migration].dbo.stg_Phone_cont_final
		 		 SET npe01__PreferredPhone__c=CASE T2.sf_field_API WHEN 'MobilePhone' THEN 'Mobile' WHEN 'HomePhone' THEN 'Home' WHEN 'npe01__WorkPhone__c' THEN 'Work' WHEN 'OtherPhone' THEN 'Other' END 
				 FROM [GDB_TC1_migration].dbo.stg_Phone_cont_final T1
				 LEFT JOIN (SELECT  pdl_PersonID, pdl_PrimaryContact, T2.SF_Field_Value, sf_field_API  
							FROM GDB_TC1_migration.dbo.stg_Phone_cont T2
							WHERE T2.pdl_PrimaryContact=1 AND sf_field_API NOT LIKE '%email%') T2
				 ON (T1.pdl_PersonID = T2.pdl_PersonID)
				 WHERE T2.pdl_PrimaryContact=1 

				--check					
					SELECT t1.*, T2.*
	 				FROM [GDB_TC1_migration].dbo.stg_Phone_cont_final T1
					LEFT JOIN (SELECT  pdl_PersonID, pdl_PrimaryContact, T2.SF_Field_Value, sf_field_API  
							FROM GDB_TC1_migration.dbo.stg_Phone_cont T2
							WHERE T2.pdl_PrimaryContact=1 AND sf_field_API NOT LIKE '%email%') T2
					ON (T1.pdl_PersonID = T2.pdl_PersonID)
					WHERE T2.pdl_PrimaryContact=1 
		END 

		BEGIN --ADDRESS 
			--delete stg tbls
			DROP TABLE [GDB_TC1_migration].dbo.stg_Address
			 
			--create master stg tbl
				SELECT DISTINCT 
				 T.zrefAccountID, T1.ad_AddressDetailID, T1.ad_PersonID, T1.ad_AddressCode, T1.ad_AddressID
				,T.psn_MailingAddress
				,CASE WHEN T1.ad_AddressCode=T.psn_MailingAddress THEN 'TRUE' ELSE 'FALSE' END AS npsp__Default_Address__c
				,T3.[Description] AS npsp__Address_Type__c
				
				,T2.add_AddressID  
				,T2.add_Address  
				,T2.add_Address2  
				,T2.add_City  
 				,CASE WHEN T2.add_State IS NULL THEN T2.add_CountryRegion ELSE T2.add_State end AS add_State   --If [add_State] is null migrate [add_CountryRegion] to State/Province
				,CASE WHEN T2.add_Zip IS NULL THEN T2.add_CountryCode ELSE T2.add_Zip END AS add_ZipCode --If [add_Zip] is null migrate [add_CountryCode] to Zip/Postal Code

				,T2.add_Country  
				,T2.add_USCounty  
				,T2.add_Directions  
			
				,CASE WHEN T2.add_Address2 IS NOT NULL THEN T2.add_Address + CHAR(10) + T2.add_Address2  WHEN T2.add_Address2 IS NULL THEN T2.add_Address END AS AddressLines
		 		,T2.add_AddressID zrefAddressId ,T2.add_State zrefState, T2.add_CountryRegion zrefCountryRegion, T2.add_Zip zrefZip, T2.add_CountryCode AS zrefCountryCode
			 		
			 	INTO GDB_TC1_migration.dbo.stg_Address
				FROM GDB_TC1_kade.dbo.tblAddressDetail AS T1
				INNER JOIN GDB_TC1_kade.DBO.tblAddress AS T2 ON T1.ad_AddressID=T2.add_AddressID
	 			INNER JOIN GDB_TC1_kade.dbo.tblPerson AS T ON T1.ad_PersonID = T.psn_PersonID	
				LEFT JOIN GDB_TC1_kade.DBO.trefAddressCode AS T3 ON T1.ad_AddressCode=T3.AddressCode
				ORDER BY T.zrefAccountId, T1.ad_AddressID
				--80420. 
		  
				SELECT * FROM GDB_TC1_migration.dbo.stg_Address
				WHERE ad_AddressID IN  (SELECT ad_AddressID FROM  GDB_TC1_migration.dbo.stg_Address GROUP BY ad_AddressID HAVING COUNT(*) >1)
				ORDER BY ad_AddressID


				SELECT * FROM GDB_TC1_migration.dbo.stg_Address
				WHERE zrefAccountID='12030' OR zrefAccountID='577'
				ORDER BY zrefAccountID

				SELECT * FROM GDB_TC1_kade.dbo.tblAddress WHERE add_AddressID ='15162'
				
				SELECT * FROM GDB_TC1_kade.dbo.tblAddressDetail WHERE ad_AddressID='15162'


		END

		BEGIN--TBLCONTACT HH NAME

			ALTER TABLE GDB_TC1_kade.DBO.tblContacts
			ADD zrefRecordType NVARCHAR(3)

			ALTER TABLE GDB_TC1_kade.DBO.tblContacts
			ADD zrefAccountName NVARCHAR(80)
			
			
			--update record type 
				UPDATE GDB_TC1_kade.dbo.tblContacts
				SET zrefRecordType ='ORG'
				WHERE con_OrgName IS NOT NULL 

				UPDATE GDB_TC1_kade.dbo.tblContacts
				SET zrefRecordType ='HHD'
				WHERE con_OrgName IS  NULL 

			--update account name
				UPDATE GDB_TC1_kade.dbo.tblContacts
				SET zrefAccountName = con_OrgName
				WHERE con_OrgName IS NOT NULL 

				UPDATE GDB_TC1_kade.dbo.tblContacts 
				SET zrefAccountName = LTRIM(RTRIM(CONCAT(con_NamePrefix, ' ' , con_NameFirst, ' ' , con_NameLast, ' ' , con_NameSuffix)))
				WHERE con_OrgName IS null	

			--create unique account record

				DROP TABLE GDB_TC1_migration.dbo.stg_tblContact_Account
				SELECT con_ContactID AS zrefAccountId, zrefAccountName, ROW_NUMBER() OVER (PARTITION BY zrefAccountName ORDER BY con_OrgName) AS zrefSeq
				INTO GDB_TC1_migration.dbo.stg_tblContact_Account
				FROM GDB_TC1_kade.dbo.tblContacts
				
				DELETE GDB_TC1_migration.dbo.stg_tblContact_Account 
				WHERE zrefSeq!=1

				SELECT * FROM GDB_TC1_migration.dbo.stg_tblContact_Account ORDER BY zrefAccountName
		 			
					SELECT  t1.con_ContactID, t1.con_NamePrefix, t1.con_NameFirst, t1.con_NameLast, t1.con_NameSuffix, t1.con_OrgName, t1.zrefRecordType, t1.zrefAccountName
					,ROW_NUMBER() OVER (PARTITION BY t1.zrefAccountName ORDER BY t1.con_OrgName) AS zrefSeq, 
					t2.*
					FROM GDB_TC1_kade.dbo.tblContacts AS t1
					LEFT JOIN GDB_TC1_migration.dbo.stg_tblContact_Account AS t2 ON t1.zrefAccountName= t2.zrefAccountName
					WHERE t1.zrefRecordType ='ORG'
					ORDER BY con_OrgName
	END
	
	BEGIN
			--create unique account record

				DROP TABLE GDB_TC1_migration.dbo.stg_tblVolEquipment
			
				SELECT vps_PathScarfID, vps_OrgName, ROW_NUMBER() OVER(PARTITION BY vps_OrgName ORDER BY vps_OrgName) AS zrefSeq
				,vps_Item, vps_PersonID, vps_DogID, CAST(vps_StartDate AS DATE) vps_StartDate, CAST(vps_TestCompletion AS DATE) AS vps_TestCompletion
				,vps_OrgPhone, vps_OrgAddress, vps_OrgCity,  vps_OrgState, vps_OrgZip
				,CAST(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5',ISNULL(CAST(vps_OrgAddress AS NVARCHAR(20)),' ')
				+ISNULL(vps_OrgCity,' ') +ISNULL(vps_OrgState,' ') +ISNULL(CAST(vps_OrgZip AS NVARCHAR(10)),' '))), 3, 10) AS NVARCHAR(10))  AS zrefAddrId
			--	INTO GDB_TC1_migration.dbo.stg_tblVolEquipment
				FROM GDB_TC1_kade.dbo.tblVolEquipment
				WHERE vps_OrgName IS NOT NULL 
					--USE zrefSeq=1 to create Accounts. 
				--149
				 
				SELECT * 
				INTO GDB_TC1_migration.dbo.stg_tblVolEquipment_1
				FROM GDB_TC1_migration.dbo.stg_tblVolEquipment
				WHERE zrefSeq=1
				
				SELECT *
				FROM GDB_TC1_kade.DBO.tblVolEquipment WHERE vps_OrgName IS NOT NULL 
                
				SELECT *
				FROM GDB_TC1_migration.DBO.stg_tblVolEquipment    ORDER BY vps_OrgName

				SELECT T1.vps_PathScarfID, T1.vps_OrgName, T1.zrefSeq, T1.zrefAddrId, T1.vps_OrgAddress, T1.vps_OrgCity,  T1.vps_OrgState, T1.vps_OrgZip, 
				T2.vps_PathScarfID AS zrefAccountId
				FROM GDB_TC1_migration.DBO.stg_tblVolEquipment  T1
				LEFT JOIN GDB_TC1_migration.dbo.stg_tblVolEquipment_1 AS T2 ON T1.vps_OrgName=T2.vps_OrgName
				ORDER BY vps_OrgName, T1.zrefSeq, zrefAddrId
	END 

	BEGIN	
		--UPDATE tblEquipFood and CHART_EquipFood
				--tblEquipFood associated with stored procedure and can't run an update on the data. needed to create stg tbl. 

				DROP TABLE GDB_TC1_migration.dbo.stg_tblEquipFood
			
			--create stg tbl.	
				SELECT *
				INTO GDB_TC1_migration.dbo.stg_tblEquipFood
				FROM GDB_TC1_kade.dbo.tblEquipFood 

				SELECT  deq_EquipFood, deq_Type, deq_Size
                FROM GDB_TC1_migration.dbo.stg_tblEquipFood
				WHERE deq_EquipFood IS NULL OR deq_Type IS NULL OR deq_Size IS NULL 
 
			--update stg tbl
				UPDATE GDB_TC1_migration.dbo.stg_tblEquipFood 
				SET deq_EquipFood='NULL'
				WHERE deq_EquipFood IS NULL 

				UPDATE GDB_TC1_migration.dbo.stg_tblEquipFood 
				SET deq_Type='NULL'
				WHERE deq_Type IS NULL 

				UPDATE GDB_TC1_migration.dbo.stg_tblEquipFood 
				SET deq_Size='NULL'
				WHERE deq_Size IS NULL 

			--chart update 
				SELECT * FROM GDB_TC1_maps.dbo.CHART_EquipFood
				
				DELETE GDB_TC1_maps.dbo.CHART_EquipFood
				WHERE deq_EquipFood IS NULL AND deq_Type IS NULL AND [Convert] IS NULL 

				UPDATE GDB_TC1_maps.dbo.CHART_EquipFood
				SET deq_EquipFood='NULL'
				WHERE deq_EquipFood ='' 

				UPDATE GDB_TC1_maps.dbo.CHART_EquipFood 
				SET deq_Type='NULL'
				WHERE deq_Type ='' 

				UPDATE GDB_TC1_maps.dbo.CHART_EquipFood 
				SET deq_Size='NULL'
				WHERE deq_Size ='' 
				
				--test
					SELECT DISTINCT T1.deq_EquipFood, T1.deq_Type, T1.deq_Size, T2.deq_EquipFood, T2.deq_Type, T2.deq_Size
									,T2.[Convert], T2.Item_Name, T2.[Type]
					FROM GDB_TC1_migration.dbo.stg_tblEquipFood AS T1
					LEFT JOIN GDB_TC1_maps.dbo.CHART_EquipFood  AS T2 ON T1.deq_EquipFood =T2.deq_EquipFood 
																	 AND T1.deq_Type=T2.deq_Type 
																	 AND T1.deq_Size=T2.deq_Size
			 		WHERE T2.deq_EquipFood ='dry kibble'  
					ORDER BY  T2.[Convert], T1.deq_EquipFood, T1.deq_Type, T1.deq_Size

				  --INSERT INTO GDB_TC1_maps.dbo.CHART_EquipFood (deq_EquipFood, deq_Type, deq_Size, [Convert], Item_Name, [Type])
				  --	VALUES ('Dry Kibble', 'Natural Balance Fat', 'Split-feed 2 cups', 'Yes', 'Dry Kibble - Natural Balance Fat', 'Food');
	END


	BEGIN -- UPDATE DEPARTMENT CODE

		
		UPDATE GDB_TC1_kade.DBO.trefGDBInvItemsDetail  
		SET iid_DepartmentCode=RTRIM(LTRIM(iid_DepartmentCode))

	END 




	BEGIN --UPDATE DOG 
		
		--DISABLE TRIGGERS on tblDog. --kade --> tblDog--> Triggers --> disable all triggers (triggers prevent update on tbl)
		
		SELECT dog_DogID, dog_Status, dog_Sex FROM GDB_TC1_kade.dbo.tblDog
		WHERE dog_Status IS NULL  OR dog_Sex IS NULL  

		UPDATE GDB_TC1_kade.dbo.tblDog
		SET dog_Status='NULL' WHERE dog_Status IS NULL
        UPDATE GDB_TC1_kade.dbo.tblDog
		SET dog_Sex='U' WHERE dog_Sex IS NULL    --U in place of "NULL" 

	END 

	BEGIN --tblDogReleaseReasons

		SELECT ROW_NUMBER() OVER (PARTITION BY drr_DogID ORDER BY drr_DogID, drr_CreatedDate DESC, drr_ModifiedDate DESC, drr_ID DESC) AS zrefSeq
		,* 
		INTO GDB_TC1_migration.dbo.stg_tblDogReleaseReasons
		FROM GDB_TC1_kade.DBO.tblDogReleaseReasons 

		SELECT * FROM GDB_TC1_migration.dbo.stg_tblDogReleaseReasons
		WHERE drr_DogID IN (SELECT drr_DogID FROM GDB_TC1_migration.dbo.stg_tblDogReleaseReasons GROUP BY drr_DogID HAVING COUNT(*)>1)
				
		SELECT * FROM GDB_TC1_migration.dbo.stg_tblDogReleaseReasons
		WHERE zrefSeq>1

		DELETE GDB_TC1_migration.dbo.stg_tblDogReleaseReasons
		WHERE zrefSeq>1
		--5216

	END 
	
	BEGIN 	--tblStaffDogVacc
				DROP TABLE GDB_TC1_migration.dbo.stg_tblStaffDogVacc
 				DROP TABLE GDB_TC1_migration.dbo.stg_tblStaffDogVacc_final
				
				
				SELECT stv_DogID , stv_Vaccine
					  ,CASE WHEN stv_Vaccine='1' THEN  'DA2PP_Vaccine_Given__c'
							WHEN stv_Vaccine='2' THEN  'Rabies_Vaccine_Given__c'
							WHEN stv_Vaccine='3' THEN  'Leptospirosis_Vaccine_Given__c'
							WHEN stv_Vaccine='4' THEN  'Bordetella_Vaccine_Given__c'
						END AS SF_Field
						,CAST(CAST(stv_DateGiven AS DATE) AS NVARCHAR(10)) SF_Value
						
			 	INTO GDB_TC1_migration.dbo.stg_tblStaffDogVacc
				FROM GDB_TC1_kade.dbo.tblStaffDogVacc  
			
				UNION ALL 
			
				SELECT DISTINCT stv_DogID, NULL AS stv_Vaccine
					  ,'Vaccine_Exemption__c' AS SF_Field
					, CASE stv_Exemption WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS SF_Value
				FROM GDB_TC1_kade.dbo.tblStaffDogVacc 
				WHERE stv_DateGiven IS NOT NULL 
			 

				--create final table
		 		EXEC GDB_TC1_migration.dbo.HC_PivotWizard_p	'stv_DogID',								            --fields to include as unique identifier/normally it is just a unique ID field.
															'SF_Field',												--column that stores all new column headings/fields name.
															'SF_Value',												--values
															'[GDB_TC1_migration].dbo.stg_tblStaffDogVacc_final',	--INTO..     
															'[GDB_TC1_migration].dbo.stg_tblStaffDogVacc',			--FROM..
															'SF_Value is not null'							--WHERE..
				--TC1: 195


				--check
				SELECT * FROM GDB_TC1_migration.dbo.stg_tblStaffDogVacc ORDER BY 	stv_DogID	
				SELECT * FROM GDB_TC1_migration.dbo.stg_tblStaffDogVacc_final ORDER BY 	stv_DogID					
				--check dupes
				SELECT * FROM GDB_TC1_migration.dbo.stg_tblStaffDogVacc_final 
				WHERE stv_DogID  IN (SELECT stv_DogID  FROM  GDB_TC1_migration.dbo.stg_tblStaffDogVacc_final  GROUP BY stv_DogID HAVING COUNT(*)>1)
				ORDER BY 	stv_DogID					
					
	END 
	 
	BEGIN-- DOG PLACEMENT APPLICATION fod
				--NOTE: go to kade db--> tblFosterCareQuestOtherDogs --> Triggers (disable all triggers) 

				SELECT * FROM GDB_TC1_kade.DBO.tblFosterCareQuestOtherDogs
				WHERE fod_Age IS NULL 

			--update null values/only found on age.
				UPDATE GDB_TC1_kade.DBO.tblFosterCareQuestOtherDogs
				SET fod_Age='n/a'
				WHERE fod_Age IS NULL 

 
			--create stg table for pivot
				SELECT 
 				'Other_Dog_'+CAST(ROW_NUMBER() OVER( PARTITION BY fod_QuestionnaireID ORDER BY fod_QuestionnaireID, fod_ID) AS NVARCHAR(1)) +'__c' AS SF_Field,
					'Sex: '+ fod_Sex +CHAR(10)+ 
					'Breed: '+ fod_Breed +CHAR(10)+
					'Altered: '+ fod_Altered+CHAR(10)+
					'Vaccination: '+ fod_Vacc+CHAR(10)+
					'Flea Control: '+ fod_FleaControl+CHAR(10)+
					'Age: '+ fod_Age+CHAR(10) AS SF_Value
				,*
				INTO GDB_TC1_migration.dbo.stg_tblFosterCareQuestOtherDogs 
				FROM GDB_TC1_kade.DBO.tblFosterCareQuestOtherDogs 
				
				
			--create final table
		 		EXEC GDB_TC1_migration.dbo.HC_PivotWizard_p	'fod_QuestionnaireID',								            --fields to include as unique identifier/normally it is just a unique ID field.
															'SF_Field',												--column that stores all new column headings/fields name.
															'SF_Value',												--values
															'[GDB_TC1_migration].dbo.stg_tblFosterCareQuestOtherDogs_final',	--INTO..     
															'[GDB_TC1_migration].dbo.stg_tblFosterCareQuestOtherDogs',			--FROM..
															'SF_Value is not null'							--WHERE..
				--TC1: 56
			--test
				SELECT * FROM [GDB_TC1_migration].dbo.stg_tblFosterCareQuestOtherDogs
				SELECT * FROM [GDB_TC1_migration].dbo.stg_tblFosterCareQuestOtherDogs_final

END 
			 
 	BEGIN-- DOG PLACEMENT APPLICATION
			--NOTE: go to kade db--> tblK9BuddyAppOtherDogs --> Triggers (disable all triggers) 

			  
		 
			--create stg table for pivot
				SELECT 
 				'Other_Dog_'+CAST(ROW_NUMBER() OVER( PARTITION BY kod_K9AppID ORDER BY kod_K9AppID, kod_ID) AS NVARCHAR(1)) +'__c' AS SF_Field
			 	, CASE WHEN kod_Sex IS NOT NULL THEN COALESCE('Sex: ' + LTRIM(RTRIM(kod_Sex)), '') ELSE 'Sex: N/A' END + CHAR(10) +
				  CASE WHEN kod_Breed IS NOT NULL THEN COALESCE('Breed: ' + LTRIM(RTRIM(kod_Breed)), '') ELSE 'Breed: N/A' END + CHAR(10) +
				  CASE WHEN kod_Altered IS NOT NULL THEN COALESCE('Altered: ' + LTRIM(RTRIM(kod_Altered)), '') ELSE 'Altered: N/A' END + CHAR(10) +
				  CASE WHEN kod_WhenOwned IS NOT NULL THEN COALESCE('When Owned: ' + LTRIM(RTRIM(kod_WhenOwned)), '') ELSE 'When Owned: N/A' END + CHAR(10) +
				  CASE WHEN kod_Kept IS NOT NULL THEN COALESCE('Kept: ' + LTRIM(RTRIM(kod_Kept)), '') ELSE 'Kept: N/A' END + CHAR(10) +
				  CASE WHEN kod_Age IS NOT NULL THEN COALESCE('Age: ' + LTRIM(RTRIM(kod_Age)), '') ELSE 'Age: N/A' END + CHAR(10) + 
				  CASE WHEN kod_WhatBecameDog IS NOT NULL THEN COALESCE('When Became Dog: ' + LTRIM(RTRIM(kod_WhatBecameDog)), '') ELSE 'When Became Dog: N/A' END + CHAR(10)  
				  AS SF_Value	  --RIPTION = Contactenate with NOTES 
 				,*
				INTO GDB_TC1_migration.dbo.stg_tblK9BuddyAppOtherDogs
				FROM GDB_TC1_kade.DBO.tblK9BuddyAppOtherDogs 
				--tc1 314
				
			--create final table
		 		EXEC GDB_TC1_migration.dbo.HC_PivotWizard_p	'kod_K9AppID',								            --fields to include as unique identifier/normally it is just a unique ID field.
															'SF_Field',												--column that stores all new column headings/fields name.
															'SF_Value',												--values
															'[GDB_TC1_migration].dbo.stg_tblK9BuddyAppOtherDogs_final',	--INTO..     
															'[GDB_TC1_migration].dbo.stg_tblK9BuddyAppOtherDogs',			--FROM..
															'SF_Value is not null'							--WHERE..
				--TC1: 178
			--test
				SELECT * FROM [GDB_TC1_migration].dbo.stg_tblK9BuddyAppOtherDogs
				SELECT * FROM [GDB_TC1_migration].dbo.stg_tblK9BuddyAppOtherDogs_final

END 
			 
 
 BEGIN-- create stg tbl tblVBPayments

 		--tblVBPayments
				--DELETE
				DROP TABLE GDB_TC1_migration.dbo.stg_tblVBPayments
				DROP TABLE GDB_TC1_migration.dbo.stg_tblVBPayments_1
				
				--CREATE
				SELECT		DISTINCT
							 p.vbi_VetEntryID 
		 					,p.vbi_RefNo  AS  Reference_No__c		
							,'iv-' +CAST(v.iv_VendorID AS NVARCHAR(20))  AS  [Vendor__r:Legacy_Id__c]	-- concatenate any records that are one to many	-- Yes/Link [TblVendor]
						 	,p.vbi_BatchID  AS  Batch_ID__c		
							,p.vbi_TransactNum  AS  Transaction_Number__c		
						  
			 	INTO GDB_TC1_migration.dbo.stg_tblVBPayments
				FROM GDB_TC1_kade.DBO.tblVBPayments AS p
				LEFT JOIN GDB_TC1_kade.dbo.tblGDBVendor AS v ON p.vbi_VendorID=CAST(v.iv_VendorID  AS varchar(30))
				ORDER BY vbi_VetEntryID
					--181086
				
			 

				--CREATE
				SELECT * 
 				,ROW_NUMBER() OVER (PARTITION BY vbi_VetEntryID ORDER BY vbi_VetEntryID) AS zrefSeq
				INTO GDB_TC1_migration.dbo.stg_tblVBPayments_1
				FROM GDB_TC1_migration.dbo.stg_tblVBPayments
					--181086

				--CHECK
				SELECT * FROM 	GDB_TC1_migration.dbo.stg_tblVBPayments_1
				WHERE vbi_VetEntryID IN (SELECT vbi_VetEntryID FROM GDB_TC1_migration.dbo.stg_tblVBPayments_1 GROUP BY vbi_VetEntryID HAVING COUNT(*)>1)
				ORDER BY vbi_VetEntryID	
				
END 	



BEGIN --CREATE UNIQUE IDS to update on CONTACTS, CLIENT MEDICAL ID and MEDICAL CHARTS f
					SELECT COUNT(*) FROM GDB_TC1_kade.dbo.tblNursingMedicalNotes  --11635
					SELECT COUNT(*) FROM GDB_TC1_kade.dbo.tblNursingDiabeticNotes -- 1670
					SELECT COUNT(*) FROM GDB_TC1_kade.dbo.tblNursingIntake		  -- 1097
					
					SELECT  DISTINCT 
 							T1.mn_MedicalNoteID AS MedicalChartLegacyId
							,'tblNursingMedicalNotes' AS SrcTbl
							,T3.psn_PersonID
							,UPPER(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', CAST(T3.psn_PersonID AS NVARCHAR(20)))), 3, 16)) AS Client_Medical_ID__c 
							,T1.mn_ClientID  AS ClientID 
							
							--,DENSE_RANK()  OVER( ORDER BY T3.psn_PersonID) test_dense    --create the same number of the group of id, i.e. the same Id will have the same seq num. 
				 	INTO GDB_TC1_migration.dbo.stg_tblMedicalChartIds
					FROM GDB_TC1_kade.dbo.tblNursingMedicalNotes AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.mn_ClientID
					LEFT JOIN GDB_TC1_kade.DBO.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
 		 
					--11635
					UNION 
					SELECT  DISTINCT
							T1.dn_DiabeticNoteID AS MedicalChartLegacyId
							,'tblNursingDiabeticNotes' AS SrcTbl
							,T3.psn_PersonID
							,UPPER(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', CAST(T3.psn_PersonID AS NVARCHAR(20)))), 3, 16)) AS Client_Medical_ID__c 
							,T1.dn_ClientID  AS ClientID 
					FROM GDB_TC1_kade.dbo.tblNursingDiabeticNotes AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.dn_ClientID
					LEFT JOIN GDB_TC1_kade.DBO.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
 					  
					--1670 
					UNION 
					SELECT  DISTINCT
							T1.ni_NursingIntakeID AS MedicalChartLegacyId
							,'tblNursingIntake' AS SrcTbl
							,T3.psn_PersonID
							,UPPER(SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', CAST(T3.psn_PersonID AS NVARCHAR(20)))), 3, 16)) AS Client_Medical_ID__c 
							,T1.ni_ClientID  AS ClientID 							
					FROM GDB_TC1_kade.dbo.tblNursingIntake AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPersonRel AS T2 ON T2.prl_ClientInstanceID=T1.ni_ClientID
					LEFT JOIN GDB_TC1_kade.DBO.tblPerson AS T3 ON T3.psn_PersonID=T2.prl_PersonID
 					ORDER BY psn_PersonID, SrcTbl
					--1097
					--tc1  14402

					
				--create stg tbl for updates on contacts. 	
					SELECT DISTINCT psn_PersonID, Client_Medical_ID__c 
					INTO GDB_TC1_migration.dbo.stg_Contact_tblMedicalChartId
					FROM  GDB_TC1_migration.dbo.stg_tblMedicalChartIds
					ORDER BY psn_PersonID, Client_Medical_ID__c

					--check dupes
					SELECT * FROM GDB_TC1_migration.dbo.stg_Contact_tblMedicalChartId
					WHERE psn_PersonID IN (SELECT psn_PersonID FROM GDB_TC1_migration.dbo.stg_Contact_tblMedicalChartId GROUP BY psn_PersonID HAVING COUNT(*)>1)
					ORDER BY psn_PersonID
					--1005
END 