USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblDog
			WHERE   SF_Object LIKE '%dog%'
			
			SELECT  [Source_Field],  SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%xxxxxxxxxxxxxxxxxxxx%'
END 

BEGIN --DROP DOG UPDATE

	DROP TABLE GDB_TC1_migration.dbo.IMP_DOG_UPDATE

END 

BEGIN-- DOG UPDATES

				SELECT
					 T1.dog_DogID AS Legacy_Id__c
					,T1.dog_SireID  AS  [Sire__r:Legacy_Id__c]		-- Yes/Link[dog_dogid]
					,T1.dog_DamID  AS  [Dam__r:Legacy_Id__c]		-- Yes/Link[dog_dogid]
					,'DBG-'+CAST(T2.dbg_BreedingID AS NVARCHAR(30))   AS  [Litter__r:Legacy_Id__c]		-- Yes/Link[tblDogBreeding.dbg_BreedingID]
				INTO GDB_TC1_migration.dbo.IMP_DOG_UPDATE 
				FROM GDB_TC1_kade.dbo.tblDog AS T1
				LEFT JOIN GDB_TC1_kade.dbo.tblDogBreeding AS T2 ON T2.dbg_BreedingID=T1.dog_LitterID
			    WHERE T1.dog_SireID IS NOT NULL OR T1.dog_DamID IS NOT NULL OR T1.dog_LitterID  IS NOT NULL 

END 


BEGIN --audit
	SELECT * FROM GDB_TC1_migration.dbo.IMP_DOG_UPDATE
END 