USE GDB_TC1_migration
GO
BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblVetProcAuth 
			WHERE   SF_Object LIKE '%allow%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%xxxxxxxxxxxxxxxxxxxx%'
END 


BEGIN --DROP ALLOWANCE

		DROP TABLE GDB_TC1_migration.DBO.IMP_ALLOWANCE

END 

BEGIN -- CREATE ALLOWANCE
			SELECT DISTINCT 
			   CASE WHEN X1.ID  IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'Pfa-1-'+CAST(T1.pfa_AuthID AS NVARCHAR(30)) AS Legacy_Id__c
				,'Pfa-'+CAST(T1.pfa_AuthID AS NVARCHAR(30)) AS [Case__r:Legacy_Id__c]
		 		,T2.DiagnosisText AS  Release_Code_Status__c	
				,T1.pfa_Amount1  AS  Allowance_Amount__c	
		 		,CASE T1.pfa_AmountType1 WHEN '19' THEN 'Pratt Fund (One Time)' WHEN  '39' THEN 'Pratt Fund (Annual)' END AS  Amount_Type__c	-- 19 = Pratt Fund (One Time) 39 = Pratt Fund (Annual)
	 			,CAST(T1.pfa_ExpirationDate1 AS DATE) AS  Expiration_Date__c	
			 	,CASE T1.pfa_AmountForLife1 WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS  Amount_For_Life__c	
			 	,T1.pfa_FundPayout  AS  Fund_Payout__c	
				,T1.pfa_FormLevel  AS  Form_Level__c	
				,CASE T1.pfa_Hide  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Hide__c	
				,CASE T1.pfa_AuthComplete WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS   Complete__c	
				,NULL AS  Start_Date__c
				,NULL AS  [Dog__r:Legacy_ID__c]	 
				,NULL AS  [Vet_Record__r:Legacy_ID__c]	 
				,NULL AS  Category__c	
				,NULL AS  Type__c	 
				,NULL AS  Paid__c	
				,NULL AS  Life__c	
				,NULL AS  Expired__c	
				,NULL AS  Pre_existing_Diagnosis_Code__c	 
				,NULL AS  Pre_existing_Diagnosis_Text__c	 
				,NULL AS  Pre_existing_Diagnosis_Category__c	 
				,NULL AS  Pre_existing_Dog_Release_Category__c	 
	 			,NULL AS  Pre_existing_Diagnosis_Release_Status__c
				,NULL AS  Amount_Paid__c	
				,NULL AS  [Vet_Procedure__r:Legacy_Id__c]					

				,T1.pfa_AuthID AS zrefID
				,'tblPrattFundAuthorization' AS zrefSrc
			INTO GDB_TC1_migration.dbo.IMP_ALLOWANCE
			FROM GDB_TC1_kade.DBO.tblPrattFundAuthorization AS T1
			LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T4 ON T4.FileNum=T1.pfa_WhoBy
			LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__c = T4.FileNum
			LEFT JOIN GDB_TC1_kade.DBO.trefVetPetChamp AS T2 ON T1.pfa_DxCode=T2.DiagnosisCode
			WHERE T1.pfa_Amount1 IS NOT NULL 
			
			UNION ALL 

			SELECT DISTINCT 
				CASE WHEN X1.ID  IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'Pfa-2-'+CAST(T1.pfa_AuthID AS NVARCHAR(30)) AS Legacy_Id__c
				,'Pfa-'+CAST(T1.pfa_AuthID AS NVARCHAR(30)) AS [Case__r:Legacy_Id__c]
 				,T2.DiagnosisText AS  Release_Code_Status__c	
				,T1.pfa_Amount2  AS  Allowance_Amount__c		-- If populated create second Allowance record.
				,CASE T1.pfa_AmountType2  WHEN '19' THEN 'Pratt Fund (One Time)' WHEN  '39' THEN 'Pratt Fund (Annual)' END AS  Amount_Type__c	
				,CAST(T1.pfa_ExpirationDate2 AS DATE) AS  Expiration_Date__c	
				,CASE T1.pfa_AmountForLife2  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END   AS  Amount_For_Life__c	
				,T1.pfa_FundPayout  AS  Fund_Payout__c	
				,T1.pfa_FormLevel  AS  Form_Level__c	
				,CASE T1.pfa_Hide  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END  AS  Hide__c	
				,CASE T1.pfa_AuthComplete WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS   Complete__c	
				,NULL AS  Start_Date__c
				,NULL AS  [Dog__r:Legacy_ID__c]	 
				,NULL AS  [Vet_Record__r:Legacy_ID__c]	 
				,NULL AS  Category__c	
				,NULL AS  Type__c	 
				,NULL AS  Paid__c	
				,NULL AS  Life__c	
				,NULL AS  Expired__c	
				,NULL AS  Pre_existing_Diagnosis_Code__c	 
				,NULL AS  Pre_existing_Diagnosis_Text__c	 
				,NULL AS  Pre_existing_Diagnosis_Category__c	 
				,NULL AS  Pre_existing_Dog_Release_Category__c	 
	 			,NULL AS  Pre_existing_Diagnosis_Release_Status__c
				,NULL AS  Amount_Paid__c	
				,NULL AS  [Vet_Procedure__r:Legacy_Id__c]					
				,T1.pfa_AuthID AS zrefID
				,'tblPrattFundAuthorization' AS zrefSrc

			FROM GDB_TC1_kade.DBO.tblPrattFundAuthorization AS T1
			LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T4 ON T4.FileNum=T1.pfa_WhoBy
			LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__c = T4.FileNum
			LEFT JOIN GDB_TC1_kade.DBO.trefVetPetChamp AS T2 ON T1.pfa_DxCode=T2.DiagnosisCode
			WHERE T1.pfa_Amount2 IS NOT NULL
	
		UNION
            
			SELECT 
				CASE WHEN X1.ID  IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]() END AS OwnerId
				,'Vta-'+CAST(T1.vta_ID AS NVARCHAR(30)) AS  Legacy_Id__c	-- concatenate 'Vta-'+[vta_ID]
				,'Vta-'+CAST(T1.vta_ID AS NVARCHAR(30)) AS  [Case__r:Legacy_Id__c]	-- concatenate 'Vta-'+[vta_ID]
				,NULL AS Release_Code_Status__c -- filler
				,T1.vta_AuthAmount  AS  Allowance_Amount__c	
				,NULL AS Amount_Type__c  --filler
 				,CAST(T1.vta_ExpDate  AS DATE)  AS  Expiration_Date__c	
				,NULL AS  Amount_For_Life__c	 --filler
				,NULL AS  Fund_Payout__c		--filler
				,NULL AS  Form_Level__c			--filler
				,NULL AS  Hide__c	--filler
				,NULL AS  Complete__c--filler

				,CAST(T1.vta_StartDate  AS DATE) AS  Start_Date__c
				,T8.dog_DogID  AS  [Dog__r:Legacy_ID__c]	     -- link to Dog through Vet Record
				,'VDL-'+CAST(T.vdl_VetEntryID AS NVARCHAR(30))  AS  [Vet_Record__r:Legacy_ID__c]	-- link to Dog through Vet Record
				,T3.CategoryDesc   AS  Category__c	
				,T4.aut_Type  AS  Type__c	-- migrate value from [aut_Type]
				,CASE T1.vta_Paid WHEN '1' THEN 'TRUE' ELSE 'FALSE' END AS  Paid__c	
				,CASE T1.vta_life WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Life__c	
				,CASE T1.vta_Expired WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END   AS  Expired__c	
				,T1.vta_PreExstDiagCode  AS  Pre_existing_Diagnosis_Code__c	-- migrate as is 
				,T5.DiagnosisText  AS        Pre_existing_Diagnosis_Text__c	-- migrate [DiagnosisText]
				,T6.CategoryDesc AS          Pre_existing_Diagnosis_Category__c	-- link through [trefVetPetChamp].[DiagnosisCategory]=[trefVetDiagCategory], migrate value from [CategoryDesc]
				,T7.ReleaseCategory AS       Pre_existing_Dog_Release_Category__c	-- migrate [DogReleaseCategory]
				,T5.RelCodeStatus AS         Pre_existing_Diagnosis_Release_Status__c	-- migrate [trefVetPetChamp].[RelCodeStatus]
				,NULL AS  Amount_Paid__c	
				,NULL AS  [Vet_Procedure__r:Legacy_Id__c]			 
				,T1.vta_ID AS zrefID
				,'tblVetAuthorization' AS zrefSrc
	
				FROM GDB_TC1_kade.dbo.tblVetAuthorization AS T1
				LEFT JOIN GDB_TC1_kade.dbo.tblVetEntry AS T ON T.vdl_VetEntryID=T1.vta_VetEntryID
				LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T8 ON T8.dog_DogID=T.vdl_DogID
				LEFT JOIN GDB_TC1_kade.dbo.tblStaff AS T2 ON T2.FileNum=t1.vta_AuthBy
				LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__c = T2.FileNum
				LEFT JOIN GDB_TC1_kade.DBO.trefVetDiagCategory AS T3 ON T1.vta_Category=T3.CategoryCode
				LEFT JOIN GDB_TC1_kade.dbo.trefVetAuthorizationType AS T4 ON T4.aut_ID=T1.vta_AuthType
				LEFT JOIN GDB_TC1_kade.dbo.trefVetPetChamp AS T5 ON T5.DiagnosisCode=T1.vta_PreExstDiagCode
				LEFT JOIN GDB_TC1_kade.dbo.trefVetDiagCategory AS T6 ON T6.CategoryCode=T5.DiagnosisCategory
				LEFT JOIN GDB_TC1_kade.dbo.trefDogReleaseCategories AS T7 ON T7.ReleaseCategory=T5.DogReleaseCategory

			UNION 

			SELECT DISTINCT GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
				,'Vpa-'+ CAST(T1.vpa_ID  AS NVARCHAR(30)) AS  Legacy_ID__c							-- concatenate 'Vpa-'+[vpa_ID]
				,'Vta-'+ CAST(T3.vta_ID AS NVARCHAR(30)) AS  [Case__r:Legacy_Id__c]
				,NULL AS Release_Code_Status__c
				,NULL AS Allowance_Amount__c
				,NULL AS Amount_Type__c
				,NULL AS Expiration_Date__c	
				,NULL AS Amount_For_Life__c
				,NULL AS Fund_Payout__c
				,NULL AS Form_Level__c
				,NULL AS Hide__c
				,NULL AS Complete__c
				,NULL AS Start_Date__c
				,T6.dog_DogID  AS  [Dog__r:Legacy_Id__c]											-- Link thru tblVetProc to tblVetEntry to tblDog   --Link tblVetProc.vdp_ProcID
			    ,'VDL-'+ CAST(T5.vdl_VetEntryID  AS NVARCHAR(30))AS  [Vet_Record__r:Legacy_Id__c]	-- Link thru tblVetProc to tblVetEntry             --Link tblVetProc.vdp_ProcID
 				,NULL AS  Category__c
 				,T2.aut_Type  AS  Type__c															-- migrate value from [aut_Type]  
				,NULL AS  Paid__c	
				,NULL AS  Life__c	
				,NULL AS  Expired__c	
				,NULL AS  Pre_existing_Diagnosis_Code__c	 
				,NULL AS  Pre_existing_Diagnosis_Text__c	 
				,NULL AS  Pre_existing_Diagnosis_Category__c	 
				,NULL AS  Pre_existing_Dog_Release_Category__c	 
				,NULL AS  Pre_existing_Diagnosis_Release_Status__c							
				,T1.vpa_Amount  AS  Amount_Paid__c	
				,'Vdp-'+ CAST(T4.vdp_ProcID AS varchar(30)) AS  [Vet_Procedure__r:Legacy_Id__c]		--Link tblVetProc.vdp_ProcID
				,T1.vpa_ID AS zrefID
				,'tblVetProcAuth' AS zrefSrc

			FROM GDB_TC1_kade.DBO.tblVetProcAuth AS T1
			LEFT JOIN GDB_TC1_kade.dbo.trefVetAuthorizationType AS T2 ON T2.aut_ID=T1.vpa_AuthType
			LEFT JOIN GDB_TC1_kade.dbo.tblVetAuthorization AS T3 ON T3.vta_ID=T1.vpa_VetAuthID
			LEFT JOIN GDB_TC1_kade.dbo.tblVetProc AS T4 ON T4.vdp_ProcID = T1.vpa_ProcID
			LEFT JOIN GDB_TC1_kade.dbo.tblVetEntry AS T5 ON T5.vdl_VetEntryID=T4.vdp_VetEntryID
			LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T6 ON T6.dog_DogID=T5.vdl_DogID

END 


BEGIN-- AUDIT
	
			SELECT * FROM GDB_TC1_migration.dbo.IMP_ALLOWANCE 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_ALLOWANCE GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			
  		--0
			SELECT zrefSrc, COUNT(*) C
			FROM GDB_TC1_migration.dbo.IMP_ALLOWANCE 
			GROUP BY zrefSrc
		 
			  
END 
 