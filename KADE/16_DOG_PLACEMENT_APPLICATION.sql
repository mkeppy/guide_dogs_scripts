USE GDB_TC1_migration
GO


BEGIN--maps 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblK9BuddyApplication
			WHERE   SF_Object LIKE '%dog%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.XXXXXXXXXXXX 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%'
END 

/*Record types
0123D0000004ax6QAA	Adoption	Dog_Placement_Application__c
0123D0000004ax7QAA	Foster	Dog_Placement_Application__c
0123D0000004ax8QAA	K9 Buddy	Dog_Placement_Application__c
*/

BEGIN -- drop DOG PLACEMENT APPLICATION
	DROP TABLE GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_cca
	DROP TABLE GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_fcq
	DROP TABLE GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_k9a 
END 

BEGIN -- create IMP_DOG_PLACEMENT_APPLICATION CCA
					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId

					,'CCA-' + CAST(T1.cca_CCAID  AS NVARCHAR(30)) AS  Legacy_Id__c	-- Concatenate "CCA-" with cca_CCAID	
					,CASE T1.cca_Active WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Active__c		
					,T2.psn_PersonID AS  [Contact__r:Legacy_Id__c]		-- Link [tblPerson].[PersonID]
				 	,T3.dog_DogID AS  [Dog__r:Legacy_Id__c]				-- Link [tblDog].[dog_DogID]
					,T1.cca_CampusPlaced  AS  Campus__c		
					,CAST(T1.cca_AppDate  AS DATE) AS  Application_Date__c		
					,T1.cca_OwnRent  AS  Own_Rent__c		
					,T1.cca_LandlordName  AS  Landlord__c		
					,T1.cca_LandlordPhone  AS  Landlord_Phone__c		
					,CASE T1.cca_CallLandlordOK WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Call_Landlord_OK__c	-- migrate blanks as 'No'	
					,T1.cca_WhyContact  AS  Why_Contact__c		
					,T1.cca_VisitCampus  AS  Visit_Campus__c		
					,CASE T1.cca_TravelToDog  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Travel_To_Dog__c	-- migrate blanks as 'No'	
					,CASE T1.cca_DogAllergy  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Dog_Allergy__c	-- migrate blanks as 'No'	
					,T1.cca_NumberAdults  AS  Number_Adults__c		
					,T1.cca_Children0to6  AS  Children_0_to_6__c		
					,T1.cca_Children7to10  AS  Children_7_to_10__c		
					,T1.cca_Children11to13  AS  Children_11_to_13__c		
					,T1.cca_Children14to17  AS  Children_14_to_17__c		
					,CASE T1.cca_DisabledResident  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Disabled_Resident__c	-- migrate blanks as 'No'	
					,T1.cca_DisabledDescription  AS  Disabled_Description__c		
					,T1.cca_CurrentDogs  AS  Current_Dogs__c		
					,T1.cca_CurrentCats  AS  Current_Cats__c		
					,T1.cca_PreviousDogs  AS  Previous_Dogs__c		
					,T1.cca_PreviousCats  AS  Previous_Cats__c		
					,CASE T1.cca_DogCrateOK  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Dog_Crate__c	-- migrate blanks as 'No'	
					,T1.cca_HoursAlone  AS  Hours_Alone__c		
					,T1.cca_WhereAlone  AS  Where_Alone__c		
					,T1.cca_WhereAloneOther  AS  Where_Alone_Other__c		
					,T1.cca_WhereNight  AS  Where_At_Night__c		
					,T1.cca_WhereNightOther  AS  Where_Night_Other__c		
					,CASE T1.cca_HaveFence   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Fenced_Yard__c	-- migrate blanks as 'No'	
					,T1.cca_FenceHeightFeet  AS  Fence_Height_Feet__c		
					,T1.cca_FenceHeightInches  AS  Fence_Height_Inches__c		
					,CASE T1.cca_FenceInstallOK  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Fence_Install_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_FenceCheckOK  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Fence_Check_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_GSD   WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  GSD__c		-- migrate blanks as 'No'	
					,CASE T1.cca_GLD   WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  GLD__c		-- migrate blanks as 'No'	
					,CASE T1.cca_LABy  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  LAB_y__c	-- migrate blanks as 'No'	
					,CASE T1.cca_LABb  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  LAB_b__c	-- migrate blanks as 'No'	
					,CASE T1.cca_LGXy  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  LGX_y__c	-- migrate blanks as 'No'	
					,CASE T1.cca_LGXb  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  LGX_b__c	-- migrate blanks as 'No'	
					,CASE T1.cca_PDL   WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  PDL__c		-- migrate blanks as 'No'	
					,CASE T1.cca_SCC   WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  SCC__c		-- migrate blanks as 'No'	
					,T1.cca_DogSex  AS  Dog_Sex__c		
					,T1.cca_DogAge  AS  Dog_Age__c		
					,CASE T1.cca_MinorMedicalOK   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Minor_Medical_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_HighActivityOK   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  High_Activity_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_AssertiveOK   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Assertive_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_SensitiveOK   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Sensitive_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_BarkingOK   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Barking_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_ChewDigOK   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  ChewDig_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_IncompatDogsOK   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Incompat_Dogs_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_IncompatCatsOK   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Incompat_Cats_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_NotHousetrainedOK   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Not_Housetrained_OK__c	-- migrate blanks as 'No'	
					,CASE T1.cca_SpecialActivityDisaster   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Special_Activity_Disaster__c	-- migrate blanks as 'No'	
					,CASE T1.cca_SpecialActivityLaw   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Special_Activity_Law__c	-- migrate blanks as 'No'	
					,CASE T1.cca_SpecialActivityTherapy   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Special_Activity_Therapy__c	-- migrate blanks as 'No'	
					,CASE T1.cca_SpecialActivityDetection  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Special_Activity_Detection__c	-- migrate blanks as 'No'	
					,CASE T1.cca_SpecialActivityHandicap   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Special_Activity_Handicap__c	-- migrate blanks as 'No'	
					,CASE T1.cca_SpecialActivitySport   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Special_Activity_Sport__c	-- migrate blanks as 'No'	
					,CASE T1.cca_SpecialActivityHearing   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Special_Activity_Hearing__c	-- migrate blanks as 'No'	
					,T1.cca_SpecialActivityOther  AS  Special_Activity_Other__c		
					,CASE T1.cca_WhereHearFriend   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Where_Hear_Friend__c	-- migrate blanks as 'No'	
					,CASE T1.cca_WhereHearNewspaper   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Where_Hear_Newspaper__c	-- migrate blanks as 'No'	
					,CASE T1.cca_WhereHearInternet  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  Where_Hear_Internet__c	-- migrate blanks as 'No'	
					,CASE T1.cca_WhereHearBillboard   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Where_Hear_Billboard__c	-- migrate blanks as 'No'	
					,CASE T1.cca_WhereHearVet   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Where_Hear_Vet__c	-- migrate blanks as 'No'	
					,CASE T1.cca_WhereHearGDB   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Where_Hear_GDB__c	-- migrate blanks as 'No'	
					,T1.cca_WhereHearOrg  AS  Where_Hear_Org__c		
					,T1.cca_WhereHearOther  AS  Where_Hear_Other__c		
					,T1.cca_OtherInfo  AS  Other_Info__c		
					,CASE T1.cca_GDBInvolvementEmp   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  GDB_Involvement_Emp__c	-- migrate blanks as 'No'	
					,CASE T1.cca_GDBInvolvementFamily   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  GDB_Involvement_Family__c	-- migrate blanks as 'No'	
					,CASE T1.cca_GDBInvolvementCLIApp   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  GDB_Involvement_CLIApp__c	-- migrate blanks as 'No'	
					,CASE T1.cca_GDBInvolvementFamilyCLIApp   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  GDB_Involvement_Family_CLI_App__c	-- migrate blanks as 'No'	
					,T1.cca_GDBInvolvementADP  AS  GDB_Involvement_ADP__c	-- migrate blanks as 'No'	
					,T1.cca_GDBInvolvementOther  AS  GDB_Involvement_Other__c		
					,CASE T1.cca_GDBNews   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  GDB_News__c	-- migrate blanks as 'No'	
					,CASE T1.cca_CommConn   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Comm_Conn__c	-- migrate blanks as 'No'	
					,CASE T1.cca_GDBMaterial   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  GDB_Material__c	-- migrate blanks as 'No'	
					,CASE T1.cca_ObedienceClass   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Obedience_Class__c	-- migrate blanks as 'No'	
					,T1.cca_ObedienceClassDesc  AS  Obedience_Class_Desc__c		
					,T1.cca_Screening  AS  Screening__c		
					,CASE T1.cca_Accepted   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Accepted__c	-- migrate blanks as 'No'	
					,T1.cca_Source  AS  Source__c		
					,CAST(T1.cca_CreatedDate AS DATE) AS  CreatedDate		
					,'0123D0000004ax6QAA'  AS  RecordTypeID	-- Adoption	

					,T1.cca_CCAID AS zrefID
					,'tblCareerChangeApplication' AS zrefSrc
					
					INTO GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_cca
					
					FROM GDB_TC1_kade.dbo.tblCareerChangeApplication AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T2 ON T2.psn_PersonID = T1.cca_PersonID
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T3 ON T3.dog_DogID = T1.cca_DogID
 
END 

	BEGIN--audit

		SELECT * FROM GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_cca
		WHERE Legacy_Id__c IN (SELECT Legacy_Id__c FROM GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_cca GROUP BY Legacy_Id__c HAVING COUNT(*)>1)

	END 

BEGIN -- CREATE IMP_DOG_PLACEMENT_APPLICATION_fcq
				SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'fcq-'+CAST(T1.fcq_ID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'fcq-'+[fcq_Id]	
					,T2.psn_PersonID AS  [Contact__r:Legacy_Id__c]		-- Link [tblPerson].[PersonID]
					,CAST(T1.fcq_Date AS DATE) AS  Date__c		
					,T1.fcq_Home  AS  Home__c		
					,CASE T1.fcq_Stairs   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Stairs__c	-- migrate blank as "No"	
					,CASE T1.fcq_FencedYard   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Fenced_Yard__c	-- migrate blank as "No"	
					,CASE T1.fcq_Pool   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Pool__c	-- migrate blank as "No"	
					,CASE T1.fcq_PoolEnclosed   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Pool_Enclosed__c	-- Migrate blanks as No	
					,CASE T1.fcq_OwnedDogBefore   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Owned_Dob_Before__c	-- migrate blank as "No"	
					,CASE T1.fcq_ObedienceClass   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Obedience_Class__c	-- migrate blank as "No"	
					,CASE T1.fcq_OwnCats   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Owns_Cats__c	-- migrate blank as "No"	
					,CASE T1.fcq_ChildrenVisit   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Children_Visit__c	-- migrate blank as "No"	
					,CASE T1.fcq_WorkAwayHome   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Work_Away_From_Home__c	-- migrate blank as "No"	
					,CASE T1.fcq_DogGoToWork   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Dog_Go_To_Work__c	-- migrate blank as "No"	
					,CASE T1.fcq_GenderPref   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Gender_Preference__c	-- migrate 'F' as Female	
					,CASE T1.fcq_CharMedicalProblems  WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END  AS    Char_Medical_Problems__c		
					,CASE T1.fcq_CharHighActivityLevel  WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END  AS  Char_High_Activity_Level__c		
					,CASE T1.fcq_CharAssertiveness WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END   AS  Char_Assertiveness__c		
					,CASE T1.fcq_CharSensitivity  WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END  AS  Char_Sensitivity__c		
					,CASE T1.fcq_CharInappropriate  WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END  AS  Char_Inappropriate__c		
					,CASE T1.fcq_CharIncompatiblility  WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END  AS  Char_Incompatibility__c		
					,CASE T1.fcq_CharNotHousetrained  WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END  AS  Char_Not_House_Trained__c		
					,CASE T1.fcq_CharSenior  WHEN '-1' THEN 'TRUE' ELSE 'FALSE' END  AS  Char_Senior__c		
					,CASE T1.fcq_GDBAppointments  WHEN 'Yes' THEN 'Yes' ELSE 'No' END   AS  GDB_Appointments__c	-- migrate blank as "No"	
					,CASE T1.fcq_MedicalAttention   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Medical_Attention__c	-- migrate blank as "No"	
					,CASE T1.fcq_WalkDog   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Walk_Dog__c	-- migrate blank as "No"	
					,CASE T1.fcq_IllDog   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Ill_Dog__c	-- migrate blank as "No"	
					,CASE T1.fcq_Guidelines   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Guidelines__c	-- migrate blank as "No"	
					,CASE T1.fcq_Relieving   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Relieving__c	-- migrate blank as "No"	
					,T1.fcq_HandlingInternal  AS  Handling_internal__c		
					,T1.fcq_MedicalInternal  AS  Medical_Internal__c		
					,T1.fcq_Comments  AS  Comments__c		
					,'0123D0000004ax7QAA' AS  RecordTypeID	-- Foster	
					,T3.Other_Dog_1__c
					,T3.Other_Dog_2__c
					,T3.Other_Dog_3__c
					,T3.Other_Dog_4__c
					,T3.Other_Dog_5__c
					 INTO GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_fcq
					FROM GDB_TC1_kade.dbo.tblFosterCareQuestionnaire AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T2 ON T2.psn_PersonID = T1.fcq_PersonID
					LEFT JOIN [GDB_TC1_migration].dbo.stg_tblFosterCareQuestOtherDogs_final AS T3 ON T3.fod_QuestionnaireID = T1.fcq_ID


END --TC1: 89

	BEGIN--audit

		SELECT * FROM GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_fcq
		WHERE Legacy_Id__c IN (SELECT Legacy_Id__c FROM GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_fcq GROUP BY Legacy_Id__c HAVING COUNT(*)>1)

	END   
 

BEGIN -- -- CREATE IMP_DOG_PLACEMENT_APPLICATION_k9a

			SELECT  CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]() END AS OwnerId  -- Link [tblStaff].[FileNum]
					,'k9a-'+CAST(T1.k9a_AppID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'k9a-'+[k9a_AppID]	
					,T2.psn_PersonID  AS  [Contact__r:Legacy_ID__c]		-- Link [tblPerson].[PersonID]
					,CAST(T1.k9a_AppDate  AS DATE )  AS  Application_Date__c		
			 		,CASE T1.k9a_FutureApplicant   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Future_Applicant__c	-- migrate blanks as 'No'	
					,T1.k9a_ParentGuardian  AS  Parent_Guardian__c		
					,T1.k9a_Occupation  AS  Ocupation__c		
					,T1.k9a_Employer  AS  Employer__c		
					,CAST(T1.k9a_DOB AS DATE) AS  Date_of_Birth__c		
					,T1.K9a_Grade  AS  Grade__c
					,T1.k9a_CauseofBlindness  AS  Cause_of_Blindness__c		
					,T1.k9a_VisualAcuity  AS  Visual_Acuity__c		
					,CASE T1.k9a_LegallyBlind   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Legally_Blind__c	-- migrate blanks as 'No'	
					,T1.[k9a_Own/Rent]  AS  Own_Rent__c		
					,T1.k9a_landlordInfo  AS  Landlord__c		
					,T1.k9a_Campus  AS  Campus__c		
					,CASE T1.k9a_Allergic   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Allergic__c	-- migrate blanks as 'No'	
					,T1.k9a_ChildUnder18Ages  AS  Children_Under_18_Ages__c		
					,CASE T1.k9a_SpecialNeed   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Special_Need__c	-- migrate blanks as 'No'	
					,T1.k9a_SpecialNeedText  AS  Special_Need_Text__c		
					,CASE T1.k9a_ObedienceClass   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Obedience_Class__c	-- migrate blanks as 'No'	
					,CASE T1.k9a_DogCrate   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Dog_Crate__c	-- migrate blanks as 'No'	
					,T1.k9a_HoursAlone  AS  Hours_Alone__c		
					,T1.k9a_WhereWhenAlone  AS  Where_Alone__c		
					,T1.k9a_WhereAtNight  AS  Where_At_Night__c		
					,CASE T1.k9a_FencedYard   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Fenced_Yard__c	-- migrate blanks as 'No'	
					,T1.k9a_FenceHeight  AS  Fence_Height__c	
					,CASE T1.k9a_EnclosedDogRun   WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  Enclosed_Dog_Run__c	-- migrate blanks as 'No'	
					,T1.k9a_EnclosedFenceHeight  AS  Enclosed_Fence_Height__c		
					,T1.k9a_MayGDBFencedCheck  AS  Fence_Check_OK__c		
					,T1.k9a_MayGDBHomeInterview  AS  May_GDB_Home_Interview__c		
					,T1.k9a_HowAcquirePreviousDogs  AS  How_Acquire_Previous_Dogs__c		
					,T1.k9a_OtherPets  AS  Other_Pets__c		
					,T1.k9a_HowDidYouHear  AS  How_Did_You_Hear__c		
					,CAST(T1.k9a_CreatedDate AS DATE)  AS  CreatedDate		
					,T1.k9a_Why  AS  Why_Contact__c		
					,T1.k9a_Summary  AS  Summary__c		
					,'0123D0000004ax8QAA'  AS  RecordTypeID	-- K9 Buddy	
					,T3.Other_Dog_1__c
					,T3.Other_Dog_2__c
					,T3.Other_Dog_3__c
					,T3.Other_Dog_4__c
					,T3.Other_Dog_5__c
				 INTO GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_k9a 
				 FROM GDB_TC1_kade.dbo.tblK9BuddyApplication AS T1
				 LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T2 ON T2.psn_PersonID = T1.k9a_PersonID
				 LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.k9a_WhoBy
				 LEFT JOIN [GDB_TC1_migration].dbo.stg_tblK9BuddyAppOtherDogs_final AS T3 ON T3.kod_K9AppID=T1.k9a_AppID
END -- tc1 264

	BEGIN--audit

		SELECT * FROM GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_k9a
		WHERE Legacy_Id__c IN (SELECT Legacy_Id__c FROM GDB_TC1_migration.dbo.IMP_DOG_PLACEMENT_APPLICATION_k9a GROUP BY Legacy_Id__c HAVING COUNT(*)>1)

	END   
 