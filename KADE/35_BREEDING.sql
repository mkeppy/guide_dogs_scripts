USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblCWTPostWhelp
			WHERE   SF_Object LIKE '%bree%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblDogBreeding 
			WHERE  SF_Object_2 LIKE '%bre%'
END 
 
 
/*

SELECT * FROM GDB_TC1_migration.DBO.XTR_RECORD_TYPE WHERE SOBJECTTYPE LIKE '%BREE%'
 

*/
BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_BREEDING

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
 							,'DBG-'+CAST(T1.dbg_BreedingID AS NVARCHAR(30)) AS  Legacy_Id__c		
							,T2.dog_DogID AS  [Bitch__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,T3.dog_DogID AS  [Stud__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,T5.dog_DogID AS [Stud_2__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
							,CAST(T1.dbg_DateSeason AS DATE) AS  Date_Season__c		
							,CAST(T1.dbg_DateSeasonEnd AS DATE) AS  Date_Season_End__c		
							,CASE T1.dbg_WNL WHEN 'Yes' THEN 'TRUE' ELSE 'FALSE' END  AS  WNL__c		
							,CASE T1.dbg_SkipMating WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Skip_Mating__c		
							,CAST(T1.dbg_LastUltraDate  AS DATE) AS  Last_Ultra_Date__c		
							,CASE T1.dbg_LitterConceived WHEN '1' THEN 'TRUE' ELSE 'FALSE' END  AS  Litter_Conceived__c		
							,CAST(T1.dbg_SeasonComments AS NVARCHAR(MAX)) AS  Season_Comments__c		
							,CAST(T1.dbg_KennelComments AS NVARCHAR(MAX)) AS  Kennel_Comments__c		
							,CASE WHEN T1.dbg_IR IS NULL THEN 'No' ELSE T1.dbg_IR END  AS  IR__c	-- if blank migrate as "No"	
							,CASE WHEN T1.dbg_SLSDiet  IS NULL THEN 'No' ELSE  T1.dbg_SLSDiet END  AS  SLS_Diet__c	-- if blank migrate as "No"	
							,T1.dbg_VetNotification  AS  Vet_Notification__c		
							,CASE WHEN T1.dbg_SeasonType IS NULL THEN 'No' ELSE  T1.dbg_SeasonType END  AS  Season_Type__c	-- if blank migrate as "No"	
							,CAST(T1.dbg_CreatedDate AS DATE) AS  CreatedDate	
 							,NULL AS  Whelp_Date__c		
							,NULL AS  Litter_Info__c		
							,NULL AS  Weight__c		
							,NULL AS  Weight_Date__c		
							,NULL AS  Body_Condition__c		
							,NULL AS  Heartguard_Given__c		
							,NULL AS  BSC_Date__c		
							,NULL AS  Food_Type__c		
							,NULL AS  Food_Amount__c		
							,NULL AS  Vacs__c		
							,NULL AS  Vaccine_Date__c		
							,NULL AS  Vaccine_Due__c		
							,NULL AS  Current_Treatment_Diagnosis__c	
								
							--reference
							,T1.dbg_BreedingID AS zrefID
							,'tblDogBreeding' AS zrefSrc
					
					INTO GDB_TC1_migration.DBO.IMP_BREEDING
					FROM GDB_TC1_kade.dbo.tblDogBreeding AS T1
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.dbg_BitchID
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T3 ON T3.dog_DogID=T1.dbg_StudID
					LEFT JOIN GDB_TC1_kade.dbo.tblMultiSireLitter AS T4 ON T4.msl_LitterID=T1.dbg_BreedingID
					LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T5 ON T5.dog_DogID=T4.msl_StudID
				UNION
					SELECT   CASE WHEN X1.ID IS NOT NULL THEN X1.ID ELSE GDB_TC1_migration.[dbo].[fnc_OwnerId]() END AS OwnerId  -- Link [tblStaff].[FileNum]
					 		,'PW-'+CAST(T1.pw_RecID AS NVARCHAR(30))  AS  Legacy_ID__c	-- concatenate 'Pw-'+[pw_RecID]	
 							,T2.dog_DogID AS  [Bitch__r:Legacy_Id__c]		-- Link [tblDog].[dog_DogID]
 							--filler
								,NULL AS [Stud__r:Legacy_Id__c]
								,NULL AS [Stud_2__r:Legacy_Id__c]
 								,NULL  AS  Date_Season__c		
								,NULL  AS  Date_Season_End__c		
								,NULL  AS  WNL__c		
								,NULL  Skip_Mating__c		
								,NULL  Last_Ultra_Date__c		
								,NULL  Litter_Conceived__c		
								,NULL  Season_Comments__c		
								,NULL  Kennel_Comments__c		
								,NULL  IR__c	 
								,NULL  SLS_Diet__c	 
								,NULL  Vet_Notification__c		
								,NULL  Season_Type__c	
							,CAST(T1.pw_CreatedDate AS DATE) AS CreatedDate 
	 						,CAST(T1.pw_WhelpDate AS DATE) AS  Whelp_Date__c		
							,T1.pw_LitterInfo  AS  Litter_Info__c		
							,T1.pw_Weight  AS  Weight__c		
							,CAST(T1.pw_WeightDate AS DATE) AS  Weight_Date__c		
							,T1.pw_BodyCondition  AS  Body_Condition__c		
							,CAST(T1.pw_HeartguardGiven  AS DATE) AS  Heartguard_Given__c		
							,CAST(T1.pw_BCSDate AS DATE)  AS  BSC_Date__c		
							,T1.pw_FoodType  AS  Food_Type__c		
							,T1.pw_FoodAmount  AS  Food_Amount__c		
							,T1.pw_Vacs  AS  Vacs__c		
							,T1.pw_VaccDate  AS  Vaccine_Date__c		
							,T1.pw_VaccDue  AS  Vaccine_Due__c		
							,T1.pw_CurrTreatDiag  AS  Current_Treatment_Diagnosis__c		
 							--reference
							,T1.pw_RecID AS zrefID
							,'tblCWTPostWhelp' AS zrefSrc
						FROM GDB_TC1_kade.dbo.tblCWTPostWhelp AS T1
						LEFT JOIN GDB_TC1_kade.dbo.tblDog AS T2 ON T2.dog_DogID=T1.pw_DogID
						LEFT JOIN GDB_TC1_migration.dbo.XTR_USERS AS X1 ON X1.ADP__C=T1.pw_ReportedBy



END 

BEGIN-- AUDIT

 
	SELECT * FROM GDB_TC1_migration.dbo.IMP_BREEDING
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_BREEDING GROUP BY Legacy_ID__c HAVING COUNT(*)>1)

	 		SELECT zrefSrc, COUNT(*) C
			FROM GDB_TC1_migration.dbo.IMP_BREEDING 
			GROUP BY zrefSrc
		 
END 
 



























