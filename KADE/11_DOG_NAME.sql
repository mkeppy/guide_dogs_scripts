USE GDB_TC1_migration
GO
BEGIN-- 
			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblDog 
			WHERE   SF_Object LIKE '%dog%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblDog 
			WHERE  SF_Object_2 LIKE '%dog%'
END 


BEGIN--- CREATE DOG NAME 
		
			SELECT DISTINCT
					 GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					 ,T1.dog_Name  AS  [Name] 	
					,'TRUE'  AS  Acceptable_Name__c	-- migrate as TRUE
		 	INTO  GDB_TC1_migration.dbo.IMP_DOG_NAME  
			FROM GDB_TC1_kade.dbo.tblDog AS T1 
			LEFT JOIN GDB_TC1_kade.dbo.tblDogUnacceptableNames AS T2 ON T2.dun_Name=T1.dog_Name 
			WHERE T2.dun_Name IS NULL    --dnc unacceptable names. 

			UNION all
			
			SELECT DISTINCT
					 GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					 ,T1.dun_Name  AS  [Name]	
					,'FALSE'  AS  Acceptable_Name__c	--  FALSE
			FROM GDB_TC1_kade.dbo.tblDogUnacceptableNames AS T1

			ORDER BY Name__c
END --17276 

BEGIN--audit 
		--check duplicates. 
			SELECT * FROM  GDB_TC1_migration.dbo.IMP_DOG_NAME
			WHERE Name__c IN (SELECT Name__c FROM GDB_TC1_migration.dbo.IMP_DOG_NAME GROUP BY Name__c HAVING COUNT(*)>1)
 

		--REMOVE QUOTES
			EXEC GDB_TC1_migration.dbo.sp_FindStringInTable '%"%', 'DBO', 'IMP_DOG_NAME'

			UPDATE GDB_TC1_migration.dbo.IMP_DOG_NAME SET Name__c=REPLACE(Name__c,'"','''') where Name__c like '%"%'
 	
		--check count
		
			SELECT acceptable_name__c, COUNT(*) C
			FROM GDB_TC1_migration.dbo.IMP_DOG_NAME GROUP BY Acceptable_Name__c	

		--check
			SELECT * FROM GDB_TC1_migration.dbo.IMP_DOG_NAME  
END 