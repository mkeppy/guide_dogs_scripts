USE GDB_TC1_migration
GO


BEGIN-- 
 			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblStdntSugRelReasons
			WHERE   SF_Object LIKE '%app%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblPersonRel 
			WHERE  SF_Object_2 LIKE '%APP%'
END 



BEGIN -- DROP IMP

	DROP TABLE GDB_TC1_migration.DBO.IMP_APPLICATION_STATUS

END 

BEGIN -- CREATE IMP 

					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'prl-'+CAST(T1.prl_RelationID AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenate 'prl'+[prl_RelationID]	
					,T3.psn_PersonID  AS  [Contact__r:Legacy_Id__c]			-- Link [tblPerson].[psn_PersonID]
					,'cli-'+CAST(T2.cli_ClientID AS NVARCHAR(30))  AS  [Application__r:Legacy_Id__c]				-- migrate to APPLICATION STATUS WHEN prl_ClientInstanceID<>0	-- Link/[tblClient].[cli_ClientID] (REF [tblPersonRel].[prl_RelationID])/foreign key in many tables
					,NULL AS Suggested_Release_Reason__c
					,NULL AS Importance__c
					,T1.prl_RelationCode  AS  Relation_Code__c		
					,T1.prl_RelationCode  AS  Dog_Relation_Code__c	
					,NULL AS Status__c	
					,NULL AS Type__c
					,NULL AS Date_Changed__c
					,CAST(T1.prl_StartDate  AS DATE) AS  Start_Date__c		
					,CAST(T1.prl_EndDate  AS DATE) AS  End_Date__c	
					,NULL AS Notes__c	
					,T4.erc_ERCodeText  AS  End_Reason__c		-- Yes/Link [dbo_trefEndReasonCodes]
					,T5.rct_ERCodeCatText AS  End_Reason_Cateogry__c		
					 

					,prl_RelationID AS zrefID
					,'tblPersonRel' AS zrefSrc
					INTO GDB_TC1_migration.dbo.IMP_APPLICATION_STATUS
					FROM GDB_TC1_kade.dbo.tblPersonRel AS T1
					INNER JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID = T1.prl_RelationID
				 	LEFT JOIN GDB_TC1_kade.DBO.tblPerson AS T3 ON T3.psn_PersonID=T1.prl_PersonID
					LEFT JOIN GDB_TC1_kade.dbo.trefEndReasonCodes AS T4 ON T4.erc_ID=T1.prl_EndReason
					LEFT JOIN GDB_TC1_kade.dbo.trefEndReasonCodesCat AS T5 ON T5.rct_ID=T4.erc_RECodeCat
					WHERE T1.prl_ClientInstanceID<>'0'
 
				 UNION 
					
					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'cs-'+CAST(T1.cs_StatusChgID AS NVARCHAR(30))  AS  Legacy_ID__c
					,T4.psn_PersonID  AS  [Contact__r:Legacy_Id__c]   	-- Link through [tblPersonRel] to [tblPerson]	-- Link [tblPersonRel].[prl_ClientInstanceID]		
					,'cli-'+CAST(T2.cli_ClientID AS NVARCHAR(30))   AS  [Application__r:Legacy_Id__c]			-- Link [tblClient].[cli_ClientId]
					,NULL AS Suggested_Release_Reason__c
					,NULL AS Importance__c
					,NULL AS Relation_Code__c
					,NULL AS  Dog_Relation_Code__c		
 					,T5.ClientStatus AS  Status__c	-- migrate value from [ClientStatus]	-- Ref/Yes trefClientStatus
					,T5.[Type]  AS  Type__c	-- migrate value from [Type]	-- Ref/Yes trefClientStatus
					,CAST(T1.cs_DateChanged AS DATE) AS  Date_Changed__c		
					,NULL AS Start_Date__c
					,CAST(T1.cs_DateEnd AS DATE) AS  End_Date__c		
					,CAST(T1.cs_Notes AS NVARCHAR(MAX)) AS  Notes__c	
					,NULL AS End_Reason__c	
					,NULL AS End_Reason_Cateogry__c

					,cs_StatusChgID AS zrefID
					,'tblClientStatus' AS zrefSrc
					FROM GDB_TC1_kade.dbo.tblClientStatus AS T1
					INNER JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID = T1.cs_ClientID
					LEFT JOIN GDB_TC1_kade.DBO.tblPersonRel AS T3 ON T3.prl_ClientInstanceID=T1.cs_ClientID
					LEFT JOIN GDB_TC1_kade.DBO.tblPerson AS T4 ON T4.psn_PersonID=T3.prl_PersonID
					LEFT JOIN GDB_TC1_kade.dbo.trefClientStatus AS T5 ON T5.StatusCode=T1.cs_StatusCode

		 		 UNION 

				 	SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,'ssr-'+CAST(T1.ssr_StdntSuggRelReason  AS NVARCHAR(30)) AS  Legacy_ID__c	-- concatenation 'Ssr-'+[ssr_StdntSuggRelReason]	
					,NULL   AS  [Contact__r:Legacy_Id__c] 
					,'cli-'+CAST(T2.cli_ClientID AS NVARCHAR(30))  AS  [Application__r:Legacy_Id__c]		-- Link [tblClient].[cli_ClientID]
					,T3.sen_SugRelReason  AS  Suggested_Release_Reason__c	-- migrate value from [sen_SugRelReason]	-- Yes/Link [trefStdntSugRelReasons].[sen_SuggReasonID]
					,T1.ssr_Importance  AS  Importance__c		

					,NULL AS Relation_Code__c
					,NULL AS Dog_Relation_Code__c		
 					,NULL AS Status__c	 
					,NULL AS Type__c	 
					,NULL AS Date_Changed__c		
					,NULL AS Start_Date__c
					,NULL AS End_Date__c		
					,NULL AS Notes__c	
					,NULL AS End_Reason__c	
					,NULL AS End_Reason_Cateogry__c

					,ssr_StdntSuggRelReason AS zrefID
					,'tblStdntSugRelReasons' AS zrefSrc
			
					FROM GDB_TC1_kade.dbo.tblStdntSugRelReasons AS T1
					INNER JOIN GDB_TC1_kade.dbo.tblClient AS T2 ON T2.cli_ClientID = T1.ssr_ClientID
					LEFT JOIN GDB_TC1_kade.dbo.trefStdntSugRelReasons AS T3 ON T3.sen_SuggRelReasonID=T1.ssr_SugRelReason
 


END --tc1: 117688

BEGIN-- AUDIT


			SELECT * FROM GDB_TC1_migration.dbo.IMP_APPLICATION_STATUS 
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_APPLICATION_STATUS GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			


			SELECT zrefSrc, COUNT(*) C
			FROM GDB_TC1_migration.dbo.IMP_APPLICATION_STATUS 
			GROUP BY zrefSrc
END 




























