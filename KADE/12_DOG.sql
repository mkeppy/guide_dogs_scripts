USE GDB_TC1_migration
GO
BEGIN-- 
			SELECT  [Source_Field],  [Convert], SF_Object, SF_Field_API, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API  AS SCRIPT
				 	,CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblStaffDogVacc
			WHERE   SF_Object LIKE '%dog%'
			
			SELECT  [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, ',T1.'+[Source_Field] +'  AS  '+ SF_Field_API_2  AS SCRIPT
				 	,CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END AS TRANSLATIONRULES
					,CASE WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END AS [ConvertLinkRef]
			FROM GDB_TC1_maps.dbo.tblInstr 
			WHERE  SF_Object_2 LIKE '%xxxxxxxxxxxxxxxxxxxx%'
END 

BEGIN -- drop IMP DOG

	DROP TABLE GDB_TC1_migration.dbo.IMP_DOG

END 

BEGIN--- CREATE DOG
			SELECT	 DISTINCT
				--tblDog	
					 GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
					,T1.dog_DogID  AS  Legacy_ID__c		
					,T1.dog_GeneticDBNum  AS  Genetic_DB_Num__c		
					,T1.dog_GDBTattoo  AS  GDB_Tattoo__c		
					,X1.[NAME]
					,X1.ID  AS  Call_Name__c		
					,X2.ID  AS RecordTypeId
					,T1.dog_FullAKCName  AS  Full_AKC_Name__c		
					,T1.dog_AKCNumber  AS  AKC_Number__c		
					,T1.dog_MicrochipID  AS  Microchip_ID__c		
					,T1.dog_MicrochipID2  AS  Microchip_ID_2__c		
					,T3.[Description]  AS  Breed__c		-- Yes/Link[trefDogBreed].[DogBreedCode]
					,T1.dog_LGXCode  AS  LGX__c		
					,T1.dog_PercentLAB  AS  Percent_LAB__c		
					,T1.dog_PercentGLD  AS  Percent_GLD__c		
					,T1.dog_FilialCode  AS  Filial__c		
					,T1.dog_Color  AS  Color__c		
					,CASE T1.dog_Sex  WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END AS  Sex__c		
					,CAST(T1.dog_DateWhelp  AS DATE) AS  Date_Whelp__c		
					,CAST(T1.dog_DateInProgram   AS DATE) AS  Date_In_Program__c		
					,CAST(T1.dog_DateRelease   AS DATE) AS  Date_Release__c		
					,CAST(T1.dog_GiftofDogSent AS DATE)  AS  Gift_of_Dog_Sent__c		
					,T4.psn_PersonID  AS  [Gift_of_Dog_Person__r:Legacy_Id__c]			-- Link [tblPerson].[PersonID]
					,CAST(T1.dog_GiftofDogSecReq AS DATE)   AS  Gift_of_Dog_Sec_Req__c		
					,CAST(T1.dog_GiftofDogRec AS DATE)   AS  Gift_of_Dog_Rec__c		
					,CAST(T1.dog_DateNeuter AS DATE)   AS  Date_Neuter__c		
					,CAST(T1.dog_NeuterLetterSent AS DATE)   AS  Neuter_Letter_Sent__c		
					,CASE T1.dog_EarlyNeuterActive WHEN 1 THEN 'TRUE' ELSE 'FALSE' end AS  Early_Neuter_Active__c		
					,CAST(T1.dog_DateObit  AS DATE)   AS Date_Obit__c		
 					,T1.dog_Status  AS  Level__c		
					,T5.psn_PersonID  AS  [Person__r:Legacy_Id__c] 		-- Yes/Link [tblPerson].[psn_PersonID]
					,T1.dog_Notes  AS  Notes__c		
					,CASE T1.dog_Ponly WHEN 1 THEN 'TRUE' ELSE 'FALSE' end   AS  Ponly__c		
					,CAST(T1.dog_AlterPerBrd  AS DATE)  AS  Alter_Per_Brd__c		
					,T1.dog_CutNotification   AS  Cut_Notification__c		
					,CAST(T1.dog_DatePlaced  AS DATE)  AS  Date_Placed__c		
					,T1.dog_TRNCampus  AS  Training_Campus__c		
					,CAST(T1.dog_DateRecall  AS DATE)  AS  Date_Recall__c		
					,T1.dog_FYRecall  AS  FY_Recall__c		
					,CAST(T1.dog_DateTrain  AS DATE)  AS  Date_Train__c		
					,CAST(T1.dog_DateInClass  AS DATE)  AS  Date_In_Class__c		
					,T1.dog_FY1stGrad  AS  FY_1st_Grad__c		
					,T1.dog_PedigreeText  AS  Pedigree_Text__c		
					,CAST(T1.dog_DateBreedWatchEnd  AS DATE)  AS  Date_Breed_Watch_End__c		
					,T1.tmpPedigreeID  AS  Pedigree_ID__c		
					,CASE WHEN T6.[Description] IS NULL THEN T1.dog_Source     ELSE T6.[Description] END  AS  Source__c	-- migreate [description]	-- Yes/Link [tref_DogSource].[SourceCode]
					,CASE WHEN T7.[Description] IS NULL THEN T1.dog_Source2    ELSE T7.[Description] END AS  Source2__c	-- migreate [description]	-- Yes/Link [tref_DogSource].[SourceCode]
					,T1.dog_CurrStatus AS  Status__c		
					,CAST(T1.dog_Date1stClassReady  AS DATE)  AS  Date_1st_Class_Ready__c		
					,T10.Legacy_Id__c  AS  [First_String__r:Legacy_Id__c]		-- Yes/Link [tblClass].[cls_FirstString]  --campaign level 1 
					,CASE T1.dog_GrandfatheredVPC  WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS     Grandfathered_VPC__c		
					,T1.dog_RelPlacement  AS  Rel_Placement__c		
					,CAST(T1.dog_RelSummary AS NVARCHAR(MAX)) AS  Rel_Summary__c		
					,CASE WHEN T1.dog_Location IS NOT NULL THEN T1.dog_Location ELSE T20.[Location] END AS  Location__c	  -- migrate value from [Location]	-- Link [trefVetPetChampLoc].[LocCode]	
					,T1.dog_Disposition  AS  Disposition__c		
					,T1.dog_StatusChangeBy  AS  Status_Change_By__c		
					,T12.Legacy_Id__c  AS  [Into_String__r:Legacy_Id__c]		-- Yes/ Link [tblClass] --mk added: .[cls_FirstString]  --campaign level 1 
					,T1.dog_StatusChangeNotes  AS  Status_Change_Notes__c		
					,CASE T1.dog_TRNEVL WHEN 'Yes' THEN 'Yes' ELSE 'No' END  AS  TRNEVL__c	-- Migrate null values as 'No'	
					,T1.dog_CreatedBy  AS  Kade_Created_By__c		
	  			--tblBreedingConformation
					,CAST(T13.bc_EvalDate AS DATE)  AS  Evaluation_Date__c		
					,X3.ID  AS  Evaluator__c		-- Link [tblStaff].[FileNum]
					,T13.bc_Gait  AS  Gait__c		
					,T13.bc_GaitDesc  AS  Gait_Description__c		
					,T13.bc_Profile  AS  Profile__c		
					,T13.bc_ProfileDesc  AS  Profile_Description__c		
					,T13.bc_Head  AS  Head__c		
					,T13.bc_HeadDesc  AS  Head_Description__c		
					,T13.bc_EyeEyelid  AS  Eyelid__c		
					,T13.bc_EyeEyelidDesc  AS  Eyelid_Description__c		
					,T13.bc_Bite  AS  Bite__c		
					,T13.bc_BiteDesc  AS  Bite_Description__c		
					,T13.bc_FrontAssb  AS  Front_Assembly__c		
					,T13.bc_FrontAssbDesc  AS  Front_Assembly_Description__c		
					,T13.bc_RearAssb  AS  Rear_Assembly__c		
					,T13.bc_RearAssbDesc  AS  Rear_Assembly_Description__c		
					,T13.bc_Feet  AS  Feet__c		
					,T13.bc_FeetDesc  AS  Feet_Description__c		
					,T13.bc_ColorPigment  AS  Color_Pigment__c		
					,T13.bc_ColorPigmentDesc  AS  Color_Pigment_Description__c		
					,T13.bc_Coat  AS  Coat__c		
					,T13.bc_CoatDesc  AS  Coat_Description__c		
					,T13.bc_Overall  AS  Overall__c		
					,T13.bc_TblBehavior  AS  Behavior__c		
					,T13.bc_TblBehaviorDesc  AS  Behavior_Description__c		
				--tblDogWhelping
					,T14.whp_WhelpType  AS  Whelp_Type__c
					,T14.whp_FirstWeight  AS  First_Weight__c
					,T14.whp_Clip  AS  Clip__c
					,T14.whp_Status  AS  Whelp_Status__c
				--tblControlFactor
					,T15.cf_Prelim  AS  Preliminary__c
					,T15.cf_PrelimTrend  AS  Preliminary_Trend__c
					,T15.cf_Final  AS  Final__c
					,T15.cf_Class  AS  Control_Factor_Class__c
					,T15.cf_Student  AS  Student__c
					,T15.cf_InitialComments  AS  Initial_Comments__c
					,T15.cf_OtherComments  AS  Other_Comments__c
				--tblStaffDogVacc
					,T16.DA2PP_Vaccine_Given__c
					,T16.Rabies_Vaccine_Given__c
					,T16.Leptospirosis_Vaccine_Given__c
					,T16.Bordetella_Vaccine_Given__c
					,T16.Vaccine_Exemption__c
				--tblDogReleaseReasons
					,T17.drr_Importance     AS  Importance__c		
					,T17.drr_DiagnosisCode  AS  Diagnosis_Code__c		-- Ref trefVetPetChamp
					,T18.DiagnosisText      AS  Diagnosis	-- migrate [DiagnosisText]	-- Ref trefVetPetChamp
					,T19.CategoryDesc		AS  Diagnosis_Category__c	-- link through [trefVetPetChamp].[DiagnosisCategory]=[trefVetDiagCategory], migrate value from [CategoryDesc]	-- Ref trefVetPetChamp
					,T18.DogReleaseCategory AS  Diagnosis_Release_Category__c	-- migrate [DogReleaseCategory]	-- Ref trefVetPetChamp
					,T18.RelCodeStatus      AS  Release_Code_Status__c	-- migrate [RelCodeStatus]	-- Ref trefVetPetChamp
					,T17.drr_Notes          AS  Release_Notes__c		
					

	 	 	INTO GDB_TC1_migration.dbo.IMP_DOG 	
			FROM GDB_TC1_kade.dbo.tblDog AS T1
			LEFT JOIN GDB_TC1_migration.dbo.XTR_DOG_NAME AS X1 ON T1.dog_Name=X1.[Name]
			LEFT JOIN GDB_TC1_maps.dbo.CHART_DOG AS T2 ON T2.dog_Sex = T1.dog_Sex AND T2.dog_Status = T1.dog_Status
			LEFT JOIN (SELECT * FROM GDB_TC1_migration.dbo.XTR_RECORD_TYPE WHERE SOBJECTTYPE='DOG__c') AS X2 ON T2.RecordTypeName=X2.[NAME]
			LEFT JOIN GDB_TC1_kade.dbo.trefDogBreed AS T3 ON T3.DogBreedCode=T1.dog_Breed
			LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T4 ON T4.psn_PersonID=T1.dog_GiftofDogPersonID
			LEFT JOIN GDB_TC1_kade.dbo.tblPerson AS T5 ON T5.psn_PersonID=T1.dog_PersonID
			LEFT JOIN GDB_TC1_kade.dbo.trefDogSource AS T6 ON T6.SourceCode=T1.dog_Source
			LEFT JOIN GDB_TC1_kade.dbo.trefDogSource AS T7 ON T7.SourceCode=T1.dog_Source2
			LEFT JOIN GDB_TC1_kade.dbo.trefRelCodeNew  AS T8 ON T8.RelationCode = T1.dog_CurrStatus
			--camp first strig
			LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T9 ON T9.cls_Campus= T1.dog_TRNCampus AND T9.cls_FirstString =T1.dog_FirstString
			LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_1 T10 ON T10.Legacy_Id__c=(T9.cls_FirstString  +' '+ T9.cls_Campus)
			--camp into  strig
			LEFT JOIN GDB_TC1_kade.dbo.tblClass AS T11 ON T11.cls_Campus= T1.dog_TRNCampus AND T11.cls_FirstString =T1.dog_IntoString
			LEFT JOIN GDB_TC1_migration.dbo.IMP_CAMPAIGN_level_1 T12 ON T12.Legacy_Id__c=(T11.cls_FirstString  +' '+ T11.cls_Campus)
			--
			LEFT JOIN GDB_TC1_kade.dbo.tblBreedingConformation AS T13 ON T13.bc_DogID = T1.dog_DogID
			LEFT JOIN GDB_TC1_migration.DBO.XTR_USERS AS X3 ON T13.bc_Evaluator=X3.ADP__C
			--
			LEFT JOIN GDB_TC1_kade.DBO.tblDogWhelping AS T14 ON T14.whp_DogID=T1.dog_DogID
			--
			LEFT JOIN GDB_TC1_kade.DBO.tblControlFactor AS T15 ON T15.cf_DogID = T1.dog_DogID
			--
		 	LEFT JOIN GDB_TC1_migration.dbo.stg_tblStaffDogVacc_final  T16 ON T16.stv_DogID = T1.dog_DogID
			--
			LEFT JOIN GDB_TC1_migration.dbo.stg_tblDogReleaseReasons AS T17 ON T17.drr_DogID=T1.dog_DogID
			LEFT JOIN GDB_TC1_kade.dbo.trefVetPetChamp AS T18 ON T18.DiagnosisCode=T17.drr_DiagnosisCode
			LEFT JOIN GDB_TC1_kade.DBO.trefVetDiagCategory T19 ON T19.CategoryCode=T18.DiagnosisCategory
			LEFT JOIN GDB_TC1_kade.DBO.trefVetPetChampLoc AS T20 ON T20.LocCode=T17.drr_LocationCode
			
	 


END 
   
BEGIN--AUDITS
		  
		--REMOVE QUOTES -- - CSV read/write UTF-8 enconding allows double quotes.
		 SELECT Legacy_ID__c, Status__c FROM GDB_TC1_migration.dbo.IMP_DOG
 	
		--check duple

			SELECT COUNT(*) FROM GDB_TC1_kade.dbo.tblDog --45128
			SELECT COUNT(*) FROM GDB_TC1_migration.dbo.IMP_DOG --45128

			SELECT * FROM GDB_TC1_migration.dbo.IMP_DOG --45128
			WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM GDB_TC1_migration.dbo.IMP_DOG GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
			ORDER BY Legacy_ID__c			
END 


--check campaings.
		SELECT dog_TRNCampus, dog_IntoString  
		FROM GDB_TC1_kade.dbo.tblDog  
		GROUP BY dog_TRNCampus, dog_IntoString 
		ORDER BY dog_TRNCampus, dog_IntoString 


		SELECT dog_TRNCampus, dog_IntoString  
		FROM GDB_TC1_kade.dbo.tblDog  
		GROUP BY dog_TRNCampus, dog_IntoString 
		ORDER BY dog_TRNCampus, dog_IntoString 


		SELECT T1.cls_Campus, T1.cls_FirstString  
		,T1.cls_FirstString  +' '+ T1.cls_Campus AS  [Legacy_Id__c]
		FROM GDB_TC1_kade.dbo.tblClass AS T1
		GROUP BY T1.cls_Campus, t1.cls_FirstString
		ORDER BY T1.cls_Campus, t1.cls_FirstString

--check person

	SELECT * FROM GDB_TC1_kade.dbo.tblPerson
	WHERE psn_PersonID='25504' OR psn_PersonId='69034'


	SELECT DISTINCT Legacy_ID__c, [NAME] FROM GDB_TC1_migration.dbo.IMP_DOG
