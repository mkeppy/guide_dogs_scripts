
					SELECT   GDB_TC1_migration.[dbo].[fnc_OwnerId]() AS OwnerId
				   ,t3.DiagnosisCategory
					,T4.CategoryDesc AS  Diagnosis_Category__c						-- link through [trefVetPetChamp].[DiagnosisCategory]=[trefVetDiagCategory], migrate value from [CategoryDesc]	-- Link [trefVetPetChamp].[DiagnosisCode]
					,T3.DogReleaseCategory AS  Diagnosis_Release_Category__c			-- migrate [DogReleaseCategory]	-- Link [trefVetPetChamp].[DiagnosisCode]
					,T3.RelCodeStatus AS  Release_Code_Status__c					-- migrate [RelCodeStatus]	-- Link [trefVetPetChamp].[DiagnosisCode]
					
					,'tblPRADogEvalTrends' AS zrefSrc
					
					
				 
					SELECT t1.det_TrendsRRID, t3.DiagnosisCode
					 ,    t3.*
					 --, t4.*
					FROM GDB_TC1_kade.dbo.tblPRADogEvalTrends AS T1
					INNER JOIN GDB_TC1_kade.dbo.trefVetPetChamp AS T3 ON T3.DiagnosisCode=T1.det_TrendsRRID
					--LEFT JOIN GDB_TC1_kade.dbo.trefVetDiagCategory AS T4 ON T4.CategoryCode=T3.DiagnosisCategory
					ORDER BY T1.det_TrendsRRID

					SELECT * FROM GDB_TC1_kade.dbo.trefVetPetChamp
					ORDER BY DiagnosisCode