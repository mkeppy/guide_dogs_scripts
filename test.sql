SELECT ivi_PhoneType, ivi_ContactPhone, ivi_ContactExt
,CASE WHEN ivi_PhoneType='Work' OR ivi_PhoneType='Primary' THEN 'Work' WHEN ivi_PhoneType='Cell' THEN  'Mobile' WHEN ivi_PhoneType='Secondary' THEN 'Other' END AS Phone
,CASE WHEN ivi_PhoneType='cell' THEN ivi_ContactPhone + CASE WHEN (ivi_ContactExt<>'0' AND ivi_ContactExt IS NOT NULL) THEN ', ' + ivi_ContactExt ELSE NULL END  ELSE NULL END  AS  MobilePhone	-- migrate to Mobile Phone if [ivi_PhoneType]='Cell'
			
FROM GDB_TC1_kade.dbo.tblGDBVendorInfo
 
ORDER BY ivi_PhoneType

 
 